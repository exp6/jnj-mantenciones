﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MetricArts.JnJ.Mantenciones.Configuration
{
    interface IConfigurable
    {
        void Configure();
    }
}