﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Models.Home;

namespace MetricArts.JnJ.Mantenciones.Controllers
{
    public class HomeController : Controller
    {

        [Authorize]
        public ActionResult Index()
        {
            if (User.IsInRole("Ingeniero"))
            {
                return RedirectToAction("IndexIng", "Home");
            }
            else 
            {
                return RedirectToAction("IndexAdmin", "Home");
            }

            //return View();
        }

        [Authorize]
        public ActionResult IndexAdmin()
        {
            IndexAdminViewModel model = new IndexAdminViewModel();
            return View(model);
        }

        [Authorize]
        public ActionResult IndexIng()
        {
            IndexIngViewModel model = new IndexIngViewModel(User.Identity.Name);
            return View(model);
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
