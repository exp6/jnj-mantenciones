﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Models.Mantenedores.Distribuidores;

namespace MetricArts.JnJ.Mantenciones.Controllers.Mantenedores
{
    public class DistribuidoresController : Controller
    {
        //
        // GET: /Distribuidores/

        public ActionResult Index()
        {
            DistribuidoresViewModel model = new DistribuidoresViewModel();
            return View(model);
        }

        //
        // GET: /Distribuidores/CrearNuevo
        [HttpGet]
        public ActionResult CrearNuevo()
        {
            DistribuidoresViewModel model = new DistribuidoresViewModel(User.Identity.Name);
            return View(model);
        }

        //
        // POST: /Distribuidores/CrearNuevo
        [HttpPost]
        public ActionResult CrearNuevo(DistribuidoresFormModel form)
        {
            DistribuidoresViewModel model = new DistribuidoresViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.create();
                    Mensaje = "Se ha creado al distribuidor con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new DistribuidoresViewModel(User.Identity.Name);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Distribuidores/Editar
        [HttpGet]
        public ActionResult Editar(Guid IdDistribuidor)
        {
            DistribuidoresViewModel model = new DistribuidoresViewModel(IdDistribuidor);
            return View(model);
        }

        //
        // POST: /Distribuidores/Editar
        [HttpPost]
        public ActionResult Editar(DistribuidoresFormModel form)
        {
            DistribuidoresViewModel model = new DistribuidoresViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.update();
                    Mensaje = "Se han guardado los cambios con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new DistribuidoresViewModel(form.IdDistribuidorRepuestos);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Distribuidores/Editar
        [HttpGet]
        public ActionResult Eliminar(Guid IdDistribuidor)
        {
            DistribuidoresViewModel model = new DistribuidoresViewModel(IdDistribuidor);
            model.delete();
            Mensaje = "Se ha eliminado al distribuidor con éxito.";
            return RedirectToAction("Index");
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }
    }
}
