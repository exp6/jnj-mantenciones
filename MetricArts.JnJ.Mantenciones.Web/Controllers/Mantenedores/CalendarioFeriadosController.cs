﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Models.Mantenedores.CalendarioFeriados;
using MetricArts.JnJ.Mantenciones.Helpers;

namespace MetricArts.JnJ.Mantenciones.Controllers.Mantenedores
{
    public class CalendarioFeriadosController : Controller
    {
        //
        // GET: /CalendarioFeriados/

        public ActionResult Index()
        {
            CalendarioFeriadosViewModel model = new CalendarioFeriadosViewModel();
            return View(model);
        }

        //
        // GET: /CalendarioFeriados/CrearNuevo
        [HttpGet]
        public ActionResult CrearNuevo()
        {
            CalendarioFeriadosViewModel model = new CalendarioFeriadosViewModel(User.Identity.Name);
            return View(model);
        }

        //
        // POST: /CalendarioFeriados/CrearNuevo
        [HttpPost]
        public ActionResult CrearNuevo(CalendarioFeriadosFormModel form)
        {
            CalendarioFeriadosViewModel model = new CalendarioFeriadosViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.UserName = User.Identity.Name;
                    bool valid = model.create();

                    if (valid)
                    {
                        Mensaje = "Se ha creado el calendario con éxito.";
                        return RedirectToAction("Index");
                    }
                    else {
                        model = new CalendarioFeriadosViewModel(User.Identity.Name);
                        Mensaje = "El año ingresado ya existe en el mantenedor. Favor cambiarlo por uno diferente.";
                    }

                    
                    
                }
            }
            catch (Exception e)
            {
                model = new CalendarioFeriadosViewModel(User.Identity.Name);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /CalendarioFeriados/Editar
        [HttpGet]
        public ActionResult Editar(Guid IdCalendario)
        {
            CalendarioFeriadosViewModel model = new CalendarioFeriadosViewModel(IdCalendario);
            return View(model);
        }

        //
        // POST: /CalendarioFeriados/Editar
        [HttpPost]
        public ActionResult Editar(CalendarioFeriadosFormModel form)
        {
            CalendarioFeriadosViewModel model = new CalendarioFeriadosViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.UserName = User.Identity.Name;
                    model.update();
                    Mensaje = "Se han guardado los cambios con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new CalendarioFeriadosViewModel(form.IdCalendarioFeriadosPais);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /CalendarioFeriados/Editar
        [HttpGet]
        public ActionResult Eliminar(Guid IdCalendario)
        {
            CalendarioFeriadosViewModel model = new CalendarioFeriadosViewModel(IdCalendario);
            model.delete();
            Mensaje = "Se ha eliminado el calendario con éxito.";
            return RedirectToAction("Index");
        }

        //
        // GET: /CalendarioFeriados/GetFechasCalendario?IdCalendario
        [HttpGet]
        public ActionResult GetFechasCalendario(Guid IdCalendario)
        {
            try
            {
                var fechasCalendario = Utils.getDBConnection().CalendarioFeriadosFechas.OrderBy(x => x.Fecha).Where(x => x.IdCalendarioFeriadosPais == IdCalendario).Select(x => new
                {
                    FechaCalendario = x.Fecha.ToShortDateString()
                }).ToList();

                return Json(fechasCalendario, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }
    }
}
