﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Models.Mantenedores.KitRepuestos;
using MetricArts.JnJ.Mantenciones.Helpers;

namespace MetricArts.JnJ.Mantenciones.Controllers.Mantenedores
{
    public class KitRepuestosController : Controller
    {
        //
        // GET: /KitRepuestos/

        public ActionResult Index()
        {
            KitRepuestosViewModel model = new KitRepuestosViewModel();
            return View(model);
        }

        //
        // GET: /KitRepuestos/CrearNuevo
        [HttpGet]
        public ActionResult CrearNuevo()
        {
            KitRepuestosViewModel model = new KitRepuestosViewModel(User.Identity.Name);
            return View(model);
        }

        //
        // POST: /KitRepuestos/CrearNuevo
        [HttpPost]
        public ActionResult CrearNuevo(KitRepuestosFormModel form)
        {
            KitRepuestosViewModel model = new KitRepuestosViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.create();
                    Mensaje = "Se ha creado el KIT con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new KitRepuestosViewModel(User.Identity.Name);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /KitRepuestos/Editar
        [HttpGet]
        public ActionResult Editar(Guid IdKitRepuesto)
        {
            KitRepuestosViewModel model = new KitRepuestosViewModel(IdKitRepuesto);
            return View(model);
        }

        //
        // POST: /KitRepuestos/Editar
        [HttpPost]
        public ActionResult Editar(KitRepuestosFormModel form)
        {
            KitRepuestosViewModel model = new KitRepuestosViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.update();
                    Mensaje = "Se han guardado los cambios con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new KitRepuestosViewModel(form.IdKitRepuesto);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /KitRepuestos/Eliminar
        [HttpGet]
        public ActionResult Eliminar(Guid IdKitRepuesto)
        {
            KitRepuestosViewModel model = new KitRepuestosViewModel(IdKitRepuesto);
            model.delete();
            Mensaje = "Se ha eliminado el KIT con éxito.";
            return RedirectToAction("Index");
        }

        //
        // GET: /KitRepuestos/AsociarRepuestos
        [HttpGet]
        public ActionResult AsociarRepuestos(Guid IdKitRepuesto)
        {
            KitRepuestosViewModel model = new KitRepuestosViewModel(IdKitRepuesto);
            return View(model);
        }

        //
        // POST: /KitRepuestos/AsociarRepuestos
        [HttpPost]
        public ActionResult AsociarRepuestos(KitRepuestosFormModel form)
        {
            KitRepuestosViewModel model = new KitRepuestosViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.addRepuestos();
                    Mensaje = "Se han asociado los repuestos con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new KitRepuestosViewModel(form.IdKitRepuesto);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }

        //
        // GET: /KitRepuestos/GetKitRepuestosRepuestos?IdKitRepuesto
        [HttpGet]
        public ActionResult GetKitRepuestosRepuestos(Guid IdKitRepuesto)
        {
            try
            {
                var kitRepuestoRepuestos = Utils.getDBConnection().KitRepuestoRepuestos.OrderBy(x => x.Repuesto.Codigo).Where(x => x.IdKitRepuesto == IdKitRepuesto).Select(x => new
                {
                    Codigo = x.Repuesto.Codigo,
                    Categoria = x.Repuesto.Categoria,
                    Cantidad = x.Repuesto.Cantidad,
                    Descripcion = x.Repuesto.Descripcion
                }).ToList();

                return Json(kitRepuestoRepuestos, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
    }
}
