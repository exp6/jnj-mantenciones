﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using MetricArts.JnJ.Mantenciones.Models;
using MetricArts.JnJ.Mantenciones.Models.Mantenedores.Usuarios;

namespace MetricArts.JnJ.Mantenciones.Controllers.Mantenedores
{
    public class UsuariosController : Controller
    {
        //
        // GET: /Usuarios/

        public ActionResult Index()
        {
            UsuariosViewModel model = new UsuariosViewModel();
            return View(model);
        }

        //
        // GET: /Usuarios/AddNew
        [HttpGet]
        public ActionResult AddNew()
        {
            UsuariosViewModel model = new UsuariosViewModel(User.Identity.Name);
            return View(model);
        }

        //
        // POST: /Usuarios/AddNew
        [HttpPost]
        public ActionResult AddNew(UsuariosFormModel form)
        {
            UsuariosViewModel model = new UsuariosViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.UserName = User.Identity.Name;

                    string errorMessage = model.createMembership();

                    if (!string.IsNullOrEmpty(errorMessage))
                    {
                        Mensaje = errorMessage;
                        model.FORM = form;
                        return View(model);
                    }

                    model.create();
                    Mensaje = "Se ha creado al usuario con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Usuarios/Edit
        [HttpGet]
        public ActionResult Edit(Guid IdUsuario)
        {
            UsuariosViewModel model = new UsuariosViewModel(IdUsuario);
            return View(model);
        }

        //
        // POST: /Usuarios/Edit
        [HttpPost]
        public ActionResult Edit(UsuariosFormModel form)
        {
            UsuariosViewModel model = new UsuariosViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.UserName = User.Identity.Name;
                    model.update();
                    Mensaje = "Se han guardado los cambios con éxito.";

                    if (User.Identity.Name.ToLower() != form.NombreUsuario.ToLower())
                    {
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (Exception e)
            {
                model = new UsuariosViewModel(form.IdUsuario);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }


        //
        // GET: /Usuarios/EditUser
        [HttpGet]
        public ActionResult EditUser(Guid IdUsuario)
        {
            UsuariosViewModel model = new UsuariosViewModel(IdUsuario);
            return View(model);
        }

        //
        // POST: /Usuarios/EditUser
        [HttpPost]
        public ActionResult EditUser(UsuariosFormModel form)
        {
            UsuariosViewModel model = new UsuariosViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.UserName = User.Identity.Name;

                    string errorMessage = model.updateMembership();

                    if (!string.IsNullOrEmpty(errorMessage))
                    {
                        Mensaje = errorMessage;
                        model.FORM = form;
                        return View(model);
                    }

                    model.updateUser();

                    if (User.Identity.Name.ToLower() == form.NombreUsuario.ToLower())
                    {
                        Mensaje = "Se han guardado los cambios con éxito. Favor ingrese nuevamente al sistema.";
                        return RedirectToAction("LogOff", "Account");
                    }

                    Mensaje = "Se han guardado los cambios con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new UsuariosViewModel(form.IdUsuario);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Usuarios/Delete
        [HttpGet]
        public ActionResult Delete(Guid IdUsuario)
        {
            UsuariosViewModel model = new UsuariosViewModel(IdUsuario);
            model.delete();
            Mensaje = "Se ha eliminado al usuario con éxito.";
            return RedirectToAction("Index");
        }

        //
        // GET: /Usuarios/ResetPassword
        [HttpGet]
        public ActionResult ResetPassword(Guid IdUsuario)
        {
            UsuariosViewModel model = new UsuariosViewModel(IdUsuario);
            model.resetPassword();
            Mensaje = "Se ha reseteado la contraseña con éxito. Para mayor información, haga click en el ícono que se encuentra en la columna Resetear Contraseña.";
            return RedirectToAction("Index");
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }
    }
}
