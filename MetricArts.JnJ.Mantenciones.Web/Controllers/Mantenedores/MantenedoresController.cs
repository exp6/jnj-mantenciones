﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetricArts.JnJ.Mantenciones.Controllers.Mantenedores
{
    public class MantenedoresController : Controller
    {
        //
        // GET: /Mantenedores/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Mantenedores/Usuarios

        public ActionResult Usuarios()
        {
            return RedirectToAction("Index", "Usuarios");
        }

        //
        // GET: /Mantenedores/Franquicias

        public ActionResult Franquicias()
        {
            return RedirectToAction("Index", "Franquicias");
        }

        //
        // GET: /Mantenedores/Clientes

        public ActionResult Clientes()
        {
            return RedirectToAction("Index", "Clientes");
        }

        //
        // GET: /Mantenedores/Sucursales

        public ActionResult Sucursales()
        {
            return RedirectToAction("Index", "Sucursales");
        }

        //
        // GET: /Mantenedores/Contactos

        public ActionResult Contactos()
        {
            return RedirectToAction("Index", "Contactos");
        }

        //
        // GET: /Mantenedores/Equipos

        public ActionResult Equipos()
        {
            return RedirectToAction("Index", "Equipos");
        }

        //
        // GET: /Mantenedores/Cuestionarios

        public ActionResult Cuestionarios()
        {
            return RedirectToAction("Index", "Cuestionarios");
        }

        //
        // GET: /Mantenedores/PlantillasCorreos

        public ActionResult PlantillasCorreos()
        {
            return RedirectToAction("Index", "PlantillasCorreos");
        }

        //
        // GET: /Mantenedores/PlantillasPDF

        public ActionResult PlantillasPDF()
        {
            return RedirectToAction("Index", "PlantillasPDF");
        }

        //
        // GET: /Mantenedores/KitRepuestos

        public ActionResult KitRepuestos()
        {
            return RedirectToAction("Index", "KitRepuestos");
        }

        //
        // GET: /Mantenedores/Repuestos

        public ActionResult Repuestos()
        {
            return RedirectToAction("Index", "Repuestos");
        }

        //
        // GET: /Mantenedores/Distribuidores

        public ActionResult Distribuidores()
        {
            return RedirectToAction("Index", "Distribuidores");
        }

        //
        // GET: /Mantenedores/CalendarioFeriados

        public ActionResult CalendarioFeriados()
        {
            return RedirectToAction("Index", "CalendarioFeriados");
        }
    }
}
