﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Helpers;
using MetricArts.JnJ.Mantenciones.Models.Mantenedores.Cuestionarios;

namespace MetricArts.JnJ.Mantenciones.Controllers.Mantenedores
{
    public class CuestionariosController : Controller
    {
        //
        // GET: /Cuestionarios/

        public ActionResult Index()
        {
            CuestionariosViewModel model = new CuestionariosViewModel();
            return View(model);
        }

        //
        // GET: /Cuestionarios/CrearNuevo
        [HttpGet]
        public ActionResult CrearNuevo()
        {
            CuestionariosViewModel model = new CuestionariosViewModel(User.Identity.Name);
            return View(model);
        }

        //
        // POST: /Cuestionarios/CrearNuevo
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CrearNuevo(CuestionariosFormModel form)
        {
            CuestionariosViewModel model = new CuestionariosViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.create();
                    Mensaje = "Se ha creado el cuestionario con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new CuestionariosViewModel(User.Identity.Name);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Cuestionarios/Editar
        [HttpGet]
        public ActionResult Editar(Guid IdCuestionario)
        {
            CuestionariosViewModel model = new CuestionariosViewModel(IdCuestionario);
            return View(model);
        }

        //
        // POST: /Cuestionarios/Editar
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Editar(CuestionariosFormModel form)
        {
            CuestionariosViewModel model = new CuestionariosViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.update();
                    Mensaje = "Se han guardado los cambios con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new CuestionariosViewModel(form.IdCuestionario);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Cuestionarios/Eliminar
        [HttpGet]
        public ActionResult Eliminar(Guid IdCuestionario)
        {
            CuestionariosViewModel model = new CuestionariosViewModel(IdCuestionario);
            model.delete();
            Mensaje = "Se ha eliminado el cuestionario con éxito.";
            return RedirectToAction("Index");
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }

        //
        // GET: /Cuestionarios/GetPreguntas?IdCuestionario
        //[HttpGet]
        //public ActionResult GetPreguntas(Guid IdCuestionario)
        //{
        //    try
        //    {
        //        var cuestionarioPreguntas = Utils.getDBConnection().CuestionarioPreguntas.OrderBy(x => x.Orden).Where(x => x.IdCuestionario == IdCuestionario).Select(x => new
        //        {
        //            Enunciado = x.Pregunta.Enunciado,
        //            Respuestas = x.Pregunta.CuestionarioPreguntas.Select(y => new
        //                    {
        //                        IdRepuestas = y.IdRespuesta,
        //                        IdRepuestaSugerida = y.RespuestaSugerida.IdRespuestaSugerida,
        //                        RepuestaSugerida = y.RespuestaSugerida.,
        //                        atendidoPor = y.Usuario.Nombre + " " + y.Usuario.ApellidoPaterno,
        //                        fechaModificacion = y.FechaActualizacion.ToString()
        //                    }
        //        }).ToList();

        //        return Json(cuestionarioPreguntas, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception e)
        //    {
        //        return Json("", JsonRequestBehavior.AllowGet);
        //    }
        //}
    }
}
