﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Models.Mantenedores.Sucursales;

namespace MetricArts.JnJ.Mantenciones.Controllers.Mantenedores
{
    public class SucursalesController : Controller
    {
        //
        // GET: /Sucursales/

        public ActionResult Index()
        {
            SucursalesViewModel model = new SucursalesViewModel();
            return View(model);
        }

        //
        // GET: /Sucursales/CrearNuevo
        [HttpGet]
        public ActionResult CrearNuevo()
        {
            SucursalesViewModel model = new SucursalesViewModel(User.Identity.Name);
            return View(model);
        }

        //
        // POST: /Sucursales/CrearNuevo
        [HttpPost]
        public ActionResult CrearNuevo(SucursalesFormModel form)
        {
            SucursalesViewModel model = new SucursalesViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.create();
                    Mensaje = "Se ha creado la sucursal con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new SucursalesViewModel(User.Identity.Name);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Sucursales/Editar
        [HttpGet]
        public ActionResult Editar(Guid IdSucursal)
        {
            SucursalesViewModel model = new SucursalesViewModel(IdSucursal);
            return View(model);
        }

        //
        // POST: /Sucursales/Editar
        [HttpPost]
        public ActionResult Editar(SucursalesFormModel form)
        {
            SucursalesViewModel model = new SucursalesViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.update();
                    Mensaje = "Se han guardado los cambios con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new SucursalesViewModel(form.IdSucursal);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Sucursales/Editar
        [HttpGet]
        public ActionResult Eliminar(Guid IdSucursal)
        {
            SucursalesViewModel model = new SucursalesViewModel(IdSucursal);
            model.delete();
            Mensaje = "Se ha eliminado la sucursal con éxito.";
            return RedirectToAction("Index");
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }

    }
}
