﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Models.Mantenedores.Repuestos;

namespace MetricArts.JnJ.Mantenciones.Controllers.Mantenedores
{
    public class RepuestosController : Controller
    {
        //
        // GET: /Repuestos/

        public ActionResult Index()
        {
            RepuestosViewModel model = new RepuestosViewModel();
            return View(model);
        }

        //
        // GET: /Repuestos/CrearNuevo
        [HttpGet]
        public ActionResult CrearNuevo()
        {
            RepuestosViewModel model = new RepuestosViewModel(User.Identity.Name);
            return View(model);
        }

        //
        // POST: /Repuestos/CrearNuevo
        [HttpPost]
        public ActionResult CrearNuevo(RepuestosFormModel form)
        {
            RepuestosViewModel model = new RepuestosViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.create();
                    Mensaje = "Se ha creado el repuesto con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new RepuestosViewModel(User.Identity.Name);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Repuestos/Editar
        [HttpGet]
        public ActionResult Editar(Guid IdRepuesto)
        {
            RepuestosViewModel model = new RepuestosViewModel(IdRepuesto);
            return View(model);
        }

        //
        // POST: /Repuestos/Editar
        [HttpPost]
        public ActionResult Editar(RepuestosFormModel form)
        {
            RepuestosViewModel model = new RepuestosViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.update();
                    Mensaje = "Se han guardado los cambios con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new RepuestosViewModel(form.IdRepuesto);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Repuestos/Editar
        [HttpGet]
        public ActionResult Eliminar(Guid IdRepuesto)
        {
            RepuestosViewModel model = new RepuestosViewModel(IdRepuesto);
            model.delete();
            Mensaje = "Se ha eliminado el repuesto con éxito.";
            return RedirectToAction("Index");
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }
    }
}
