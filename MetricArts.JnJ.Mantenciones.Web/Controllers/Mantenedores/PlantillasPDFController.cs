﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Models.Mantenedores.PlantillasPDF;

namespace MetricArts.JnJ.Mantenciones.Controllers.Mantenedores
{
    public class PlantillasPDFController : Controller
    {
        //
        // GET: /PlantillasPDF/

        public ActionResult Index()
        {
            PlantillasPDFViewModel model = new PlantillasPDFViewModel();
            return View(model);
        }

        //
        // GET: /PlantillasPDF/CrearNuevo
        [HttpGet]
        public ActionResult CrearNuevo()
        {
            PlantillasPDFViewModel model = new PlantillasPDFViewModel(User.Identity.Name);
            return View(model);
        }

        //
        // POST: /PlantillasPDF/CrearNuevo
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CrearNuevo(PlantillasPDFFormModel form)
        {
            PlantillasPDFViewModel model = new PlantillasPDFViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.create();
                    Mensaje = "Se ha creado la plantilla con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new PlantillasPDFViewModel(User.Identity.Name);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /PlantillasPDF/Editar
        [HttpGet]
        public ActionResult Editar(Guid IdPlantilla)
        {
            PlantillasPDFViewModel model = new PlantillasPDFViewModel(IdPlantilla);
            return View(model);
        }

        //
        // POST: /PlantillasPDF/Editar
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Editar(PlantillasPDFFormModel form)
        {
            PlantillasPDFViewModel model = new PlantillasPDFViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.update();
                    Mensaje = "Se han guardado los cambios con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new PlantillasPDFViewModel(form.IdPlantilla);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /PlantillasPDF/Editar
        [HttpGet]
        public ActionResult Eliminar(Guid IdPlantilla)
        {
            PlantillasPDFViewModel model = new PlantillasPDFViewModel(IdPlantilla);
            model.delete();
            Mensaje = "Se ha eliminado la plantilla con éxito.";
            return RedirectToAction("Index");
        }

        //
        // GET: /PlantillasPDF/ObtenerPlantillaHtml
        [HttpGet]
        public ActionResult ObtenerPlantillaHtml(Guid IdPlantilla)
        {
            PlantillasPDFViewModel model = new PlantillasPDFViewModel(IdPlantilla);
            string msg = model.getPlantillaHtml(IdPlantilla);
            return Content(msg);
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }

    }
}
