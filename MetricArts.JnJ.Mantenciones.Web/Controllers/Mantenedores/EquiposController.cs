﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Models.Mantenedores.Equipos;

namespace MetricArts.JnJ.Mantenciones.Controllers.Mantenedores
{
    public class EquiposController : Controller
    {
        //
        // GET: /Equipos/

        public ActionResult Index()
        {
            EquiposViewModel model = new EquiposViewModel();
            return View(model);
        }

        //
        // GET: /Equipos/CrearNuevo
        [HttpGet]
        public ActionResult CrearNuevo()
        {
            EquiposViewModel model = new EquiposViewModel(User.Identity.Name);
            return View(model);
        }

        //
        // POST: /Equipos/CrearNuevo
        [HttpPost]
        public ActionResult CrearNuevo(EquiposFormModel form)
        {
            EquiposViewModel model = new EquiposViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.create();
                    Mensaje = "Se ha creado el equipo con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new EquiposViewModel(User.Identity.Name);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Equipos/Editar
        [HttpGet]
        public ActionResult Editar(Guid IdEquipo)
        {
            EquiposViewModel model = new EquiposViewModel(IdEquipo);
            return View(model);
        }

        //
        // POST: /Equipos/Editar
        [HttpPost]
        public ActionResult Editar(EquiposFormModel form)
        {
            EquiposViewModel model = new EquiposViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.update();
                    Mensaje = "Se han guardado los cambios con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new EquiposViewModel(form.IdEquipo);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Equipos/Editar
        [HttpGet]
        public ActionResult Eliminar(Guid IdEquipo)
        {
            EquiposViewModel model = new EquiposViewModel(IdEquipo);
            model.delete();
            Mensaje = "Se ha eliminado el equipo con éxito.";
            return RedirectToAction("Index");
        }

        //
        // GET: /Equipos/Inactivar
        [HttpGet]
        public ActionResult Inactivar(Guid IdEquipo)
        {
            EquiposViewModel model = new EquiposViewModel(IdEquipo);
            model.inactivar();
            Mensaje = "Se ha dado de baja el equipo con éxito.";
            return RedirectToAction("Index");
        }

        //
        // GET: /Equipos/Activar
        [HttpGet]
        public ActionResult Activar(Guid IdEquipo)
        {
            EquiposViewModel model = new EquiposViewModel(IdEquipo);
            model.activar();
            Mensaje = "Se ha vuelto a activar el equipo con éxito.";
            return RedirectToAction("Index");
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }
    }
}
