﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Models.Mantenedores.Contactos;

namespace MetricArts.JnJ.Mantenciones.Controllers.Mantenedores
{
    public class ContactosController : Controller
    {
        //
        // GET: /Contactos/

        public ActionResult Index()
        {
            ContactosViewModel model = new ContactosViewModel();
            return View(model);
        }

        //
        // GET: /Contactos/CrearNuevo
        [HttpGet]
        public ActionResult CrearNuevo()
        {
            ContactosViewModel model = new ContactosViewModel(User.Identity.Name);
            return View(model);
        }

        //
        // POST: /Contactos/CrearNuevo
        [HttpPost]
        public ActionResult CrearNuevo(ContactosFormModel form)
        {
            ContactosViewModel model = new ContactosViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.create();
                    Mensaje = "Se ha creado el contacto con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new ContactosViewModel(User.Identity.Name);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Contactos/Editar
        [HttpGet]
        public ActionResult Editar(Guid IdContacto)
        {
            ContactosViewModel model = new ContactosViewModel(IdContacto);
            return View(model);
        }

        //
        // POST: /Contactos/Editar
        [HttpPost]
        public ActionResult Editar(ContactosFormModel form)
        {
            ContactosViewModel model = new ContactosViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.update();
                    Mensaje = "Se han guardado los cambios con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new ContactosViewModel(form.IdContacto);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Contactos/Editar
        [HttpGet]
        public ActionResult Eliminar(Guid IdContacto)
        {
            ContactosViewModel model = new ContactosViewModel(IdContacto);
            model.delete();
            Mensaje = "Se ha eliminado el contacto con éxito.";
            return RedirectToAction("Index");
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }
    }
}
