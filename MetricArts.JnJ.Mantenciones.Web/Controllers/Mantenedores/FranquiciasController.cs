﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Models.Mantenedores.Franquicias;

namespace MetricArts.JnJ.Mantenciones.Controllers.Mantenedores
{
    public class FranquiciasController : Controller
    {
        //
        // GET: /Franquicias/
        public ActionResult Index()
        {
            FranquiciasViewModel model = new FranquiciasViewModel();
            return View(model);
        }

        //
        // GET: /Franquicias/CrearNuevo
        [HttpGet]
        public ActionResult CrearNuevo()
        {
            FranquiciasViewModel model = new FranquiciasViewModel(User.Identity.Name);
            return View(model);
        }

        //
        // POST: /Franquicias/CrearNuevo
        [HttpPost]
        public ActionResult CrearNuevo(FranquiciasFormModel form)
        {
            FranquiciasViewModel model = new FranquiciasViewModel(form);

            try 
            {
                if(ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.create();
                    Mensaje = "Se ha creado la franquicia con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch(Exception e) 
            {
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Franquicias/Editar
        [HttpGet]
        public ActionResult Editar(Guid IdFranquicia)
        {
            FranquiciasViewModel model = new FranquiciasViewModel(IdFranquicia);
            return View(model);
        }

        //
        // POST: /Franquicias/Editar
        [HttpPost]
        public ActionResult Editar(FranquiciasFormModel form)
        {
            FranquiciasViewModel model = new FranquiciasViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.update();
                    Mensaje = "Se han guardado los cambios con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Franquicias/Editar
        [HttpGet]
        public ActionResult Eliminar(Guid IdFranquicia)
        {
            FranquiciasViewModel model = new FranquiciasViewModel(IdFranquicia);
            model.delete();
            Mensaje = "Se ha eliminado la franquicia con éxito.";
            return RedirectToAction("Index");
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }
    }
}
