﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Models.Mantenedores.PlantillasCorreo;

namespace MetricArts.JnJ.Mantenciones.Controllers.Mantenedores
{
    public class PlantillasCorreosController : Controller
    {
        //
        // GET: /PlantillasCorreos/

        public ActionResult Index()
        {
            PlantillasCorreoViewModel model = new PlantillasCorreoViewModel();
            return View(model);
        }

        //
        // GET: /PlantillasCorreos/CrearNuevo
        [HttpGet]
        public ActionResult CrearNuevo()
        {
            PlantillasCorreoViewModel model = new PlantillasCorreoViewModel(User.Identity.Name);
            return View(model);
        }

        //
        // POST: /PlantillasCorreos/CrearNuevo
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CrearNuevo(PlantillasCorreoFormModel form)
        {
            PlantillasCorreoViewModel model = new PlantillasCorreoViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.create();
                    Mensaje = "Se ha creado la plantilla con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new PlantillasCorreoViewModel(User.Identity.Name);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /PlantillasCorreos/Editar
        [HttpGet]
        public ActionResult Editar(Guid IdPlantilla)
        {
            PlantillasCorreoViewModel model = new PlantillasCorreoViewModel(IdPlantilla);
            return View(model);
        }

        //
        // POST: /PlantillasCorreos/Editar
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Editar(PlantillasCorreoFormModel form)
        {
            PlantillasCorreoViewModel model = new PlantillasCorreoViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.update();
                    Mensaje = "Se han guardado los cambios con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new PlantillasCorreoViewModel(form.IdPlantilla);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /PlantillasCorreos/Editar
        [HttpGet]
        public ActionResult Eliminar(Guid IdPlantilla)
        {
            PlantillasCorreoViewModel model = new PlantillasCorreoViewModel(IdPlantilla);
            model.delete();
            Mensaje = "Se ha eliminado la plantilla con éxito.";
            return RedirectToAction("Index");
        }

        //
        // GET: /PlantillasCorreos/ObtenerPlantillaHtml
        [HttpGet]
        public ActionResult ObtenerPlantillaHtml(Guid IdPlantilla)
        {
            PlantillasCorreoViewModel model = new PlantillasCorreoViewModel(IdPlantilla);
            string msg = model.getPlantillaHtml(IdPlantilla);
            return Content(msg);
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }

    }
}
