﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Models.Mantenedores.Clientes;

namespace MetricArts.JnJ.Mantenciones.Controllers.Mantenedores
{
    public class ClientesController : Controller
    {
        //
        // GET: /Clientes/

        public ActionResult Index()
        {
            ClientesViewModel model = new ClientesViewModel();
            return View(model);
        }

        //
        // GET: /Clientes/CrearNuevo
        [HttpGet]
        public ActionResult CrearNuevo()
        {
            ClientesViewModel model = new ClientesViewModel(User.Identity.Name);
            return View(model);
        }

        //
        // POST: /Clientes/CrearNuevo
        [HttpPost]
        public ActionResult CrearNuevo(ClientesFormModel form)
        {
            ClientesViewModel model = new ClientesViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.create();
                    Mensaje = "Se ha creado al cliente con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new ClientesViewModel(User.Identity.Name);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Clientes/Editar
        [HttpGet]
        public ActionResult Editar(Guid IdCliente)
        {
            ClientesViewModel model = new ClientesViewModel(IdCliente);
            return View(model);
        }

        //
        // POST: /Clientes/Editar
        [HttpPost]
        public ActionResult Editar(ClientesFormModel form)
        {
            ClientesViewModel model = new ClientesViewModel(form);

            try
            {
                if (ModelState.IsValid)
                {
                    form.userName = User.Identity.Name;
                    model.update();
                    Mensaje = "Se han guardado los cambios con éxito.";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                model = new ClientesViewModel(form.IdCliente);
                Mensaje = "No se han podido guardar los cambios. Intente nuevamente. Si el problema persiste, favor contactarse con servicio técnico.";
            }
            return View(model);
        }

        //
        // GET: /Clientes/Editar
        [HttpGet]
        public ActionResult Eliminar(Guid IdCliente)
        {
            ClientesViewModel model = new ClientesViewModel(IdCliente);
            model.delete();
            Mensaje = "Se ha eliminado al cliente con éxito.";
            return RedirectToAction("Index");
        }

        public string Mensaje
        {
            set
            {
                TempData["Mensaje"] = value;
            }
        }
    }
}
