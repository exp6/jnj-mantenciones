﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Core;
using MetricArts.JnJ.Mantenciones.Helpers;

namespace MetricArts.JnJ.Mantenciones.Controllers
{
    public class ServicioConsultaController : Controller
    {
        protected MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();

        //
        // GET: /ServicioConsulta/ObtenerClientes
        public ActionResult ObtenerClientes(Guid franquiciaId)
        {
            try
            {
                if (franquiciaId != null)
                {
                    IEnumerable<SelectListItem> clientes = Utils.getDBConnection().Clientes.OrderBy(x => x.NombreEmpresa).Where(x => x.IdFranquicia == franquiciaId)
                        .Select(x => new SelectListItem { Text = x.NombreEmpresa, Value = x.IdCliente.ToString() });
                    return Json(clientes, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /ServicioConsulta/ObtenerSucursales
        public ActionResult ObtenerSucursales(Guid idCliente)
        {
            try
            {
                if (idCliente != null)
                {
                    IEnumerable<SelectListItem> sucursales = Utils.getDBConnection().Sucursals.OrderBy(x => x.NombreSucursal).Where(x => x.IdCliente == idCliente)
                        .Select(x => new SelectListItem { Text = x.NombreSucursal, Value = x.IdSucursal.ToString() });
                    return Json(sucursales, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /ServicioConsulta/ObtenerEquiposSucursal
        public ActionResult ObtenerEquiposSucursal(Guid? idSucursal, int AnioPlanificacion)
        {
            try
            {
                if (idSucursal != null)
                {
                    var equipos = db.Equipos.OrderBy(x => x.Nombre).Where(x => x.IdSucursal == idSucursal && x.Estado.Value == 1)
                        .Select(x => new
                        {
                            nombreEquipo = x.Nombre,
                            codigoEquipo = x.CodigoEquipo,
                            IdEquipo = x.IdEquipo.ToString(),
                            contrato = x.TipoContrato.TipoContrato1,
                            fechasPropuesta = Utils.CalculoMesesPropuesta(x.IdEquipo, getFechaUltimaMantencion(x.IdEquipo), x.FechaInstalacion, x.TipoContrato.Codigo, AnioPlanificacion),
                            fechaInstalacion = Utils.getFechaStrFormat(x.FechaInstalacion, "dd-MM-yyyy"),
                            fechaUltimaMantencion = getFechaUltimaMantecionStr(x.IdEquipo),
                            fechaActual = DateTime.Now.ToString("dd-MM-yyyy")
                        }).ToList();

                    return Json(equipos, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception e)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /ServicioConsulta/ObtenerEquiposSucursalDescripcion
        public ActionResult ObtenerEquiposSucursalDescripcion(Guid? idSucursal)
        {
            try
            {
                if (idSucursal != null)
                {
                    var equipos = Utils.getDBConnection().Equipos.OrderBy(x => x.Nombre).Where(x => x.IdSucursal == idSucursal && x.Estado.Value == 1)
                        .Select(x => new
                        {
                            nombreEquipo = x.Nombre,
                            codigoEquipo = x.CodigoEquipo,
                            idEquipo = x.IdEquipo
                        }).ToList();

                    return Json(equipos, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /ServicioConsulta/ObtenerEquiposPlanificacion
        public ActionResult ObtenerEquiposPlanificacion(Guid? idPlanificacion)
        {
            try
            {
                if (idPlanificacion != null)
                {
                    MantencionesDataContext dbTemp = Utils.getDBConnection();
                    Planificacion plan = dbTemp.Planificacions.Where(x => x.IdPlanificacion == idPlanificacion).SingleOrDefault();
                    IEnumerable<Mantencion> mantenciones = dbTemp.Mantencions.Where(x => x.IdPlanificacion == idPlanificacion && x.Habilitado);

                    var equipos = dbTemp.Equipos.OrderBy(x => x.Nombre).Where(x => x.Mantencions.Intersect(mantenciones).Any())
                        .Select(x => new
                        {
                            nombreEquipo = x.Nombre,
                            codigoEquipo = x.CodigoEquipo,
                            idEquipo = x.IdEquipo,
                            tipoContrato = x.TipoContrato.TipoContrato1
                        }).ToList();

                    return Json(equipos, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        private String getFechaUltimaMantecionStr(Guid idEquipo)
        {
            //Equipo equipo = db.Equipos.Where(x => x.IdEquipo == idEquipo).SingleOrDefault();
            //DateTime? fecha = getFechaUltimaMantencion(idEquipo) ?? equipo.FechaUltimaMantencion;
            DateTime? fecha = getFechaUltimaMantencion(idEquipo);
            String fechaStr = (fecha != null ? fecha.Value.ToString("dd-MM-yyyy") : "N/A");
            return fechaStr;
        }

        private DateTime? getFechaUltimaMantencion(Guid idEquipo) 
        {
            Equipo equipo = db.Equipos.Where(x => x.IdEquipo == idEquipo).SingleOrDefault();
            Mantencion m = db.Mantencions.OrderByDescending(x => x.FechaConfirmacion)
                .Where(x => x.IdEquipo == idEquipo && x.FechaConfirmacion != null && x.Habilitado).FirstOrDefault();
            return (m != null ? m.FechaConfirmacion : (equipo.FechaUltimaMantencion != null ? equipo.FechaUltimaMantencion : null));
        }

        //
        // GET: /ServicioConsulta/ObtenerContactoSucursal
        public ActionResult ObtenerContactoSucursal(Guid? idSucursal)
        {
            try
            {
                if (idSucursal != null)
                {
                    Contacto contacto = Utils.getDBConnection().Contactos.Where(x => x.IdSucursal == idSucursal).FirstOrDefault();
                    return Json(contacto.Correo, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /ServicioConsulta/ObtenerPlanificacionesSucursal
        public ActionResult ObtenerPlanificacionesSucursal(Guid? idSucursal)
        {
            try
            {
                if (idSucursal != null)
                {
                    IEnumerable<Mantencion> mantenciones = db.Mantencions.Where(x => x.Equipo.IdSucursal == idSucursal && x.Habilitado);
                    var planificaciones = db.Planificacions
                        .Where(x => x.Mantencions.Intersect(mantenciones).Any()).Select(x => new
                    {
                        idPlanificacion = x.IdPlanificacion,
                        anio = x.Ano,
                        nombre = x.Nombre,
                    }).ToList();

                    //var planificaciones = db.Planificacions.OrderBy(x => x.Nombre).Where(x => x.ID == idSucursal)
                    //    .Select(x => new
                    //    {
                    //        anio = x.Ano,
                    //        nombre = x.Nombre,
                    //    }).Distinct().ToList();

                    return Json(planificaciones, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        

        //
        // GET: /ServicioConsulta/ObtenerFormatosCorreo
        public ActionResult ObtenerFormatosCorreo(Guid? idTipoNotificacion)
        {
            try
            {
                var formatos = Utils.getDBConnection().MailTemplates.OrderBy(x => x.Nombre).Where(x => x.IdTipoNotificacion == idTipoNotificacion)
                        .Select(x => new
                {
                    Text = x.Nombre,
                    Value = x.IdMailTemplate
                }).ToList();

                return Json(formatos, JsonRequestBehavior.AllowGet);
                
            }
            catch
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /ServicioConsulta/ObtenerFormatosPDF
        public ActionResult ObtenerFormatosPDF(Guid? idTipoNotificacion)
        {
            try
            {
                var formatos = Utils.getDBConnection().PDFTemplates.OrderBy(x => x.Nombre).Where(x => x.IdTipoNotificacion == idTipoNotificacion)
                        .Select(x => new
                        {
                            Text = x.Nombre,
                            Value = x.IdPDFTemplate
                        }).ToList();

                return Json(formatos, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /ServicioConsulta/ObtenerIngenieros
        public ActionResult ObtenerIngenieros(Guid? idTipoNotificacion) 
        {
            try
            {
                var ingenieros = Utils.getDBConnection().Ingenieros.OrderBy(x => x.Usuario.Nombre)
                        .Select(x => new
                        {
                            Text = x.Usuario.Nombre + " " + x.Usuario.ApellidoPaterno,
                            Value = x.IdIngeniero
                        }).ToList();

                return Json(ingenieros, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /ServicioConsulta/ObtenerMantencion
        public ActionResult ObtenerMantencion(Guid? IdMantencion)
        {
            try
            {
                var mantencion = Utils.getDBConnection().Mantencions.Where(x => x.IdMantencion == IdMantencion && x.Habilitado)
                        .Select(x => new
                        {
                            id = x.IdMantencion.ToString(),
                            equipo = x.Equipo.Nombre,
                            tipoContrato = x.TipoContrato.TipoContrato1,
                            estadoMantencion = x.EstadoMantencion.Estado,
                            estadoMantencionCodigo = x.EstadoMantencion.Codigo,
                            sucursal = x.Equipo.Sucursal.NombreSucursal,
                            cliente = x.Equipo.Sucursal.Cliente.NombreEmpresa,
                            ingeniero = x.Ingeniero.Usuario.Nombre + " " + x.Ingeniero.Usuario.ApellidoPaterno,
                            codigoEquipo = x.Equipo.CodigoEquipo,
                            fechaConfirmacion = x.FechaConfirmacion.ToString(),
                            fechaInstalacionEquipo = x.Equipo.FechaInstalacion.ToShortDateString(),
                            periodoPropuesto = (x.MesPeriodo.ToString().Length == 1 ? "0" + x.MesPeriodo.ToString() : x.MesPeriodo.ToString()) + "-" + x.Planificacion.Ano,
                            planificacion = x.Planificacion.Nombre,
                            franquicia = x.Equipo.Sucursal.Cliente.Franquicia.NombreFranquicia,
                            idEquipo = x.Equipo.IdEquipo,
                            checkListRespondido = (x.CuestionarioMantencions.Where(y => y.IdMantencion == x.IdMantencion && y.Cuestionario.TipoCuestionario.Codigo == 1).SingleOrDefault() != null ? x.CuestionarioMantencions.Where(y => y.IdMantencion == x.IdMantencion && y.Cuestionario.TipoCuestionario.Codigo == 1).SingleOrDefault().Respondida : false),
                            estadosHistorico = x.EstadoHistoricoMantencions.OrderByDescending(z => z.FechaActualizacion).Select(y => new
                            {
                                estadoDescripcion = y.EstadoMantencion.Estado,
                                observaciones = y.Observaciones,
                                atendidoPor = y.Usuario.Nombre + " " + y.Usuario.ApellidoPaterno,
                                fechaModificacion = y.FechaActualizacion.ToString()
                            }
                            ).ToArray()
                        });

                return Json(mantencion, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /ServicioConsulta/ObtenerKitsEquipo
        public ActionResult ObtenerKitsEquipo(Guid? IdEquipo)
        {
            try
            {
                var kits = Utils.getDBConnection().KitRepuestos.OrderBy(x => x.Nombre).Where(x => x.IdEquipo == IdEquipo)
                        .Select(x => new
                        {
                            Text = x.Nombre,
                            Value = x.IdKitRepuesto
                        }).ToList();

                return Json(kits, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /ServicioConsulta/ObtenerRepuestos
        public ActionResult ObtenerRepuestos(Guid? idKitRepuestos)
        {
            try
            {
                var repuestos = Utils.getDBConnection().KitRepuestoRepuestos.Where(x => x.IdKitRepuesto == idKitRepuestos).Select(x => x.Repuesto);
                var kits = repuestos.OrderBy(x => x.Codigo)
                        .Select(x => new
                        {
                            IdRepuesto = x.IdRepuesto,
                            Codigo = x.Codigo,
                            Descripcion = x.Descripcion,
                            Categoria = x.Categoria,
                            Cantidad = x.Cantidad
                        }).ToList();

                return Json(kits, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /ServicioConsulta/ObtenerContactoDistribuidor
        public ActionResult ObtenerContactoDistribuidor(Guid? IdDistribuidor)
        {
            try
            {
                if (IdDistribuidor != null)
                {
                    DistribuidorRepuesto distribuidor = Utils.getDBConnection().DistribuidorRepuestos.Where(x => x.IdDistribuidorRepuestos == IdDistribuidor).FirstOrDefault();
                    return Json(distribuidor.CorreoContacto, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /ServicioConsulta/ObtenerChecklists
        public ActionResult ObtenerChecklists()
        {
            try
            {
                var checklists = Utils.getDBConnection().Cuestionarios.Where(x => x.TipoCuestionario.Codigo == 1)
                        .Select(x => new
                        {
                            Text = x.Descripcion,
                            Value = x.IdCuestionario.ToString()
                        }).ToList();

                return Json(checklists, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /ServicioConsulta/ObtenerFechasMantencion
        public ActionResult ObtenerFechasMantencion()
        {
            try
            {
                
                    MantencionesDataContext dbTemp = Utils.getDBConnection();
                    var estadosValidos = new[] { 1 };

                    var mantenciones = dbTemp.Mantencions.OrderByDescending(x => x.FechaIngreso)
                                                            .Where(x => estadosValidos.Contains(x.EstadoMantencion.Codigo) && x.Habilitado)
                                                            .Select(x => new
                                                            {
                                                                Fecha = x.Planificacion.Ano + "-" + (x.MesPeriodo.ToString().Length < 2 ? "0" + x.MesPeriodo.ToString() : x.MesPeriodo.ToString())
                                                            }).ToList();

                    return Json(mantenciones, JsonRequestBehavior.AllowGet);
                
            }
            catch (Exception e)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /ServicioConsulta/ObtenerComunas
        public ActionResult ObtenerComunas(Guid regionId)
        {
            try
            {
                if (regionId != null)
                {
                    IEnumerable<SelectListItem> comunas = Utils.getDBConnection().Comunas.OrderBy(x => x.Nombre).Where(x => x.IdRegion == regionId)
                        .Select(x => new SelectListItem { Text = x.Nombre, Value = x.IdComuna.ToString() });
                    return Json(comunas, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /ServicioConsulta/ObtenerSucursalesSinContactos
        public ActionResult ObtenerSucursalesSinContactos(Guid idCliente)
        {
            try
            {
                if (idCliente != null)
                {
                    List<Guid> sucursalesConContactos = Utils.getDBConnection().Contactos.Distinct().Select(x => x.IdSucursal).ToList();
                    List<Guid> totalSucursales = Utils.getDBConnection().Sucursals.Select(x => x.IdSucursal).ToList();
                    List<Guid> sucursalesSinContactos = totalSucursales.Except(sucursalesConContactos).Union(sucursalesConContactos.Except(totalSucursales)).ToList();

                    IEnumerable<SelectListItem> sucursales = Utils.getDBConnection().Sucursals.OrderBy(x => x.NombreSucursal).Where(x => x.IdCliente == idCliente && sucursalesSinContactos.Contains(x.IdSucursal))
                        .Select(x => new SelectListItem { Text = x.NombreSucursal, Value = x.IdSucursal.ToString() });
                    return Json(sucursales, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /ServicioConsulta/ObtenerEquiposSucursalKit
        public ActionResult ObtenerEquiposSucursalKit(Guid? idSucursal)
        {
            try
            {
                if (idSucursal != null)
                {
                    var equipos = Utils.getDBConnection().Equipos.OrderBy(x => x.Nombre).Where(x => x.IdSucursal == idSucursal && !x.KitRepuestos.Any())
                        .Select(x => new
                        {
                            nombreFranquicia = x.Sucursal.Cliente.Franquicia.NombreFranquicia,
                            nombreCliente = x.Sucursal.Cliente.NombreEmpresa,
                            nombreSucursal = x.Sucursal.NombreSucursal,
                            nombreEquipo = x.Nombre,
                            codigoEquipo = x.CodigoEquipo,
                            idEquipo = x.IdEquipo
                        }).ToList();

                    return Json(equipos, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /ServicioConsulta/ObtenerTiposPregunta
        public ActionResult ObtenerTiposPregunta()
        {
            try
            {
                var tiposPregunta = Utils.getDBConnection().TipoPreguntas.OrderBy(x => x.Codigo)
                        .Select(x => new
                        {
                            idTipoPregunta = x.IdTipoPregunta,
                            tipoPregunta = x.Descripcion,
                            codigo = x.Codigo
                        }).ToList();

                return Json(tiposPregunta, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
    }
}
