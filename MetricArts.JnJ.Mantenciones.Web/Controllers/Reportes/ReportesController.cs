﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetricArts.JnJ.Mantenciones.Controllers.Reportes
{
    public class ReportesController : Controller
    {
        //
        // GET: /Reportes/
        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Reportes/Correos
        public ActionResult Correos()
        {
            return View();
        }

        //
        // GET: /Reportes/Mantenciones
        public ActionResult Mantenciones()
        {
            return View();
        }
    }
}
