﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Models.Planificaciones;
using MetricArts.JnJ.Mantenciones.Models.Planificaciones.PlanificacionesAutomaticas;
using MetricArts.JnJ.Mantenciones.Models.Mantenciones;
using MetricArts.JnJ.Mantenciones.Core;
using System.Web.Routing;
using MetricArts.JnJ.Mantenciones.Helpers;

namespace MetricArts.JnJ.Mantenciones.Controllers.GestionMantenciones.Planificaciones
{
    public class PlanificacionesController : Controller
    {
        //
        // GET: /Planificaciones/

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GenerarAutomatica()
        {
            PlanificacionesAutomaticasViewModel model = new PlanificacionesAutomaticasViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult GenerarAutomatica(PlanificacionesAutomaticasFormModel FORM)
        {
            new PlanificacionesAutomaticasViewModel(FORM, User.Identity.Name);
            return RedirectToAction("PlanificacionesPropuestas", "Planificaciones");
        }

        [HttpGet]
        public ActionResult CrearNueva()
        {
            CrearPlanificacionViewModel model = new CrearPlanificacionViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult CrearNueva(CrearPlanificacionFormModel FORM)
        {
            CrearPlanificacionViewModel model = new CrearPlanificacionViewModel(FORM, User.Identity.Name);
            model.persist();
            return RedirectToAction("PlanificacionesPropuestas", "Planificaciones");
        }

        public ActionResult ObtenerClientesSeleccionados(Guid[] IdClientesSeleccionados, int AnioPlanificacion)
        {
            try
            {
                if (IdClientesSeleccionados.Length != 0)
                {
                    var sucursales = Utils.getDBConnection().Sucursals.OrderBy(x => x.NombreSucursal).Where(x => IdClientesSeleccionados.Contains(x.IdCliente))
                        .Select(x => new { NombreEmpresa = x.Cliente.NombreEmpresa, 
                            NombreSucursal = x.NombreSucursal, 
                            ID = x.IdSucursal.ToString(), 
                            NombreFranquicia = x.Cliente.Franquicia.NombreFranquicia,
                                           Tiempo = AnioPlanificacion,
                            CantidadEquipos = x.Equipos.Count
                        });

                    return Json(sucursales, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult PDFPreview(Guid IdSucursal, int Anio)
        {
            PDFPreviewViewModel model = new PDFPreviewViewModel(IdSucursal, Anio);
            return PartialView(model);
        }

        [HttpGet]
        public ActionResult PlanificacionesVigentes()
        {
            VerMantencionesVigentesViewModel model = new VerMantencionesVigentesViewModel(DateTime.Now.Year, DateTime.Now.ToString("dd-MM-yyyy"));
            return View(model);
        }

        [HttpPost]
        public ActionResult PlanificacionesVigentes(VerMantencionesVigentesFormModel FORM)
        {
            VerMantencionesVigentesViewModel model = new VerMantencionesVigentesViewModel(FORM);
            return View(model);
        }

        [HttpGet]
        public ActionResult PlanificacionesPropuestas()
        {
            VerMantencionesPropuestasViewModel model = new VerMantencionesPropuestasViewModel();
            return View(model);
        }

        public ActionResult ObtenerPropuestasPorPeriodo(int? mesPeriodo, int? anio)
        {
            try
            {
                if (mesPeriodo != null && anio != null)
                {
                    MantencionesDataContext dbTemp = Utils.getDBConnection();
                    var estadosValidos = new[] { 1 };
                    var mantenciones = dbTemp.Mantencions.OrderByDescending(x => x.FechaIngreso)
                                                            .Where(x => x.MesPeriodo == mesPeriodo && x.Planificacion.Ano == anio && estadosValidos.Contains(x.EstadoMantencion.Codigo))
                                                            .Select(x => new
                                                            {
                                                                Equipo = x.Equipo.Nombre,
                                                                Contrato = x.TipoContrato.TipoContrato1,
                                                                Codigo = x.Equipo.CodigoEquipo,
                                                                Anio = x.Planificacion.Ano,
                                                                Cliente = x.Equipo.Sucursal.Cliente.NombreEmpresa,
                                                                Sucursal = x.Equipo.Sucursal.NombreSucursal,
                                                                Ingeniero = x.Ingeniero.Usuario.Nombre + " " + x.Ingeniero.Usuario.ApellidoPaterno,
                                                                IdIngeniero = x.IdIngeniero,
                                                                ID = x.IdMantencion,
                                                                EstadoCodigo = x.EstadoMantencion.Codigo
                                                            }).ToList();

                    return Json(mantenciones, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult PropuestasReasignacionFecha(string fechaSelected, Guid IdMantencion)
        {
            VigentesReasignacionFechaViewModel model = new VigentesReasignacionFechaViewModel(fechaSelected, IdMantencion);
            return Json(model.persistChangesPropuestas());
        }

        [HttpPost]
        public ActionResult InactivarMantencion(Guid IdMantencion)
        {
            InactivarMantencionViewModel model = new InactivarMantencionViewModel(IdMantencion);
            return Json(model.persistChanges());
        }

        [HttpPost]
        public ActionResult HabilitarMantencion(Guid IdMantencion)
        {
            InactivarMantencionViewModel model = new InactivarMantencionViewModel(IdMantencion);
            return Json(model.persistChangesHabilitar());
        }
    }
}
