﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetricArts.JnJ.Mantenciones.Controllers.GestionMantenciones
{
    [Authorize]
    public class GestionMantencionesController : Controller
    {
        //
        // GET: /GestionMantenciones/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Automaticas()
        {
            return RedirectToAction("GenerarAutomatica", "Planificaciones");
        }

        public ActionResult CrearMantencionManual()
        {
            return RedirectToAction("CrearMantencionManual", "Mantenciones");
        }

        public ActionResult CrearPlanificacionManual()
        {
            return RedirectToAction("CrearNueva", "Planificaciones");
        }

        public ActionResult VerMantencionesVigentes()
        {
            return RedirectToAction("PlanificacionesVigentes", "Planificaciones");
        }

        public ActionResult VerMantencionesPropuestas()
        {
            return RedirectToAction("PlanificacionesPropuestas", "Planificaciones");
        }

        public ActionResult Notificaciones()
        {
            return RedirectToAction("Index", "Notificaciones");
        }

        public ActionResult Seguimiento()
        {
            return RedirectToAction("Seguimiento", "Mantenciones");
        }

        public ActionResult Repuestos()
        {
            return RedirectToAction("Repuestos", "Mantenciones");
        }
    }
}
