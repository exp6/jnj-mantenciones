﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Models.Mantenciones;
using MetricArts.JnJ.Mantenciones.Helpers;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Controllers.GestionMantenciones.Mantenciones
{
    public class ClientsController : Controller
    {
        //
        // GET: /Clients/DescargaPDF?ID=
        public ActionResult DescargaPDF(Guid ID)
        {
            MensajeStage msg = Utils.getDBConnection().MensajeStages.Where(x => x.IdMensajeStage == ID).SingleOrDefault();
            string html = msg.CuerpoHTMLPDF;
            byte[] buffer = Utils.createPDF(html);
            return File(buffer, "application/pdf", msg.Planificacion.Nombre + "-" + msg.Planificacion.Ano + "_" + DateTime.Now.ToShortDateString() + ".pdf");
        }

        //
        // GET: /Clients/Checklist?ID=
        public ActionResult Checklist(Guid ID)
        {
            return View();
        }

        //
        // GET: /Clients/EncuestaSatisfaccion?ID=
        public ActionResult EncuestaSatisfaccion(Guid ID)
        {
            return View();
        }

        //
        // GET: /Clients/AgendarMantenciones?ID=
        [HttpGet]
        public ActionResult AgendarMantenciones(String ID)
        {
            AgendarMantencionesViewModel model = new AgendarMantencionesViewModel(ID);
            return View(model);
        }

        //
        // POST: /Clients/AgendarMantenciones?ID=
        [HttpPost]
        public ActionResult AgendarMantenciones(Guid IdPlanificacion, Guid IdMensajeStage, Guid IdEquipo, String[] fechasSelected)
        {
            AgendarMantencionesFormModel FORM = new AgendarMantencionesFormModel();
            FORM.IdPlanificacionIn = IdPlanificacion;
            FORM.IdMensajeStageIn = IdMensajeStage;
            FORM.IdEquipoIn = IdEquipo;
            FORM.fechasSelectedIn = fechasSelected;
            AgendarMantencionesViewModel model = new AgendarMantencionesViewModel(FORM);
            model.persist();
            return View(model);
        }

        //
        // GET: /Clients/ObtenerFeriados
        [HttpGet]
        public ActionResult ObtenerFeriados(Guid IdPlanificacion)
        {
            try
            {
                Planificacion pl = Utils.getDBConnection().Planificacions.Single(x => x.IdPlanificacion == IdPlanificacion);
                CalendarioFeriadosPai c = Utils.getDBConnection().CalendarioFeriadosPais.Single(x => x.Anio == pl.Ano);
                Pais p = Utils.getDBConnection().Pais.Single(x => x.Nombre == "Chile");

                    if(c != null) {
                        var fechas = Utils.getDBConnection().CalendarioFeriadosFechas.Where(x => x.IdCalendarioFeriadosPais == c.IdCalendarioFeriadosPais && x.CalendarioFeriadosPai.Pais == p)
                        .Select(x => new
                        {
                            fecha = x.Fecha.ToShortDateString()
                        }).ToList();

                        return Json(fechas, JsonRequestBehavior.AllowGet);
                    }
                    else {
                        return Json("", JsonRequestBehavior.AllowGet);
                    }        
            }
            catch (Exception e)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Clients/ObtenerDisponibilidadIngeniero
        [HttpGet]
        public ActionResult ObtenerDisponibilidadIngeniero(Guid IdPlanificacion)
        {
            try
            {
                Planificacion pl = Utils.getDBConnection().Planificacions.Single(x => x.IdPlanificacion == IdPlanificacion);
                List<Ingeniero> ings = pl.Mantencions.Where(x => x.Planificacion == pl && x.Habilitado).Select(x => x.Equipo.Ingeniero).ToList();

                List<string> fechas = new List<string>();
                List<string> fechasAux;

                foreach(Ingeniero i in ings) {
                    fechasAux = i.Mantencions.Where(x => x.FechaConfirmacion != null).Select(x => x.FechaConfirmacion.Value.ToShortDateString()).ToList();
                    fechas.AddRange(fechasAux);
                }

                return Json(fechas, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
    }
}
