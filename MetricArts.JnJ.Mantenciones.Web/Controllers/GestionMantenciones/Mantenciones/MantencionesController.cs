﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Models.Mantenciones;
using MetricArts.JnJ.Mantenciones.Models.Notificaciones;
using MetricArts.JnJ.Mantenciones.Core;
using MetricArts.JnJ.Mantenciones.Helpers;
using System.Globalization;

namespace MetricArts.JnJ.Mantenciones.Controllers.GestionMantenciones.Mantenciones
{
    [Authorize]
    public class MantencionesController : Controller
    {
        //
        // GET: /Mantenciones/

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult CrearMantencionManual() {
            CrearMantencionViewModel model = new CrearMantencionViewModel();
            return View(model);
        }

        [HttpGet]
        public ActionResult Seguimiento()
        {
            SeguimientoViewModel model = new SeguimientoViewModel(DateTime.Now.Year, DateTime.Now.ToString("dd-MM-yyyy"));
            return View(model);
        }

        [HttpPost]
        public ActionResult Seguimiento(SeguimientoFormModel FORM)
        {
            SeguimientoViewModel model = new SeguimientoViewModel(FORM);
            return View(model);
        }

        //
        // POST: /Mantenciones/CambiarEstadoMantencion
        [HttpPost]
        public ActionResult CambiarEstadoMantencion(Guid? IdMantencion, int? nuevoEstado, int? anio, string fecha, string obs)
        {
            SeguimientoFormModel FORM = new SeguimientoFormModel();
            FORM.IdMantencion = IdMantencion.Value;
            FORM.nuevoEstado = nuevoEstado.Value;
            FORM.anioPlanificacion = anio;
            FORM.fechaSelected = fecha;
            FORM.userName = User.Identity.Name;
            FORM.observaciones = obs;
            SeguimientoViewModel model = new SeguimientoViewModel(FORM);
            model.peristNuevoEstado();
            return null;
        }

        public ActionResult ObtenerMantencionesPorFecha(string FechaSeleccionada, int tipoMantencion)
        {
            try
            {
                if (FechaSeleccionada != null)
                {
                    MantencionesDataContext dbTemp = Utils.getDBConnection();
                    DateTime fechaSelected = DateTime.ParseExact(FechaSeleccionada, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    var estadosValidos = (tipoMantencion == 1 ? new[] { 3, 4, 5 } : new[] { 2 });
                    var mantenciones = dbTemp.Mantencions.OrderByDescending(x => x.FechaIngreso)
                                                            .Where(x => x.FechaConfirmacion == fechaSelected && estadosValidos.Contains(x.EstadoMantencion.Codigo) && x.Habilitado)
                                                            .Select(x => new
                                                            {
                                                                Equipo = x.Equipo.Nombre,
                                                                Codigo = x.Equipo.CodigoEquipo,
                                                                Anio = x.Planificacion.Ano,
                                                                Cliente = x.Equipo.Sucursal.Cliente.NombreEmpresa,
                                                                Sucursal = x.Equipo.Sucursal.NombreSucursal,
                                                                Ingeniero = x.Ingeniero.Usuario.Nombre + " " + x.Ingeniero.Usuario.ApellidoPaterno,
                                                                IdIngeniero = x.IdIngeniero,
                                                                ID = x.IdMantencion,
                                                                EstadoCodigo = x.EstadoMantencion.Codigo,
                                                                Checklist = (x.CuestionarioMantencions.Where(y => y.IdMantencion == x.IdMantencion && y.Cuestionario.TipoCuestionario.Codigo == 1).SingleOrDefault() != null ? x.CuestionarioMantencions.Where(y => y.IdMantencion == x.IdMantencion && y.Cuestionario.TipoCuestionario.Codigo == 1).SingleOrDefault().Cuestionario.Descripcion : "<i>No Asignado</i>"),
                                                                ChecklistRespondido = (x.CuestionarioMantencions.Where(y => y.IdMantencion == x.IdMantencion && y.Cuestionario.TipoCuestionario.Codigo == 1).SingleOrDefault() != null ? x.CuestionarioMantencions.Where(y => y.IdMantencion == x.IdMantencion && y.Cuestionario.TipoCuestionario.Codigo == 1).SingleOrDefault().Respondida : false )
                                                            }).ToList();

                    return Json(mantenciones, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception e)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        public ActionResult CalendarioJsonFeed(int tipoMantencion)
        {
            try
            {
                var estadosValidos = (tipoMantencion == 1 ? new[] { 3, 4, 5 } : new[] { 2 });
                var coloresEventos = new[] { "#F89406", "#0088CC", "#BD362F", "#51A351" };

                IQueryable<Mantencion> mantenciones = Utils.getDBConnection().Mantencions.Where(x => x.FechaConfirmacion.HasValue && estadosValidos.Contains(x.EstadoMantencion.Codigo) && x.Habilitado);
                var eventos = mantenciones
                    .Select(x => new
                    {
                        start = x.FechaConfirmacion.ToString(),
                        title = x.Equipo.Sucursal.Cliente.NombreEmpresa + " - " + x.Equipo.Sucursal.NombreSucursal + " - " + x.Equipo.Nombre,
                        id = x.IdMantencion.ToString(),
                        color = coloresEventos.GetValue(x.EstadoMantencion.Codigo - 2), 
                        tipoContrato = x.TipoContrato.TipoContrato1,
                        estadoMantencion = x.EstadoMantencion.Estado,
                        sucursal = x.Equipo.Sucursal.NombreSucursal,
                        cliente = x.Equipo.Sucursal.Cliente.NombreEmpresa,
                        ingeniero = x.Ingeniero.Usuario.Nombre + " " + x.Ingeniero.Usuario.ApellidoPaterno,
                        codigoEquipo = x.Equipo.CodigoEquipo
                    }).ToList();

                return Json(eventos, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult VigentesReasignacionIngeniero(Guid IdIngeniero, Guid IdMantencion)
        {
            VigentesReasignacionIngenieroViewModel model = new VigentesReasignacionIngenieroViewModel(IdIngeniero, IdMantencion);
            return Json(model.persistChanges());
        }

        [HttpPost]
        public ActionResult VigentesReasignacionFecha(string fechaSelected, Guid IdMantencion)
        {
            VigentesReasignacionFechaViewModel model = new VigentesReasignacionFechaViewModel(fechaSelected, IdMantencion);
            return Json(model.persistChanges());
        }

        [HttpPost]
        public ActionResult VigentesAsignacionChecklist(Guid IdCuestionario, Guid IdMantencion)
        {
            VigentesAsignacionChecklistViewModel model = new VigentesAsignacionChecklistViewModel(IdCuestionario, IdMantencion);
            return Json(model.persistChanges());
        }

        [HttpGet]
        public ActionResult Repuestos()
        {
            RepuestosViewModel model = new RepuestosViewModel(User.Identity.Name);
            return View(model);
        }

        public ActionResult PlantillaCorreoRepuestosPreview(Guid IdMailTemplate, Guid IdDistribuidor, Guid IdSolicitudKitRepuesto)
        {
            var uri = new Uri(Request.Url.AbsoluteUri);
            string CurrentURL = uri.GetLeftPart(UriPartial.Authority);
            RepuestosFormModel FORM = new RepuestosFormModel();
            FORM.userName = User.Identity.Name;
            FORM.IdSolicitudKitRepuestos = IdSolicitudKitRepuesto;
            FORM.IdDistribuidor = IdDistribuidor;
            FORM.IdMailTemplate = IdMailTemplate;
            RepuestosViewModel model = new RepuestosViewModel(FORM);
            model.setMailTemplate();
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult EnviarSolicitudRepuestos(Guid IdMailTemplate, Guid IdDistribuidor, Guid IdSolicitudKitRepuesto, string contactosPara, string contactosCC, string contactosBCC)
        {
            var uri = new Uri(Request.Url.AbsoluteUri);
            string CurrentURL = uri.GetLeftPart(UriPartial.Authority);

            RepuestosFormModel FORM = new RepuestosFormModel();
            FORM.userName = User.Identity.Name;
            FORM.IdMailTemplate = IdMailTemplate;
            FORM.IdDistribuidor = IdDistribuidor;
            FORM.IdSolicitudKitRepuestos = IdSolicitudKitRepuesto;

            FORM.contactoPara = contactosPara;
            FORM.contactosCC = contactosCC;
            FORM.contactosBCC = contactosBCC;
            FORM.urlPrefix = CurrentURL;

            RepuestosViewModel model = new RepuestosViewModel(FORM);
            return Json(model.persistChanges());
        }

        [HttpPost]
        public ActionResult InactivarMantencion(Guid IdMantencion)
        {
            InactivarMantencionViewModel model = new InactivarMantencionViewModel(IdMantencion);
            return Json(model.persistChanges());
        }

        [HttpPost]
        public ActionResult HabilitarMantencion(Guid IdMantencion)
        {
            InactivarMantencionViewModel model = new InactivarMantencionViewModel(IdMantencion);
            return Json(model.persistChangesHabilitar());
        }
    }
}
