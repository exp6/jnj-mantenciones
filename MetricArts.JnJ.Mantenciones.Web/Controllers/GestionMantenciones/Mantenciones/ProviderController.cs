﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Models.Mantenciones;

namespace MetricArts.JnJ.Mantenciones.Controllers.GestionMantenciones.Mantenciones
{
    public class ProviderController : Controller
    {
        //
        // GET: /Provider/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Provider/ConfirmarReciboMail?ID

        public ActionResult ConfirmarReciboMail(String ID)
        {
            new ProviderViewModel(ID);
            return View();
        }
    }
}
