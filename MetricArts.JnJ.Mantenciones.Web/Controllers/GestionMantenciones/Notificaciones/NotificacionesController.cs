﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Models.Notificaciones;
using MetricArts.JnJ.Mantenciones.Core;
using MetricArts.JnJ.Mantenciones.Helpers;

namespace MetricArts.JnJ.Mantenciones.Controllers.GestionMantenciones.Notificaciones
{
    public class NotificacionesController : Controller
    {
        //
        // GET: /GestionNotificaciones/

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Crear()
        {
            CrearNotificacionViewModel model = new CrearNotificacionViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Crear(CrearNotificacionFormModel FORM)
        {
            var uri = new Uri(Request.Url.AbsoluteUri);
            string CurrentURL = uri.GetLeftPart(UriPartial.Authority) + HttpRuntime.AppDomainAppVirtualPath + "/";
            new CrearNotificacionViewModel(FORM, User.Identity.Name, CurrentURL);
            return RedirectToAction("Seguimiento", "Notificaciones");
        }

        [HttpGet]
        public ActionResult Seguimiento()
        {
            SeguimientoNotificacionesViewModel model = new SeguimientoNotificacionesViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Seguimiento(SeguimientoNotificacionesFormModel FORM)
        {
            SeguimientoNotificacionesViewModel model = new SeguimientoNotificacionesViewModel(FORM);

            if (ModelState.IsValid)
            {
                model.doFilter();
            }
            return View(model);
        }

        public ActionResult SeguimientoEstado(Guid IdNotificacion)
        {
            ButtonsPreviewNotificaciones btnModel = new ButtonsPreviewNotificaciones(IdNotificacion);
            return PartialView(btnModel);
        }

        [HttpGet]
        public ActionResult SeguimientoEditar(Guid IdNotificacion)
        {
            ButtonsPreviewNotificaciones btnModel = new ButtonsPreviewNotificaciones(IdNotificacion);
            btnModel.sortContactos();
            return PartialView(btnModel);
        }

        [HttpPost]
        public ActionResult SeguimientoEditar(Guid IdNotificacion, string contactosPara, string contactosCC, string contactosBCC)
        {
            EditarNotificacionFormModel FORM = new EditarNotificacionFormModel();
            FORM.IdNotificacion = IdNotificacion;
            FORM.contactosPara = contactosPara;
            FORM.contactosCC = contactosCC;
            FORM.contactosBCC = contactosBCC;
            ButtonsPreviewNotificaciones btnModel = new ButtonsPreviewNotificaciones(FORM, IdNotificacion);
            btnModel.persistChangesEditar();
            return PartialView(btnModel);
        }

        [HttpGet]
        public ActionResult SeguimientoReenviar(Guid IdNotificacion)
        {
            ButtonsPreviewNotificaciones btnModel = new ButtonsPreviewNotificaciones(IdNotificacion);
            return PartialView(btnModel);
        }

        [HttpPost]
        public ActionResult SeguimientoReenviar(Guid IdNotificacion, string FechaReenvio)
        {
            EditarNotificacionFormModel FORM = new EditarNotificacionFormModel();
            FORM.fechaReenvio = FechaReenvio;
            ButtonsPreviewNotificaciones btnModel = new ButtonsPreviewNotificaciones(FORM, IdNotificacion);
            btnModel.persistChangesReenviar();
            return PartialView(btnModel);
        }

        [HttpPost]
        public ActionResult SeguimientoReenviarNow(SeguimientoNotificacionesFormModel FORM) 
        {
            SeguimientoNotificacionesViewModel model = new SeguimientoNotificacionesViewModel(FORM);
            model.resendNotificacion();
            return RedirectToAction("Seguimiento", "Notificaciones");
        }

        public ActionResult SeguimientoPDFPreview(Guid IdNotificacion)
        {
            ButtonsPreviewNotificaciones btnModel = new ButtonsPreviewNotificaciones(IdNotificacion);
            return PartialView(btnModel);
        }

        public ActionResult SeguimientoMailPreview(Guid IdNotificacion)
        {
            ButtonsPreviewNotificaciones btnModel = new ButtonsPreviewNotificaciones(IdNotificacion);
            return PartialView(btnModel);
        }

        [HttpPost]
        public ActionResult SeguimientoEliminar(SeguimientoNotificacionesFormModel FORM)
        {
            SeguimientoNotificacionesViewModel model = new SeguimientoNotificacionesViewModel(FORM);
            model.deleteNotificacion();
            return RedirectToAction("Seguimiento", "Notificaciones");
        }

        [HttpGet]
        public ActionResult Reenviar()
        {
            return View();
        }

        public ActionResult PlantillaCorreoPreview(Guid IdMailTemplate, Guid? IdSucursal, Guid? IdPlanificacion, Guid[] IdsEquiposSeleccionados)
        {
            var uri = new Uri(Request.Url.AbsoluteUri);

            //string CurrentURL = GetBaseUrl();

            string CurrentURL = uri.GetLeftPart(UriPartial.Authority) + HttpRuntime.AppDomainAppVirtualPath;
            Guid IdUsuario = Utils.getDBConnection().Usuarios.Where(x => x.NombreUsuario == User.Identity.Name).SingleOrDefault().IdUsuario;
            NotificacionesPreviewViewModel model = new NotificacionesPreviewViewModel(1, IdMailTemplate, IdSucursal, IdPlanificacion, IdsEquiposSeleccionados, CurrentURL, IdUsuario);
            return PartialView(model);
        }

        public ActionResult PlantillaPDFPreview(Guid IdPDFTemplate, Guid IdSucursal, Guid IdPlanificacion, Guid[] IdsEquiposSeleccionados)
        {
            var uri = new Uri(Request.Url.AbsoluteUri);

            //string CurrentURL = GetBaseUrl();
            string CurrentURL = uri.GetLeftPart(UriPartial.Authority) + HttpRuntime.AppDomainAppVirtualPath;
            Guid IdUsuario = Utils.getDBConnection().Usuarios.Where(x => x.NombreUsuario == User.Identity.Name).SingleOrDefault().IdUsuario;
            NotificacionesPreviewViewModel model = new NotificacionesPreviewViewModel(2, IdPDFTemplate, IdSucursal, IdPlanificacion, IdsEquiposSeleccionados, CurrentURL, IdUsuario);
            return PartialView(model);
        }

        public string GetBaseUrl()
        {
            var request = HttpContext.Request;
            var appUrl = HttpRuntime.AppDomainAppVirtualPath;

            if (appUrl != "/") appUrl += "/";

            var baseUrl = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, appUrl);

            return baseUrl;
        }
    }
}
