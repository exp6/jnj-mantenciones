﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Models.PortalIngeniero;
using MetricArts.JnJ.Mantenciones.Core;
using MetricArts.JnJ.Mantenciones.Helpers;

namespace MetricArts.JnJ.Mantenciones.Controllers.PortalIngeniero
{
    public class PortalIngenieroController : Controller
    {
        //
        // GET: /PortalIngeniero/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /PortalIngeniero/Bandeja

        public ActionResult Bandeja()
        {
            BandejaViewModel model = new BandejaViewModel(User.Identity.Name);
            return View(model);
        }

        [HttpGet]
        public ActionResult CalendarioJsonFeed()
        {
            try
            {
                Ingeniero ing = Utils.getDBConnection().Ingenieros.Where(x => x.Usuario.NombreUsuario == User.Identity.Name).SingleOrDefault();
                var estadosValidos = new[] { 2, 3, 4, 5 };
                //var estadosValidos = new[] { 1 };
                var coloresEventos = new[] {"#F89406", "#0088CC", "#BD362F", "#51A351" };

                IQueryable<Mantencion> mantenciones = Utils.getDBConnection().Mantencions
                    .Where(x => x.FechaConfirmacion.HasValue && x.Ingeniero == ing && estadosValidos.Contains(x.EstadoMantencion.Codigo));
                var eventos = mantenciones
                    .Select(x => new
                    {
                        start = x.FechaConfirmacion.ToString(),
                        title = x.Equipo.Sucursal.Cliente.NombreEmpresa + " - " + x.Equipo.Sucursal.NombreSucursal + " - " + x.Equipo.Nombre,
                        id = x.IdMantencion.ToString(),
                        color = coloresEventos.GetValue(x.EstadoMantencion.Codigo - 2),
                        tipoContrato = x.TipoContrato.TipoContrato1,
                        estadoMantencion = x.EstadoMantencion.Estado,
                        sucursal = x.Equipo.Sucursal.NombreSucursal,
                        cliente = x.Equipo.Sucursal.Cliente.NombreEmpresa,
                        ingeniero = x.Ingeniero.Usuario.Nombre + " " + x.Ingeniero.Usuario.ApellidoPaterno,
                        codigoEquipo = x.Equipo.CodigoEquipo
                    }).ToList();

                return Json(eventos, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //
        // POST: /PortalIngeniero/CambiarEstadoMantencion
        [HttpPost]
        public ActionResult CambiarEstadoMantencion(Guid? IdMantencion, int? nuevoEstado, string obs)
        {
            BandejaFormModel FORM = new BandejaFormModel();
            FORM.IdMantencion = IdMantencion.Value;
            FORM.nuevoEstado = nuevoEstado.Value;
            FORM.userName = User.Identity.Name;
            FORM.observaciones = obs;
            BandejaViewModel model = new BandejaViewModel(FORM);
            model.peristNuevoEstado();
            return null;
        }

        //
        // GET: /PortalIngeniero/Repuestos

        public ActionResult Repuestos()
        {
            RepuestosIngenieroViewModel model = new RepuestosIngenieroViewModel(User.Identity.Name);
            return View(model);
        }

        //
        // POST: /PortalIngeniero/EnviarSolicitudRepuesto
        [HttpPost]
        public ActionResult EnviarSolicitudRepuesto(Guid IdMantencion, Guid IdKit, Guid[] RespuestosSeleccionados, string[] RespuestosCantidad)
        {
            RepuestosIngenieroFormModel FORM = new RepuestosIngenieroFormModel();
            FORM.userName = User.Identity.Name;
            FORM.IdMantencion = IdMantencion;
            FORM.IdKitRepuesto = IdKit;
            FORM.RespuestosSeleccionados = RespuestosSeleccionados;
            FORM.RespuestosCantidad = RespuestosCantidad;
            RepuestosIngenieroViewModel model = new RepuestosIngenieroViewModel(FORM);
            model.persistSolicitud();
            return null;
        }

        public ActionResult ObtenerDetalleRepuestos(Guid? IdSolicitudKit)
        {
            try
            {
                var repuestos = Utils.getDBConnection().SolicitudRepuestos
                    .Where(x => x.IdSolicitudKitRepuesto == IdSolicitudKit)
                        .Select(x => new
                        {
                            Codigo = x.Repuesto.Codigo,
                            Descripcion = x.Repuesto.Descripcion,
                            Categoria = x.Repuesto.Categoria,
                            Cantidad = x.CantidadSolicitada
                        }).ToList();

                return Json(repuestos, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ResponderChecklist(Guid IdMantencion)
        {
            ResponderChecklistViewModel model = new ResponderChecklistViewModel(IdMantencion);
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult ResponderChecklist(Guid CuestionarioMantencionId, Guid[] PreguntaIds, string[] Respuestas, string[] TipoPreguntas, string[] DatosClientes, string[] DatosClientesAttr)
        {
            ResponderChecklistFormModel FORM = new ResponderChecklistFormModel();
            FORM.userName = User.Identity.Name;
            FORM.IdMantencion = Guid.Empty;

            FORM.CuestionarioMantencionId = CuestionarioMantencionId;
            FORM.PreguntaIds = PreguntaIds;
            FORM.Respuestas = Respuestas;
            FORM.TipoPreguntas = TipoPreguntas;
            FORM.DatosClientes = DatosClientes;
            FORM.DatosClientesAttr = DatosClientesAttr;

            ResponderChecklistViewModel model = new ResponderChecklistViewModel(FORM);
            model.persistRespuestas();

            return null;
        }

        [HttpGet]
        public ActionResult DescargaChecklist(Guid IdMantencion)
        {
            DescargaChecklistViewModel model = new DescargaChecklistViewModel(IdMantencion);
            string html = model.getHtmlChecklist();
            string fileName = model.getFileName();
            byte[] buffer = Utils.createPDF(html);
            return File(buffer, "application/pdf", fileName + ".pdf");
        }
    }
}
