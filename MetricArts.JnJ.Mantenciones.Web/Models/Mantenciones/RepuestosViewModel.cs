﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Core;
using MetricArts.JnJ.Mantenciones.Helpers;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenciones
{
    public class RepuestosViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public RepuestosFormModel FORM { get; set; }
        public IEnumerable<SolicitudKitRepuesto> EnEsperaAtencion { get; set; }
        public IEnumerable<SolicitudKitRepuesto> Atendidas { get; set; }
        public IEnumerable<SolicitudKitRepuesto> Aceptadas { get; set; }

        public int CantEnEsperaAtencion { get; set; }
        public int CantAtendidas { get; set; }
        public int CantAceptadas { get; set; }

        public IEnumerable<SelectListItem> Distribuidores { get; set; }
        public IEnumerable<SelectListItem> FormatosCorreo { get; set; }

        public string templateHTML { get; set; }
        public Usuario usuarioSession { get; set; }

        public RepuestosViewModel(string userName)
        {
            usuarioSession = db.Usuarios.Where(x => x.NombreUsuario == userName).SingleOrDefault();
            EnEsperaAtencion = db.SolicitudKitRepuestos.OrderByDescending(x => x.FechaSolicitud).Where(x => x.EstadoSolicitud.Codigo == 1);
            Atendidas = db.SolicitudKitRepuestos.OrderByDescending(x => x.FechaSolicitud).Where(x => x.EstadoSolicitud.Codigo == 2);
            Aceptadas = db.SolicitudKitRepuestos.OrderByDescending(x => x.FechaSolicitud).Where(x => x.EstadoSolicitud.Codigo == 3);

            CantEnEsperaAtencion = EnEsperaAtencion.Count();
            CantAtendidas = Atendidas.Count();
            CantAceptadas = Aceptadas.Count();

            Distribuidores = db.DistribuidorRepuestos.OrderBy(x => x.Nombre)
                .Select(x => new SelectListItem()
            {
                Text = x.Nombre,
                Value = x.IdDistribuidorRepuestos.ToString()
            });

            FormatosCorreo = db.MailTemplates.Where(x => x.TipoNotificacion.Codigo == 4)
                .Select(x => new SelectListItem()
            {
                Text = x.Nombre,
                Value = x.IdMailTemplate.ToString()
            });
        }

        public RepuestosViewModel(RepuestosFormModel form)
            : this(form.userName)
        {
            FORM = form;
        }

        public void setMailTemplate() 
        {
            MailTemplate mailTemplate = db.MailTemplates.Where(x => x.IdMailTemplate == FORM.IdMailTemplate).SingleOrDefault();
            templateHTML = Utils.getHTMLParsed(mailTemplate.CuerpoHTML, Guid.Empty, Guid.Empty, null, null, db, 0, Guid.Empty, FORM.IdSolicitudKitRepuestos, FORM.IdDistribuidor, usuarioSession.IdUsuario, 0);
        }

        public bool persistChanges()  
        {
            try
            {
                //ACTUALIZA ESTADO DE LA SOLICITUD
                SolicitudKitRepuesto solcitudKit = db.SolicitudKitRepuestos.Where(x => x.IdSolicitudKitRepuesto == FORM.IdSolicitudKitRepuestos).SingleOrDefault();
                EstadoSolicitud enviada = db.EstadoSolicituds.Where(x => x.Codigo == 2).SingleOrDefault();

                solcitudKit.EstadoSolicitud = enviada;

                //INSERTA UNA NUEVA NOTIFICACION DE LA SOLICITUD A MENSAJERIA
                Guid msgStageId = Guid.NewGuid();
                MailTemplate mailTemplate = db.MailTemplates.Where(x => x.IdMailTemplate == FORM.IdMailTemplate).SingleOrDefault();
                string cuerpoHTMLMail = Utils.getHTMLParsed(mailTemplate.CuerpoHTML, Guid.Empty, Guid.Empty, null, FORM.urlPrefix, db, 1, msgStageId, FORM.IdSolicitudKitRepuestos, FORM.IdDistribuidor, usuarioSession.IdUsuario, 0);

                PDFTemplate pdfTemplate = null;
                string cuerpoHTMLPDF = null;

                EstadoMensaje estadoMsg = db.EstadoMensajes.Where(x => x.Codigo == 1).SingleOrDefault();

                MensajeStage mensaje = new MensajeStage
                {
                    IdMensajeStage = msgStageId,
                    TipoNotificacion = mailTemplate.TipoNotificacion,
                    Sucursal = solcitudKit.Mantencion.Equipo.Sucursal,
                    EstadoMensaje = estadoMsg,
                    MailTemplate = mailTemplate,
                    PDFTemplate = pdfTemplate,
                    Planificacion = solcitudKit.Mantencion.Planificacion,
                    CuerpoHTMLMail = cuerpoHTMLMail,
                    CuerpoHTMLPDF = cuerpoHTMLPDF,
                    ContactoPara = FORM.contactoPara,
                    ContactoCC = FORM.contactosCC,
                    ContactoBCC = FORM.contactosBCC,
                    FechaIngreso = DateTime.Now,
                    FechaProgrEnvio = DateTime.Now,
                    CreadoPor = usuarioSession.NombreUsuario
                };

                db.MensajeStages.InsertOnSubmit(mensaje);

                NotificacionEquipo ne = new NotificacionEquipo
                {
                    IdNotificacionEquipo = Guid.NewGuid(),
                    MensajeStage = mensaje,
                    Equipo = solcitudKit.Mantencion.Equipo,
                    FechaIngreso = DateTime.Now,
                    CreadoPor = FORM.userName
                };

                db.NotificacionEquipos.InsertOnSubmit(ne);

                db.SubmitChanges();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}