﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Helpers;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenciones
{
    public class VigentesAsignacionChecklistViewModel
    {
        public Guid IdCuestionario { get; set; }
        public Guid IdMantencion { get; set; }

        public VigentesAsignacionChecklistViewModel(Guid idCuestionario, Guid idMantencion) 
        {
            IdCuestionario = idCuestionario;
            IdMantencion = idMantencion;
        }

        public bool persistChanges()
        {
            try
            {
                MantencionesDataContext dbTemp = Utils.getDBConnection();
                Mantencion m = dbTemp.Mantencions.Where(x => x.IdMantencion == IdMantencion).SingleOrDefault();
                Cuestionario c = dbTemp.Cuestionarios.Where(x => x.IdCuestionario == IdCuestionario).SingleOrDefault();
                
                bool ExisteEnBD = dbTemp.CuestionarioMantencions.Where(x => x.Mantencion == m && x.Cuestionario.TipoCuestionario.Codigo == 1).Any();
                CuestionarioMantencion cm;

                if (ExisteEnBD)
                {
                    //ACTUALIZO EXISTENTE
                    cm = dbTemp.CuestionarioMantencions.Where(x => x.Mantencion == m && x.Cuestionario.TipoCuestionario.Codigo == 1).FirstOrDefault();
                    cm.Cuestionario = c;
                }
                else 
                {
                    //INSERTO NUEVO
                    cm = new CuestionarioMantencion {
                        IdCuestionarioMantencion = Guid.NewGuid(),
                        Cuestionario = c,
                        Mantencion = m,
                        Respondida = false
                    };
                }

                dbTemp.SubmitChanges();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}