﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenciones
{
    public class CrearMantencionFormModel
    {
        [DisplayName("Nombre Planificación")]
        public string Nombre { get; set; }

        [DisplayName("Tipo Contrato")]
        public Guid IdTipoContrato { get; set; }

        [StringLength(1023)]
        [DisplayName("Descripción")]
        public string Descripcion { get; set; }
    }
}