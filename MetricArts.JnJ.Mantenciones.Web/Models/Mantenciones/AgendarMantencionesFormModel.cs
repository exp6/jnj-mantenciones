﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenciones
{
    public class AgendarMantencionesFormModel
    {
        public Guid IdPlanificacionIn { get; set; }
        public Guid IdMensajeStageIn { get; set; }
        public Guid IdEquipoIn { get; set; }
        public String[] fechasSelectedIn { get; set; }
    }
}