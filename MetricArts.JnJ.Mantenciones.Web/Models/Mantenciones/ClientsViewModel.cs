﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenciones
{
    public class ClientsViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public ClientsFormModel FORM;
        public Planificacion plan { get; set; }
        public Sucursal sucursal { get; set; }
        public IEnumerable<Equipo> equipos { get; set; }

        public ClientsViewModel(Guid ID) 
        {
            plan = db.Planificacions.Where(x => x.IdPlanificacion == ID).SingleOrDefault();
            IEnumerable<Mantencion> mantenciones = db.Mantencions.Where(x => x.IdPlanificacion == ID);
            equipos = db.Equipos.OrderBy(x => x.Nombre).Where(x => x.Mantencions.Intersect(mantenciones).Any());
            sucursal = equipos.FirstOrDefault().Sucursal;
        }

        //public ClientsViewModel(ClientsFormModel form)
        //    : this()
        //{
        //    FORM = form;
        //}
    }
}