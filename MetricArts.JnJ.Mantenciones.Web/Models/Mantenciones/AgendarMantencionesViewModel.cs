﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;
using System.Globalization;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenciones
{
    public class AgendarMantencionesViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public AgendarMantencionesFormModel FORM;
        public Planificacion plan { get; set; }
        public Sucursal sucursal { get; set; }
        public IEnumerable<Equipo> equipos { get; set; }
        public Dictionary<Guid, bool> completedRegisterStatus { get; set; }
        public MensajeStage msg { get; set; }

        public AgendarMantencionesViewModel(String ID) 
        {
            Guid[] ids = ID.Split('_').Select(s => Guid.Parse(s)).ToArray();

            msg = db.MensajeStages.Where(x => x.IdMensajeStage == ids[1]).SingleOrDefault();
            plan = db.Planificacions.Where(x => x.IdPlanificacion == ids[0]).SingleOrDefault();
            IEnumerable<Mantencion> mantenciones = db.Mantencions.Where(x => x.IdPlanificacion == ids[0] && x.Habilitado);
            equipos = db.Equipos.OrderBy(x => x.Nombre).Where(x => x.Mantencions.Intersect(mantenciones).Any());
            sucursal = equipos.FirstOrDefault().Sucursal;

            completedRegisterStatus = new Dictionary<Guid, bool>();
            foreach(var e in equipos) 
            {
                completedRegisterStatus.Add(e.IdEquipo, getCompleteRegisterStatus(e.IdEquipo, plan.IdPlanificacion));
            }

            if(msg.EstadoMensaje.Codigo != 4) {
                EstadoMensaje received = db.EstadoMensajes.Where(x => x.Codigo == 4).SingleOrDefault();
                msg.EstadoMensaje = received;
                db.SubmitChanges();
            }
        }

        public AgendarMantencionesViewModel(AgendarMantencionesFormModel form)
            : this(form.IdPlanificacionIn.ToString()+"_"+form.IdMensajeStageIn.ToString())
        {
            FORM = form;
        }

        private bool getCompleteRegisterStatus(Guid idEquipo, Guid idPlanificacion) {

            IEnumerable<Mantencion> mantencionesTemp = db.Mantencions.OrderBy(x => x.CorrelativoPlan)
                .Where(x => x.IdEquipo == idEquipo
                && x.IdPlanificacion == idPlanificacion && x.Habilitado);

            int counter = 0;

            foreach(var m in mantencionesTemp)
            {
                if(m.FechaConfirmacion == null) { break; }

                ++counter;
            }

            return (counter == mantencionesTemp.Count());
        }

        public void persist() {
            List<Mantencion> mantencionesTemp = db.Mantencions.OrderBy(x => x.CorrelativoPlan)
                .Where(x => x.IdEquipo == FORM.IdEquipoIn
                && x.IdPlanificacion == FORM.IdPlanificacionIn && x.Habilitado).ToList();

            Mantencion mantencion;
            EstadoMantencion EnEsperaVisita = db.EstadoMantencions.Where(x => x.Codigo == 2).SingleOrDefault();
            CuestionarioMantencion cm;

            for (int i = 0; i < mantencionesTemp.Count(); i++)
            {
                mantencion = mantencionesTemp[i];
                mantencion.FechaConfirmacion = DateTime.ParseExact(FORM.fechasSelectedIn[i], "dd-MM-yyyy", CultureInfo.InvariantCulture);
                mantencion.EstadoMantencion = EnEsperaVisita;

                if (mantencion.Equipo.Cuestionario != null)
                {
                    cm = new CuestionarioMantencion
                    {
                        IdCuestionarioMantencion = Guid.NewGuid(),
                        Cuestionario = mantencion.Equipo.Cuestionario,
                        Mantencion = mantencion,
                        Respondida = false
                    };

                    db.CuestionarioMantencions.InsertOnSubmit(cm);
                }
            }

            db.SubmitChanges();
        }
    }
}