﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Models.Planificaciones;
using MetricArts.JnJ.Mantenciones.Core;
using MetricArts.JnJ.Mantenciones.Helpers;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenciones
{
    public class CrearMantencionViewModel
    {
        public CrearMantencionFormModel FORM { get; set; }
        public SeleccionEstructuraNegocioFormModel formSeleccionNegocio { get; set; }
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public IEnumerable<SelectListItem> TipoContrato { get; set; }
        public IEnumerable<Franquicia> Franquicias { get; set; }
        public IEnumerable<SelectListItem> FranquiciasSelect { get; set; }
        public Pais PaisDeUsuario { get; set; }

        public CrearMantencionViewModel() 
        {
            //TODO: El país debe poder obtenerse del usuario
            PaisDeUsuario = db.Pais.Where(x => x.Nombre == "Chile").SingleOrDefault();

            IEnumerable<Sucursal> sucursalesLocales = db.Sucursals.Where(x => x.Comuna.Region.Pais.IdPais == PaisDeUsuario.IdPais);
            Franquicias = Utils.getFranquiciasByPais(sucursalesLocales, db);

            FranquiciasSelect = Franquicias.Select(x => new SelectListItem()
            {
                Text = x.NombreFranquicia,
                Value = x.IdFranquicia.ToString()
            });

            TipoContrato = db.TipoContratos.OrderBy(x => x.Codigo)
                .Select(x => new SelectListItem() {
                    Text = x.TipoContrato1,
                    Value = x.IdTipoContrato.ToString()
                });
        }

        public CrearMantencionViewModel(CrearMantencionFormModel form, SeleccionEstructuraNegocioFormModel formSeleccionNegocioIn) : this()
        {
            FORM = form;
            formSeleccionNegocio = formSeleccionNegocioIn;
        }
    }
}