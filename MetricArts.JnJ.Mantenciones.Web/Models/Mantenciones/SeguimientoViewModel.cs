﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenciones
{
    public class SeguimientoViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public SeguimientoFormModel FORM { get; set; }
        public IEnumerable<SelectListItem> Clientes { get; set; }
        public IEnumerable<SelectListItem> Sucursales { get; set; }
        public IEnumerable<SelectListItem> AniosPlanificacion { get; set; }
        public IEnumerable<SelectListItem> Ingenieros { get; set; }
        public IEnumerable<Mantencion> Mantenciones { get; set; }
        public IEnumerable<Mantencion> MantencionesTotal { get; set; }

        public int anioPlanificacion { get; set; }
        public string fechaSeleccion { get; set; }

        public int CantEnCurso { get; set; }
        public int CantEnEsperaVisita { get; set; }
        public int CantEnEsperaMomento { get; set; }
        public int CantFinalizada { get; set; }
        public int CantTotal { get; set; }

        public SeguimientoViewModel(int year, string fecha) 
        {
            Clientes = db.Clientes.OrderBy(x => x.NombreEmpresa).Select(x => new SelectListItem()
            {
                Text = x.Franquicia.NombreFranquicia + " - " + x.NombreEmpresa,
                Value = x.IdCliente.ToString()
            });

            Ingenieros = db.Ingenieros.Select(x => new SelectListItem()
            {
                Text = x.Usuario.Nombre + " " + x.Usuario.ApellidoPaterno,
                Value = x.IdIngeniero.ToString()
            });

            AniosPlanificacion = db.Planificacions.GroupBy(x => x.Ano).Select(x => new SelectListItem()
            {
                Text = x.First().Ano.ToString(),
                Value = x.First().Ano.ToString()
            });

            anioPlanificacion = year == 0 ? DateTime.Now.Year : year;
            fechaSeleccion = fecha == null ? DateTime.Now.ToString("dd-MM-yyyy") : fecha;

            var estadosValidos = new[] { 3, 4, 5 };

            Mantenciones = db.Mantencions.Where(x => x.FechaConfirmacion.HasValue && x.FechaConfirmacion == DateTime.Now && estadosValidos.Contains(x.EstadoMantencion.Codigo));

            MantencionesTotal = db.Mantencions.Where(x => x.FechaConfirmacion.HasValue && estadosValidos.Contains(x.EstadoMantencion.Codigo));

            CantEnCurso = MantencionesTotal.Where(x => x.EstadoMantencion.Codigo == 3).Count();
            CantEnEsperaMomento = MantencionesTotal.Where(x => x.EstadoMantencion.Codigo == 4).Count();
            CantFinalizada = MantencionesTotal.Where(x => x.EstadoMantencion.Codigo == 5).Count();
            CantTotal = MantencionesTotal.Count();
        }

        public SeguimientoViewModel(SeguimientoFormModel form)
            : this(form.anioPlanificacion ?? DateTime.Now.Year, form.fechaSelected)
        {
            FORM = form;
        }

        public void peristNuevoEstado()
        {
            Mantencion m = db.Mantencions.Where(x => x.IdMantencion == FORM.IdMantencion).SingleOrDefault();
            Usuario u = db.Usuarios.Where(x => x.NombreUsuario == FORM.userName).SingleOrDefault();
            EstadoMantencion nuevoEstado = db.EstadoMantencions.Where(x => x.Codigo == FORM.nuevoEstado).SingleOrDefault();
            
            m.EstadoMantencion = nuevoEstado;

            EstadoHistoricoMantencion estadoHistorico = new EstadoHistoricoMantencion
            {
                IdEstadoHistoricoMantencion = Guid.NewGuid(),
                Mantencion = m,
                EstadoMantencion = nuevoEstado,
                Usuario = u,
                FechaActualizacion = DateTime.Now,
                Observaciones = FORM.observaciones
            };
            db.EstadoHistoricoMantencions.InsertOnSubmit(estadoHistorico);

            db.SubmitChanges();
        }
    }
}