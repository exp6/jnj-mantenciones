﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;
using MetricArts.JnJ.Mantenciones.Helpers;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenciones
{
    public class InactivarMantencionViewModel
    {
        public Guid IdMantencion { get; set; }

        public InactivarMantencionViewModel(Guid idMantencion) 
        {
            IdMantencion = idMantencion;
        }

        public bool persistChanges()
        {
            try
            {
                MantencionesDataContext dbTemp = Utils.getDBConnection();
                Mantencion m = dbTemp.Mantencions.Single(x => x.IdMantencion == IdMantencion);
                m.Habilitado = false;
                dbTemp.SubmitChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool persistChangesHabilitar()
        {
            try
            {
                MantencionesDataContext dbTemp = Utils.getDBConnection();
                Mantencion m = dbTemp.Mantencions.Single(x => x.IdMantencion == IdMantencion);
                m.Habilitado = true;
                dbTemp.SubmitChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}