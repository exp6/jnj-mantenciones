﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Helpers;
using MetricArts.JnJ.Mantenciones.Core;
using System.Globalization;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenciones
{
    public class VigentesReasignacionFechaViewModel
    {
        public string FechaSelected { get; set; }
        public Guid IdMantencion { get; set; }

        public VigentesReasignacionFechaViewModel(string fechaSelected, Guid idMantencion) 
        {
            FechaSelected = fechaSelected;
            IdMantencion = idMantencion;
        }

        public bool persistChanges()
        {
            try
            {
                MantencionesDataContext dbTemp = Utils.getDBConnection();
                Mantencion m = dbTemp.Mantencions.Single(x => x.IdMantencion == IdMantencion);
                m.FechaConfirmacion = DateTime.ParseExact(FechaSelected, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                dbTemp.SubmitChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool persistChangesPropuestas()
        {
            try
            {
                MantencionesDataContext dbTemp = Utils.getDBConnection();
                Mantencion m = dbTemp.Mantencions.Single(x => x.IdMantencion == IdMantencion);
                DateTime newDate = DateTime.ParseExact(FechaSelected, "MM-yyyy", CultureInfo.InvariantCulture);
                m.MesPeriodo = newDate.Month;
                dbTemp.SubmitChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}