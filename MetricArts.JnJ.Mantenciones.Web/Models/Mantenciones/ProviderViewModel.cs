﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenciones
{
    public class ProviderViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public MensajeStage msg { get; set; }
        public SolicitudKitRepuesto solicitud { get; set; }

        public ProviderViewModel(String ID) 
        {
            Guid[] ids = ID.Split('_').Select(s => Guid.Parse(s)).ToArray();

            solicitud = db.SolicitudKitRepuestos.Where(x => x.IdSolicitudKitRepuesto == ids[0]).SingleOrDefault();
            msg = db.MensajeStages.Where(x => x.IdMensajeStage == ids[1]).SingleOrDefault();

            EstadoMensaje received = db.EstadoMensajes.Where(x => x.Codigo == 4).SingleOrDefault();
            msg.EstadoMensaje = received;
            EstadoSolicitud aceptada = db.EstadoSolicituds.Where(x => x.Codigo == 3).SingleOrDefault();
            solicitud.EstadoSolicitud = aceptada;

            db.SubmitChanges();
        }
    }
}