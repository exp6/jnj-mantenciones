﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenciones
{
    public class RepuestosFormModel
    {
        [DisplayName("Distribuidores")]
        public Guid? IdDistribuidor { get; set; }

        public Guid? IdFormatoCorreo { get; set; }

        public Guid IdMailTemplate { get; set; }
        public Guid IdSolicitudKitRepuestos { get; set; }

        public String contactoPara { get; set; }

        public String contactosCC { get; set; }

        public String contactosBCC { get; set; }

        public string userName { get; set; }

        public string urlPrefix { get; set; }
    }
}