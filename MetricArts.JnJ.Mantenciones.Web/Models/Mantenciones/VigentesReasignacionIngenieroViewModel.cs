﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Helpers;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenciones
{
    public class VigentesReasignacionIngenieroViewModel
    {
        public Guid IdIngeniero { get; set; }
        public Guid IdMantencion { get; set; }

        public VigentesReasignacionIngenieroViewModel(Guid idIngeniero, Guid idMantencion) 
        {
            IdIngeniero = idIngeniero;
            IdMantencion = idMantencion;
        }

        public bool persistChanges()  
        {
            try
            {
                MantencionesDataContext dbTemp = Utils.getDBConnection();
                Mantencion m = dbTemp.Mantencions.Where(x => x.IdMantencion == IdMantencion).SingleOrDefault();
                Ingeniero i = dbTemp.Ingenieros.Where(x => x.IdIngeniero == IdIngeniero).SingleOrDefault();
                m.Ingeniero = i;
                dbTemp.SubmitChanges();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}