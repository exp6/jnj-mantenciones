﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Models.Planificaciones;
using MetricArts.JnJ.Mantenciones.Core;
using MetricArts.JnJ.Mantenciones.Helpers;
using System.Globalization;

namespace MetricArts.JnJ.Mantenciones.Models.Notificaciones
{
    public class CrearNotificacionViewModel
    {
        public CrearNotificacionFormModel FORM { get; set; }
        public SeleccionEstructuraNegocioFormModel formSeleccionNegocio { get; set; }
        private MantencionesDataContext db = new MantencionesDataContext()
            .WithConnectionStringFromConfiguration();

        public List<TipoNotificacion> TiposNotificacion { get; set; }
        public IEnumerable<Franquicia> Franquicias { get; set; }
        public IEnumerable<SelectListItem> FranquiciasSelect { get; set; }
        public Pais PaisDeUsuario { get; set; }
        public string urlLink { get; set; }

        public CrearNotificacionViewModel() {
            TiposNotificacion = db.TipoNotificacions.OrderBy(x => x.Nombre).ToList();

            //TODO: El país debe poder obtenerse del usuario
            PaisDeUsuario = db.Pais.Where(x => x.Nombre == "Chile").SingleOrDefault();

            IEnumerable<Sucursal> sucursalesLocales = db.Sucursals.Where(x => x.Comuna.Region.Pais.IdPais == PaisDeUsuario.IdPais);
            Franquicias = Utils.getFranquiciasByPais(sucursalesLocales, db);

            FranquiciasSelect = Franquicias.Select(x => new SelectListItem()
            {
                Text = x.NombreFranquicia,
                Value = x.IdFranquicia.ToString()
            });

        }

        public CrearNotificacionViewModel(CrearNotificacionFormModel form, string userName, string url)
            : this()
        {
            FORM = form;
            form.userName = userName;
            urlLink = url;
            this.persist();
        }

        private void persist() {
            //INSERTAR NOTIFICACION
            TipoNotificacion tipoNotificacion = db.TipoNotificacions.Where(x => x.IdTipoNotificacion == FORM.IdTipoNotificacion).SingleOrDefault();
            Sucursal sucursal = db.Sucursals.Where(x => x.IdSucursal == FORM.IdSucursal1).SingleOrDefault();
            EstadoMensaje estadoMsg = db.EstadoMensajes.Where(x => x.Codigo == 1).SingleOrDefault();

            Guid[] equiposSeleccionados = FORM.equiposSeleccionadosStr.Split(';').Select(s => Guid.Parse(s)).ToArray();

            Planificacion plan = db.Planificacions.Where(x => x.IdPlanificacion == FORM.IdPlanificacion1).SingleOrDefault();

            Guid msgStageId = Guid.NewGuid();

            Guid IdUsuario = db.Usuarios.Where(x => x.NombreUsuario == FORM.userName).SingleOrDefault().IdUsuario;

            MailTemplate mailTemplate = db.MailTemplates.Where(x => x.IdMailTemplate == FORM.IdFormatoCorreo1).SingleOrDefault();
            string cuerpoHTMLMail = Utils.getHTMLParsed(mailTemplate.CuerpoHTML, sucursal.IdSucursal, plan.IdPlanificacion, equiposSeleccionados, urlLink, db, 1, msgStageId, Guid.Empty, Guid.Empty, IdUsuario, plan.Ano);

            PDFTemplate pdfTemplate = null;
            string cuerpoHTMLPDF = null;

            DateTime fechaEnvioFormat = DateTime.ParseExact(FORM.fechaEnvio, "dd-MM-yyyy", CultureInfo.InvariantCulture);

            if(!FORM.pdfAttached){
                pdfTemplate = db.PDFTemplates.Where(x => x.IdPDFTemplate == FORM.IdFormatoPDF1).SingleOrDefault();
                cuerpoHTMLPDF = Utils.getHTMLParsed(pdfTemplate.CuerpoHTML, sucursal.IdSucursal, plan.IdPlanificacion, equiposSeleccionados, urlLink, db, 1, msgStageId, Guid.Empty, Guid.Empty, IdUsuario, plan.Ano);
            }

            MensajeStage mensaje = new MensajeStage {
                IdMensajeStage = msgStageId,
                TipoNotificacion = tipoNotificacion,
                Sucursal = sucursal,
                EstadoMensaje = estadoMsg,
                MailTemplate = mailTemplate,
                PDFTemplate = pdfTemplate,
                Planificacion = plan,
                CuerpoHTMLMail = cuerpoHTMLMail,
                CuerpoHTMLPDF = cuerpoHTMLPDF,
                ContactoPara = FORM.contactoPara,
                ContactoCC = FORM.contactosCC,
                ContactoBCC = FORM.contactosBCC,
                FechaIngreso = DateTime.Now,
                FechaProgrEnvio = fechaEnvioFormat,
                CreadoPor = FORM.userName
            };

            db.MensajeStages.InsertOnSubmit(mensaje);

            foreach (var item in equiposSeleccionados)
            {
                Equipo equipo = db.Equipos.Where(x => x.IdEquipo == item).SingleOrDefault();

                NotificacionEquipo ne = new NotificacionEquipo { 
                    IdNotificacionEquipo = Guid.NewGuid(),
                    MensajeStage = mensaje,
                    Equipo = equipo,
                    FechaIngreso = DateTime.Now,
                    CreadoPor = FORM.userName
                };

                db.NotificacionEquipos.InsertOnSubmit(ne);
            }

            db.SubmitChanges();
        }
    }
}