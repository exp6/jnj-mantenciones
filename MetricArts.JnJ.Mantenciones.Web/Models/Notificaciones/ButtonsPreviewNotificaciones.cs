﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;
using System.Globalization;

namespace MetricArts.JnJ.Mantenciones.Models.Notificaciones
{
    public class ButtonsPreviewNotificaciones
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public MensajeStage notificacion { get; set; }

        public EditarNotificacionFormModel FORM { get; set; }

        public String[] contactosPara { get; set; }
        public String[] contactosCC { get; set; }
        public String[] contactosBCC { get; set; }
        public String contactoSucursal { get; set; }

        public ButtonsPreviewNotificaciones(Guid IdNotificacion)
        {
            notificacion = db.MensajeStages.Where(x => x.IdMensajeStage == IdNotificacion).SingleOrDefault();
            contactosPara = new String[0];
            contactosCC = new String[0];
            contactosBCC = new String[0];
            contactoSucursal = "";
        }

        public ButtonsPreviewNotificaciones(EditarNotificacionFormModel form, Guid IdNotificacion)
            : this(IdNotificacion)
        {
            FORM = form;
        }

        public void sortContactos() 
        {
            String cPara = notificacion.ContactoPara;
            String cCC = notificacion.ContactoCC;
            String cBCC = notificacion.ContactoBCC;

            contactosPara = cPara.Split(';').ToArray();
            contactoSucursal = contactosPara[0];
            contactosCC = cCC != null ? cCC.Split(';').ToArray() : new String[0];
            contactosBCC = cBCC != null ? cBCC.Split(';').ToArray() : new String[0];
        }

        public void persistChangesEditar() {
            notificacion.ContactoPara = FORM.contactosPara;
            notificacion.ContactoCC = ((FORM.contactosCC == null || FORM.contactosCC == "") ? null : FORM.contactosCC);
            notificacion.ContactoBCC = ((FORM.contactosBCC == null || FORM.contactosBCC == "") ? null : FORM.contactosBCC);
            db.SubmitChanges();
        }

        public void persistChangesReenviar()
        {
            DateTime fechaReenvioFormat = DateTime.ParseExact(FORM.fechaReenvio, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            notificacion.FechaProgrEnvio = fechaReenvioFormat;
            EstadoMensaje queued = db.EstadoMensajes.Where(x => x.Codigo == 1).SingleOrDefault();
            notificacion.EstadoMensaje = queued;
            db.SubmitChanges();
        }
    }
}