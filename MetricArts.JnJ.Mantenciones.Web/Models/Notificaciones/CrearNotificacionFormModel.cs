﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace MetricArts.JnJ.Mantenciones.Models.Notificaciones
{
    public class CrearNotificacionFormModel
    {
        public Guid? IdTipoNotificacion { get; set; }

        [Display(Name = "Unidad de Negocio")]
        public Guid? IdFranquicia { get; set; }

        [DisplayName("Cliente")]
        public Guid? IdCliente1 { get; set; }

        [DisplayName("Sucursal")]
        public Guid? IdSucursal1 { get; set; }

        [DisplayName("Planificación")]
        public Guid? IdPlanificacion1 { get; set; }

        [DisplayName("Plantilla de PDF")]
        public Guid? IdFormatoPDF1 { get; set; }

        [DisplayName("Plantilla de Correo")]
        public Guid? IdFormatoCorreo1 { get; set; }

        public String[] idEquiposSeleccionados { get; set; }

        public String contactoPara { get; set; }

        public String contactosCC { get; set; }

        public String contactosBCC { get; set; }

        public String equiposSeleccionadosStr { get; set; }

        public string userName { get; set; }

        public bool pdfAttached { get; set; }

        public String fechaEnvio { get; set; }
    }
}