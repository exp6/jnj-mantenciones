﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace MetricArts.JnJ.Mantenciones.Models.Notificaciones
{
    public class SeguimientoNotificacionesFormModel
    {
        [Display(Name = "Unidad de Negocio")]
        public Guid? IdFranquicia { get; set; }

        [DisplayName("Cliente")]
        public Guid? IdCliente1 { get; set; }

        [DisplayName("Sucursal")]
        public Guid? IdSucursal1 { get; set; }

        public int? page { get; set; }

        public Guid IdNotificacion { get; set; }
    }
}