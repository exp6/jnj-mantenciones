﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MetricArts.JnJ.Mantenciones.Models.Notificaciones
{
    public class EditarNotificacionFormModel
    {
        public String contactosPara { get; set; }

        public String contactosCC { get; set; }

        public String contactosBCC { get; set; }

        public String equiposSeleccionadosStr { get; set; }

        public string userName { get; set; }

        public Guid IdNotificacion { get; set; }

        public string fechaReenvio { get; set; }
    }
}