﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;
using MetricArts.JnJ.Mantenciones.Helpers;

namespace MetricArts.JnJ.Mantenciones.Models.Notificaciones
{
    public class NotificacionesPreviewViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public string templateHTML { get; set; }

        public NotificacionesPreviewViewModel(int template, Guid IdTemplate, Guid? IdSucursal, Guid? IdPlanificacion, Guid[] IdsEquiposSeleccionados, string url, Guid IdUsuario)
        {
            switch (template)
            {
                case 1: //Mail
                    MailTemplate templateMail = db.MailTemplates.Where(x => x.IdMailTemplate == IdTemplate).FirstOrDefault();
                    //templateHTML = templateMail.CuerpoHTML;
                    templateHTML = Utils.getHTMLParsed(templateMail.CuerpoHTML, IdSucursal, IdPlanificacion, IdsEquiposSeleccionados, url, db, 0, Guid.Empty, Guid.Empty, Guid.Empty, IdUsuario, 0);
                    break;
                case 2: //PDF
                    PDFTemplate templatePDF = db.PDFTemplates.Where(x => x.IdPDFTemplate == IdTemplate).FirstOrDefault();
                    //templateHTML = templatePDF.CuerpoHTML;
                    templateHTML = Utils.getHTMLParsed(templatePDF.CuerpoHTML, IdSucursal, IdPlanificacion, IdsEquiposSeleccionados, url, db, 0, Guid.Empty, Guid.Empty, Guid.Empty, IdUsuario, 0);
                    break;
            }
        }

        
    }
}