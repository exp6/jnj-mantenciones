﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Core;
using MetricArts.JnJ.Mantenciones.Helpers;

namespace MetricArts.JnJ.Mantenciones.Models.Notificaciones
{
    public class SeguimientoNotificacionesViewModel
    {
        public SeguimientoNotificacionesFormModel FORM { get; set; }
        private MantencionesDataContext db = new MantencionesDataContext()
            .WithConnectionStringFromConfiguration();
        public IEnumerable<Franquicia> Franquicias { get; set; }
        public IEnumerable<SelectListItem> FranquiciasSelect { get; set; }
        public Pais PaisDeUsuario { get; set; }

        public IEnumerable<MensajeStage> notificaciones { get; set; }

        public Guid IdNotificacion { get; set; }

        public SeguimientoNotificacionesViewModel() {
            //TODO: El país debe poder obtenerse del usuario
            PaisDeUsuario = db.Pais.Where(x => x.Nombre == "Chile").SingleOrDefault();

            IEnumerable<Sucursal> sucursalesLocales = db.Sucursals.Where(x => x.Comuna.Region.Pais.IdPais == PaisDeUsuario.IdPais);
            Franquicias = Utils.getFranquiciasByPais(sucursalesLocales, db);

            FranquiciasSelect = Franquicias.Select(x => new SelectListItem()
            {
                Text = x.NombreFranquicia,
                Value = x.IdFranquicia.ToString()
            });

            if (db.MensajeStages.Count() > 0)
            {
                notificaciones = db.MensajeStages.OrderByDescending(x => x.FechaIngreso);
            }
        }

        public SeguimientoNotificacionesViewModel(SeguimientoNotificacionesFormModel form)
            : this()
        {
            FORM = form;
        }

        public void doFilter() {
            if (FORM.IdFranquicia.HasValue)
            {
                if (FORM.IdCliente1.HasValue)
                {
                    if (FORM.IdSucursal1.HasValue)
                    {
                        notificaciones = db.MensajeStages.OrderByDescending(x => x.FechaIngreso).Where(x => x.IdSucursal == FORM.IdSucursal1);
                    }
                    else
                    {
                        notificaciones = db.MensajeStages.OrderByDescending(x => x.FechaIngreso).Where(x => x.Sucursal.IdCliente == FORM.IdCliente1);
                    }
                }
                else
                {
                    notificaciones = db.MensajeStages.OrderByDescending(x => x.FechaIngreso).Where(x => x.Sucursal.Cliente.IdFranquicia == FORM.IdFranquicia);
                }
            }
            else
            {
                notificaciones = db.MensajeStages.OrderByDescending(x => x.FechaIngreso);
            }
        }

        public void deleteNotificacion()
        {
            MensajeStage notificacion = db.MensajeStages.Where(x => x.IdMensajeStage == FORM.IdNotificacion).SingleOrDefault();
            db.NotificacionEquipos.DeleteAllOnSubmit(notificacion.NotificacionEquipos);
            db.MensajeStages.DeleteOnSubmit(notificacion);
            db.SubmitChanges();
        }

        public void resendNotificacion() {
            MensajeStage notificacion = db.MensajeStages.Where(x => x.IdMensajeStage == FORM.IdNotificacion).SingleOrDefault();
            EstadoMensaje queued = db.EstadoMensajes.Where(x => x.Codigo == 1).SingleOrDefault();
            notificacion.EstadoMensaje = queued;
            notificacion.FechaProgrEnvio = DateTime.Now;
            db.SubmitChanges();
        }
    }
}