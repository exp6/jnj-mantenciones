﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace MetricArts.JnJ.Mantenciones.Models.PortalIngeniero
{
    public class BandejaFormModel
    {
        [DisplayName("Cliente")]
        public Guid? IdCliente { get; set; }

        [DisplayName("Sucursal")]
        public Guid? IdSucursal { get; set; }

        public Guid IdMantencion { get; set; }
        public int nuevoEstado { get; set; }

        public string observaciones { get; set; }

        public string userName { get; set; }
    }
}