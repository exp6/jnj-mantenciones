﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.PortalIngeniero
{
    public class RepuestosIngenieroViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public RepuestosIngenieroFormModel FORM { get; set; }
        public Ingeniero IngenieroSession { get; set; }
        public IEnumerable<SolicitudKitRepuesto> SolicitudesKitRepuestos { get; set; }

        public RepuestosIngenieroViewModel(string userName) 
        {
            IngenieroSession = db.Ingenieros.Where(x => x.Usuario.NombreUsuario == userName).SingleOrDefault();

            SolicitudesKitRepuestos = db.SolicitudKitRepuestos.Where(x => x.Mantencion.Ingeniero == IngenieroSession);
        }

        public RepuestosIngenieroViewModel(RepuestosIngenieroFormModel form)
            : this(form.userName)
        {
            FORM = form;
        }

        public void persistSolicitud() 
        {
            Repuesto[] repuestosSeleccionados = db.Repuestos.Where(x => FORM.RespuestosSeleccionados.Contains(x.IdRepuesto)).ToArray();
            Mantencion mantencion = db.Mantencions.Where(x => x.IdMantencion == FORM.IdMantencion).SingleOrDefault();
            Ingeniero solicitante = db.Ingenieros.Where(x => x.Usuario.NombreUsuario == FORM.userName).SingleOrDefault();
            EstadoSolicitud creada = db.EstadoSolicituds.Where(x => x.Codigo == 1).SingleOrDefault();
            KitRepuesto kit = db.KitRepuestos.Where(x => x.IdKitRepuesto == FORM.IdKitRepuesto).SingleOrDefault();
            SolicitudRepuesto solicitud;
            List<SolicitudRepuesto> solicitudes = new List<SolicitudRepuesto>();

            SolicitudKitRepuesto solicitudKit = new SolicitudKitRepuesto {
                IdSolicitudKitRepuesto = Guid.NewGuid(),
                Mantencion = mantencion,
                KitRepuesto = kit,
                Ingeniero = solicitante,
                EstadoSolicitud = creada,
                FechaSolicitud = DateTime.Now
            };

            for (int i = 0; i < repuestosSeleccionados.Count(); i++)
            {
                solicitud = new SolicitudRepuesto
                {
                    IdSolicitudRepuesto = Guid.NewGuid(),
                    Repuesto = repuestosSeleccionados[i],
                    SolicitudKitRepuesto = solicitudKit,
                    CantidadSolicitada = Int32.Parse(FORM.RespuestosCantidad[i])
                };
                solicitudes.Add(solicitud);
            }
            db.SolicitudRepuestos.InsertAllOnSubmit(solicitudes);
            db.SubmitChanges();
        }
    }
}