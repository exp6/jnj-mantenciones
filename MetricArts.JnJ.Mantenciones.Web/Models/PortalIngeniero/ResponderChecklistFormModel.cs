﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.PortalIngeniero
{
    public class ResponderChecklistFormModel
    {
        //public IEnumerable<Respuesta> Respuestas { get; set; }
        public Guid IdMantencion { get; set; }
        public string userName { get; set; }
        public Guid IdCuestionarioMantencion { get; set; }

        public Guid CuestionarioMantencionId { get; set; } 
        public Guid[] PreguntaIds { get; set; } 
        public string[] Respuestas { get; set; }
        public string[] TipoPreguntas { get; set; }

        public string[] DatosClientes { get; set; }
        public string[] DatosClientesAttr { get; set; }
    }
}