﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.PortalIngeniero
{
    public class BandejaViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public BandejaFormModel FORM { get; set; }
        public IEnumerable<SelectListItem> Clientes { get; set; }
        public Ingeniero IngenieroSession { get; set; }
        public IEnumerable<Mantencion> Mantenciones { get; set; }

        //Estados Mantencion para Ingeniero
        public int CantEnCurso { get; set; }
        public int CantEnEsperaVisita { get; set; }
        public int CantEnEsperaMomento { get; set; }
        public int CantFinalizada { get; set; }
        public int CantTotal { get; set; }

        public BandejaViewModel(string userName) 
        {
            Clientes = db.Clientes.OrderBy(x => x.Franquicia.NombreFranquicia).Select(x => new SelectListItem()
            {
                Text = x.Franquicia.NombreFranquicia + " - " + x.NombreEmpresa,
                Value = x.IdCliente.ToString()
            });

            IngenieroSession = db.Ingenieros.Where(x => x.Usuario.NombreUsuario == userName).SingleOrDefault();

            var estadosValidos = new[] { 2, 3, 4, 5 };
            //var estadosValidos = new[] { 1 };

            Mantenciones = db.Mantencions.OrderBy(x => x.FechaConfirmacion)
                .Where(x => x.FechaConfirmacion.HasValue && x.Ingeniero == IngenieroSession && estadosValidos.Contains(x.EstadoMantencion.Codigo) && x.Habilitado);

            CantEnEsperaVisita = Mantenciones.Where(x => x.EstadoMantencion.Codigo == 2).Count();
            CantEnCurso = Mantenciones.Where(x => x.EstadoMantencion.Codigo == 3).Count();
            CantEnEsperaMomento = Mantenciones.Where(x => x.EstadoMantencion.Codigo == 4).Count();
            CantFinalizada = Mantenciones.Where(x => x.EstadoMantencion.Codigo == 5).Count();
            CantTotal = Mantenciones.Count();
        }

        public BandejaViewModel(BandejaFormModel form)
            : this(form.userName)
        {
            FORM = form;
        }

        public void peristNuevoEstado() 
        {            
            Mantencion m = db.Mantencions.Where(x => x.IdMantencion == FORM.IdMantencion).SingleOrDefault();
            Usuario u = db.Usuarios.Where(x => x.NombreUsuario == FORM.userName).SingleOrDefault();
            EstadoMantencion nuevoEstado = db.EstadoMantencions.Where(x => x.Codigo == FORM.nuevoEstado).SingleOrDefault();

            m.EstadoMantencion = nuevoEstado;

            EstadoHistoricoMantencion estadoHistorico = new EstadoHistoricoMantencion { 
                IdEstadoHistoricoMantencion = Guid.NewGuid(),
                Mantencion = m,
                EstadoMantencion = nuevoEstado,
                Usuario = u,
                FechaActualizacion = DateTime.Now,
                Observaciones = FORM.observaciones
            };
            db.EstadoHistoricoMantencions.InsertOnSubmit(estadoHistorico);

            db.SubmitChanges();
        }
    }
}