﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.PortalIngeniero
{
    public class DescargaChecklistViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        private CuestionarioMantencion cuestionarioMantencion { get; set; }

        public DescargaChecklistViewModel(Guid IdMantencion) 
        {
            cuestionarioMantencion = db.CuestionarioMantencions.Single(x => x.IdMantencion == IdMantencion);
        }

        public string getHtmlChecklist() 
        {
            Cuestionario cuestionario = cuestionarioMantencion.Cuestionario;
            string encabezado = cuestionario.Encabezado;
            string pie = cuestionario.Pie;

            List<Respuesta> respuestas = cuestionarioMantencion.Respuestas.ToList();
            List<Guid> preguntasRespondidas = respuestas.Select(x => x.IdPregunta).ToList();

            List<CuestionarioPregunta> cps = cuestionario.CuestionarioPreguntas.ToList();
            Pregunta p;

            foreach(CuestionarioPregunta cp in cps) 
            {
                p = cp.Pregunta;
                
            }

            return "";
        }

        public string getFileName()
        {


            return "";
        }
    }
}