﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MetricArts.JnJ.Mantenciones.Models.PortalIngeniero
{
    public class RepuestosIngenieroFormModel
    {
        public string userName { get; set; }
        public Guid IdMantencion { get; set; }
        public Guid IdKitRepuesto { get; set; }
        public Guid[] RespuestosSeleccionados { get; set; }
        public string[] RespuestosCantidad { get; set; }
    }
}