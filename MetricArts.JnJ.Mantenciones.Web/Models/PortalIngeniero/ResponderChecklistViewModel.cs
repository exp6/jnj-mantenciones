﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.PortalIngeniero
{
    public class ResponderChecklistViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public ResponderChecklistFormModel FORM { get; set; }
        public Mantencion mantencion { get; set; }
        public Cuestionario checklist { get; set; }
        public IEnumerable<CuestionarioPregunta> Preguntas { get; set; }
        public CuestionarioMantencion CuestionarioMantencion { get; set; }
        public IEnumerable<Respuesta> Respuestas { get; set; }

        public ResponderChecklistViewModel(Guid IdMantencion) 
        {
            mantencion = db.Mantencions.Single(x => x.IdMantencion == IdMantencion);

            if (mantencion != null) 
            {
                CuestionarioMantencion = mantencion.CuestionarioMantencions.Single(x => x.Mantencion == mantencion && x.Cuestionario.TipoCuestionario.Codigo == 1);
                checklist =  (CuestionarioMantencion != null ? CuestionarioMantencion.Cuestionario :  mantencion.Equipo.Cuestionario);

                if(checklist != null) 
                {
                    Preguntas = checklist.CuestionarioPreguntas.OrderBy(x => x.Orden).Where(x => x.IdCuestionario == checklist.IdCuestionario);
                    Respuestas = db.Respuestas.Where(x => x.CuestionarioMantencion == CuestionarioMantencion);
                }
            }
        }

        public ResponderChecklistViewModel(ResponderChecklistFormModel form)
        {
            FORM = form;
        }

        public void persistRespuestas() 
        {
            CuestionarioMantencion cm = db.CuestionarioMantencions.Single(x => x.IdCuestionarioMantencion == FORM.CuestionarioMantencionId);
            Ingeniero ingeniero = db.Ingenieros.Single(x => x.Usuario.NombreUsuario == FORM.userName);

            if(cm.Cuestionario.Abierto) 
            {
                for (int i = 0; i < FORM.DatosClientesAttr.Length; i++)
                {
                    switch (FORM.DatosClientesAttr[i])
                    {
                        case "Nombre_Centro":
                            cm.Nombre_Centro = FORM.DatosClientes[i];
                            break;
                        case "Direccion":
                            cm.Direccion = FORM.DatosClientes[i];
                            break;
                        case "Departamento_Ubicacion":
                            cm.Departamento_Ubicacion = FORM.DatosClientes[i];
                            break;
                        case "Ciudad":
                            cm.Ciudad = FORM.DatosClientes[i];
                            break;
                        case "Estado":
                            cm.Estado = FORM.DatosClientes[i];
                            break;
                        case "Codigo_Postal":
                            cm.Codigo_Postal = FORM.DatosClientes[i];
                            break;
                        case "Contacto_Principal":
                            cm.Contacto_Principal = FORM.DatosClientes[i];
                            break;
                        case "Nombre_Tecnico":
                            cm.Nombre_Tecnico = FORM.DatosClientes[i];
                            break;
                        case "Fecha":
                            cm.Fecha = FORM.DatosClientes[i];
                            break;
                    }
                }
            }

            Respuesta respuesta;
            Pregunta pregunta;
            RespuestaSugerida respuestaSugerida = null;
            string otroTexto = "";
            List<Respuesta> Respuestas = new List<Respuesta>();

            for (int i = 0; i < FORM.PreguntaIds.Count(); i++) 
            {
                pregunta = db.Preguntas.Single(x => x.IdPregunta == FORM.PreguntaIds[i]);
                respuestaSugerida = null;
                otroTexto = "";

                switch(Int32.Parse(FORM.TipoPreguntas[i])) 
                {
                    case 1:
                        
                        break;
                    case 2:

                        break;
                    case 3:
                        respuestaSugerida = db.RespuestaSugeridas.Single(x => x.IdRespuestaSugerida == new Guid(FORM.Respuestas[i]));
                        break;
                    case 4:

                        break;
                    case 5:
                        otroTexto = FORM.Respuestas[i];
                        break;
                }

                cm.Respondida = false;

                respuesta = new Respuesta
                {
                    IdRespuesta = Guid.NewGuid(),
                    RespuestaSugerida = respuestaSugerida,
                    Pregunta = pregunta,
                    CuestionarioMantencion = cm,
                    Ingeniero = ingeniero,
                    OtroTexto = otroTexto
                };

                if (respuestaSugerida != null)
                {
                    if (db.Respuestas.Any(x => x.IdRespuestaSugerida == respuestaSugerida.IdRespuestaSugerida && x.IdCuestionarioMantencion == cm.IdCuestionarioMantencion))
                        continue;
                }

                Respuestas.Add(respuesta);
            }

            List<Guid> respuestasIds = FORM.Respuestas == null ? new List<Guid> { Guid.Empty } : FORM.Respuestas.Select(s => s == "" ? Guid.Empty : Guid.Parse(s)).ToList();
            List<Guid> elementosTotales = db.Respuestas.Where(x => x.IdCuestionarioMantencion == cm.IdCuestionarioMantencion && x.IdRespuestaSugerida != null).Select(x => x.IdRespuestaSugerida.Value).ToList();
            List<Guid> elementosAEliminarIds = elementosTotales.Except(respuestasIds).Union(respuestasIds.Except(elementosTotales)).ToList();
            List<Respuesta> elementosAEliminar = db.Respuestas.Where(x => elementosAEliminarIds.Contains(x.IdRespuestaSugerida.Value) && x.IdCuestionarioMantencion == cm.IdCuestionarioMantencion).ToList();

            db.Respuestas.DeleteAllOnSubmit(elementosAEliminar);
            db.Respuestas.InsertAllOnSubmit(Respuestas);
            db.SubmitChanges();
        }

    }
}