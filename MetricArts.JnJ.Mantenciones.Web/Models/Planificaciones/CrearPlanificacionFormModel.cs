﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace MetricArts.JnJ.Mantenciones.Models.Planificaciones
{
    public class CrearPlanificacionFormModel
    {
        [Display(Name = "Nombre Planificación")]
        public String Nombre { get; set; }

        [StringLength(1023)]
        [Display(Name = "Descripción")]
        public String Descripcion { get; set; }

        [Display(Name = "Año de planificación")]
        public int Anio { get; set; }

        public string userName { get; set; }

        public String equiposSeleccionadosStr { get; set; }

        public String equiposSeleccionadosPeriodosStr { get; set; }

        public int modoPlan { get; set; }

        public Guid IdSucursal1 { get; set; }
        
        public string Error
        {
            get { return String.Empty; }
        }

        public string this[string columnName]
        {
            get
            {
                return String.Empty;
            }
        }
    }
}