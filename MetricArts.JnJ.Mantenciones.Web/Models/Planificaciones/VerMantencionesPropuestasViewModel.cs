﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.Planificaciones
{
    public class VerMantencionesPropuestasViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public VerMantencionesPropuestasFormModel FORM;
        public SeleccionEstructuraNegocioFormModel formSeleccionNegocio { get; set; }
        public IEnumerable<SelectListItem> Clientes { get; set; }
        public IEnumerable<Mantencion> Mantenciones { get; set; }
        public IEnumerable<Mantencion> MantencionesTotal { get; set; }
        public IEnumerable<Mantencion> MantencionesInactivas { get; set; }
        public int CantMantTotal { get; set; }

        public VerMantencionesPropuestasViewModel() 
        {
            Clientes = db.Clientes.OrderBy(x => x.NombreEmpresa).Select(x => new SelectListItem()
            {
                Text = x.NombreEmpresa,
                Value = x.IdCliente.ToString()
            });

            var estadosValidos = new[] { 1 };

            Mantenciones = db.Mantencions.OrderByDescending(x => x.FechaIngreso)
                .Where(x => !x.FechaConfirmacion.HasValue && x.MesPeriodo == DateTime.Now.Month && x.Planificacion.Ano == DateTime.Now.Year && estadosValidos.Contains(x.EstadoMantencion.Codigo) && x.Habilitado);

            MantencionesTotal = db.Mantencions.OrderByDescending(x => x.FechaIngreso)
                .Where(x => !x.FechaConfirmacion.HasValue && estadosValidos.Contains(x.EstadoMantencion.Codigo) && x.Habilitado);

            MantencionesInactivas = db.Mantencions.OrderByDescending(x => x.FechaIngreso)
                .Where(x => !x.FechaConfirmacion.HasValue && estadosValidos.Contains(x.EstadoMantencion.Codigo) && !x.Habilitado);

            CantMantTotal = MantencionesTotal.Count();
        }

        public VerMantencionesPropuestasViewModel(VerMantencionesPropuestasFormModel form, SeleccionEstructuraNegocioFormModel formSeleccionNegocioIn)
            : this()
        {
            FORM = form;
            formSeleccionNegocio = formSeleccionNegocioIn;
        }
    }
}