﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MetricArts.JnJ.Mantenciones.Models.Planificaciones
{
    public class SeleccionEstructuraNegocioFormModel
    {
        [DisplayName("Unidad de Negocio")]
        public Guid IdFranquicia { get; set; }

        [DisplayName("Cliente")]
        public Guid? IdCliente { get; set; }

        [DisplayName("Sucursal")]
        public Guid? IdSucursal { get; set; }
    }
}