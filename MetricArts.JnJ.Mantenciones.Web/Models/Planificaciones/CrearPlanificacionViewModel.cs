﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Core;
using MetricArts.JnJ.Mantenciones.Helpers;
using System.Globalization;

namespace MetricArts.JnJ.Mantenciones.Models.Planificaciones
{
    public class CrearPlanificacionViewModel
    {
        private MantencionesDataContext db = Utils.getDBConnection();
        public CrearPlanificacionFormModel FORM { get; set; }
        public SeleccionEstructuraNegocioFormModel formSeleccionNegocio { get; set; }
        public IEnumerable<SelectListItem> Anios { get; set; }
        private const int CANTIDAD_ANIOS_POSTERIOR = 4;

        public IEnumerable<Franquicia> Franquicias { get; set; }
        public IEnumerable<SelectListItem> FranquiciasSelect { get; set; }
        public Pais PaisDeUsuario { get; set; }

        public bool ModoPlanAutomatico { get; set; }

        public CrearPlanificacionViewModel() 
        {
            this.init();
        }

        private void init()
        {
            initDropDown();
            initAniosPlan();
        }

        public CrearPlanificacionViewModel(CrearPlanificacionFormModel form, string userName)
            : this()
        {
            FORM = form;
            FORM.userName = userName;
        }

        private void initDropDown()
        {
            //TODO: El país debe poder obtenerse del usuario
            PaisDeUsuario = db.Pais.Where(x => x.Nombre == "Chile").SingleOrDefault();

            IEnumerable<Sucursal> sucursalesLocales = db.Sucursals.Where(x => x.Comuna.Region.Pais.IdPais == PaisDeUsuario.IdPais);
            Franquicias = Utils.getFranquiciasByPais(sucursalesLocales, db);

            FranquiciasSelect = Franquicias.Select(x => new SelectListItem()
            {
                Text = x.NombreFranquicia,
                Value = x.IdFranquicia.ToString()
            });
        }

        private void initAniosPlan() {
            //int anioInicio = DateTime.Now.Month <= 2 ? DateTime.Now.Year : DateTime.Now.AddYears(1).Year;
            int anioInicio = DateTime.Now.Year;
            int ultimoAnio = anioInicio + CANTIDAD_ANIOS_POSTERIOR;

            List<SelectListItem> items = new List<SelectListItem>();

            for (int i = anioInicio; i <= ultimoAnio; i++)
            {
                var item = new SelectListItem
                {
                    Text = i.ToString(),
                    Value = i.ToString()
                };

                items.Add(item);
            }
            Anios = items;
        }

        public void persist() {
            int anioPlanificacion = FORM.Anio;
            EstadoPlan creada = db.EstadoPlans.Where(x => x.Codigo == 1).SingleOrDefault();

            Sucursal sucursal = db.Sucursals.Where(x => x.IdSucursal == FORM.IdSucursal1).SingleOrDefault();

            Planificacion plan = new Planificacion { 
                IdPlanificacion = Guid.NewGuid(),
                Sucursal = sucursal,
                Ano = anioPlanificacion,
                Nombre = FORM.Nombre,
                Descripcion = (FORM.Descripcion == null ? "" : FORM.Descripcion),
                EstadoPlan = creada,
                CreadoPor = FORM.userName,
                FechaIngreso = DateTime.Now
            };

            Guid[] equiposSeleccionados = FORM.equiposSeleccionadosStr.Split(';').Select(s => Guid.Parse(s)).ToArray();
            TipoMantencion tipoMantencion;
            TipoContrato tipoContrato;

            Mantencion mantencion;

            EstadoMantencion estadoInicial = db.EstadoMantencions.Where(x => x.Codigo == 1).SingleOrDefault();
            Equipo equipo;
            DateTime periodoPropuesto;

            switch (FORM.modoPlan) 
            {
                case 0: //Modo Manual
                    String[] periodos = FORM.equiposSeleccionadosPeriodosStr.Split(';').ToArray();
                    tipoMantencion = db.TipoMantencions.Where(x => x.Codigo == 1).SingleOrDefault();
                    
                    Guid idEquipo;
                    String periodo;
                    DateTime fechaInicio, fechaFinal;

                    for(int i = 0; i < equiposSeleccionados.Count(); i++) {
                        idEquipo = equiposSeleccionados[i];
                        equipo = db.Equipos.Where(x => x.IdEquipo == idEquipo).SingleOrDefault();

                        periodo = periodos[i] + "-" + anioPlanificacion;
                        periodoPropuesto = DateTime.ParseExact(periodo, "MM-yyyy", CultureInfo.InvariantCulture);

                        fechaInicio = new DateTime(periodoPropuesto.Year, periodoPropuesto.Month, 1);
                        fechaFinal = fechaInicio.AddMonths(1).AddDays(-1);

                        tipoContrato = equipo.TipoContrato;

                        Ingeniero ingeniero = db.Ingenieros.Where(x => x.IdIngeniero == equipo.IdIngeniero).SingleOrDefault();

                        //Mantenciones por UN PERIODO INGRESADO MANUALMENTE
                        mantencion = new Mantencion()
                        {
                            IdMantencion = Guid.NewGuid(),
                            Planificacion = plan,
                            TipoMantencion = tipoMantencion,
                            EstadoMantencion = estadoInicial, // TODO: TIPAR ESTOS DATOS
                            Equipo = equipo,
                            Ingeniero = ingeniero,
                            TipoContrato = tipoContrato,
                            CorrelativoPlan = 1, // TODO: TIPAR ESTOS DATOS
                            FechaEstimativaDesde = fechaInicio,
                            FechaEstimativaHasta = fechaFinal,
                            MesPeriodo = periodoPropuesto.Month,
                            Habilitado = true,
                            FechaIngreso = DateTime.Now
                        };

                        db.Mantencions.InsertOnSubmit(mantencion);
                    }
                    break;
                case 1: //Modo Automatico
                    tipoMantencion = db.TipoMantencions.Where(x => x.Codigo == 2).SingleOrDefault();

                    foreach (var equipoItem in equiposSeleccionados)
                    {
                        equipo = db.Equipos.Where(x => x.IdEquipo == equipoItem).SingleOrDefault();

                        List<String> periodosPropuestosEquipo = Utils
                        .CalculoMesesPropuesta(equipoItem, Utils.getFechaUltimaMantencion(equipoItem),
                        equipo.FechaInstalacion, equipo.TipoContrato.Codigo, anioPlanificacion);

                        tipoContrato = equipo.TipoContrato;

                        int correlativo = 1;

                        Ingeniero ingeniero = db.Ingenieros.Where(x => x.IdIngeniero == equipo.IdIngeniero).SingleOrDefault();

                        //Mantenciones por CADA PERIODO CALCULADO AUTOMATICAMENTE
                        foreach (String fechaStr in periodosPropuestosEquipo)
                        {
                            periodoPropuesto = DateTime.ParseExact(fechaStr, "MM-yyyy", CultureInfo.InvariantCulture);
                            fechaInicio = new DateTime(periodoPropuesto.Year, periodoPropuesto.Month, 1);
                            fechaFinal = fechaInicio.AddMonths(1).AddDays(-1);

                            mantencion = new Mantencion()
                            {
                                IdMantencion = Guid.NewGuid(),
                                Planificacion = plan,
                                TipoMantencion = tipoMantencion,
                                EstadoMantencion = estadoInicial, // TODO: TIPAR ESTOS DATOS
                                Equipo = equipo,
                                Ingeniero = ingeniero,
                                TipoContrato = tipoContrato,
                                CorrelativoPlan = correlativo, // TODO: TIPAR ESTOS DATOS
                                FechaEstimativaDesde = fechaInicio,
                                FechaEstimativaHasta = fechaFinal,
                                MesPeriodo = periodoPropuesto.Month,
                                Habilitado = true,
                                FechaIngreso = DateTime.Now
                            };

                            ++correlativo;
                            db.Mantencions.InsertOnSubmit(mantencion);
                        }
                    }
                    break;
            }
            db.SubmitChanges();
        }
    }
}