﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MetricArts.JnJ.Mantenciones.Models.Planificaciones
{
    public class VerMantencionesVigentesFormModel
    {

        [DisplayName("Ingeniero")]
        public Guid? IdIngeniero { get; set; }

        [DisplayName("Año Planificación")]
        public int? anioPlanificacion { get; set; }

        [DisplayName("Cliente")]
        public Guid? IdCliente { get; set; }

        [DisplayName("Sucursal")]
        public Guid? IdSucursal { get; set; }

        public string fechaSelected { get; set; }

        public Guid? IdCuestionario { get; set; }
    }
}