﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;
using MetricArts.JnJ.Mantenciones.Helpers;
using System.Web.Security;
using System.Web.Routing;
using System.Globalization;

namespace MetricArts.JnJ.Mantenciones.Models.Planificaciones.PlanificacionesAutomaticas
{
    public class PlanificacionesAutomaticasViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public PlanificacionesAutomaticasFormModel FORM { get; set; }
        public Pais PaisDeUsuario { get; set; }
        public IEnumerable<Franquicia> Franquicias { get; set; }
        public IEnumerable<Franquicia> FranquiciasFiltered { get; set; }
        public IEnumerable<SelectListItem> FranquiciasSelect { get; set; }
        public IEnumerable<SelectListItem> Paises { get; set; }
        public List<ClientesSeleccionadosDTO> ClientesSeleccionados { get; set; }
        public String fechaEnvio { get; set; }
        public IEnumerable<SelectListItem> Anios { get; set; }
        private const int CANTIDAD_ANIOS_POSTERIOR = 4;

        public PlanificacionesAutomaticasViewModel() 
        {
            FORM = new PlanificacionesAutomaticasFormModel();

            //TODO: El país debe poder obtenerse del usuario
            PaisDeUsuario = db.Pais.Where(x => x.Nombre == "Chile").SingleOrDefault();

            IEnumerable<Sucursal> sucursalesLocales = db.Sucursals.Where(x => x.Comuna.Region.Pais.IdPais == PaisDeUsuario.IdPais);
            Franquicias = getFranquiciasByPais(sucursalesLocales);

            FranquiciasFiltered = Franquicias;
            FranquiciasSelect = Franquicias.Select(x => new SelectListItem()
            {
                Text = x.NombreFranquicia,
                Value = x.IdFranquicia.ToString()
            });

            Paises = db.Pais.OrderBy(x => x.Nombre).Select(x => new SelectListItem()
            {
                Text = x.Nombre,
                Value = x.IdPais.ToString()
            });

            ClientesSeleccionados = new List<ClientesSeleccionadosDTO>();
            foreach (var fr in Franquicias) 
            {
                foreach(var cl in fr.Clientes)
                {
                    ClientesSeleccionados.Add(new ClientesSeleccionadosDTO
                    {
                        Checked = false,
                        ClienteSeleccionado = cl,
                        IdClienteSeleccionado = cl.IdCliente
                    });
                }
            }

            FORM.ClientesSeleccionados = ClientesSeleccionados;
            //FORM.anio = DateTime.Now.AddYears(1).Year;
            initAniosPlan();
        }

        public PlanificacionesAutomaticasViewModel(PlanificacionesAutomaticasFormModel form, string userName)
            : this()
        {
            FORM = form;
            ClientesSeleccionados = FORM.ClientesSeleccionados;
            FORM.userName = userName;
            this.persist();
        }

        private void initAniosPlan()
        {
            int anioInicio = DateTime.Now.Month <= 2 ? DateTime.Now.Year : DateTime.Now.AddYears(1).Year;
            int ultimoAnio = anioInicio + CANTIDAD_ANIOS_POSTERIOR;

            List<SelectListItem> items = new List<SelectListItem>();

            for (int i = anioInicio; i <= ultimoAnio; i++)
            {
                var item = new SelectListItem
                {
                    Text = i.ToString(),
                    Value = i.ToString()
                };

                items.Add(item);
            }
            Anios = items;
        }

        private void persist() 
        {
            Planificacion planificacion;
            Mantencion mantencion;
            MensajeStage notificacion;
            var fechaEnvio = FORM.fechaEnvio; // La fecha se utiliza para el envio de notificaciones.
            TipoMantencion tipoMantencion = db.TipoMantencions.Where(x => x.Codigo == 2).SingleOrDefault();

            EstadoMantencion EnEsperaConfirmacionFecha = db.EstadoMantencions.Where(x => x.Codigo == 1).SingleOrDefault();
            EstadoPlan creada = db.EstadoPlans.Where(x => x.Codigo == 1).SingleOrDefault();

            TipoContrato tipoContrato;
            Ingeniero ingeniero;

            foreach (var item in ClientesSeleccionados.Where(x => x.Checked))
            {
                Cliente clienteSelected = db.Clientes.Where(x => x.IdCliente == item.IdClienteSeleccionado).FirstOrDefault();
                foreach (var sucursal in clienteSelected.Sucursals)
                {
                    planificacion = new Planificacion()
                    {
                        IdPlanificacion = Guid.NewGuid(),
                        Sucursal = sucursal,
                        Ano = FORM.anio,
                        Nombre = "Planificación Anual" + FORM.anio,
                        CreadoPor = FORM.userName,
                        EstadoPlan = creada,
                        Descripcion = "Planificaciones Anuales para el año " + FORM.anio,
                        FechaIngreso = DateTime.Now
                    };

                    foreach (var equipo in sucursal.Equipos)
                    {
                        
                        DateTime fecha = equipo.FechaUltimaMantencion ?? equipo.FechaInstalacion;
                        //List<DateTime> periodos = Utils.CalculoMesesPropuesta(fecha, equipo.TipoContrato.Codigo, FORM.anio);
                        List<String> periodos = Utils.CalculoMesesPropuesta(equipo.IdEquipo, Utils.getFechaUltimaMantencion(equipo.IdEquipo), equipo.FechaInstalacion, equipo.TipoContrato.Codigo, FORM.anio);
                        DateTime fechaInicio, fechaFinal;

                        int correlativo = 1;

                        tipoContrato = equipo.TipoContrato;
                        ingeniero = db.Ingenieros.Where(x => x.IdIngeniero == equipo.IdIngeniero).SingleOrDefault();

                        DateTime periodoPropuesto;

                        foreach(var periodo in periodos)
                        {
                            periodoPropuesto = DateTime.ParseExact(periodo, "MM-yyyy", CultureInfo.InvariantCulture);

                            fechaInicio = new DateTime(periodoPropuesto.Year, periodoPropuesto.Month, 1);
                            fechaFinal = fechaInicio.AddMonths(1).AddDays(-1);

                            //Se insertan las mantenciones de acuerdo a los periodos asociados por fecha de instalacion del equipo.
                            mantencion = new Mantencion()
                            {
                                IdMantencion = Guid.NewGuid(),
                                Planificacion = planificacion,
                                TipoMantencion = tipoMantencion, // TODO: TIPAR ESTOS DATOS
                                EstadoMantencion = EnEsperaConfirmacionFecha, // TODO: TIPAR ESTOS DATOS
                                Equipo = equipo,
                                Ingeniero = ingeniero,
                                TipoContrato = tipoContrato,
                                CorrelativoPlan = correlativo, // TODO: TIPAR ESTOS DATOS
                                FechaEstimativaDesde = fechaInicio,
                                FechaEstimativaHasta = fechaFinal,
                                MesPeriodo = periodoPropuesto.Month,
                                Habilitado = true,
                                FechaIngreso = DateTime.Now
                            };

                            ++correlativo;
                            db.Mantencions.InsertOnSubmit(mantencion);
                        }
                    }

                    //INSERTAR NOTIFICACIONES
                    TipoNotificacion tipoNotificacion = db.TipoNotificacions.Where(x => x.Codigo == 1).SingleOrDefault();
                    EstadoMensaje estadoMsg = db.EstadoMensajes.Where(x => x.Codigo == 1).SingleOrDefault();

                    Guid[] equiposSeleccionados = sucursal.Equipos.Select(x => x.IdEquipo).ToArray();
                    Guid msgStageId = Guid.NewGuid();

                    Usuario usuario = db.Usuarios.Where(x => x.NombreUsuario == FORM.userName).SingleOrDefault();

                    MailTemplate mailTemplate = db.MailTemplates.Where(x => x.IdMailTemplate == new Guid("FFDC949C-9BDB-44DB-9BBC-F0620CA8F9A0")).SingleOrDefault();
                    string cuerpoHTMLMail = Utils.getHTMLParsed(mailTemplate.CuerpoHTML, sucursal.IdSucursal, planificacion.IdPlanificacion, equiposSeleccionados, String.Empty, db, 1, msgStageId, Guid.Empty, Guid.Empty, usuario.IdUsuario, FORM.anio);

                    PDFTemplate pdfTemplate = db.PDFTemplates.Where(x => x.IdPDFTemplate == new Guid("6DADAD6D-416C-4295-BC0E-CC222DB8D159")).SingleOrDefault();
                    string cuerpoHTMLPDF = Utils.getHTMLParsed(pdfTemplate.CuerpoHTML, sucursal.IdSucursal, planificacion.IdPlanificacion, equiposSeleccionados, String.Empty, db, 1, msgStageId, Guid.Empty, Guid.Empty, usuario.IdUsuario, FORM.anio);

                    DateTime fechaEnvioFormat = DateTime.ParseExact(fechaEnvio, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    notificacion = new MensajeStage
                    {
                        IdMensajeStage = msgStageId,
                        TipoNotificacion = tipoNotificacion,
                        Sucursal = sucursal,
                        EstadoMensaje = estadoMsg,
                        MailTemplate = mailTemplate,
                        PDFTemplate = pdfTemplate,
                        Planificacion = planificacion,
                        CuerpoHTMLMail = cuerpoHTMLMail,
                        CuerpoHTMLPDF = cuerpoHTMLPDF,
                        ContactoPara = (sucursal.Contactos.FirstOrDefault() != null ? sucursal.Contactos.FirstOrDefault().Correo : ""),
                        ContactoCC = "",
                        ContactoBCC = "",
                        FechaIngreso = DateTime.Now,
                        FechaProgrEnvio = fechaEnvioFormat,
                        CreadoPor = FORM.userName
                    };

                    db.MensajeStages.InsertOnSubmit(notificacion);

                    foreach (var item1 in equiposSeleccionados)
                    {
                        Equipo equipo = db.Equipos.Where(x => x.IdEquipo == item1).SingleOrDefault();

                        NotificacionEquipo ne = new NotificacionEquipo
                        {
                            IdNotificacionEquipo = Guid.NewGuid(),
                            MensajeStage = notificacion,
                            Equipo = equipo,
                            FechaIngreso = DateTime.Now,
                            CreadoPor = FORM.userName
                        };

                        db.NotificacionEquipos.InsertOnSubmit(ne);
                    }
                }
            }
            db.SubmitChanges();
        }

        private IEnumerable<Cliente> getClientesByPais(IEnumerable<Sucursal> sucursalesLocales)
        {
            return db.Clientes.Where(x => x.Sucursals.Intersect(sucursalesLocales).Any());
        }

        private IEnumerable<Franquicia> getFranquiciasByPais(IEnumerable<Sucursal> sucursalesLocales)
        {
            IEnumerable<Cliente> clientesLocales = db.Clientes.Where(x => x.Sucursals.Intersect(sucursalesLocales).Any());
            return db.Franquicias.Where(x => x.Clientes.Intersect(clientesLocales).Any());
        }
    }
}