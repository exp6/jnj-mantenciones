﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.Planificaciones.PlanificacionesAutomaticas
{
    public class ClientesSeleccionadosDTO
    {
        public Cliente ClienteSeleccionado { get; set; }
        public bool Checked { get; set; }
        public Guid IdClienteSeleccionado { get; set; }
    }
}