﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.Planificaciones.PlanificacionesAutomaticas
{
    public class EquiposPlanificacionDTO
    {
        public Equipo equipo { get; set; }
        public List<String> periodosMantencion { get; set; }
    }
}