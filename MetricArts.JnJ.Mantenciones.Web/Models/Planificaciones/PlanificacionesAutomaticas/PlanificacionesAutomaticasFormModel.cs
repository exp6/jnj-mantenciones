﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.Planificaciones.PlanificacionesAutomaticas
{
    public class PlanificacionesAutomaticasFormModel : IDataErrorInfo
    {
        [Display(Name = "Unidad de Negocio")]
        public Guid IdFranquicia { get; set; }

        [Display(Name = "País")]
        public Guid IdPais { get; set; }

        [DisplayName("Cliente")]
        public Guid? IdCliente { get; set; }

        public String[] idClientesSeleccionados { get; set; }

        public List<ClientesSeleccionadosDTO> ClientesSeleccionados { get; set; }

        public String fechaEnvio { get; set; }

        public int anio { get; set; }

        public string userName { get; set; }

        public string Error
        {
            get { return String.Empty; }
        }

        public string this[string columnName]
        {
            get
            {
                return String.Empty;
            }
        }
    }
}