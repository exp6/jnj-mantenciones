﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;
using MetricArts.JnJ.Mantenciones.Helpers;

namespace MetricArts.JnJ.Mantenciones.Models.Planificaciones.PlanificacionesAutomaticas
{
    public class PDFPreviewViewModel
    {
        public Sucursal sucursal { get; set; }
        public List<EquiposPlanificacionDTO> equiposPlanificados { get; set; }
        public string year { get; set; }

        public PDFPreviewViewModel(Guid IdSucursal, int anio)
        { 
            MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();

            sucursal = db.Sucursals.Where(x => x.IdSucursal == IdSucursal).SingleOrDefault();

            List<String> periodos;

            equiposPlanificados = new List<EquiposPlanificacionDTO>();
            EquiposPlanificacionDTO equipoPlanificacion;
            List<String> periodosPlanificados;
            String periodoPlanificado;

            year = "" + anio;

            foreach(var equipo in sucursal.Equipos)
            {
                periodos = Utils.CalculoMesesPropuesta(equipo.IdEquipo, Utils.getFechaUltimaMantencion(equipo.IdEquipo), equipo.FechaInstalacion, equipo.TipoContrato.Codigo, anio);
                periodosPlanificados = new List<String>();

                foreach(var periodo in periodos)
                {
                    periodoPlanificado = periodo;
                    periodosPlanificados.Add(periodoPlanificado);
                }

                equipoPlanificacion = new EquiposPlanificacionDTO { 
                    equipo = equipo,
                    periodosMantencion = periodosPlanificados
                };

                equiposPlanificados.Add(equipoPlanificacion);
            }

        }
    }
}