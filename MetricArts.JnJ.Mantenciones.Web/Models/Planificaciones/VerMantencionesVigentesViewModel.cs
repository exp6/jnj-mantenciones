﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Core;
using System.Globalization;

namespace MetricArts.JnJ.Mantenciones.Models.Planificaciones
{
    public class VerMantencionesVigentesViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public SeleccionEstructuraNegocioFormModel formSeleccionNegocio { get; set; }
        public VerMantencionesVigentesFormModel FORM { get; set; }
        public IEnumerable<SelectListItem> Clientes { get; set; }
        public IEnumerable<SelectListItem> Sucursales { get; set; }
        public IPagedList<Planificacion> Planificaciones { get; set; }

        public IEnumerable<SelectListItem> AniosPlanificacion { get; set; }
        public IEnumerable<SelectListItem> Ingenieros { get; set; }
        public IEnumerable<Mantencion> Mantenciones { get; set; }
        public IEnumerable<Mantencion> MantencionesTotal { get; set; }
        public IEnumerable<Mantencion> MantencionesInactivas { get; set; }

        public int anioPlanificacion { get; set; }
        public string fechaSeleccion { get; set; }

        public int CantTotal { get; set; }

        public IEnumerable<SelectListItem> Checklists { get; set; } 

        public VerMantencionesVigentesViewModel(int year, string fecha) 
        {
            Clientes = db.Clientes.OrderBy(x => x.NombreEmpresa).Select(x => new SelectListItem()
            {
                Text = x.Franquicia.NombreFranquicia + " - " + x.NombreEmpresa,
                Value = x.IdCliente.ToString()
            });

            Ingenieros = db.Ingenieros.Select(x => new SelectListItem()
            {
                Text = x.Usuario.Nombre + " " + x.Usuario.ApellidoPaterno,
                Value = x.IdIngeniero.ToString()
            });

            AniosPlanificacion = db.Planificacions.GroupBy(x => x.Ano).Select(x => new SelectListItem()
            {
                Text = x.First().Ano.ToString(),
                Value = x.First().Ano.ToString()
            });

            var estadosValidos = new[] { 2 };

            Mantenciones = db.Mantencions.OrderByDescending(x => x.FechaIngreso)
                .Where(x => x.FechaConfirmacion.HasValue && x.FechaConfirmacion == DateTime.Now && estadosValidos.Contains(x.EstadoMantencion.Codigo) && x.Habilitado);

            MantencionesTotal = db.Mantencions.OrderBy(x => x.FechaConfirmacion).Where(x => x.FechaConfirmacion.HasValue && estadosValidos.Contains(x.EstadoMantencion.Codigo) && x.Habilitado);

            MantencionesInactivas = db.Mantencions.OrderBy(x => x.FechaConfirmacion).Where(x => x.FechaConfirmacion.HasValue && estadosValidos.Contains(x.EstadoMantencion.Codigo) && !x.Habilitado);

            anioPlanificacion = year == 0 ? DateTime.Now.Year : year;
            fechaSeleccion = fecha == null ? DateTime.Now.ToString("dd-MM-yyyy") : fecha;

            CantTotal = MantencionesTotal.Count();

            Checklists = db.Cuestionarios.Where(x => x.TipoCuestionario.Codigo == 1).Select(x => new SelectListItem()
            {
                Text = x.Descripcion,
                Value = x.IdCuestionario.ToString()
            });
        }

        public VerMantencionesVigentesViewModel(VerMantencionesVigentesFormModel form)
            : this(form.anioPlanificacion ?? DateTime.Now.Year, form.fechaSelected)
        {
            FORM = form;
            doFilter();
        }

        public void doFilter() {
            if (FORM.IdCliente.HasValue)
            {
                if (FORM.IdSucursal.HasValue)
                {
                    Mantenciones = db.Mantencions.OrderByDescending(x => x.FechaIngreso).Where(x => x.FechaConfirmacion.HasValue && x.Equipo.IdSucursal == FORM.IdSucursal && x.Habilitado);
                }
                else
                {
                    Mantenciones = db.Mantencions.OrderByDescending(x => x.FechaIngreso).Where(x => x.FechaConfirmacion.HasValue && x.Equipo.Sucursal.IdCliente == FORM.IdCliente && x.Habilitado);
                }
            }
            else
            {
                Mantenciones = db.Mantencions.OrderByDescending(x => x.FechaIngreso).Where(x => x.FechaConfirmacion.HasValue && x.Habilitado);
            }

            int anioSelected = FORM.anioPlanificacion ?? DateTime.Now.Year;
            DateTime fechaConfirmacion = FORM.fechaSelected == null ? DateTime.Now : DateTime.ParseExact(FORM.fechaSelected, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            Mantenciones = Mantenciones.Where(x => x.FechaConfirmacion.HasValue && x.Planificacion.Ano == anioSelected && x.FechaConfirmacion == fechaConfirmacion && x.Habilitado);
            
            if (FORM.IdIngeniero.HasValue)
            {
                Mantenciones = Mantenciones.Where(x => x.FechaConfirmacion.HasValue && x.IdIngeniero == FORM.IdIngeniero && x.Habilitado);
            }
        }
    }
}