﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.Accounts.Account
{
    public class LoginUtils
    {
        public static string getCustomUserName(string username)
        {
            MantencionesDataContext db = new MantencionesDataContext().WithConnectionStringFromConfiguration();

            Usuario customUser = db.Usuarios.Single(x => x.NombreUsuario == username);

            return (customUser != null ? customUser.Nombre : username);
        }

        public static Usuario getCustomUser(string username)
        {
            MantencionesDataContext db = new MantencionesDataContext().WithConnectionStringFromConfiguration();

            Usuario customUser = db.Usuarios.Single(x => x.NombreUsuario == username);

            if (customUser == null)
            {
                customUser = new Usuario();
                customUser.Nombre = username;
            }

            return customUser;
        }
    }
}