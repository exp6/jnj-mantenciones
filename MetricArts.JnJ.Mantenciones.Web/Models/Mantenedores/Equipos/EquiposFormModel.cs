﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.Equipos
{
    public class EquiposFormModel
    {
        [DisplayName("Nombre")]
        public string Nombre { get; set; }

        [DisplayName("Descripción")]
        public string Descripcion { get; set; }

        [DisplayName("Código Equipo (Número Serie)")]
        public string CodigoEquipo { get; set; }

        [DisplayName("Fecha de instalación del equipo")]
        public string FechaInstalacion { get; set; }

        [DisplayName("Fecha última mantención (opcional)")]
        public string FechaUltimaMantencion { get; set; }

        public Guid IdEquipo { get; set; }

        [DisplayName("Ingeniero asignado")]
        public Guid IdIngeniero { get; set; }

        [DisplayName("Sucursal instalación del equipo")]
        public Guid IdSucursal { get; set; }

        [DisplayName("Contrato del equipo")]
        public Guid IdTipoContrato { get; set; }

        [DisplayName("Checklist")]
        public Guid IdCuestionario { get; set; }

        public string userName { get; set; }
    }
}