﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;
using System.Web.Mvc;
using System.Globalization;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.Equipos
{
    public class EquiposViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public EquiposFormModel FORM { get; set; }
        public IEnumerable<Equipo> Equipos { get; set; }
        public Equipo equipo { get; set; }
        public IEnumerable<SelectListItem> Ingenieros { get; set; }
        public IEnumerable<SelectListItem> Sucursales { get; set; }
        public IEnumerable<SelectListItem> TipoContratos { get; set; }
        public IEnumerable<SelectListItem> Checklists { get; set; }

        public EquiposViewModel()
        {
            Equipos = db.Equipos.OrderBy(x => x.Nombre); 
        }

        public EquiposViewModel(string userName) 
        {
            Ingenieros = db.Ingenieros.Select(x => new SelectListItem()
            {
                Text = x.Usuario.Nombre + " " + x.Usuario.ApellidoPaterno,
                Value = x.IdIngeniero.ToString()
            });

            Sucursales = db.Sucursals.OrderBy(x => x.Cliente.NombreEmpresa).Select(x => new SelectListItem()
            {
                Text = x.Cliente.Franquicia.NombreFranquicia + " - " + x.Cliente.NombreEmpresa + " - " + x.NombreSucursal,
                Value = x.IdSucursal.ToString()
            });

            TipoContratos = db.TipoContratos.OrderBy(x => x.Codigo).Select(x => new SelectListItem()
            {
                Text = x.TipoContrato1,
                Value = x.IdTipoContrato.ToString()
            });

            Checklists = db.Cuestionarios.OrderBy(x => x.Descripcion).Where(x => x.TipoCuestionario.Codigo == 1).Select(x => new SelectListItem()
            {
                Text = x.Descripcion,
                Value = x.IdCuestionario.ToString()
            });
        }

        public EquiposViewModel(Guid idEquipo)
        {
            Ingenieros = db.Ingenieros.Select(x => new SelectListItem()
            {
                Text = x.Usuario.Nombre + " " + x.Usuario.ApellidoPaterno,
                Value = x.IdIngeniero.ToString()
            });

            Sucursales = db.Sucursals.OrderBy(x => x.Cliente.Franquicia.NombreFranquicia).Select(x => new SelectListItem()
            {
                Text = x.Cliente.Franquicia.NombreFranquicia + " - " + x.Cliente.NombreEmpresa + " - " + x.NombreSucursal,
                Value = x.IdSucursal.ToString()
            });

            TipoContratos = db.TipoContratos.OrderBy(x => x.Codigo).Select(x => new SelectListItem()
            {
                Text = x.TipoContrato1,
                Value = x.IdTipoContrato.ToString()
            });

            Checklists = db.Cuestionarios.OrderBy(x => x.Descripcion).Where(x => x.TipoCuestionario.Codigo == 1).Select(x => new SelectListItem()
            {
                Text = x.Descripcion,
                Value = x.IdCuestionario.ToString()
            });

            equipo = db.Equipos.Where(x => x.IdEquipo == idEquipo).SingleOrDefault();
            FORM = new EquiposFormModel();
            FORM.Nombre = equipo.Nombre;
            FORM.Descripcion = equipo.Descripcion;
            FORM.CodigoEquipo = equipo.CodigoEquipo;
            FORM.IdEquipo = equipo.IdEquipo;
            FORM.IdTipoContrato = equipo.TipoContrato.IdTipoContrato;
            FORM.FechaInstalacion = equipo.FechaInstalacion.ToString("dd-MM-yyyy");
            FORM.FechaUltimaMantencion = equipo.FechaUltimaMantencion != null ? equipo.FechaUltimaMantencion.Value.ToString("dd-MM-yyyy") : equipo.FechaInstalacion.ToString("dd-MM-yyyy");
            FORM.IdIngeniero = equipo.Ingeniero.IdIngeniero;
            FORM.IdSucursal = equipo.Sucursal.IdSucursal;
        }

        public EquiposViewModel(EquiposFormModel form)
        {
            FORM = form;
        }

        public bool tieneMantenciones(Equipo e)
        {
            return db.Mantencions.Any(x => x.Equipo == e);
        }

        /**
         * Funciones CRUD
        **/

        public void create()
        {
            TipoContrato contrato = db.TipoContratos.Single(x => x.IdTipoContrato == FORM.IdTipoContrato);
            Ingeniero ingeniero = db.Ingenieros.Single(x => x.IdIngeniero == FORM.IdIngeniero);
            Sucursal sucursal = db.Sucursals.Single(x => x.IdSucursal == FORM.IdSucursal);
            Cuestionario checklist = db.Cuestionarios.Single(x => x.IdCuestionario == FORM.IdCuestionario);

            Equipo nuevo = new Equipo
            {
                IdEquipo = Guid.NewGuid(),
                Nombre = FORM.Nombre,
                CodigoEquipo = FORM.CodigoEquipo,
                Descripcion = FORM.Descripcion,
                FechaInstalacion = DateTime.ParseExact(FORM.FechaInstalacion, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                //FechaUltimaMantencion = FORM.FechaUltimaMantencion == null ? DateTime.ParseExact(FORM.FechaInstalacion, "dd-MM-yyyy", CultureInfo.InvariantCulture) : DateTime.ParseExact(FORM.FechaUltimaMantencion, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                FechaUltimaMantencion = FORM.FechaUltimaMantencion == null ? (DateTime?)null : DateTime.ParseExact(FORM.FechaUltimaMantencion, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                TipoContrato = contrato,
                Ingeniero = ingeniero,
                Sucursal = sucursal,
                Cuestionario = checklist,
                CreadoPor = FORM.userName,
                FechaIngreso = DateTime.Now
            };
            db.Equipos.InsertOnSubmit(nuevo);
            db.SubmitChanges();
        }

        public void update()
        {
            TipoContrato contrato = db.TipoContratos.Single(x => x.IdTipoContrato == FORM.IdTipoContrato);
            Ingeniero ingeniero = db.Ingenieros.Single(x => x.IdIngeniero == FORM.IdIngeniero);
            Sucursal sucursal = db.Sucursals.Single(x => x.IdSucursal == FORM.IdSucursal);
            Cuestionario checklist = db.Cuestionarios.Single(x => x.IdCuestionario == FORM.IdCuestionario);

            Equipo e = db.Equipos.Where(x => x.IdEquipo == FORM.IdEquipo).SingleOrDefault();
            e.Nombre = FORM.Nombre;
            e.CodigoEquipo = FORM.CodigoEquipo;
            e.Descripcion = FORM.Descripcion ?? "";
            e.FechaInstalacion = DateTime.ParseExact(FORM.FechaInstalacion, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            //e.FechaUltimaMantencion = FORM.FechaUltimaMantencion == null ? DateTime.ParseExact(FORM.FechaInstalacion, "dd-MM-yyyy", CultureInfo.InvariantCulture) : DateTime.ParseExact(FORM.FechaUltimaMantencion, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            e.FechaUltimaMantencion = FORM.FechaUltimaMantencion == null ? (DateTime?)null : DateTime.ParseExact(FORM.FechaUltimaMantencion, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            e.TipoContrato = contrato;
            e.Ingeniero = ingeniero;
            e.Sucursal = sucursal;
            e.Cuestionario = checklist;
            db.SubmitChanges();
        }

        public void delete()
        {
            Equipo e = db.Equipos.Where(x => x.IdEquipo == FORM.IdEquipo).SingleOrDefault();
            db.Equipos.DeleteOnSubmit(e);
            db.SubmitChanges();
        }

        public void inactivar()
        {
            Equipo e = db.Equipos.Where(x => x.IdEquipo == FORM.IdEquipo).SingleOrDefault();
            e.Estado = 0;
            db.SubmitChanges();
        }

        public void activar()
        {
            Equipo e = db.Equipos.Where(x => x.IdEquipo == FORM.IdEquipo).SingleOrDefault();
            e.Estado = 1;
            db.SubmitChanges();
        }

        /**
         * FIN Funciones CRUD
        **/
    }
}