﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.Contactos
{
    public class ContactosFormModel
    {
        [DisplayName("Nombres")]
        public string Nombre { get; set; }

        [DisplayName("Apellido Paterno")]
        public string Apaterno { get; set; }

        [DisplayName("Apellido Materno")]
        public string Amaterno { get; set; }

        [DisplayName("Correo")]
        public string Correo { get; set; }

        [DisplayName("Teléfono Oficina")]
        public string TelefonoOficina { get; set; }

        [DisplayName("Teléfono Celular")]
        public string TelefonoCelular { get; set; }

        [DisplayName("Cargo")]
        public string Cargo { get; set; }

        [DisplayName("Franquicia")]
        public Guid IdFranquicia { get; set; }

        [DisplayName("Cliente")]
        public Guid IdCliente { get; set; }

        [DisplayName("Sucursal")]
        public Guid IdSucursal { get; set; }

        public Guid IdSucursal1 { get; set; }

        public Guid IdContacto { get; set; }

        public string userName { get; set; }
    }
}