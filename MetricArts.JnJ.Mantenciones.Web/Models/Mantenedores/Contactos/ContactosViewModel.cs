﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.Contactos
{
    public class ContactosViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public ContactosFormModel FORM { get; set; }
        public IEnumerable<Contacto> Contactos { get; set; }
        public Contacto contacto { get; set; }
        public IEnumerable<SelectListItem> Franquicias { get; set; }
        public IEnumerable<SelectListItem> Clientes { get; set; }
        public IEnumerable<SelectListItem> Sucursales { get; set; }

        public ContactosViewModel() 
        {
            Contactos = db.Contactos.OrderBy(x => x.Correo);
        }

        public ContactosViewModel(string userName)
        {
            Franquicias = db.Franquicias.OrderBy(x => x.NombreFranquicia).Select(x => new SelectListItem()
            {
                Text = x.NombreFranquicia,
                Value = x.IdFranquicia.ToString()
            });
        }

        public ContactosViewModel(Guid idContacto)
        {
            contacto = db.Contactos.Single(x => x.IdContacto == idContacto);
            FORM = new ContactosFormModel();
            FORM.IdContacto = contacto.IdContacto;
            FORM.Nombre = contacto.Nombre;
            FORM.Apaterno = contacto.Apaterno;
            FORM.Amaterno = contacto.Amaterno;
            FORM.Correo = contacto.Correo;
            FORM.TelefonoOficina = contacto.TelefonoOficina;
            FORM.TelefonoCelular = contacto.TelefonoCelular;
            FORM.Cargo = contacto.Cargo;
            FORM.IdFranquicia = contacto.Sucursal.Cliente.IdFranquicia;
            FORM.IdCliente = contacto.Sucursal.IdCliente;
            FORM.IdSucursal = contacto.IdSucursal;

            Franquicias = db.Franquicias.OrderBy(x => x.NombreFranquicia).Select(x => new SelectListItem()
            {
                Text = x.NombreFranquicia,
                Value = x.IdFranquicia.ToString()
            });

            Clientes = db.Clientes.OrderBy(x => x.NombreEmpresa).Where(x => x.IdFranquicia == FORM.IdFranquicia).Select(x => new SelectListItem()
            {
                Text = x.NombreEmpresa,
                Value = x.IdCliente.ToString()
            });

            Sucursales = db.Sucursals.OrderBy(x => x.NombreSucursal).Where(x => x.IdSucursal == FORM.IdSucursal).Select(x => new SelectListItem()
            {
                Text = x.NombreSucursal,
                Value = x.IdSucursal.ToString()
            });
        }

        public ContactosViewModel(ContactosFormModel form)
        {
            FORM = form;
        }

        /**
         * Funciones CRUD
        **/

        public void create() 
        {
            Sucursal s = db.Sucursals.Single(x => x.IdSucursal == FORM.IdSucursal1);

            Contacto nuevo = new Contacto
            {
                IdContacto = Guid.NewGuid(),
                Sucursal = s,
                Nombre = FORM.Nombre,
                Apaterno = FORM.Apaterno,
                Amaterno = FORM.Amaterno,
                Correo = FORM.Correo,
                TelefonoOficina = FORM.TelefonoOficina,
                TelefonoCelular = FORM.TelefonoCelular,
                Cargo = FORM.Cargo,
                Responsable = true,
                Estado = 1
            };
            db.Contactos.InsertOnSubmit(nuevo);
            db.SubmitChanges();
        }

        public void update()
        {
            Sucursal s = db.Sucursals.Single(x => x.IdSucursal == FORM.IdSucursal);

            Contacto c = db.Contactos.Single(x => x.IdContacto == FORM.IdContacto);
            c.Sucursal = s;
            c.Nombre = FORM.Nombre;
            c.Apaterno = FORM.Apaterno;
            c.Amaterno = FORM.Amaterno;
            c.Correo = FORM.Correo;
            c.TelefonoOficina = FORM.TelefonoOficina;
            c.TelefonoCelular = FORM.TelefonoCelular;
            c.Cargo = FORM.Cargo;
            db.SubmitChanges();
        }

        public void delete()
        {
            Contacto c = db.Contactos.Single(x => x.IdContacto == FORM.IdContacto);
            db.Contactos.DeleteOnSubmit(c);
            db.SubmitChanges();
        }

        /**
         * FIN Funciones CRUD
        **/
    }
}