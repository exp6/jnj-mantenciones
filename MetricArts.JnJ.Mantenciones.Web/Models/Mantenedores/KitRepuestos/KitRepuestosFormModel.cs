﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.KitRepuestos
{
    public class KitRepuestosFormModel
    {
        [DisplayName("Nombre")]
        public string Nombre { get; set; }

        [DisplayName("Franquicia")]
        public Guid IdFranquicia { get; set; }

        [DisplayName("Cliente")]
        public Guid IdCliente { get; set; }

        [DisplayName("Sucursal")]
        public Guid IdSucursal { get; set; }

        public Guid IdEquipo { get; set; }

        public Guid IdKitRepuesto { get; set; }

        public string RepuestosIds { get; set; }

        public string userName { get; set; }
    }
}