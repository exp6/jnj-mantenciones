﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.KitRepuestos
{
    public class KitRepuestosViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public KitRepuestosFormModel FORM { get; set; }
        public IEnumerable<KitRepuesto> KitRepuestos { get; set; }
        public KitRepuesto kitRepuesto { get; set; }
        public IEnumerable<SelectListItem> Franquicias { get; set; }
        public IEnumerable<SelectListItem> Clientes { get; set; }
        public IEnumerable<SelectListItem> Sucursales { get; set; }

        public List<Repuesto> repuestosTotales { get; set; }
        public List<Repuesto> repuestosDisponibles { get; set; }
        public List<Repuesto> repuestosAsociados { get; set; }

        public KitRepuestosViewModel() 
        {
            KitRepuestos = db.KitRepuestos.OrderBy(x => x.Nombre);
        }

        public KitRepuestosViewModel(string userName)
        {
            Franquicias = db.Franquicias.OrderBy(x => x.NombreFranquicia).Select(x => new SelectListItem()
            {
                Text = x.NombreFranquicia,
                Value = x.IdFranquicia.ToString()
            });
        }

        public KitRepuestosViewModel(Guid idKitRepuesto)
        {
            kitRepuesto = db.KitRepuestos.Single(x => x.IdKitRepuesto == idKitRepuesto);
            FORM = new KitRepuestosFormModel();
            FORM.Nombre = kitRepuesto.Nombre;
            FORM.IdKitRepuesto = kitRepuesto.IdKitRepuesto;

            Franquicias = db.Franquicias.OrderBy(x => x.NombreFranquicia).Select(x => new SelectListItem()
            {
                Text = x.NombreFranquicia,
                Value = x.IdFranquicia.ToString()
            });

            Clientes = db.Clientes.OrderBy(x => x.NombreEmpresa).Where(x => x.IdFranquicia == FORM.IdFranquicia).Select(x => new SelectListItem()
            {
                Text = x.NombreEmpresa,
                Value = x.IdCliente.ToString()
            });

            Sucursales = db.Sucursals.OrderBy(x => x.NombreSucursal).Where(x => x.IdSucursal == FORM.IdSucursal).Select(x => new SelectListItem()
            {
                Text = x.NombreSucursal,
                Value = x.IdSucursal.ToString()
            });

            repuestosAsociados = db.KitRepuestoRepuestos.Where(x => x.IdKitRepuesto == kitRepuesto.IdKitRepuesto).Select(x => x.Repuesto).ToList();
            repuestosTotales = db.Repuestos.OrderBy(x => x.Codigo).ToList();
            repuestosDisponibles = repuestosTotales.Except(repuestosAsociados).Union(repuestosAsociados.Except(repuestosTotales)).ToList();
        }

        public KitRepuestosViewModel(KitRepuestosFormModel form)
        {
            FORM = form;
        }

        /**
         * Funciones CRUD
        **/

        public void create()
        {
            Equipo e = db.Equipos.Single(x => x.IdEquipo == FORM.IdEquipo);

            KitRepuesto nuevo = new KitRepuesto
            {
                IdKitRepuesto = Guid.NewGuid(),
                Equipo = e,
                Nombre = FORM.Nombre,
                CreadoPor = FORM.userName,
                FechaIngreso = DateTime.Now
            };
            db.KitRepuestos.InsertOnSubmit(nuevo);
            db.SubmitChanges();
        }

        public void update()
        {
            KitRepuesto k = db.KitRepuestos.Single(x => x.IdKitRepuesto == FORM.IdKitRepuesto);
            k.Nombre = FORM.Nombre;
            db.SubmitChanges();
        }

        public void delete()
        {
            KitRepuesto k = db.KitRepuestos.Single(x => x.IdKitRepuesto == FORM.IdKitRepuesto);
            IQueryable<KitRepuestoRepuesto> vs = db.KitRepuestoRepuestos.Where(x => x.IdKitRepuesto == k.IdKitRepuesto);
            db.KitRepuestoRepuestos.DeleteAllOnSubmit(vs);
            db.KitRepuestos.DeleteOnSubmit(k);
            db.SubmitChanges();
        }

        public void addRepuestos()
        {
            List<Guid> repuestosIds = FORM.RepuestosIds == null ? new List<Guid> { Guid.Empty } : FORM.RepuestosIds.Split(';').Select(s => Guid.Parse(s)).ToList();
            List<Guid> elementosTotales = db.KitRepuestoRepuestos.Where(x => x.IdKitRepuesto == FORM.IdKitRepuesto).Select(x => x.IdRepuesto).ToList();
            List<Guid> elementosAEliminarIds = elementosTotales.Except(repuestosIds).Union(repuestosIds.Except(elementosTotales)).ToList();
            List<KitRepuestoRepuesto> elementosAEliminar = db.KitRepuestoRepuestos.Where(x => elementosAEliminarIds.Contains(x.IdRepuesto) && x.IdKitRepuesto == FORM.IdKitRepuesto).ToList();

            db.KitRepuestoRepuestos.DeleteAllOnSubmit(elementosAEliminar);

            KitRepuestoRepuesto newKitRepuestoRepuesto;
            KitRepuesto kitRepuesto1 = db.KitRepuestos.Single(x => x.IdKitRepuesto == FORM.IdKitRepuesto);
            Repuesto repuesto1;

            IQueryable<KitRepuestoRepuesto> kitRepuestoR = db.KitRepuestoRepuestos.Where(x => x.IdKitRepuesto == FORM.IdKitRepuesto);

            for (int i = 0; i < repuestosIds.Count(); i++)
            {
                if (kitRepuestoR.Any(x => x.IdRepuesto == repuestosIds[i]) || repuestosIds[i] == Guid.Empty)
                {
                    continue;
                }
                else
                {
                    repuesto1 = db.Repuestos.Single(x => x.IdRepuesto == repuestosIds[i]);

                    newKitRepuestoRepuesto = new KitRepuestoRepuesto
                    {
                        IdKitRepuestoRepuesto = Guid.NewGuid(),
                        KitRepuesto = kitRepuesto1,
                        Repuesto = repuesto1
                    };

                    db.KitRepuestoRepuestos.InsertOnSubmit(newKitRepuestoRepuesto);
                }
            }
            db.SubmitChanges();
        }

        /**
         * FIN Funciones CRUD
        **/
    }
}