﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using MetricArts.JnJ.Mantenciones.Models.Mantenedores.Usuarios.Users;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.Usuarios
{
    public class UsuariosFormModel
    {
        public Guid IdUsuario { get; set; }

        [Display(Name = "Nombres")]
        public string Nombres { get; set; }

        [Display(Name = "Apellido Paterno")]
        public string ApellidoPaterno { get; set; }

        [Display(Name = "Apellido Materno")]
        public string ApellidoMaterno { get; set; }

        [Display(Name = "Telefono Contacto")]
        public string TelefonoContacto { get; set; }

        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [Display(Name = "Antigua Contraseña")]
        public string OldPassword { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Nombre Usuario")]
        public string NombreUsuario { get; set; }

        [Display(Name = "Roles de Usuario")]
        public List<RolesSeleccionDTO> RolesSeleccionados { get; set; }

        [Display(Name = "Cambiar Contraseña?")]
        public bool PasswordChange { get; set; }

        public string UserName { get; set; }
    }
}