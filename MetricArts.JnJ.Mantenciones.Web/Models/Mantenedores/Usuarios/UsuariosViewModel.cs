﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;
using MetricArts.JnJ.Mantenciones.Models.Mantenedores.Usuarios.Users;
using MetricArts.JnJ.Mantenciones.Models.Accounts.Account;
using System.Web.Security;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.Usuarios
{
    public class UsuariosViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public UsuariosFormModel FORM { get; set; }
        public IEnumerable<Usuario> Users { get; set; }
        public Usuario user { get; set; }
        public List<RolesSeleccionDTO> userRoles { get; set; }

        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }

        public UsuariosViewModel() 
        {
            Users = db.Usuarios.OrderBy(x => x.Nombre).Where(x => x.EsHabilitado == true);
        }

        public UsuariosViewModel(string userName)
        {
            FORM = new UsuariosFormModel();
            FORM.RolesSeleccionados = getAllRoles();
        }

        public UsuariosViewModel(Guid IdUsuario)
        {
            user = db.Usuarios.Where(x => x.IdUsuario == IdUsuario).SingleOrDefault();
            FORM = new UsuariosFormModel();
            FORM.Nombres = user.Nombre;
            FORM.NombreUsuario = user.NombreUsuario;
            FORM.ApellidoPaterno = user.ApellidoPaterno;
            FORM.ApellidoMaterno = user.ApellidoMaterno;
            FORM.TelefonoContacto = user.Telefono;
            FORM.Email = user.CorreoElectronico;
            FORM.IdUsuario = user.IdUsuario;
            FORM.RolesSeleccionados = getAllRoles(user.IdUsuario); 
        }

        private List<RolesSeleccionDTO> getAllRoles()
        {
            List<TipoUsuario> roles = db.TipoUsuarios.ToList();
            userRoles = new List<RolesSeleccionDTO>();
            RolesSeleccionDTO dto;

            foreach (TipoUsuario rol in roles)
            {
                dto = new RolesSeleccionDTO
                {
                    RolSeleccionado = rol.Nombre,
                    Checked = true,
                    IdRolSeleccionado = rol.IdTipoUsuario
                };
                userRoles.Add(dto);
            }
            return userRoles;
        }

        private List<RolesSeleccionDTO> getAllRoles(Guid IdUsuario)
        {
            IQueryable<TipoUsuario> roles = db.TipoUsuarios;
            List<int?> rolesUsuario = db.UsuarioTipoUsuarios.Where(x => x.IdUsuario == IdUsuario).Select(x => x.IdTipoUsuario).ToList();
            userRoles = new List<RolesSeleccionDTO>();

            RolesSeleccionDTO dto;

            foreach (TipoUsuario rol in roles)
            {
                dto = new RolesSeleccionDTO
                {
                    RolSeleccionado = rol.Nombre,
                    Checked = rolesUsuario.Contains(rol.IdTipoUsuario),
                    IdRolSeleccionado = rol.IdTipoUsuario
                };
                userRoles.Add(dto);
            }
            return userRoles;
        }

        public UsuariosViewModel(UsuariosFormModel form)
        {
            FORM = form;
        }

        public bool tieneMantenciones(Usuario u) 
        {
            if(u.UsuarioTipoUsuarios.Any(x => x.IdTipoUsuario == 2))
            {
                Ingeniero i = db.Ingenieros.Single(x => x.IdUsuario == u.IdUsuario);
                return db.Mantencions.Any(x => x.Ingeniero == i);
            }
            return false;
        }

        /**
         * Funciones CRUD
        **/
        private void initializeMembershipContext()
        {
            if (FormsService == null) { FormsService = new FormsAuthenticationService(); }
            if (MembershipService == null) { MembershipService = new AccountMembershipService(); }
        }

        public string createMembership()
        {
            initializeMembershipContext();
            MembershipCreateStatus createStatus = MembershipService.CreateUser(FORM.NombreUsuario.ToLower(), FORM.Password, FORM.Email);

            string errorMessage = string.Empty;

            if (createStatus == MembershipCreateStatus.DuplicateUserName)
                errorMessage = "Nombre de usuario duplicado. Debe escoger uno diferente";
            if (createStatus == MembershipCreateStatus.DuplicateEmail)
                errorMessage = "Email de usuario duplicado. Debe escoger uno diferente";
            if (createStatus == MembershipCreateStatus.InvalidPassword)
                errorMessage = "Contraseña invalida. Asegurese de que el largo sea al menos de 6 caracteres.";

            return errorMessage;
        }


        public void create()
        {
            Usuario newUser = new Usuario
            {
                IdUsuario = Guid.NewGuid(),
                Nombre = FORM.Nombres,
                NombreUsuario = FORM.NombreUsuario.ToLower(),
                ApellidoPaterno = FORM.ApellidoPaterno,
                ApellidoMaterno = FORM.ApellidoMaterno ?? "",
                Telefono = FORM.TelefonoContacto ?? "",
                CorreoElectronico = FORM.Email,
                EsHabilitado = true,
                CreadoPor = FORM.UserName,
                FechaCreacion = DateTime.Now
            };

            List<RolesSeleccionDTO> rolesSeleccionados = FORM.RolesSeleccionados.Where(x => x.Checked).ToList();
            List<UsuarioTipoUsuario> rolesUsuario = new List<UsuarioTipoUsuario>();
            UsuarioTipoUsuario ru;
            TipoUsuario rol;
            List<string> rolesStr = new List<string>();

            foreach (RolesSeleccionDTO rs in rolesSeleccionados)
            {
                rol = db.TipoUsuarios.Single(x => x.IdTipoUsuario == rs.IdRolSeleccionado);

                ru = new UsuarioTipoUsuario
                {
                    IdUsuarioTipoUsuario = Guid.NewGuid(),
                    TipoUsuario = rol,
                    Usuario = newUser
                };

                if(rol.IdTipoUsuario == 2) 
                {
                    createIngeniero(newUser);
                }

                rolesStr.Add(rol.Nombre.ToLower());
                rolesUsuario.Add(ru);
            }

            Roles.AddUserToRoles(FORM.NombreUsuario.ToLower(), rolesStr.ToArray());
            db.UsuarioTipoUsuarios.InsertAllOnSubmit(rolesUsuario);
            db.SubmitChanges();
        }

        private void createIngeniero(Usuario newUser) 
        {
            Ingeniero newIngeniero = new Ingeniero {
                IdIngeniero = Guid.NewGuid(),
                Usuario = newUser,
                Habilitado = true
            };

            db.Ingenieros.InsertOnSubmit(newIngeniero);
        }

        public void update()
        {
            Usuario u = db.Usuarios.Where(x => x.IdUsuario == FORM.IdUsuario).SingleOrDefault();
            u.Nombre = FORM.Nombres;
            u.ApellidoPaterno = FORM.ApellidoPaterno;
            u.ApellidoMaterno = FORM.ApellidoMaterno ?? "";
            u.Telefono = FORM.TelefonoContacto ?? "";
            u.CorreoElectronico = FORM.Email;

            MembershipUser mUser = Membership.GetUser(FORM.NombreUsuario.ToLower());
            mUser.Email = FORM.Email;
            Membership.UpdateUser(mUser);

            db.SubmitChanges();
        }

        public void updateUser()
        {
            Usuario usuario = db.Usuarios.Single(x => x.IdUsuario == FORM.IdUsuario);
            List<int?> rolesUsuario = db.UsuarioTipoUsuarios.Where(x => x.IdUsuario == usuario.IdUsuario).Select(x => x.IdTipoUsuario).ToList();
            UsuarioTipoUsuario usuarioRol;
            TipoUsuario r;

            for (int i = 0; i < FORM.RolesSeleccionados.Count(); i++)
            {
                r = db.TipoUsuarios.Single(x => x.IdTipoUsuario == FORM.RolesSeleccionados[i].IdRolSeleccionado);

                if (FORM.RolesSeleccionados[i].Checked)
                {
                    if (!rolesUsuario.Contains(r.IdTipoUsuario))
                    {
                        usuarioRol = new UsuarioTipoUsuario
                        {
                            IdUsuarioTipoUsuario = Guid.NewGuid(),
                            Usuario = usuario,
                            TipoUsuario = r
                        };

                        if (r.IdTipoUsuario == 2)
                        {
                            Ingeniero ing = db.Ingenieros.SingleOrDefault(x => x.IdUsuario == usuarioRol.IdUsuario);
                            
                            if (ing != null)
                            {
                                ing.Habilitado = true;
                            }
                            else 
                            {
                                ing = new Ingeniero {
                                    IdIngeniero = Guid.NewGuid(),
                                    Usuario = usuario,
                                    Habilitado = true
                                };

                                db.Ingenieros.InsertOnSubmit(ing);
                            }
                        }

                        db.UsuarioTipoUsuarios.InsertOnSubmit(usuarioRol);
                    }
                }
                else
                {
                    if (rolesUsuario.Contains(r.IdTipoUsuario))
                    {
                        usuarioRol = db.UsuarioTipoUsuarios.Single(x => x.TipoUsuario == r && x.Usuario == usuario);

                        if(r.IdTipoUsuario == 2) 
                        {
                            Ingeniero ing = db.Ingenieros.SingleOrDefault(x => x.IdUsuario == usuarioRol.IdUsuario);
                            if(ing != null){ ing.Habilitado = false; }
                        }

                        db.UsuarioTipoUsuarios.DeleteOnSubmit(usuarioRol);
                    }
                }
            }

            db.SubmitChanges();
        }

        public string updateMembership()
        {
            Usuario usuario = db.Usuarios.Single(x => x.IdUsuario == FORM.IdUsuario);

            initializeMembershipContext();
            string errorMessage = string.Empty;

            TipoUsuario r;
            string[] rolesUsuario = Roles.GetRolesForUser(usuario.NombreUsuario.ToLower());
            rolesUsuario = rolesUsuario.Select(s => s.ToLowerInvariant()).ToArray();

            for (int i = 0; i < FORM.RolesSeleccionados.Count(); i++)
            {
                r = db.TipoUsuarios.Single(x => x.IdTipoUsuario == FORM.RolesSeleccionados[i].IdRolSeleccionado);

                if (FORM.RolesSeleccionados[i].Checked)
                {
                    if (!rolesUsuario.Contains(r.Nombre.ToLower()))
                    {
                        Roles.AddUserToRole(usuario.NombreUsuario.ToLower(), r.Nombre.ToLower());
                    }
                }
                else
                {
                    if (rolesUsuario.Contains(FORM.RolesSeleccionados[i].RolSeleccionado.ToLower()))
                    {
                        Roles.RemoveUserFromRole(usuario.NombreUsuario.ToLower(), r.Nombre.ToLower());
                    }
                }
            }

            return errorMessage;
        }

        public void delete()
        {
            List<UsuarioTipoUsuario> ur = db.UsuarioTipoUsuarios.Where(x => x.IdUsuario == FORM.IdUsuario).ToList();

            db.UsuarioTipoUsuarios.DeleteAllOnSubmit(ur);

            Usuario u = db.Usuarios.Single(x => x.IdUsuario == FORM.IdUsuario);

            Ingeniero ing = db.Ingenieros.SingleOrDefault(x => x.IdUsuario == u.IdUsuario);

            if (ing != null) { db.Ingenieros.DeleteOnSubmit(ing); }

            //u.EsHabilitado = false;

            db.Usuarios.DeleteOnSubmit(u);

            deleteMembership(u);

            db.SubmitChanges();
        }

        private void deleteMembership(Usuario u)
        {
            Membership.DeleteUser(u.NombreUsuario.ToLower(), true);
        }

        public void resetPassword()
        {
            Usuario u = db.Usuarios.Single(x => x.IdUsuario == FORM.IdUsuario);
            MembershipUser user = Membership.GetUser(u.NombreUsuario.ToLower());
            user.IsApproved = true;
            user.UnlockUser();
            string buff = user.ResetPassword();
            user.ChangePassword(buff, u.NombreUsuario.ToLower() + "@ocd");
            Membership.UpdateUser(user);
        }

        /**
         * FIN Funciones CRUD
        **/
    }
}