﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.Usuarios.Users
{
    public class RolesSeleccionDTO
    {
        public string RolSeleccionado { get; set; }
        public bool Checked { get; set; }
        public int IdRolSeleccionado { get; set; }
    }
}