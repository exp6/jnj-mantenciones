﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.Clientes
{
    public class ClientesViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public ClientesFormModel FORM { get; set; }
        public IEnumerable<Cliente> Clientes { get; set; }
        public IEnumerable<SelectListItem> Franquicias { get; set; }
        public IEnumerable<SelectListItem> TipoClientes { get; set; }
        public Cliente cliente { get; set; }

        public ClientesViewModel() 
        {
            Clientes = db.Clientes.OrderBy(x => x.NombreEmpresa);
        }

        public ClientesViewModel(string userName)
        {
            Franquicias = db.Franquicias.OrderBy(x => x.NombreFranquicia).Select(x => new SelectListItem()
            {
                Text = x.NombreFranquicia,
                Value = x.IdFranquicia.ToString()
            });

            TipoClientes = db.TipoClientes.OrderBy(x => x.Codigo).Select(x => new SelectListItem()
            {
                Text = x.Nombre,
                Value = x.IdTipoCliente.ToString()
            });
        }

        public ClientesViewModel(Guid idCliente)
        {
            cliente = db.Clientes.Single(x => x.IdCliente == idCliente);
            FORM = new ClientesFormModel();
            FORM.IdCliente = cliente.IdCliente;
            FORM.NombreEmpresa = cliente.NombreEmpresa;
            FORM.Descripcion = cliente.Descripcion;
            FORM.PaginaWeb = cliente.PaginaWeb;
            FORM.CodigoCliente = cliente.CodigoCliente;
            FORM.RepresentanteCuenta = cliente.RepresentanteCuenta;
            FORM.CorreoRepresentante = cliente.CorreoRepresentante;
            FORM.IdFranquicia = cliente.IdFranquicia;
            FORM.IdTipoCliente = cliente.IdTipoCliente;

            Franquicias = db.Franquicias.OrderBy(x => x.NombreFranquicia).Select(x => new SelectListItem()
            {
                Text = x.NombreFranquicia,
                Value = x.IdFranquicia.ToString()
            });

            TipoClientes = db.TipoClientes.OrderBy(x => x.Codigo).Select(x => new SelectListItem()
            {
                Text = x.Nombre,
                Value = x.IdTipoCliente.ToString()
            });
        }

        public ClientesViewModel(ClientesFormModel form)
        {
            FORM = form;
        }

        /**
         * Funciones CRUD
        **/

        public void create()
        {
            Franquicia f = db.Franquicias.Single(x => x.IdFranquicia == FORM.IdFranquicia);
            TipoCliente tc = db.TipoClientes.Single(x => x.IdTipoCliente == FORM.IdTipoCliente);

            Cliente nuevo = new Cliente
            {
                IdCliente = Guid.NewGuid(),
                Franquicia = f,
                TipoCliente = tc,
                NombreEmpresa = FORM.NombreEmpresa,
                Descripcion = FORM.Descripcion,
                PaginaWeb = FORM.PaginaWeb,
                CodigoCliente = FORM.CodigoCliente,
                RepresentanteCuenta = FORM.RepresentanteCuenta,
                CorreoRepresentante = FORM.CorreoRepresentante,
                FechaCreacion = DateTime.Now
            };
            db.Clientes.InsertOnSubmit(nuevo);
            db.SubmitChanges();
        }

        public void update()
        {
            Franquicia f = db.Franquicias.Single(x => x.IdFranquicia == FORM.IdFranquicia);
            TipoCliente tc = db.TipoClientes.Single(x => x.IdTipoCliente == FORM.IdTipoCliente);

            Cliente c = db.Clientes.Single(x => x.IdCliente == FORM.IdCliente);
            c.Franquicia = f;
            c.TipoCliente = tc;
            c.NombreEmpresa = FORM.NombreEmpresa;
            c.Descripcion = FORM.Descripcion ?? "";
            c.PaginaWeb = FORM.PaginaWeb;
            c.CodigoCliente = FORM.CodigoCliente;
            c.RepresentanteCuenta = FORM.RepresentanteCuenta;
            c.CorreoRepresentante = FORM.CorreoRepresentante;

            db.SubmitChanges();
        }

        public void delete()
        {
            Cliente c = db.Clientes.Single(x => x.IdCliente == FORM.IdCliente);
            db.Clientes.DeleteOnSubmit(c);
            db.SubmitChanges();
        }

        /**
         * FIN Funciones CRUD
        **/
    }
}