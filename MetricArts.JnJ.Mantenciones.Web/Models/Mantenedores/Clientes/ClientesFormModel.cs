﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.Clientes
{
    public class ClientesFormModel
    {
        [DisplayName("Nombre")]
        public string NombreEmpresa { get; set; }

        [DisplayName("Descripción")]
        public string Descripcion { get; set; }

        [DisplayName("Pagina Web")]
        public string PaginaWeb { get; set; }

        [DisplayName("Código Cliente")]
        public string CodigoCliente { get; set; }

        [DisplayName("Nombre Representante")]
        public string RepresentanteCuenta { get; set; }

        [DisplayName("Email Representante")]
        public string CorreoRepresentante { get; set; }

        [DisplayName("Franquicia")]
        public Guid IdFranquicia { get; set; }

        [DisplayName("Tipo Cliente")]
        public Guid IdTipoCliente { get; set; }

        public Guid IdCliente { get; set; }

        public string userName { get; set; }
    }
}