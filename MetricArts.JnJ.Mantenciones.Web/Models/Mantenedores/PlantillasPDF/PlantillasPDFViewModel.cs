﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.PlantillasPDF
{
    public class PlantillasPDFViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public PlantillasPDFFormModel FORM { get; set; }
        public IEnumerable<PDFTemplate> PlantillasPDF { get; set; }
        public PDFTemplate plantillaPDF { get; set; }
        public IEnumerable<SelectListItem> TiposNotificacion { get; set; }
        public IEnumerable<MapTag> Tags { get; set; }

        public PlantillasPDFViewModel() 
        {
            PlantillasPDF = db.PDFTemplates.OrderBy(x => x.Nombre);
        }

        public PlantillasPDFViewModel(string userName)
        {
            TiposNotificacion = db.TipoNotificacions.OrderBy(x => x.Codigo).Select(x => new SelectListItem()
            {
                Text = x.Nombre,
                Value = x.IdTipoNotificacion.ToString()
            });

            Tags = db.MapTags;
        }

        public PlantillasPDFViewModel(Guid idPlantilla)
        {
            plantillaPDF = db.PDFTemplates.Single(x => x.IdPDFTemplate == idPlantilla);
            FORM = new PlantillasPDFFormModel();
            FORM.IdPlantilla = plantillaPDF.IdPDFTemplate;
            FORM.Nombre = plantillaPDF.Nombre;
            FORM.Descripcion = plantillaPDF.Descripcion;
            FORM.cuerpoHTML = plantillaPDF.CuerpoHTML;
            FORM.IdTipoNotificacion = plantillaPDF.IdTipoNotificacion.Value;

            TiposNotificacion = db.TipoNotificacions.OrderBy(x => x.Codigo).Select(x => new SelectListItem()
            {
                Text = x.Nombre,
                Value = x.IdTipoNotificacion.ToString()
            });

            Tags = db.MapTags;
        }

        public PlantillasPDFViewModel(PlantillasPDFFormModel form)
        {
            FORM = form;
        }

        public string getPlantillaHtml(Guid IdPlantilla)
        {
            return db.PDFTemplates.Single(x => x.IdPDFTemplate == IdPlantilla).CuerpoHTML;
        }

        /**
        * Funciones CRUD
       **/

        public void create()
        {
            TipoNotificacion t = db.TipoNotificacions.Single(x => x.IdTipoNotificacion == FORM.IdTipoNotificacion);

            PDFTemplate nuevo = new PDFTemplate
            {
                IdPDFTemplate = Guid.NewGuid(),
                TipoNotificacion = t,
                Nombre = FORM.Nombre,
                Descripcion = FORM.Descripcion,
                CuerpoHTML = FORM.cuerpoHTML,
                FechaCreacion = DateTime.Now,
                CreadoPor = FORM.userName
            };
            db.PDFTemplates.InsertOnSubmit(nuevo);
            db.SubmitChanges();
        }

        public void update()
        {
            TipoNotificacion t = db.TipoNotificacions.Single(x => x.IdTipoNotificacion == FORM.IdTipoNotificacion);

            PDFTemplate p = db.PDFTemplates.Single(x => x.IdPDFTemplate == FORM.IdPlantilla);
            p.TipoNotificacion = t;
            p.Nombre = FORM.Nombre;
            p.Descripcion = FORM.Descripcion;
            p.CuerpoHTML = FORM.cuerpoHTML;
            db.SubmitChanges();
        }

        public void delete()
        {
            PDFTemplate p = db.PDFTemplates.Single(x => x.IdPDFTemplate == FORM.IdPlantilla);
            db.PDFTemplates.DeleteOnSubmit(p);
            db.SubmitChanges();
        }

        /**
         * FIN Funciones CRUD
        **/
    }
}