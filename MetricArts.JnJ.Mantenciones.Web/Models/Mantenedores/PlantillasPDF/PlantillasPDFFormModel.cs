﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.PlantillasPDF
{
    public class PlantillasPDFFormModel
    {
        [DisplayName("Nombre Plantilla")]
        public string Nombre { get; set; }

        [DisplayName("Descripción")]
        public string Descripcion { get; set; }

        [DisplayName("Cuerpo Plantilla (Puede agregar TAGS en el caso sea necesario generar valores dinámicos en el texto)")]
        public string cuerpoHTML { get; set; }

        public Guid IdTipoNotificacion { get; set; }

        public Guid IdPlantilla { get; set; }

        public string userName { get; set; }
    }
}