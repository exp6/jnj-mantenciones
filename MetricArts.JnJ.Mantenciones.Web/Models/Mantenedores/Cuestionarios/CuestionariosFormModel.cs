﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.Cuestionarios
{
    public class CuestionariosFormModel
    {
        [DisplayName("Nombre Descriptivo")]
        public string Descripcion { get; set; }

        [DisplayName("Encabezado")]
        public string Encabezado { get; set; }

        [DisplayName("Pie")]
        public string Pie { get; set; }

        public string preguntasJson { get; set; }

        public string Preguntas { get; set; }
        public string PreguntasIds { get; set; }

        [DisplayName("¿Incorporar formulario Datos Cliente?")]
        public bool Abierto { get; set; }

        [DisplayName("Tipo Cuestionario")]
        public Guid IdTipoCuestionario { get; set; }

        public Guid IdCuestionario { get; set; }

        public string userName { get; set; }
    }
}