﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Core;
using Newtonsoft.Json;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.Cuestionarios
{
    public class CuestionariosViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public CuestionariosFormModel FORM { get; set; }
        public IEnumerable<Cuestionario> Cuestionarios { get; set; }
        public Cuestionario cuestionario { get; set; }
        public IEnumerable<SelectListItem> TipoCuestionarios { get; set; }
        public List<CuestionarioPregunta> Preguntas { get; set; }
        public IEnumerable<SelectListItem> TipoPreguntas { get; set; }
        public int cantidadPreguntas { get; set; }

        public CuestionariosViewModel() 
        {
            Cuestionarios = db.Cuestionarios.OrderBy(x => x.Descripcion);
        }

        public CuestionariosViewModel(string userName)
        {
            TipoCuestionarios = db.TipoCuestionarios.OrderBy(x => x.Codigo).Select(x => new SelectListItem()
            {
                Text = x.Descripcion,
                Value = x.IdTipoCuestionario.ToString()
            });
        }

        public CuestionariosViewModel(Guid idCuestionario)
        {
            cuestionario = db.Cuestionarios.Single(x => x.IdCuestionario == idCuestionario);
            FORM = new CuestionariosFormModel();
            FORM.IdCuestionario = cuestionario.IdCuestionario;
            FORM.Descripcion = cuestionario.Descripcion;
            FORM.Encabezado = cuestionario.Encabezado;
            FORM.Pie = cuestionario.Pie;
            FORM.IdTipoCuestionario = cuestionario.IdTipoCuestionario.Value;
            FORM.Abierto = cuestionario.Abierto;

            TipoCuestionarios = db.TipoCuestionarios.OrderBy(x => x.Codigo).Select(x => new SelectListItem()
            {
                Text = x.Descripcion,
                Value = x.IdTipoCuestionario.ToString()
            });

            Preguntas = db.CuestionarioPreguntas.OrderBy(x => x.Orden).Where(x => x.IdCuestionario == idCuestionario).ToList();

            TipoPreguntas = db.TipoPreguntas.OrderBy(x => x.Codigo).Select(x => new SelectListItem()
            {
                Text = x.Descripcion,
                Value = x.Codigo.ToString()
            });

            cantidadPreguntas = Preguntas.Count();
        }

        public CuestionariosViewModel(CuestionariosFormModel form)
        {
            FORM = form;
        }

        /**
         * Funciones CRUD
        **/

        public void create()
        {
            TipoCuestionario tc = db.TipoCuestionarios.Single(x => x.IdTipoCuestionario == FORM.IdTipoCuestionario);

            Cuestionario newCuestionario = new Cuestionario
            {
                IdCuestionario = Guid.NewGuid(),
                TipoCuestionario = tc,
                Descripcion = FORM.Descripcion,
                Encabezado = FORM.Encabezado,
                FechaInicio = DateTime.Now,
                FechaTermino = DateTime.Now,
                Abierto = FORM.Abierto,
                Pie = FORM.Pie
            };

            List<PreguntaCheckListJSON> preguntasObj = JsonConvert.DeserializeObject<List<PreguntaCheckListJSON>>(FORM.preguntasJson);

            Pregunta newPregunta;
            List<CuestionarioPregunta> asociacion = new List<CuestionarioPregunta>();
            CuestionarioPregunta cp;
            int counter = 0;
            TipoPregunta tp;
            RespuestaSugerida rs;
            CuestionarioPreguntaRespuesta cpr;

            foreach (PreguntaCheckListJSON p in preguntasObj)
            {
                tp = db.TipoPreguntas.Single(x => x.Codigo == Int32.Parse(p.tipo));

                newPregunta = new Pregunta
                {
                    IdPregunta = Guid.NewGuid(),
                    TipoPregunta = tp,
                    Enunciado = p.enunciado
                };

                cp = new CuestionarioPregunta
                {
                    IdCuestionarioPregunta = Guid.NewGuid(),
                    Cuestionario = newCuestionario,
                    Orden = (Byte?)Byte.Parse((counter) + ""),
                    Pregunta = newPregunta
                };

                asociacion.Add(cp);

                for (int i = 0; i < p.alternativas.Length; i++)
                {
                    rs = new RespuestaSugerida {
                        IdRespuestaSugerida = Guid.NewGuid(),
                        RespuestaTexto = p.alternativas[i]
                    };

                    cpr = new CuestionarioPreguntaRespuesta {
                        IdCuestionarioPreguntaRespuesta = Guid.NewGuid(),
                        CuestionarioPregunta = cp,
                        RespuestaSugerida = rs,
                        Orden = Byte.Parse((i + 1) + "")
                    };

                    db.CuestionarioPreguntaRespuestas.InsertOnSubmit(cpr);
                }

                counter = counter + 1;
            }

            db.Cuestionarios.InsertOnSubmit(newCuestionario);
            db.SubmitChanges();
        }

        public void update()
        {
            Cuestionario c = db.Cuestionarios.Single(x => x.IdCuestionario == FORM.IdCuestionario);
            c.Descripcion = FORM.Descripcion;
            c.Encabezado = FORM.Encabezado;
            c.Pie = FORM.Pie;
            c.Abierto = FORM.Abierto;

            TipoCuestionario tc = db.TipoCuestionarios.Single(x => x.IdTipoCuestionario == FORM.IdTipoCuestionario);

            c.TipoCuestionario = tc;

            List<PreguntaCheckListJSON> preguntasObj = JsonConvert.DeserializeObject<List<PreguntaCheckListJSON>>(FORM.preguntasJson);
            
            Pregunta newPregunta;
            TipoPregunta tp;
            List<CuestionarioPregunta> asociacion = new List<CuestionarioPregunta>();
            CuestionarioPregunta cp;
            byte? maxCode = db.CuestionarioPreguntas.Where(x => x.IdCuestionario == c.IdCuestionario).Max(x => x.Orden);
            RespuestaSugerida rs;
            CuestionarioPreguntaRespuesta cpr;
            int counter;

            //PRIMERO SE ELIMINAN LOS VALORES QUE NO SE ENCUENTRAN EN CONTEXTO

            List<Guid> preguntasIdIn = preguntasObj.Where(x => x.idPregunta != "valor-nuevo").Select(x => Guid.Parse(x.idPregunta)).ToList();
            List<Guid> valoresTotales = db.CuestionarioPreguntas.Where(x => x.IdCuestionario == c.IdCuestionario).Select(x => x.IdCuestionarioPregunta).ToList();
            List<Guid> valoresAEliminarIds = valoresTotales.Except(preguntasIdIn).Union(preguntasIdIn.Except(valoresTotales)).ToList();
            List<CuestionarioPregunta> valoresAEliminar = db.CuestionarioPreguntas.Where(x => valoresAEliminarIds.Contains(x.IdCuestionarioPregunta)).ToList();

            Pregunta p1;
            List<CuestionarioPreguntaRespuesta> cprs1;
            RespuestaSugerida rs1;

            foreach (CuestionarioPregunta cp1 in valoresAEliminar)
            {
                cprs1 = cp1.CuestionarioPreguntaRespuestas.ToList();

                foreach (CuestionarioPreguntaRespuesta cpr1 in cprs1)
                {
                    rs1 = cpr1.RespuestaSugerida;
                    db.RespuestaSugeridas.DeleteOnSubmit(rs1);
                }

                db.CuestionarioPreguntaRespuestas.DeleteAllOnSubmit(cprs1);

                p1 = cp1.Pregunta;
                db.Preguntas.DeleteOnSubmit(p1);
                db.CuestionarioPreguntas.DeleteOnSubmit(cp1);
            }
            
            db.CuestionarioPreguntas.DeleteAllOnSubmit(valoresAEliminar);

            //LUEGO DE ELIMINAR, SE REALIZAN LAS ACTUALIZACIONES EN BD.

            counter = (maxCode.Value > 1 ? (maxCode.Value - 1) : 0);

            foreach (PreguntaCheckListJSON p in preguntasObj)
            {
                if(p.idPregunta == "valor-nuevo") 
                {
                    //ES UNA PREGUNTA NUEVA Y SE INGRESA

                    tp = db.TipoPreguntas.Single(x => x.Codigo == Int32.Parse(p.tipo));

                    newPregunta = new Pregunta
                    {
                        IdPregunta = Guid.NewGuid(),
                        TipoPregunta = tp,
                        Enunciado = p.enunciado
                    };

                    cp = new CuestionarioPregunta
                    {
                        IdCuestionarioPregunta = Guid.NewGuid(),
                        Cuestionario = c,
                        Orden = (Byte?)Byte.Parse((counter) + ""),
                        Pregunta = newPregunta
                    };

                    asociacion.Add(cp);

                    for (int i = 0; i < p.alternativas.Length; i++)
                    {
                        rs = new RespuestaSugerida
                        {
                            IdRespuestaSugerida = Guid.NewGuid(),
                            RespuestaTexto = p.alternativas[i]
                        };

                        cpr = new CuestionarioPreguntaRespuesta
                        {
                            IdCuestionarioPreguntaRespuesta = Guid.NewGuid(),
                            CuestionarioPregunta = cp,
                            RespuestaSugerida = rs,
                            Orden = Byte.Parse((i + 1) + "")
                        };

                        db.CuestionarioPreguntaRespuestas.InsertOnSubmit(cpr);
                    }

                    counter = counter + 1;
                }
                else 
                {
                    //ES UNA PREGUNTA EXISTENTE Y SE ACTUALIZA
                    cp = db.CuestionarioPreguntas.Single(x => x.IdCuestionarioPregunta == Guid.Parse(p.idPregunta));
                    newPregunta = cp.Pregunta;
                    newPregunta.Enunciado = p.enunciado;
                    tp = db.TipoPreguntas.Single(x => x.Codigo == Int32.Parse(p.tipo));

                    newPregunta.TipoPregunta = tp;
                    List<CuestionarioPreguntaRespuesta> cprsTemp;

                    //SI AHORA ES PREGUNTA DE DESARROLLO, SE ELIMINAN LAS ALTERNATIVAS SI ES QUE TUVO PREVIAMENTE
                    if (tp.Codigo == 5) 
                    {
                        cprsTemp = db.CuestionarioPreguntaRespuestas.Where(x => x.IdCuestionarioPregunta == cp.IdCuestionarioPregunta).ToList();

                        if (cprsTemp.Any()) 
                        {
                            foreach (CuestionarioPreguntaRespuesta a in cprsTemp)
                            {
                                RespuestaSugerida rsTemp = db.RespuestaSugeridas.Single(x => x.IdRespuestaSugerida == a.IdRespuestaSugerida);
                                db.RespuestaSugeridas.DeleteOnSubmit(rsTemp);
                            }
                            db.CuestionarioPreguntaRespuestas.DeleteAllOnSubmit(cprsTemp);
                        }
                    }
                    else 
                    {
                        for (int i = 0; i < p.alternativas.Length; i++)
                        {
                            if (p.alternativasId[i] == "valor-nuevo") 
                            {
                                //ES UNA NUEVA ALTERNATIVA Y SE INGRESA
                                rs = new RespuestaSugerida
                                {
                                    IdRespuestaSugerida = Guid.NewGuid(),
                                    RespuestaTexto = p.alternativas[i]
                                };

                                cpr = new CuestionarioPreguntaRespuesta
                                {
                                    IdCuestionarioPreguntaRespuesta = Guid.NewGuid(),
                                    CuestionarioPregunta = cp,
                                    RespuestaSugerida = rs,
                                    Orden = Byte.Parse((i + 1) + "")
                                };

                                db.CuestionarioPreguntaRespuestas.InsertOnSubmit(cpr);
                            }
                            else 
                            {
                                //ES UNA ALTERNATIVA EXISTENTE Y SE ACTUALIZA
                                rs = db.RespuestaSugeridas.Single(x => x.IdRespuestaSugerida == Guid.Parse(p.alternativasId[i]));
                                rs.RespuestaTexto = p.alternativas[i];
                            }
                        }
                    }

                    
                }
            }
            db.SubmitChanges();
        }

        public void delete()
        {
            Cuestionario c = db.Cuestionarios.Single(x => x.IdCuestionario == FORM.IdCuestionario);
            List<CuestionarioPregunta> cps = c.CuestionarioPreguntas.ToList();
            Pregunta p;
            List<CuestionarioPreguntaRespuesta> cprs;
            RespuestaSugerida rs;

            foreach(CuestionarioPregunta cp in cps) {
                cprs = cp.CuestionarioPreguntaRespuestas.ToList();

                foreach(CuestionarioPreguntaRespuesta cpr in cprs) {
                    rs = cpr.RespuestaSugerida;
                    db.RespuestaSugeridas.DeleteOnSubmit(rs);
                }

                db.CuestionarioPreguntaRespuestas.DeleteAllOnSubmit(cprs);

                p = cp.Pregunta;
                db.Preguntas.DeleteOnSubmit(p);
                db.CuestionarioPreguntas.DeleteOnSubmit(cp);
            }

            db.Cuestionarios.DeleteOnSubmit(c);
            db.SubmitChanges();
        }

        /**
         * FIN Funciones CRUD
        **/
    }
}