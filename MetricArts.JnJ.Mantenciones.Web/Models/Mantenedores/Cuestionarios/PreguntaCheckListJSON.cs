﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.Cuestionarios
{
    public class PreguntaCheckListJSON
    {
        public string idPregunta { get; set; }
        public string enunciado { get; set; }
        public string tipo { get; set; }
        public string[] alternativas { get; set; }
        public string[] alternativasId { get; set; }
    }
}