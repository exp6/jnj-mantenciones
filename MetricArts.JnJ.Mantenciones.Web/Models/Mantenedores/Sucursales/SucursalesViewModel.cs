﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.Sucursales
{
    public class SucursalesViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public SucursalesFormModel FORM { get; set; }
        public IEnumerable<Sucursal> Sucursales { get; set; }
        public IEnumerable<SelectListItem> Franquicias { get; set; }
        public IEnumerable<SelectListItem> Clientes { get; set; }
        public IEnumerable<SelectListItem> Regiones { get; set; }
        public IEnumerable<SelectListItem> Comunas { get; set; }
        public Sucursal sucursal { get; set; }

        public SucursalesViewModel() 
        {
            Sucursales = db.Sucursals.OrderBy(x => x.NombreSucursal);
        }

        public SucursalesViewModel(string userName)
        {
            Franquicias = db.Franquicias.OrderBy(x => x.NombreFranquicia).Select(x => new SelectListItem()
            {
                Text = x.NombreFranquicia,
                Value = x.IdFranquicia.ToString()
            });

            Regiones = db.Regions.OrderBy(x => x.Nombre).Where(x => x.Pais.Codigo == 7).Select(x => new SelectListItem()
            {
                Text = x.Codigo + " - " + x.Nombre,
                Value = x.IdRegion.ToString()
            });
        }

        public SucursalesViewModel(Guid idSucursal)
        {
            sucursal = db.Sucursals.Single(x => x.IdSucursal == idSucursal);
            FORM = new SucursalesFormModel();
            FORM.IdSucursal = sucursal.IdSucursal;
            FORM.NombreSucursal = sucursal.NombreSucursal;
            FORM.IdFranquicia = sucursal.Cliente.IdFranquicia;
            FORM.IdCliente = sucursal.IdCliente;
            FORM.IdRegion = sucursal.Comuna == null ? Guid.Empty : sucursal.Comuna.IdRegion;
            FORM.IdComuna = sucursal.IdComuna ?? Guid.Empty;

            Franquicias = db.Franquicias.OrderBy(x => x.NombreFranquicia).Select(x => new SelectListItem()
            {
                Text = x.NombreFranquicia,
                Value = x.IdFranquicia.ToString()
            });

            Clientes = db.Clientes.OrderBy(x => x.NombreEmpresa).Where(x => x.IdFranquicia == FORM.IdFranquicia).Select(x => new SelectListItem()
            {
                Text = x.NombreEmpresa,
                Value = x.IdCliente.ToString()
            });

            Regiones = db.Regions.OrderBy(x => x.Nombre).Where(x => x.Pais.Codigo == 7).Select(x => new SelectListItem()
            {
                Text = x.Codigo + " - " + x.Nombre,
                Value = x.IdRegion.ToString()
            });

            Comunas = db.Comunas.OrderBy(x => x.Nombre).Where(x => x.IdRegion == FORM.IdRegion).Select(x => new SelectListItem()
            {
                Text = x.Nombre,
                Value = x.IdComuna.ToString()
            });
        }

        public SucursalesViewModel(SucursalesFormModel form)
        {
            FORM = form;
        }

        /**
         * Funciones CRUD
        **/

        public void create() 
        {
            Comuna c = db.Comunas.Single(x => x.IdComuna == FORM.IdComuna1);
            Cliente cl = db.Clientes.Single(x => x.IdCliente == FORM.IdCliente1);

            Sucursal nueva = new Sucursal
            {
                IdSucursal = Guid.NewGuid(),
                Comuna = c,
                Cliente = cl,
                NombreSucursal = FORM.NombreSucursal,
                Estado = 1
            };
            db.Sucursals.InsertOnSubmit(nueva);
            db.SubmitChanges();
        }

        public void update()
        {
            Comuna c = db.Comunas.Single(x => x.IdComuna == FORM.IdComuna);
            Cliente cl = db.Clientes.Single(x => x.IdCliente == FORM.IdCliente);

            Sucursal s = db.Sucursals.Single(x => x.IdSucursal == FORM.IdSucursal);
            s.Comuna = c;
            s.Cliente = cl;
            s.NombreSucursal = FORM.NombreSucursal;
            db.SubmitChanges();
        }

        public void delete()
        {
            Sucursal s = db.Sucursals.Single(x => x.IdSucursal == FORM.IdSucursal);
            db.Sucursals.DeleteOnSubmit(s);
            db.SubmitChanges();
        }

        /**
         * FIN Funciones CRUD
        **/
    }
}