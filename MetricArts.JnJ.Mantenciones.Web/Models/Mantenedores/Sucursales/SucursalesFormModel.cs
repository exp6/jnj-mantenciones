﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.Sucursales
{
    public class SucursalesFormModel
    {
        [DisplayName("Nombre")]
        public string NombreSucursal { get; set; }

        [DisplayName("Comuna")]
        public Guid IdComuna { get; set; }

        [DisplayName("Región")]
        public Guid IdRegion { get; set; }

        [DisplayName("Franquicia")]
        public Guid IdFranquicia { get; set; }

        [DisplayName("Cliente")]
        public Guid IdCliente { get; set; }

        public Guid IdCliente1 { get; set; }
        public Guid IdComuna1 { get; set; }

        public Guid IdSucursal { get; set; }

        public string userName { get; set; }
    }
}