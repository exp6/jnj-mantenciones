﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.Repuestos
{
    public class RepuestosViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public RepuestosFormModel FORM { get; set; }
        public IEnumerable<Repuesto> Repuestos { get; set; }
        public Repuesto repuesto { get; set; }
        public List<SelectListItem> Categorias { get; set; }

        public RepuestosViewModel() 
        {
            Repuestos = db.Repuestos.OrderBy(x => x.Codigo);
        }

        public RepuestosViewModel(string userName)
        {
        }

        public RepuestosViewModel(Guid idRepuesto)
        {
            repuesto = db.Repuestos.Single(x => x.IdRepuesto == idRepuesto);
            FORM = new RepuestosFormModel();
            FORM.Categoria = repuesto.Categoria;
            FORM.Codigo = repuesto.Codigo;
            FORM.Cantidad = repuesto.Cantidad;
            FORM.Descripcion = repuesto.Descripcion;
            FORM.IdRepuesto = repuesto.IdRepuesto;

            Categorias = new List<SelectListItem>();

            var item1 = new SelectListItem()
            {
                Text = "Requerido",
                Value = "Requerido"
            };

            var item2 = new SelectListItem()
            {
                Text = "Recomendado",
                Value = "Recomendado"
            };

            Categorias.Add(item1);
            Categorias.Add(item2);
        }

        public RepuestosViewModel(RepuestosFormModel form)
        {
            FORM = form;
        }

        /**
         * Funciones CRUD
        **/

        public void create() 
        {
            Repuesto nueva = new Repuesto
            {
                IdRepuesto = Guid.NewGuid(),
                Categoria = FORM.Categoria,
                Codigo = FORM.Codigo,
                Cantidad = FORM.Cantidad,
                Descripcion = FORM.Descripcion
            };
            db.Repuestos.InsertOnSubmit(nueva);
            db.SubmitChanges();
        }

        public void update()
        {
            Repuesto r = db.Repuestos.Single(x => x.IdRepuesto == FORM.IdRepuesto);
            r.Categoria = FORM.Categoria;
            r.Codigo = FORM.Codigo;
            r.Cantidad = FORM.Cantidad;
            r.Descripcion = FORM.Descripcion;
            db.SubmitChanges();
        }

        public void delete()
        {
            Repuesto r = db.Repuestos.Single(x => x.IdRepuesto == FORM.IdRepuesto);
            db.Repuestos.DeleteOnSubmit(r);
            db.SubmitChanges();
        }

        /**
         * FIN Funciones CRUD
        **/
    }
}