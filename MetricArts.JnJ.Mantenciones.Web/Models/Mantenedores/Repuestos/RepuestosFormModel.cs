﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.Repuestos
{
    public class RepuestosFormModel
    {
        [DisplayName("Categoría")]
        public string Categoria { get; set; }

        [DisplayName("Código")]
        public string Codigo { get; set; }

        [DisplayName("Cantidad")]
        public int Cantidad { get; set; }

        [DisplayName("Descripción")]
        public string Descripcion { get; set; }

        public Guid IdRepuesto { get; set; }

        public string userName { get; set; }
    }
}