﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.PlantillasCorreo
{
    public class PlantillasCorreoViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public PlantillasCorreoFormModel FORM { get; set; }
        public IEnumerable<MailTemplate> PlantillasCorreo { get; set; }
        public MailTemplate plantillaCorreo { get; set; }
        public IEnumerable<SelectListItem> TiposNotificacion { get; set; }
        public IEnumerable<MapTag> Tags { get; set; }

        public PlantillasCorreoViewModel() 
        {
            PlantillasCorreo = db.MailTemplates.OrderBy(x => x.Nombre);
        }

        public PlantillasCorreoViewModel(string userName)
        {
            TiposNotificacion = db.TipoNotificacions.OrderBy(x => x.Codigo).Select(x => new SelectListItem()
            {
                Text = x.Nombre,
                Value = x.IdTipoNotificacion.ToString()
            });

            Tags = db.MapTags;
        }

        public PlantillasCorreoViewModel(Guid idPlantilla)
        {
            plantillaCorreo = db.MailTemplates.Single(x => x.IdMailTemplate == idPlantilla);
            FORM = new PlantillasCorreoFormModel();
            FORM.IdPlantilla = plantillaCorreo.IdMailTemplate;
            FORM.Nombre = plantillaCorreo.Nombre;
            FORM.Descripcion = plantillaCorreo.Descripcion;
            FORM.cuerpoHTML = plantillaCorreo.CuerpoHTML;
            FORM.IdTipoNotificacion = plantillaCorreo.IdTipoNotificacion.Value;

            TiposNotificacion = db.TipoNotificacions.OrderBy(x => x.Codigo).Select(x => new SelectListItem()
            {
                Text = x.Nombre,
                Value = x.IdTipoNotificacion.ToString()
            });

            Tags = db.MapTags;
        }

        public PlantillasCorreoViewModel(PlantillasCorreoFormModel form)
        {
            FORM = form;
        }

        public string getPlantillaHtml(Guid IdPlantilla)
        {
            return db.MailTemplates.Single(x => x.IdMailTemplate == IdPlantilla).CuerpoHTML;
        }

        /**
        * Funciones CRUD
       **/

        public void create()
        {
            TipoNotificacion t = db.TipoNotificacions.Single(x => x.IdTipoNotificacion == FORM.IdTipoNotificacion);

            MailTemplate nuevo = new MailTemplate
            {
                IdMailTemplate = Guid.NewGuid(),
                TipoNotificacion = t,
                Nombre = FORM.Nombre,
                Descripcion = FORM.Descripcion,
                Asunto = FORM.Asunto,
                CuerpoHTML = FORM.cuerpoHTML,
                FechaCreacion = DateTime.Now,
                CreadoPor = FORM.userName
            };
            db.MailTemplates.InsertOnSubmit(nuevo);
            db.SubmitChanges();
        }

        public void update()
        {
            TipoNotificacion t = db.TipoNotificacions.Single(x => x.IdTipoNotificacion == FORM.IdTipoNotificacion);

            MailTemplate p = db.MailTemplates.Single(x => x.IdMailTemplate == FORM.IdPlantilla);
            p.TipoNotificacion = t;
            p.Nombre = FORM.Nombre;
            p.Descripcion = FORM.Descripcion;
            p.Asunto = FORM.Asunto;
            p.CuerpoHTML = FORM.cuerpoHTML;
            db.SubmitChanges();
        }

        public void delete()
        {
            MailTemplate p = db.MailTemplates.Single(x => x.IdMailTemplate == FORM.IdPlantilla);
            db.MailTemplates.DeleteOnSubmit(p);
            db.SubmitChanges();
        }

        /**
         * FIN Funciones CRUD
        **/
    }
}
