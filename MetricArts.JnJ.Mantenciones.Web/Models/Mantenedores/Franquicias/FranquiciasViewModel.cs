﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.Franquicias
{
    public class FranquiciasViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public FranquiciasFormModel FORM { get; set; }
        public IEnumerable<Franquicia> Franquicias { get; set; }
        public Franquicia franquicia { get; set; }

        public FranquiciasViewModel() 
        {
            Franquicias = db.Franquicias.OrderBy(x => x.NombreFranquicia);
        }

        public FranquiciasViewModel(string userName)
        {
        }

        public FranquiciasViewModel(Guid idFranquicia)
        {
            franquicia = db.Franquicias.Where(x => x.IdFranquicia == idFranquicia).SingleOrDefault();
            FORM = new FranquiciasFormModel();
            FORM.Nombre = franquicia.NombreFranquicia;
            FORM.Descripcion = franquicia.Descripcion;
            FORM.IdFranquicia = franquicia.IdFranquicia;
        }

        public FranquiciasViewModel(FranquiciasFormModel form)
        {
            FORM = form;
        }

        /**
         * Funciones CRUD
        **/

        public void create() 
        {
            Franquicia nueva = new Franquicia
            {
                IdFranquicia = Guid.NewGuid(),
                NombreFranquicia = FORM.Nombre,
                Codigo = db.Franquicias.Count() + 1,
                Descripcion = FORM.Descripcion,
                CreadoPor = FORM.userName,
                FechaIngreso = DateTime.Now
            };
            db.Franquicias.InsertOnSubmit(nueva);
            db.SubmitChanges();
        }

        public void update()
        {
            Franquicia f = db.Franquicias.Where(x => x.IdFranquicia == FORM.IdFranquicia).SingleOrDefault();
            f.NombreFranquicia = FORM.Nombre;
            f.Descripcion = FORM.Descripcion ?? "";
            db.SubmitChanges();
        }

        public void delete()
        {
            Franquicia f = db.Franquicias.Where(x => x.IdFranquicia == FORM.IdFranquicia).SingleOrDefault();
            db.Franquicias.DeleteOnSubmit(f);
            db.SubmitChanges();
        }

        /**
         * FIN Funciones CRUD
        **/
    }
}