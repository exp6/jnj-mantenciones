﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.Franquicias
{
    public class FranquiciasFormModel
    {
        [DisplayName("Nombre")]
        public string Nombre { get; set; }

        [DisplayName("Descripción")]
        public string Descripcion { get; set; }

        public Guid IdFranquicia { get; set; }

        public string userName { get; set; }
    }
}