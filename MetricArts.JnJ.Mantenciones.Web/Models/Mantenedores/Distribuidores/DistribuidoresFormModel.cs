﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.Distribuidores
{
    public class DistribuidoresFormModel
    {
        [DisplayName("Nombre")]
        public string Nombre { get; set; }

        [DisplayName("Correo Contacto")]
        public string CorreoContacto { get; set; }

        [DisplayName("Teléfono")]
        public string Telefono { get; set; }

        public Guid IdDistribuidorRepuestos { get; set; }

        public string userName { get; set; }
    }
}