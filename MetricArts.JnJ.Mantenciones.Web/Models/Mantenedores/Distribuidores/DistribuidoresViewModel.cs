﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.Distribuidores
{
    public class DistribuidoresViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public DistribuidoresFormModel FORM { get; set; }
        public IEnumerable<DistribuidorRepuesto> Distribuidores { get; set; }
        public DistribuidorRepuesto distribuidor { get; set; }

        public DistribuidoresViewModel() 
        {
            Distribuidores = db.DistribuidorRepuestos.OrderBy(x => x.Nombre);
        }

        public DistribuidoresViewModel(string userName)
        {
        }

        public DistribuidoresViewModel(Guid idDistribuidor)
        {
            distribuidor = db.DistribuidorRepuestos.Single(x => x.IdDistribuidorRepuestos == idDistribuidor);
            FORM = new DistribuidoresFormModel();
            FORM.IdDistribuidorRepuestos = distribuidor.IdDistribuidorRepuestos;
            FORM.Nombre = distribuidor.Nombre;
            FORM.CorreoContacto = distribuidor.CorreoContacto;
            FORM.Telefono = distribuidor.Telefono;
        }

        public DistribuidoresViewModel(DistribuidoresFormModel form)
        {
            FORM = form;
        }

        /**
         * Funciones CRUD
        **/

        public void create()
        {
            DistribuidorRepuesto nuevo = new DistribuidorRepuesto
            {
                IdDistribuidorRepuestos = Guid.NewGuid(),
                Nombre = FORM.Nombre,
                CorreoContacto = FORM.CorreoContacto,
                Telefono = FORM.Telefono,
                Estado = 1
            };
            db.DistribuidorRepuestos.InsertOnSubmit(nuevo);
            db.SubmitChanges();
        }

        public void update()
        {
            DistribuidorRepuesto d = db.DistribuidorRepuestos.Single(x => x.IdDistribuidorRepuestos == FORM.IdDistribuidorRepuestos);
            d.Nombre = FORM.Nombre;
            d.CorreoContacto = FORM.CorreoContacto;
            d.Telefono = FORM.Telefono;
            db.SubmitChanges();
        }

        public void delete()
        {
            DistribuidorRepuesto d = db.DistribuidorRepuestos.Single(x => x.IdDistribuidorRepuestos == FORM.IdDistribuidorRepuestos);
            db.DistribuidorRepuestos.DeleteOnSubmit(d);
            db.SubmitChanges();
        }

        /**
         * FIN Funciones CRUD
        **/
    }
}