﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.CalendarioFeriados
{
    public class CalendarioFeriadosFormModel
    {
        public Guid IdCalendarioFeriadosPais { get; set; }

        [Display(Name = "Pais")]
        public Guid IdPais { get; set; }

        [Display(Name = "Año")]
        public int Anio { get; set; }

        public string Fechas { get; set; }
        public string FechasIds { get; set; }

        public string UserName { get; set; }
    }
}