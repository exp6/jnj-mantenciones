﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;
using System.Web.Mvc;
using System.Globalization;

namespace MetricArts.JnJ.Mantenciones.Models.Mantenedores.CalendarioFeriados
{
    public class CalendarioFeriadosViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public CalendarioFeriadosFormModel FORM { get; set; }
        public IEnumerable<CalendarioFeriadosPai> CalendarioFechas { get; set; }
        public CalendarioFeriadosPai calendarioFecha { get; set; }
        public List<CalendarioFeriadosFecha> fechas { get; set; }
        public IEnumerable<SelectListItem> paisesItems { get; set; }
        public IEnumerable<SelectListItem> Anios { get; set; }
        private const int CANTIDAD_ANIOS_POSTERIOR = 4;

        public CalendarioFeriadosViewModel() 
        {
            CalendarioFechas = db.CalendarioFeriadosPais.OrderByDescending(x => x.Anio);

            paisesItems = db.Pais.OrderBy(x => x.Nombre).Select(x => new SelectListItem()
            {
                Text = x.Nombre,
                Value = x.IdPais.ToString()
            });

            initAniosPlan();
        }

        public CalendarioFeriadosViewModel(string userName)
        {
            paisesItems = db.Pais.OrderBy(x => x.Nombre).Select(x => new SelectListItem()
            {
                Text = x.Nombre,
                Value = x.IdPais.ToString()
            });

            initAniosPlan();
        }

        public CalendarioFeriadosViewModel(Guid idCalendario)
        {
            calendarioFecha = db.CalendarioFeriadosPais.Single(x => x.IdCalendarioFeriadosPais == idCalendario);
            FORM = new CalendarioFeriadosFormModel();
            FORM.IdCalendarioFeriadosPais = calendarioFecha.IdCalendarioFeriadosPais;
            FORM.IdPais = calendarioFecha.IdPais;
            FORM.Anio = calendarioFecha.Anio;

            fechas = db.CalendarioFeriadosFechas.OrderBy(x => x.Fecha).Where(x => x.IdCalendarioFeriadosPais == idCalendario).ToList();
            paisesItems = db.Pais.OrderBy(x => x.Nombre).Select(x => new SelectListItem()
            {
                Text = x.Nombre,
                Value = x.IdPais.ToString()
            });

            initAniosPlan();
        }

        public CalendarioFeriadosViewModel(CalendarioFeriadosFormModel form)
        {
            FORM = form;
        }

        private void initAniosPlan()
        {
            int anioInicio = DateTime.Now.Year;
            int ultimoAnio = anioInicio + CANTIDAD_ANIOS_POSTERIOR;

            List<SelectListItem> items = new List<SelectListItem>();

            for (int i = anioInicio; i <= ultimoAnio; i++)
            {
                var item = new SelectListItem
                {
                    Text = i.ToString(),
                    Value = i.ToString()
                };

                items.Add(item);
            }
            Anios = items;
        }

        /**
         * Funciones CRUD
        **/

        public bool create()
        {
            Pais p = db.Pais.Single(x => x.IdPais == FORM.IdPais);

            int exists = db.CalendarioFeriadosPais.Where(x => x.Anio == FORM.Anio).Count();

            if(exists != 0) {
                return false;
            }
            else {
                CalendarioFeriadosPai nuevo = new CalendarioFeriadosPai
                {
                    IdCalendarioFeriadosPais = Guid.NewGuid(),
                    Pais = p,
                    Anio = FORM.Anio,
                    FechaCreacion = DateTime.Now,
                    CreadoPor = FORM.UserName
                };

                string[] fechasCal = FORM.Fechas.Split(';').ToArray();

                CalendarioFeriadosFecha newFecha;

                for (int i = 0; i < fechasCal.Length; i++)
                {
                    newFecha = new CalendarioFeriadosFecha
                    {
                        IdCalendarioFeriadosFecha = Guid.NewGuid(),
                        CalendarioFeriadosPai = nuevo,
                        Fecha = DateTime.ParseExact(fechasCal[i], "dd-MM-yyyy", CultureInfo.InvariantCulture),
                        FechaCreacion = DateTime.Now,
                        CreadoPor = FORM.UserName
                    };
                }

                db.CalendarioFeriadosPais.InsertOnSubmit(nuevo);
                db.SubmitChanges();

                return true;
            }

        }

        public void update()
        {
            Pais p = db.Pais.Single(x => x.IdPais == FORM.IdPais);

            CalendarioFeriadosPai c = db.CalendarioFeriadosPais.Single(x => x.IdCalendarioFeriadosPais == FORM.IdCalendarioFeriadosPais);
            c.Pais = p;
            c.Anio = FORM.Anio;

            string[] fechasCal = FORM.Fechas.Split(';').ToArray();
            string[] fechasCalIdsStr = FORM.FechasIds.Split(';').ToArray();
            List<Guid> fechasCalIds = new List<Guid>();

            CalendarioFeriadosFecha newTags;

            int counter = 1;

            CalendarioFeriadosFecha fechaExistente;
            Guid FechaId;

            for (int i = 0; i < fechasCalIdsStr.Length; i++)
            {
                if (!fechasCalIdsStr[i].Equals("valor-nuevo"))
                {
                    fechasCalIds.Add(Guid.Parse(fechasCalIdsStr[i]));
                }
            }

            List<Guid> fechasTotales = db.CalendarioFeriadosFechas.Where(x => x.IdCalendarioFeriadosPais == FORM.IdCalendarioFeriadosPais).Select(x => x.IdCalendarioFeriadosFecha).ToList();
            List<Guid> fechasAEliminarIds = fechasTotales.Except(fechasCalIds).Union(fechasCalIds.Except(fechasTotales)).ToList();
            List<CalendarioFeriadosFecha> fechasAEliminar = db.CalendarioFeriadosFechas.Where(x => fechasAEliminarIds.Contains(x.IdCalendarioFeriadosFecha)).ToList();

            db.CalendarioFeriadosFechas.DeleteAllOnSubmit(fechasAEliminar);

            for (int i = 0; i < fechasCal.Length; i++)
            {
                if (fechasCalIdsStr[i].Equals("valor-nuevo"))
                {
                    FechaId = Guid.NewGuid();

                    newTags = new CalendarioFeriadosFecha
                    {
                        IdCalendarioFeriadosFecha = FechaId,
                        CalendarioFeriadosPai = c,
                        Fecha = DateTime.ParseExact(fechasCal[i], "dd-MM-yyyy", CultureInfo.InvariantCulture),
                        FechaCreacion = DateTime.Now,
                        CreadoPor = FORM.UserName,
                    };

                    counter++;
                    db.CalendarioFeriadosFechas.InsertOnSubmit(newTags);
                }
                else
                {
                    fechaExistente = db.CalendarioFeriadosFechas.Single(x => x.IdCalendarioFeriadosFecha == Guid.Parse(fechasCalIdsStr[i]));
                    fechaExistente.Fecha = DateTime.ParseExact(fechasCal[i], "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
            }

            db.SubmitChanges();
        }

        public void delete()
        {
            CalendarioFeriadosPai c = db.CalendarioFeriadosPais.Single(x => x.IdCalendarioFeriadosPais == FORM.IdCalendarioFeriadosPais);
            IQueryable<CalendarioFeriadosFecha> ct = db.CalendarioFeriadosFechas.Where(x => x.IdCalendarioFeriadosPais == c.IdCalendarioFeriadosPais);
            db.CalendarioFeriadosFechas.DeleteAllOnSubmit(ct);
            db.CalendarioFeriadosPais.DeleteOnSubmit(c);
            db.SubmitChanges();
        }

        /**
         * FIN Funciones CRUD
        **/
    }
}