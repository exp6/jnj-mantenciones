﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;
using MetricArts.JnJ.Mantenciones.Helpers;

namespace MetricArts.JnJ.Mantenciones.Models.Home
{
    public class IndexAdminViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        public int CantMantEnCurso { get; set; }
        public int CantMantDetenidas { get; set; }
        public int CantMantFinalizadas { get; set; }
        public int CantMantCheckListRespondido { get; set; }

        public int CantPlanPropuestas { get; set; }
        public int CantPlanVigentes { get; set; }

        public int CantMsgEnCola { get; set; }
        public int CantMsgProcesadas { get; set; }
        public int CantMsgEnviadas { get; set; }
        public int CantMsgRecibidas { get; set; }
        public int CantMsgFallidas { get; set; }

        public int CantRepEnEsperaAtencion { get; set; }
        public int CantRepEnviadas { get; set; }
        public int CantRepAceptadas { get; set; }

        public string mes { get; set; }

        public IndexAdminViewModel() 
        {
            ResumenAdminMantencione resumenMan = db.ResumenAdminMantenciones.FirstOrDefault();
            ResumenAdminPlanificacione resumenPlan = db.ResumenAdminPlanificaciones.FirstOrDefault();
            ResumenAdminNotificacione resumenMsg = db.ResumenAdminNotificaciones.FirstOrDefault();
            ResumenAdminSolicitudesRepuesto resumenRep = db.ResumenAdminSolicitudesRepuestos.FirstOrDefault();

            CantMantEnCurso = resumenMan.EnCurso ?? 0;
            CantMantDetenidas = resumenMan.EnEsperaMomentanea ?? 0;
            CantMantFinalizadas = resumenMan.Finalizada ?? 0;
            CantMantCheckListRespondido = resumenMan.CheckListRespondido ?? 0;

            CantPlanPropuestas = resumenPlan.Propuestas ?? 0;
            CantPlanVigentes = resumenPlan.Vigentes ?? 0;

            CantMsgEnCola = resumenMsg.EnCola ?? 0;
            CantMsgProcesadas = resumenMsg.Procesadas ?? 0;
            CantMsgEnviadas = resumenMsg.Enviadas ?? 0;
            CantMsgRecibidas = resumenMsg.Recibidas ?? 0;
            CantMsgFallidas = resumenMsg.Fallidas ?? 0;

            CantRepEnEsperaAtencion = resumenRep.EnEsperaAtencion ?? 0;
            CantRepEnviadas = resumenRep.Enviadas ?? 0;
            CantRepAceptadas = resumenRep.AceptadasPorDistribuidor ?? 0;

            mes = Utils.getMesStr(DateTime.Now.Month);
        }
    }
}