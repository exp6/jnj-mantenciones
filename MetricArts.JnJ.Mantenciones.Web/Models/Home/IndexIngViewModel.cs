﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;
using MetricArts.JnJ.Mantenciones.Helpers;

namespace MetricArts.JnJ.Mantenciones.Models.Home
{
    public class IndexIngViewModel
    {
        private MantencionesDataContext db = new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();

        public int CantMantEnEsperaVisita { get; set; }
        public int CantMantEnCurso { get; set; }
        public int CantMantDetenidas { get; set; }
        public int CantMantFinalizadas { get; set; }
        public int CantMantCheckListRespondido { get; set; }

        public int CantRepEnEsperaAtencion { get; set; }
        public int CantRepEnviadas { get; set; }
        public int CantRepAceptadas { get; set; }

        public string mes { get; set; }

        public IndexIngViewModel(string userName) 
        {
            Ingeniero ingeniero = db.Ingenieros.Where(x => x.Usuario.NombreUsuario == userName).FirstOrDefault();

            ResumenIngMantencionesResult resumenMan = db.ResumenIngMantenciones(ingeniero.IdIngeniero).FirstOrDefault();
            ResumenIngSolicitudesRepuestosResult resumenRep = db.ResumenIngSolicitudesRepuestos(ingeniero.IdIngeniero).FirstOrDefault();

            CantMantEnEsperaVisita = resumenMan.EnEsperaVisita ?? 0;
            CantMantEnCurso = resumenMan.EnCurso ?? 0;
            CantMantDetenidas = resumenMan.EnEsperaMomentanea ?? 0;
            CantMantFinalizadas = resumenMan.Finalizada ?? 0;
            CantMantCheckListRespondido = resumenMan.CheckListRespondido ?? 0;

            CantRepEnEsperaAtencion = resumenRep.EnEsperaAtencion ?? 0;
            CantRepEnviadas = resumenRep.Enviadas ?? 0;
            CantRepAceptadas = resumenRep.AceptadasPorDistribuidor ?? 0;

            mes = Utils.getMesStr(DateTime.Now.Month);
        }
    }
}