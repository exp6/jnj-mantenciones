﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricArts.JnJ.Mantenciones.Core;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;

namespace MetricArts.JnJ.Mantenciones.Helpers
{
    public static class Utils
    {
        
        public static IEnumerable<Franquicia> getFranquiciasByPais(IEnumerable<Sucursal> sucursalesLocales, MantencionesDataContext db)
        {
            IEnumerable<Cliente> clientesLocales = db.Clientes.Where(x => x.Sucursals.Intersect(sucursalesLocales).Any());
            return db.Franquicias.Where(x => x.Clientes.Intersect(clientesLocales).Any());
        }

        public static string getHTMLParsed(string htmlTemplate, Guid? idSucursal, Guid? IdPlanificacion, Guid[] IdsEquiposSeleccionados, string urlLink, MantencionesDataContext db, int templateMode, Guid IdMensajeStage, Guid? IdSolicitudKitRepuesto, Guid? IdDistribuidor, Guid? IdUsuarioEmisor, int anioPlanificacion)
        {
            string result = htmlTemplate;
            Sucursal sucursal = db.Sucursals.Where(x => x.IdSucursal == idSucursal).FirstOrDefault();
            Planificacion plan = db.Planificacions.Where(x => x.IdPlanificacion == IdPlanificacion).SingleOrDefault();
            //Equipo equipo = db.Equipos.Where(x => x.IdEquipo == idEquipo).FirstOrDefault();
            List<Equipo> equipos = db.Equipos.Where(x => x.IdSucursal == idSucursal).ToList();
            List<MapTag> tagsMapDb = db.MapTags.ToList();
            Dictionary<string, string> tagsMap = new Dictionary<string, string>();
            string finalURL;
            List<SolicitudRepuesto> solicitudRepuestos = db.SolicitudRepuestos.Where(x => x.IdSolicitudKitRepuesto == IdSolicitudKitRepuesto).ToList();
            DistribuidorRepuesto distribuidor = db.DistribuidorRepuestos.Where(x => x.IdDistribuidorRepuestos == IdDistribuidor).SingleOrDefault();
            Usuario usuarioEmisor = db.Usuarios.Where(x => x.IdUsuario == IdUsuarioEmisor).SingleOrDefault();



            foreach (var tag in tagsMapDb)
            {
                switch (tag.TagLlave)
                {
                    case "{{NOMBREEQUIPO}}":
                        //tagsMap.Add(tag.TagLlave, equipo.Nombre);
                        break;
                    case "{{NOMBRESUCURSAL}}":
                        if (sucursal != null)
                            tagsMap.Add(tag.TagLlave, sucursal.NombreSucursal);
                        break;
                    case "{{NUMEROSERIEEQUIPO}}":
                        //tagsMap.Add(tag.TagLlave, equipo.CodigoEquipo);
                        break;
                    case "{{NOMBREEMPRESA}}":
                        if (sucursal != null)
                            tagsMap.Add(tag.TagLlave, sucursal.Cliente.NombreEmpresa);
                        break;
                    case "{{TABLAEQUIPOS}}":
                        if (IdsEquiposSeleccionados != null)
                        {
                            string valorEquipos = "<table align='center' width='100%' border='1'>"
                            + "<thead>"
                            + "<tr>"
                            + "<th align='center'>Equipo</th>"
                            + "<th align='center'>Tipo Contrato</th>"
                            + "<th align='center'>Periodos Propuestos</th>"
                            + "</tr>"
                            + "</thead>"
                            + "<tbody>";

                            //foreach (var equipo1 in equipos)
                            Equipo equipo1;
                            foreach (var idEquipo in IdsEquiposSeleccionados)
                            {
                                equipo1 = db.Equipos.Where(x => x.IdEquipo == idEquipo).FirstOrDefault();

                                if (equipo1 != null)
                                {
                                    valorEquipos += "<tr>";
                                    valorEquipos += "<td><strong>" + equipo1.Nombre + " (Cod.: " + equipo1.CodigoEquipo + ")</strong></td>";
                                    valorEquipos += "<td align='center'>" + equipo1.TipoContrato.TipoContrato1 + "</td>";
                                    valorEquipos += "<td><ul>";

                                    if (plan != null)
                                    {
                                        if (plan.Mantencions.Any())
                                        {
                                            foreach (Mantencion mantencion in plan.Mantencions)
                                            {
                                                valorEquipos += "<li>" + mantencion.FechaEstimativaDesde.Month.ToString() + "-" + mantencion.FechaEstimativaDesde.Year.ToString() + "</li>";
                                            }
                                        }
                                        else
                                        {
                                            List<String> periodos = CalculoMesesPropuesta(equipo1.IdEquipo, getFechaUltimaMantencion(equipo1.IdEquipo), equipo1.FechaInstalacion, equipo1.TipoContrato.Codigo, (plan != null ? plan.Ano : DateTime.Now.Year));
                                            foreach (var a in periodos)
                                            {
                                                valorEquipos += "<li>" + a + "</li>";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        List<String> periodos = CalculoMesesPropuesta(equipo1.IdEquipo, getFechaUltimaMantencion(equipo1.IdEquipo), equipo1.FechaInstalacion, equipo1.TipoContrato.Codigo, (plan != null ? plan.Ano : DateTime.Now.Year));
                                        foreach (var a in periodos)
                                        {
                                            valorEquipos += "<li>" + a + "</li>";
                                        }
                                    }

                                    valorEquipos += "</ul></td>";
                                    valorEquipos += "</tr>";
                                }
                            }
                            valorEquipos += "</tbody>";
                            valorEquipos += "</table>";

                            tagsMap.Add(tag.TagLlave, valorEquipos);
                        }
                        break;
                    case "{{ANIO}}":
                        int anioPlanificacionInput = (plan != null ? plan.Ano : (anioPlanificacion != 0 ? anioPlanificacion : DateTime.Now.Year+1));

                        //if (plan != null || anioPlanificacion != 0)
                        //    tagsMap.Add(tag.TagLlave, ""+plan.Ano);
                        tagsMap.Add(tag.TagLlave, ""+anioPlanificacionInput);
                        break;
                    case "{{LINKCHECKLIST}}":
                        string checklistPath = "/"; //TODO: DEFINIR CONTROLADORES PARA DESCARGAS Y RUTEOS
                        finalURL = (templateMode == 1 ? (urlLink + checklistPath + IdPlanificacion) : "#");
                        tagsMap.Add(tag.TagLlave, "<a href='" + urlLink + checklistPath + "'>Descarga de Checklist</a>");
                        break;
                    case "{{LINKDESCARGAPDF}}":
                        //string pdfPath = "/Clients/DescargaPDF?ID=";
                        string pdfPath = "Clients/DescargaPDF?ID=";
                        finalURL = (templateMode == 1 ? (urlLink + pdfPath + IdMensajeStage) : "#");
                        tagsMap.Add(tag.TagLlave, "<a href='" + finalURL + "'>Descarga PDF Planificación Mantenciones Preventivas</a>");
                        break;
                    case "{{LINKENCUESTASATISFACCION}}":
                        string encuestaSatisfaccionPath = "/";
                        finalURL = (templateMode == 1 ? (urlLink + encuestaSatisfaccionPath + IdPlanificacion) : "#");
                        tagsMap.Add(tag.TagLlave, "<a href='" + finalURL + "'>Descarga Encuesta Satisfacción</a>");
                        break;
                    case "{{LINKAGENDAR}}":
                        //string agendarPath = "/Clients/AgendarMantenciones?ID=";
                        string agendarPath = "Clients/AgendarMantenciones?ID=";
                        finalURL = (templateMode == 1 ? (urlLink + agendarPath + IdPlanificacion + "_" + IdMensajeStage ) : "#" );
                        tagsMap.Add(tag.TagLlave, "<a href='" + finalURL + "'>Agendar Mantenciones</a>");
                        break;
                    case "{{TABLAREPUESTOS}}":
                        if (solicitudRepuestos.Count() != 0 || solicitudRepuestos != null) 
                        {
                            string valorRepuestos = "<table align='center' width='100%' border='1'>"
                            + "<thead>"
                            + "<tr>"
                            + "<th align='center'>Código</th>"
                            + "<th align='center'>Descripción</th>"
                            + "<th align='center'>Categoría</th>"
                            + "<th align='center'>Cantidad Solicitada</th>"
                            + "</tr>"
                            + "</thead>"
                            + "<tbody>";

                            Repuesto repuesto;
                            foreach (SolicitudRepuesto solicitud in solicitudRepuestos)
                            {
                                repuesto = solicitud.Repuesto;

                                if (repuesto != null)
                                {
                                    valorRepuestos += "<tr>";
                                    valorRepuestos += "<th>" + repuesto.Codigo + "</th>";
                                    valorRepuestos += "<td>" + repuesto.Descripcion + "</td>";
                                    valorRepuestos += "<td>" + repuesto.Categoria + "</td>";
                                    valorRepuestos += "<td>" + solicitud.CantidadSolicitada + "</td>";
                                    valorRepuestos += "</tr>";
                                }
                            }
                            valorRepuestos += "</tbody>";
                            valorRepuestos += "</table>";

                            tagsMap.Add(tag.TagLlave, valorRepuestos);
                        }
                        break;
                    case "{{NOMBREDISTRIBUIDOR}}":
                        if (distribuidor != null)
                            tagsMap.Add(tag.TagLlave, distribuidor.Nombre);
                        break;
                    case "{{USUARIOEMISOR}}":
                        if (usuarioEmisor != null)
                            tagsMap.Add(tag.TagLlave, (usuarioEmisor.Nombre + " " + usuarioEmisor.ApellidoPaterno));
                        break;
                    case "{{LINKCONFIRMARRECIBOCORREO}}":
                        string confirmarReciboMailPath = "/Provider/ConfirmarReciboMail?ID=";
                        finalURL = (templateMode == 1 ? (urlLink + confirmarReciboMailPath + IdSolicitudKitRepuesto + "_" + IdMensajeStage) : "#");
                        tagsMap.Add(tag.TagLlave, "<a href='" + finalURL + "'>Confirmar Solicitud</a>");
                        break;
                    case "{{CONTACTOUSUARIOEMISOR}}":
                        if (usuarioEmisor != null)
                            tagsMap.Add(tag.TagLlave, usuarioEmisor.CorreoElectronico);
                        break;
                }
            }

            foreach (KeyValuePair<string, string> entry in tagsMap)
            {
                result = result.Replace(entry.Key, entry.Value);
            }
            return result;
        }


        public static MantencionesDataContext getDBConnection() {
            return new MantencionesDataContext()
                .WithConnectionStringFromConfiguration();
        }

        public static List<String> CalculoMesesPropuesta(Guid idEquipo, DateTime? FechaUltimaMantencion, DateTime FechaInstalacion, int tipoContrato, int anioPlanificacion1)
        {
            //Si la fecha de la ULTIMA MANTENCION no existe en el sistema, se utiliza la FECHA DE INSTALACION del equipo en la sucursal.
            DateTime fecha = FechaUltimaMantencion ?? FechaInstalacion;

            String format = "MM-yyyy";
            List<String> fechasPropuestas = new List<String>();
            DateTime fechaResult;
            int i;
            int anioPlanificacion = anioPlanificacion1 - 1;
            int anioFuturo = anioPlanificacion1;

            switch (tipoContrato)
            {
                case 1:
                    //Anual
                    i = 1;
                    fechaResult = fecha;
                    while (fechaResult.Year < anioFuturo)
                    {
                        fechaResult = fechaResult.AddYears(i);
                    }
                    fechasPropuestas.Add(fechaResult.ToString(format));
                    break;
                case 2:
                    //Semestral
                    i = 6;
                    fechaResult = fecha;
                    while (fechaResult.Year <= anioPlanificacion)
                    {
                        fechaResult = fechaResult.AddMonths(i);
                    }

                    fechasPropuestas.Add(fechaResult.ToString(format));
                    fechaResult = fechaResult.AddMonths(i);

                    while (fechaResult.Year == anioFuturo)
                    {
                        fechasPropuestas.Add(fechaResult.ToString(format));
                        fechaResult = fechaResult.AddMonths(i);
                    }
                    break;
                case 3:
                    //Cuatrimestral
                    i = 4;
                    fechaResult = fecha;
                    while (fechaResult.Year <= anioPlanificacion)
                    {
                        fechaResult = fechaResult.AddMonths(i);
                    }

                    fechasPropuestas.Add(fechaResult.ToString(format));
                    fechaResult = fechaResult.AddMonths(i);

                    while (fechaResult.Year == anioFuturo)
                    {
                        fechasPropuestas.Add(fechaResult.ToString(format));
                        fechaResult = fechaResult.AddMonths(i);
                    }
                    break;
                case 4:
                    //Trimestral
                    i = 3;
                    fechaResult = fecha;
                    while (fechaResult.Year <= anioPlanificacion)
                    {
                        fechaResult = fechaResult.AddMonths(i);
                    }

                    fechasPropuestas.Add(fechaResult.ToString(format));
                    fechaResult = fechaResult.AddMonths(i);

                    while (fechaResult.Year == anioFuturo)
                    {
                        fechasPropuestas.Add(fechaResult.ToString(format));
                        fechaResult = fechaResult.AddMonths(i);
                    }
                    break;
                default:
                    //Anual
                    i = 1;
                    fechaResult = fecha;
                    while (fechaResult.Year < anioFuturo)
                    {
                        fechaResult = fecha.AddYears(i);
                    }
                    fechasPropuestas.Add(fechaResult.ToString(format));
                    break;
            }

            return fechasPropuestas;
        }

        //public static List<DateTime> CalculoMesesPropuesta(DateTime fechaInstalacion, int tipoContrato, int anioPlanificacion)
        //{
        //    List<DateTime> fechasPropuestas = new List<DateTime>();
        //    DateTime fechaResult;
        //    int i;

        //    switch (tipoContrato)
        //    {
        //        case 1:
        //            //Anual
        //            i = 1;
        //            fechaResult = fechaInstalacion;
        //            while (fechaResult.Year < anioPlanificacion)
        //            {
        //                fechaResult = fechaResult.AddYears(i);
        //            }
        //            fechasPropuestas.Add(fechaResult);
        //            break;
        //        case 2:
        //            //Semestral
        //            i = 6;
        //            fechaResult = fechaInstalacion;
        //            while (fechaResult.Year <= (anioPlanificacion - 1))
        //            {
        //                fechaResult = fechaResult.AddMonths(i);
        //            }

        //            fechasPropuestas.Add(fechaResult);
        //            fechaResult = fechaResult.AddMonths(i);

        //            while (fechaResult.Year == anioPlanificacion)
        //            {
        //                fechasPropuestas.Add(fechaResult);
        //                fechaResult = fechaResult.AddMonths(i);
        //            }
        //            break;
        //        case 3:
        //            //Trimestral
        //            i = 3;
        //            fechaResult = fechaInstalacion;
        //            while (fechaResult.Year <= (anioPlanificacion - 1))
        //            {
        //                fechaResult = fechaResult.AddMonths(i);
        //            }

        //            fechasPropuestas.Add(fechaResult);
        //            fechaResult = fechaResult.AddMonths(i);

        //            while (fechaResult.Year == anioPlanificacion)
        //            {
        //                fechasPropuestas.Add(fechaResult);
        //                fechaResult = fechaResult.AddMonths(i);
        //            }
        //            break;
        //        case 4:
        //            //Cuatrimestral
        //            i = 4;
        //            fechaResult = fechaInstalacion;
        //            while (fechaResult.Year <= (anioPlanificacion - 1))
        //            {
        //                fechaResult = fechaResult.AddMonths(i);
        //            }

        //            fechasPropuestas.Add(fechaResult);
        //            fechaResult = fechaResult.AddMonths(i);

        //            while (fechaResult.Year == anioPlanificacion)
        //            {
        //                fechasPropuestas.Add(fechaResult);
        //                fechaResult = fechaResult.AddMonths(i);
        //            }
        //            break;
        //        default:
        //            //Anual
        //            i = 1;
        //            fechaResult = fechaInstalacion;
        //            while (fechaResult.Year < (anioPlanificacion - 1))
        //            {
        //                fechaResult = fechaInstalacion.AddYears(i);
        //            }
        //            fechasPropuestas.Add(fechaResult);
        //            break;
        //    }

        //    return fechasPropuestas;
        //}

        public static String getFechaStrFormat(DateTime fecha, string format) {
            return fecha.ToString(format);
        }

        public static String getFechaUltimaMantecionStr(Guid idEquipo)
        {
            //Equipo equipo = getDBConnection().Equipos.Where(x => x.IdEquipo == idEquipo).SingleOrDefault();
            //DateTime? fecha = getFechaUltimaMantencion(idEquipo) ?? equipo.FechaUltimaMantencion;
            DateTime? fecha = getFechaUltimaMantencion(idEquipo);
            String fechaStr = (fecha != null ? fecha.Value.ToString("dd-MM-yyyy") : "N/A");
            return fechaStr;
        }

        public static DateTime? getFechaUltimaMantencion(Guid idEquipo)
        {
            Equipo equipo = getDBConnection().Equipos.Where(x => x.IdEquipo == idEquipo).SingleOrDefault();
            Mantencion m = getDBConnection().Mantencions.OrderByDescending(x => x.FechaConfirmacion)
                .Where(x => x.IdEquipo == idEquipo && x.FechaConfirmacion != null).FirstOrDefault();
            return (m != null ? m.FechaConfirmacion : (equipo.FechaUltimaMantencion != null ? equipo.FechaUltimaMantencion : null));
        }

        public static byte[] createPDF(string html)
        {
            MemoryStream msOutput = new MemoryStream();
            TextReader reader = new StringReader(html);

            // step 1: creation of a document-object
            Document document = new Document(PageSize.A4, 30, 30, 30, 30);

            // step 2:
            // we create a writer that listens to the document
            // and directs a XML-stream to a file
            PdfWriter writer = PdfWriter.GetInstance(document, msOutput);

            // step 3: we create a worker parse the document
            HTMLWorker worker = new HTMLWorker(document);

            // step 4: we open document and start the worker on the document
            document.Open();
            worker.StartDocument();

            // step 5: parse the html into the document
            worker.Parse(reader);

            // step 6: close the document and the worker
            worker.EndDocument();
            worker.Close();
            document.Close();
            msOutput.Flush();
            return msOutput.ToArray();
        }

        public static string getMesStr(int mesNum) 
        {
            switch (mesNum) 
            {
                case 1:
                    return "Enero";
                case 2:
                    return "Febrero";
                case 3:
                    return "Marzo";
                case 4:
                    return "Abril";
                case 5:
                    return "Mayo";
                case 6:
                    return "Junio";
                case 7:
                    return "Julio";
                case 8:
                    return "Agosto";
                case 9:
                    return "Septiembre";
                case 10:
                    return "Octubre";
                case 11:
                    return "Noviembre";
                case 12:
                    return "Diciembre";
                default:
                    return "";
            }
        }
    }
}