﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace MetricArts.JnJ.Mantenciones.MapperProfiles
{
    public class DefaultProfileMapper : Profile
    {
        public override string ProfileName
        {
            get
            {
                return "DefaultProfileMapper";
            }
        }

        protected override void Configure()
        {
            base.Configure();
        }
    }
}