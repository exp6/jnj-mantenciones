﻿using System.Web;
using System.Web.Optimization;

namespace MetricArts.JnJ.Mantenciones
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/JQuery/jquery-{version}.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/external").Include(
                        "~/Scripts/External/moment.js",
                        "~/Scripts/External/vertical-responsive-menu.js",
                        "~/Scripts/External/fullcalendar.js",
                        "~/Scripts/External/jquery.bootstrap.wizard.js",
                        "~/Scripts/External/fastclick.js",
                        "~/Scripts/External/jquery.dataTables.js",
                        "~/Scripts/External/dataTables.bootstrap.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/external/reporting").Include(
                        "~/Scripts/External/Reporting/*.js"
                        ));

            //bundles.Add(new ScriptBundle("~/bundles/external/text-editor/js").IncludeDirectory(
            //            "~/Scripts/ckeditor", "*.js", true
            //            ));

            bundles.Add(new ScriptBundle("~/bundles/external/text-editor/js").Include(
                        "~/Scripts/ckeditor/ckeditor.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/Bootstrap/bootstrap.min.js",
                        "~/Scripts/Bootstrap/bootstrap-datepicker.js",
                        "~/Scripts/Bootstrap/bootstrap-datepaginator.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap/locales").Include(
                        "~/Scripts/Bootstrap/Locales/bootstrap-datepicker.es.min",
                        "~/Scripts/Bootstrap/Locales/bootstrap-datepicker.fr.min",
                        "~/Scripts/Bootstrap/Locales/bootstrap-datepicker.pt.min"));


            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                        "~/Scripts/Custom/bootstrap-datepaginatorMensual.js",
                        "~/Scripts/Custom/init.js"));

            bundles.Add(new ScriptBundle("~/bundles/custom/planificaciones-automaticas").Include(
                        "~/Scripts/Custom/Planificaciones/PlanificacionesAutomaticas/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/planificaciones-manuales").Include(
                        "~/Scripts/Custom/Planificaciones/PlanificacionesManuales/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/planificaciones/propuestas").Include(
                        "~/Scripts/Custom/Planificaciones/PlanificacionesPropuestas/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/custom/notificaciones/crear").Include(
                        "~/Scripts/Custom/Notificaciones/Crear/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/notificaciones/seguimiento").Include(
                        "~/Scripts/Custom/Notificaciones/Seguimiento/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/custom/mantenciones/crear-manual").Include(
                        "~/Scripts/Custom/Mantenciones/CrearManual/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenciones/clientes").Include(
                        "~/Scripts/Custom/Mantenciones/PortalClientes/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenciones/vigentes").Include(
                        "~/Scripts/Custom/Mantenciones/Vigentes/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenciones/seguimiento").Include(
                        "~/Scripts/Custom/Mantenciones/Seguimiento/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenciones/repuestos").Include(
                        "~/Scripts/Custom/Mantenciones/Repuestos/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/custom/portal-ingeniero/bandeja").Include(
                        "~/Scripts/Custom/PortalIngeniero/Bandeja/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/portal-ingeniero/repuestos").Include(
                        "~/Scripts/Custom/PortalIngeniero/Repuestos/*.js"));

            /** ------------------ MANTENEDORES -------------------- **/

            // -------- FRANQUICIAS ----------
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/franquicias/crear").Include(
                        "~/Scripts/Custom/Mantenedores/Franquicias/crear.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/franquicias/listado").Include(
                        "~/Scripts/Custom/Mantenedores/Franquicias/listado.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/franquicias/editar").Include(
                        "~/Scripts/Custom/Mantenedores/Franquicias/editar.js"));

            // -------- CLIENTES ----------
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/clientes/crear").Include(
                        "~/Scripts/Custom/Mantenedores/Clientes/crear.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/clientes/listado").Include(
                        "~/Scripts/Custom/Mantenedores/Clientes/listado.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/clientes/editar").Include(
                        "~/Scripts/Custom/Mantenedores/Clientes/editar.js"));

            // -------- SUCURSALES ----------
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/sucursales/crear").Include(
                        "~/Scripts/Custom/Mantenedores/Sucursales/crear.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/sucursales/listado").Include(
                        "~/Scripts/Custom/Mantenedores/Sucursales/listado.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/sucursales/editar").Include(
                        "~/Scripts/Custom/Mantenedores/Sucursales/editar.js"));

            // -------- EQUIPOS ----------
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/equipos/crear").Include(
                        "~/Scripts/Custom/Mantenedores/Equipos/crear.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/equipos/listado").Include(
                        "~/Scripts/Custom/Mantenedores/Equipos/listado.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/equipos/editar").Include(
                        "~/Scripts/Custom/Mantenedores/Equipos/editar.js"));

            // -------- REPUESTOS ----------
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/repuestos/crear").Include(
                        "~/Scripts/Custom/Mantenedores/Repuestos/crear.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/repuestos/listado").Include(
                        "~/Scripts/Custom/Mantenedores/Repuestos/listado.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/repuestos/editar").Include(
                        "~/Scripts/Custom/Mantenedores/Repuestos/editar.js"));

            // -------- CONTACTOS ----------
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/contactos/crear").Include(
                        "~/Scripts/Custom/Mantenedores/Contactos/crear.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/contactos/listado").Include(
                        "~/Scripts/Custom/Mantenedores/Contactos/listado.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/contactos/editar").Include(
                        "~/Scripts/Custom/Mantenedores/Contactos/editar.js"));

            // -------- KIT REPUESTOS ----------
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/kitrepuestos/crear").Include(
                        "~/Scripts/Custom/Mantenedores/KitRepuestos/crear.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/kitrepuestos/listado").Include(
                        "~/Scripts/Custom/Mantenedores/KitRepuestos/listado.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/kitrepuestos/editar").Include(
                        "~/Scripts/Custom/Mantenedores/KitRepuestos/editar.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/kitrepuestos/asociar-repuestos").Include(
                        "~/Scripts/Custom/Mantenedores/KitRepuestos/asociar-repuestos.js"));

            // -------- DISTRIBUIDORES ----------
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/distribuidores/crear").Include(
                        "~/Scripts/Custom/Mantenedores/Distribuidores/crear.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/distribuidores/listado").Include(
                        "~/Scripts/Custom/Mantenedores/Distribuidores/listado.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/distribuidores/editar").Include(
                        "~/Scripts/Custom/Mantenedores/Distribuidores/editar.js"));

            // -------- CUESTIONARIOS ----------
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/cuestionarios/crear").Include(
                        "~/Scripts/Custom/Mantenedores/Cuestionarios/crear.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/cuestionarios/listado").Include(
                        "~/Scripts/Custom/Mantenedores/Cuestionarios/listado.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/cuestionarios/editar").Include(
                        "~/Scripts/Custom/Mantenedores/Cuestionarios/editar.js"));

            // -------- PLANTILLAS PDF ----------
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/plantillas-pdf/crear").Include(
                        "~/Scripts/Custom/Mantenedores/PlantillasPDF/crear.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/plantillas-pdf/listado").Include(
                        "~/Scripts/Custom/Mantenedores/PlantillasPDF/listado.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/plantillas-pdf/editar").Include(
                        "~/Scripts/Custom/Mantenedores/PlantillasPDF/editar.js"));

            // -------- PLANTILLAS CORREO ----------
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/plantillas-correo/crear").Include(
                        "~/Scripts/Custom/Mantenedores/PlantillasCorreo/crear.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/plantillas-correo/listado").Include(
                        "~/Scripts/Custom/Mantenedores/PlantillasCorreo/listado.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/plantillas-correo/editar").Include(
                        "~/Scripts/Custom/Mantenedores/PlantillasCorreo/editar.js"));

            // -------- CALENDARIO FERIADOS ----------
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/calendario-feriados/crear").Include(
                        "~/Scripts/Custom/Mantenedores/CalendarioFeriados/crear.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/calendario-feriados/listado").Include(
                        "~/Scripts/Custom/Mantenedores/CalendarioFeriados/listado.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/calendario-feriados/editar").Include(
                        "~/Scripts/Custom/Mantenedores/CalendarioFeriados/editar.js"));

            // -------- USUARIOS ----------
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/usuarios/crear").Include(
                        "~/Scripts/Custom/Mantenedores/Usuarios/crear.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/usuarios/listado").Include(
                        "~/Scripts/Custom/Mantenedores/Usuarios/listado.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/usuarios/editar").Include(
                        "~/Scripts/Custom/Mantenedores/Usuarios/editar.js"));
            bundles.Add(new ScriptBundle("~/bundles/custom/mantenedores/usuarios/editarUsuario").Include(
                        "~/Scripts/Custom/Mantenedores/Usuarios/editarUsuario.js"));


            /** ------------------ FIN MANTENEDORES -------------------- **/


            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/main.css"));

            bundles.Add(new StyleBundle("~/Content/Bootstrap/css").Include(
                "~/Content/Bootstrap/bootstrap.min.css",
                "~/Content/Bootstrap/bootstrap-toggle.min.css",
                "~/Content/Bootstrap/bootstrap-datepicker3.css"
                ));

            bundles.Add(new StyleBundle("~/Content/External/css").Include(
                "~/Content/External/vertical-responsive-menu.css",
                "~/Content/External/font-awesome.min.css",
                "~/Content/External/fullcalendar.css",
                "~/Content/External/dataTables.bootstrap.css"
                ));

            bundles.Add(new StyleBundle("~/Content/External/Reporting/css").Include(
                "~/Content/External/Reporting/*.css"
                ));

            bundles.Add(new ScriptBundle("~/Content/External/Reporting/maps/css").Include(
                        "~/Content/External/Reporting/maps/*.css"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/external/text-editor/css").IncludeDirectory(
                        "~/Scripts/ckeditor", "*.css", true
                        ));
        }
    }
}