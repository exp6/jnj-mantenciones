﻿/**  
**  INIT
**/
var urlGetClientes, urlGetSucursales, urlEstado, urlEditar, urlReenviar, urlGetMailTemplatePreview, urlGetPDFTemplatePreview, urlEliminar;
var triggerCloseModal = 0;

function init(url1, url2, url3, url4, url5, url6, url7) {
    console.log("[Notificaciones - Seguimiento] Inicializando variables...");

    iniciarTabla();

    urlGetClientes = url1;
    urlGetSucursales = url2;
    urlEstado = url3;
    urlEditar = url4;
    urlReenviar = url5;
    urlGetMailPreview = url6;
    urlGetPDFPreview = url7;

    console.log("[Notificaciones - Seguimiento] Variables inicializadas...");
}


function iniciarTabla() {
    $('#notificaciones-estados').DataTable({
        language: {
            lengthMenu: "Mostrar _MENU_ registros por página",
            zeroRecords: "No existen notificaciones con ese criterio de búsqueda.",
            info: "Mostrando página _PAGE_ de _PAGES_",
            infoEmpty: "Puede crear nuevas notificaciones seleccionando la opción Nueva Notificación",
            paginate: {
                first: "Primero",
                last: "Último",
                next: "Siguiente",
                previous: "Anterior"
            },
            search: "Buscar:",
            infoFiltered: "(Filtrado de un total de _MAX_ registros)"
        }
    });
}

/**
** DYNAMIC DROP DOWN LIST
**/
$('#FORM_IdFranquicia').on('change', function () {
    
    var clientes = $('#FORM_IdCliente');
    clientes.prop('disabled', true);
    var sucursales = $('#FORM_IdSucursal');
    sucursales.prop('disabled', true);
    sucursales.empty();
    clientes.empty();

    var franquicia = $(this).val();
    nombreFranquicia = $(this).find("option:selected").text();

    if (franquicia.length > 0) {
        $.getJSON(urlGetClientes, {
            franquiciaId: franquicia
        })
.done(function (data) {

    var msg = "No existen Clientes para la franquicia seleccionada";
    if (data.length > 0) {
        clientes.prop('disabled', false);
        msg = "Seleccione un cliente";
    }
    var option = $("<option/>").attr("value", "0").text(msg);
    clientes.append(option);

    $.each(data, function (i, cliente) {
        var option = $("<option/>").attr("value", cliente.Value).text(cliente.Text);
        clientes.append(option);
    });
})
.fail(function (jqxhr, textStatus, error) {
    var err = textStatus + ", " + error;
    console.log("Request Failed: " + err);
});
    }
});

$('#FORM_IdCliente').on('change', function () {
    var sucursales = $('#FORM_IdSucursal');
    sucursales.prop('disabled', true);
    sucursales.empty();
    var cliente = $(this).val();
    nombreCliente = $(this).find("option:selected").text();

    if (cliente.length > 0) {
        $.getJSON(urlGetSucursales, {
            idCliente: cliente
        })
        .done(function (data) {
            var msg = "No existen Sucursales para el cliente seleccionado";
            if (data.length > 0) {
                sucursales.prop('disabled', false);
                msg = "Seleccione una sucursal";
            }
            var option = $("<option/>").attr("value", "0").text(msg);
            sucursales.append(option);

            $.each(data, function (i, sucursal) {

                var option = $("<option/>").attr("value", sucursal.Value).text(sucursal.Text);
                sucursales.append(option);
            });
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
    }
});

/**
** FILTRO
**/
$("#filtrar").click(function () {
    if ($('#FORM_IdFranquicia option:selected').val().length == 0) { alert("Debe escoger por lo menos una unidad de negocio."); return false; }
    if ($('#FORM_IdCliente option:selected').val() != "0") { $("#FORM_IdCliente1").val($('#FORM_IdCliente option:selected').val()); }
    if ($('#FORM_IdSucursal option:selected').val() != "0") { $("#FORM_IdSucursal1").val($('#FORM_IdSucursal option:selected').val()); }
    
    $("#form1").submit();
});

/**
** MODALS
**/
$('#notificaciones-estados .btn-modal-trigger').click(function () {
    var urlStr = "";
    var idNotificacion = $(this).attr('id');
    var idModal = $(this).attr('name');

    switch (idModal) {
        case "1": urlStr = urlEstado; $("#titulo-modal").text("Estado de notificación"); break;
        case "2": urlStr = urlGetMailPreview; $("#titulo-modal").text("Cuerpo de correo electrónico"); break;
        case "3": urlStr = urlGetPDFPreview; $("#titulo-modal").text("Previsualización PDF"); break;
        case "4": urlStr = urlEditar; $("#titulo-modal").text("Editar notificación"); break;
        case "5": urlStr = urlReenviar; $("#titulo-modal").text("Reenvío de notificación"); break;
    }

    $.ajax({
        url: urlStr,
        data: { IdNotificacion: idNotificacion },
        traditional: true,
        dataType: "html",
        success: function (data) {
            $("#contenido-modal").html(data);
        }
    });

    $('#ModalPreviewSeguimiento').modal();
});

$('#notificaciones-estados .btn-modal-reload-trigger').click(function () {
    var urlStr = "";
    var idNotificacion = $(this).attr('id');
    var idModal = $(this).attr('name');

    switch (idModal) {
        case "1": urlStr = urlEstado; $("#titulo-modal").text("Estado de notificación"); break;
        case "2": urlStr = urlGetMailPreview; $("#titulo-modal").text("Cuerpo de correo electrónico"); break;
        case "3": urlStr = urlGetPDFPreview; $("#titulo-modal").text("Previsualización PDF"); break;
        case "4": urlStr = urlEditar; $("#titulo-modal").text("Editar notificación"); break;
        case "5": urlStr = urlReenviar; $("#titulo-modal").text("Reenvío de notificación"); break;
    }

    $.ajax({
        url: urlStr,
        data: { IdNotificacion: idNotificacion },
        traditional: true,
        dataType: "html",
        success: function (data) {
            $("#contenido-modal-reload").html(data);
        }
    });
    triggerCloseModal = 0;
    $('#ModalPreviewSeguimientoReload').modal();
});

$('#ModalPreviewSeguimientoReload').on('hidden.bs.modal', function () {
    if (triggerCloseModal == 0) {
        window.location.reload();
    }    
})

$("#salir-modal-reload").click(function () {
    triggerCloseModal = 1;
    $('#ModalPreviewSeguimientoReload').modal('hide');
});

$("#salir-modal-reload-x").click(function () {
    triggerCloseModal = 1;
    $('#ModalPreviewSeguimientoReload').modal('hide');
});

/**
** ELIMINAR NOTIFICACION
**/
$(".btn-eliminar").click(function () {
    var txt;
    var r = confirm("Está a punto de eliminar la notificación. ¿Desea continuar?");
    if (r == true) {
        $("#FORM_IdNotificacion").val($(this).attr('id'));
        $("#form2").submit();
    }
});

$(".btn-reenviar").click(function () {
    var txt;
    var r = confirm("Está a punto de reenviar la notificación. ¿Desea continuar?");
    if (r == true) {
        $("#FORM_IdNotificacion").val($(this).attr('id'));
        $(this).parent().submit();
    }
});