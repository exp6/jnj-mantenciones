﻿/**  
**  INIT
**/
var urlGetClientes, urlGetSucursales, urlGetContactos, urlGetPlanificaciones, urlGetFormatosCorreo, urlGetEquiposSucursal, urlGetFormatosPDF, urlGetPDFTemplatePreview, urlGetMailTemplatePreview, urlGetEquiposPlanificacion;
var IdSucursalSeleccionado, IdTipoNotificacion;
var nombreFranquicia, nombreCliente, nombreSucursal, nombreNotificacion;

function init(url1, url2, url3, url4, url5, url6, url7, url8, url9, url10) {
    console.log("[Notificaciones - Crear] Inicializando variables...");

    urlGetClientes = url1;
    urlGetSucursales = url2;
    urlGetContactos = url3;
    urlGetPlanificaciones = url4;
    urlGetFormatosCorreo = url5;
    urlGetEquiposSucursal = url6;
    urlGetFormatosPDF = url7;
    urlGetMailTemplatePreview = url8;
    urlGetPDFTemplatePreview = url9;
    urlGetEquiposPlanificacion = url10;

    $(".nav-tabs").click(function () {
        return false;
    });

    var paso = 1;

    //Inicializa wizard
    $('#crear-notificacion-wz').bootstrapWizard({
        'tabClass': 'nav nav-tabs', 'debug': false, onShow: function (tab, navigation, index) {
        }, onNext: function (tab, navigation, index) {
            
            if (!validacionStep(paso)) { return false; }

            if (paso == 1) {
                IdTipoNotificacion = $(".selected-JJ").attr("id");
                nombreNotificacion = $(".selected-JJ").text();
            }

            if (paso == 2) {
                $("#franquicia-bd").text(nombreFranquicia);
                $("#cliente-bd").text(nombreCliente);
                $("#sucursal-bd").text(nombreSucursal);
                
                $("#tipo-notificacion").text(nombreNotificacion);
                
                var formatoCorreos = $('#FORM_IdFormatoCorreo');
                formatoCorreos.prop('disabled', true);
                formatoCorreos.empty();

                var botonPreviewCorreo = $('#PreviewCorreo');
                botonPreviewCorreo.prop('disabled', true);

                var botonPreviewPDF = $('#PreviewPDF');
                botonPreviewPDF.prop('disabled', true);

                $.getJSON(urlGetFormatosCorreo, {
                    idTipoNotificacion: IdTipoNotificacion
                })
                    .done(function (data) {
                        var msg = "No existen formatos de correo para este tipo de notificacion.";
                        if (data.length > 0) {
                            formatoCorreos.prop('disabled', false);
                        
                            msg = "Seleccione una plantilla de correo";
                        }
                        var option = $("<option/>").attr("value", "0").text(msg);
                        formatoCorreos.append(option);

                        $.each(data, function (i, formato) {
                            var option = $("<option/>").attr("value", formato.Value).text(formato.Text);
                            formatoCorreos.append(option);
                        });
                    })
                    .fail(function (jqxhr, textStatus, error) {
                        var err = textStatus + ", " + error;
                        console.log("Request Failed: " + err);
                    });

                var planificacionesSelect = $("#FORM_IdPlanificacion");
                planificacionesSelect.prop('disabled', true);
                planificacionesSelect.empty();

                $.getJSON(urlGetPlanificaciones, {
                    idSucursal: IdSucursalSeleccionado
                })
                    .done(function (data) {
                        var msg = "No existen planificaciones para la sucursal seleccionada.";
                        if (data.length > 0) {
                            planificacionesSelect.prop('disabled', false);

                            msg = "Seleccione una planificación";
                        }
                        var option = $("<option/>").attr("value", "0").text(msg);
                        planificacionesSelect.append(option);

                        $.each(data, function (i, plan) {
                            var option = $("<option/>").attr("value", plan.idPlanificacion).text(plan.anio + " - " + plan.nombre);
                            planificacionesSelect.append(option);
                        });
                    })
                    .fail(function (jqxhr, textStatus, error) {
                        var err = textStatus + ", " + error;
                        console.log("Request Failed: " + err);
                    });

                var formatoPDFs = $('#FORM_IdFormatoPDF');
                formatoPDFs.prop('disabled', true);
                formatoPDFs.empty();


                var pdfAttached = $('#FORM_pdfAttached');
                pdfAttached.attr('disabled', true);

                $.getJSON(urlGetFormatosPDF, {
                    idTipoNotificacion: IdTipoNotificacion
                })
                    .done(function (data) {
                        var msg = "No existen formatos de PDF para este tipo de notificacion.";
                        
                        if (data.length > 0) {
                            //formatoPDFs.prop('disabled', false);
                            pdfAttached.attr('disabled', false);

                            msg = "Seleccione una plantilla PDF";
                        }
                        var option = $("<option/>").attr("value", "0").text(msg);
                        formatoPDFs.append(option);

                        $.each(data, function (i, formato) {
                            var option = $("<option/>").attr("value", formato.Value).text(formato.Text);
                            formatoPDFs.append(option);
                        });
                    })
                    .fail(function (jqxhr, textStatus, error) {
                        var err = textStatus + ", " + error;
                        console.log("Request Failed: " + err);
                    });

                var equiposTemp = $('#equipos-planificacion');
                equiposTemp.empty();
                var msgTemp = "No existen equipos asociados.";
                var optionTemp = $('<li class="list-group-item">' + msgTemp + '</li>');
                equiposTemp.append(optionTemp);

                //var equipos = $('#equipos-planificacion');

                //$.getJSON(urlGetEquiposSucursal, {
                //    idSucursal: IdSucursalSeleccionado
                //})
                //    .done(function (data) {
                //        var msg = "No existen equipos asociados a la sucursal.";
                //        equipos.empty();
                //        if (data.length > 0) {
                            
                //            $.each(data, function (i, equipo) {
                //                var option = $('<li class="list-group-item"><input id="' + equipo.idEquipo + '" type="checkbox" checked="checked" />   ' + equipo.nombreEquipo + ' (Cod.:' + equipo.codigoEquipo + ') </li>');
                //                equipos.append(option);
                //            });
                            
                //        }
                //        else {
                //            var option = $('<li class="list-group-item">' + msg + '</li>');
                //            equipos.append(option);
                            
                //        }
                //    })
                //    .fail(function (jqxhr, textStatus, error) {
                //        var err = textStatus + ", " + error;
                //        console.log("Request Failed: " + err);
                //    });

                var PARA = [];
                var CC = [];
                var BCC = [];

                PARA.push($("#contacto-sucursal").val());

                if ($(".contacto-destinatario").length > 0) {
                    $(".contacto-destinatario").each(function (index, element) {
                        var seleccion = $(element).find("select option:selected").val();
                        var contactoStr = $(element).find("input").val();

                        switch (seleccion) {
                            case "1":
                                PARA.push(contactoStr);
                                break;
                            case "2":
                                CC.push(contactoStr);
                                break;
                            case "3":
                                BCC.push(contactoStr);
                                break;
                        }
                    });

                    var conParaJoin = PARA.join(",");
                    var conCCJoin = CC.join(",");
                    var conBCCJoin = BCC.join(",");

                    $("#FORM_contactoPara").val(conParaJoin);
                    $("#FORM_contactosCC").val(conCCJoin);
                    $("#FORM_contactosBCC").val(conBCCJoin);
                }
                else {
                    
                    $("#FORM_contactoPara").val(PARA.join(","));
                    $("#FORM_contactosCC").val('');
                    $("#FORM_contactosBCC").val('');
                }
            }  

            ++paso;

        }, onPrevious: function (tab, navigation, index) {
            --paso;

        }, onLast: function (tab, navigation, index) {
        }, onTabClick: function (tab, navigation, index) {
        }, onTabShow: function (tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index + 1;
            var $percent = ($current / $total) * 100;
            $('#crear-notificacion-wz .progress-bar').css({ width: $percent + '%' });
            
            if ($current >= $total) {
                $('#crear-notificacion-wz').find('.pager .next').hide();
                $('#crear-notificacion-wz').find('.pager .finish').show();
                $('#crear-notificacion-wz').find('.pager .finish').removeClass('disabled');
            } else {
                $('#crear-notificacion-wz').find('.pager .next').show();
                $('#crear-notificacion-wz').find('.pager .finish').hide();
            }
        }
    });

    console.log("[Notificaciones - Crear] Variables inicializadas...");
}

/**  
**  VALIDACIONES
**/
function validacionStep(step) {
    var result = true;

    switch (step) {
        case 1:
            if ($(".selected-JJ").length == 0) { alert("Debe escoger al menos un tipo de notificación."); result = false; }
            return result;
            break;
        case 2:
            console.log("Tamaño lista sucursal: " + $("#FORM_IdSucursal").length);
            if ($("#FORM_IdSucursal option:selected").val() == "0" || $("#FORM_IdSucursal").attr("disabled") == "disabled") { alert("Debe escoger una sucursal."); result = false; }
            if ($('.InputAddContent input').val() == "") { alert("Debe ingresar un contacto válido."); result = false; }
            return result;
            break;
        case 3:
            if ($("#FORM_IdFormatoCorreo option:selected").val() == "0") { alert("Debe escoger por lo menos una plantilla de correo."); result = false; }
            var idsEquiposSeleccionados = $("#equipos-planificacion input:checked").map(function () {
                return $(this).attr('id');
            }).get();
            if (idsEquiposSeleccionados.length == 0) { alert("Debe escoger por lo menos un equipo."); result = false; }

            if (!$('#FORM_IdFormatoPDF').attr('disabled') && $('#FORM_IdFormatoPDF').val() == '0') { alert("Debe escoger una plantilla PDF válida. De lo contrario debe descartarla."); result = false; }

            return result;
            break;
        case 4:
            if ($("#FORM_fechaEnvio").val() == "") { alert("Debe ingresar una fecha de envío."); result = false; }
            return result;
            break;
    }
}

/**
** PREVISUALIZACION
**/
$('#PreviewCorreo').click(function () {
    var idMail = $("#FORM_IdFormatoCorreo option:selected").val();
    $("#titulo-formato-correo").text($("#FORM_IdFormatoCorreo option:selected").text());

    var idsEquiposSeleccionados = $("#equipos-planificacion input:checked").map(function () {
        return $(this).attr('id');
    }).get();
    
    var idPlanificacion = $("#FORM_IdPlanificacion option:selected").val();

    if (idsEquiposSeleccionados.length > 0) {
        $.ajax({
            url: urlGetMailTemplatePreview,
            data: { IdMailTemplate: idMail, IdSucursal: IdSucursalSeleccionado, IdPlanificacion: idPlanificacion, IdsEquiposSeleccionados: idsEquiposSeleccionados },
            traditional: true,
            dataType: "html",
            success: function (data) {
                $("#correo-body-html").html(data);
            }
        });

        $('#ModalPreviewCorreo').modal();
    }
    else {
        alert("Debe escoger por lo menos un equipo.");
    }

    
});

$('#PreviewPDF').click(function () {
    var idPDF = $("#FORM_IdFormatoPDF option:selected").val();
    $("#titulo-formato-pdf").text($("#FORM_IdFormatoPDF option:selected").text());

    var idsEquiposSeleccionados = $("#equipos-planificacion input:checked").map(function () {
        return $(this).attr('id');
    }).get();

    var idPlanificacion = $("#FORM_IdPlanificacion option:selected").val();

    if (idsEquiposSeleccionados.length > 0) {
        $.ajax({
            url: urlGetPDFTemplatePreview,
            data: { IdPDFTemplate: idPDF, IdSucursal: IdSucursalSeleccionado, IdPlanificacion: idPlanificacion, IdsEquiposSeleccionados: idsEquiposSeleccionados },
            traditional: true,
            dataType: "html",
            success: function (data) {
                $("#pdf-body-html").html(data);
            }
        });
        $('#ModalPreviewPDF').modal();
    }
    else {
        alert("Debe escoger por lo menos un equipo.");
    }
});

$('#FORM_IdFormatoCorreo').change(function () {
    var btn = $("#PreviewCorreo");
    if ($(this).val() == "0") {
        btn.prop('disabled', true);
    }
    else {
        btn.prop('disabled', false);
    }
});

$('#FORM_IdFormatoPDF').change(function () {
    var btn = $("#PreviewPDF");
    if ($(this).val() == "0") {
        btn.prop('disabled', true);
    }
    else {
        btn.prop('disabled', false);
    }
});

$('#FORM_pdfAttached').click(function () {
    
    if ($(this).prop('checked')) {
        $('#FORM_IdFormatoPDF').prop('disabled', true);
        $('#PreviewPDF').prop('disabled', true);
    }
    else {
        $('#FORM_IdFormatoPDF').prop('disabled', false);
        $('#PreviewPDF').prop('disabled', false);
    }
});

/**
** EQUIPOS PLANIFICACION
**/
$('#FORM_IdPlanificacion').on('change', function () {
    var equipos = $('#equipos-planificacion');
    var IdPlanificacionSelected = $(this).val();

    $.getJSON(urlGetEquiposPlanificacion, {
        idPlanificacion: IdPlanificacionSelected
    })
        .done(function (data) {
            var msg = "No existen equipos asociados a la sucursal.";
            equipos.empty();
            if (data.length > 0) {

                $.each(data, function (i, equipo) {
                    var option = $('<li class="list-group-item"><input id="' + equipo.idEquipo + '" type="checkbox" checked="checked" />   ' + equipo.nombreEquipo + ' (Cod.:' + equipo.codigoEquipo + ') - Contrato: ' + equipo.tipoContrato + ' </li>');
                    equipos.append(option);
                });

                $("#formatos-equipos-display").show();
            }
            else {
                var option = $('<li class="list-group-item">' + msg + '</li>');
                equipos.append(option);
                $("#formatos-equipos-display").hide();
            }
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
});

/**
** DYNAMIC DROP DOWN LIST
**/
$('#FORM_IdFranquicia').on('change', function () {
    $('.InputAddContent').hide();
    var clientes = $('#FORM_IdCliente');
    clientes.prop('disabled', true);
    var sucursales = $('#FORM_IdSucursal');
    sucursales.prop('disabled', true);
    sucursales.empty();
    clientes.empty();

    var franquicia = $(this).val();
    nombreFranquicia = $(this).find("option:selected").text();

    if (franquicia.length > 0) {
        $.getJSON(urlGetClientes, {
            franquiciaId: franquicia
        })
.done(function (data) {

    var msg = "No existen Clientes para la franquicia seleccionada";
    if (data.length > 0) {
        clientes.prop('disabled', false);
        msg = "Seleccione un cliente";
    }
    var option = $("<option/>").text(msg);
    clientes.append(option);

    $.each(data, function (i, cliente) {
        var option = $("<option/>").attr("value", cliente.Value).text(cliente.Text);
        clientes.append(option);
    });
})
.fail(function (jqxhr, textStatus, error) {
    var err = textStatus + ", " + error;
    console.log("Request Failed: " + err);
});
    }
});

$('#FORM_IdCliente').on('change', function () {
    $('.InputAddContent').hide();
    var sucursales = $('#FORM_IdSucursal');
    sucursales.prop('disabled', true);
    sucursales.empty();
    var cliente = $(this).val();
    nombreCliente = $(this).find("option:selected").text();

    if (cliente.length > 0) {

        $.getJSON(urlGetSucursales, {
            idCliente: cliente
        })
        .done(function (data) {
            var msg = "No existen Sucursales para el cliente seleccionado";
            if (data.length > 0) {
                sucursales.prop('disabled', false);
                msg = "Seleccione una sucursal";
            }
            var option = $("<option/>").attr("value", "0").text(msg);
            sucursales.append(option);

            $.each(data, function (i, sucursal) {

                var option = $("<option/>").attr("value", sucursal.Value).text(sucursal.Text);
                sucursales.append(option);
            });
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
    }
});

$('#FORM_IdSucursal').change(function () {
    $('.InputAddContent').hide();
    var sucursal = $(this).val();
    nombreSucursal = $(this).find("option:selected").text();
    IdSucursalSeleccionado = sucursal;

    //PROCESO DE BUSQUEDA DE CONTACTOS

    if ($(this).find("option:selected").val() != 0) {
        $.getJSON(urlGetContactos, {
            idSucursal: sucursal
        })
        .done(function (data) {
            var msg = "Contacto inválido. Favor dirigirse al mantenedor correspondiente y agregar un contacto válido.";
            if (data.length > 0) {
                $('.InputAddContent input[type=text]').val(data);
                $('.InputAddContent').show();
            }
            else {
                alert(msg);
            }
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
    }
});

$('#FORM_IdPlanificacion').on('change', function () {
    var formatoCorreos = $('#FORM_IdTipoFormatoCorreo');
    formatoCorreos.prop('disabled', true);
    formatoCorreos.empty();

    $.getJSON(urlGetFormatosCorreo)
.done(function (data) {
    var msg = "No existen formatos de correo en el sistema.";
    if (data.length > 0) {
        formatoCorreos.prop('disabled', false);
        msg = "Seleccione un formato de correo";
    }
    var option = $("<option/>").attr("value", "0").text(msg);
    formatoCorreos.append(option);

    $.each(data, function (i, formato) {
        var option = $("<option/>").attr("value", formato.Value).text(formato.Text);
        formatoCorreos.append(option);
    });
})
.fail(function (jqxhr, textStatus, error) {
    var err = textStatus + ", " + error;
    console.log("Request Failed: " + err);
});

});

/**
** TIPOS DE NOTIFICACION
**/
$("#tipo-de-notificacion li").click(function () {
    $("#FORM_IdTipoNotificacion").val($(this).attr("id"));
});

/**
** FIN WIZARD
**/
$("#fin-crear-notificaciones").click(function () {
    var idsEquiposSeleccionados = $("#equipos-planificacion input:checked").map(function () {
        return $(this).attr('id');
    }).get();
    $("#FORM_equiposSeleccionadosStr").val(idsEquiposSeleccionados.join(";"));

    $("#FORM_IdFormatoCorreo1").val($('#FORM_IdFormatoCorreo option:selected').val());
    $("#FORM_IdFormatoPDF1").val($("#FORM_IdFormatoPDF option:selected").val());

    $("#FORM_IdCliente1").val($('#FORM_IdCliente option:selected').val());
    $("#FORM_IdSucursal1").val($('#FORM_IdSucursal option:selected').val());

    $("#FORM_IdPlanificacion1").val($('#FORM_IdPlanificacion option:selected').val());

    if ($('#FORM_pdfAttached').attr('disabled')) {
        $('#FORM_pdfAttached').removeAttr('disabled');
    }

    if (!validacionStep(4)) { return false; }

    $("form").submit();
});