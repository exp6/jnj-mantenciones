﻿/**  
**  INIT
**/


function init() {
    console.log("[Mantenedores - Contactos - Crear] Inicializando variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Mantenedores - Contactos - Crear] Variables inicializadas...");
}

$("#crear-repuesto").click(function () {
    $("#FORM_Categoria").val($('#categoria option:selected').val());

    $("form").submit();
});