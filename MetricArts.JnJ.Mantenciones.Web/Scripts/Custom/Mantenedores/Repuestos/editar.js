﻿/**  
**  INIT
**/


function init() {
    console.log("[Mantenedores - Repuestos - Editar] Inicializando variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Mantenedores - Repuestos - Editar] Variables inicializadas...");
}
