﻿/**  
**  INIT
**/
var urlGetClientes, urlGetSucursales;

function init(url1, url2) {
    console.log("[Mantenedores - Contactos - Crear] Inicializando variables...");

    urlGetClientes = url1;
    urlGetSucursales = url2;

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Mantenedores - Contactos - Crear] Variables inicializadas...");
}

/**
** DYNAMIC DROP DOWN LIST
**/
$('#FORM_IdFranquicia').on('change', function () {
    var clientes = $('#FORM_IdCliente');
    clientes.prop('disabled', true);
    var sucursales = $('#FORM_IdSucursal');
    sucursales.prop('disabled', true);
    sucursales.empty();
    clientes.empty();

    var franquicia = $(this).val();
    nombreFranquicia = $(this).find("option:selected").text();

    if (franquicia.length > 0) {
        $.getJSON(urlGetClientes, {
            franquiciaId: franquicia
        })
.done(function (data) {

    var msg = "No existen Clientes para la franquicia seleccionada";
    if (data.length > 0) {
        clientes.prop('disabled', false);
        msg = "Seleccione un cliente";
    }
    var option = $("<option/>").text(msg);
    clientes.append(option);

    $.each(data, function (i, cliente) {
        var option = $("<option/>").attr("value", cliente.Value).text(cliente.Text);
        clientes.append(option);
    });
})
.fail(function (jqxhr, textStatus, error) {
    var err = textStatus + ", " + error;
    console.log("Request Failed: " + err);
});
    }
});

$('#FORM_IdCliente').on('change', function () {
    
    var sucursales = $('#FORM_IdSucursal');
    sucursales.prop('disabled', true);
    sucursales.empty();
    var cliente = $(this).val();
    nombreCliente = $(this).find("option:selected").text();

    if (cliente.length > 0) {

        $.getJSON(urlGetSucursales, {
            idCliente: cliente
        })
        .done(function (data) {
            var msg = "Ya existe un contacto registrado o no existen Sucursales para el cliente seleccionado";
            if (data.length > 0) {
                sucursales.prop('disabled', false);
                msg = "Seleccione una sucursal";
            }
            var option = $("<option/>").attr("value", "0").text(msg);
            sucursales.append(option);

            $.each(data, function (i, sucursal) {

                var option = $("<option/>").attr("value", sucursal.Value).text(sucursal.Text);
                sucursales.append(option);
            });
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
    }
});


$("#crear-contacto").click(function () {
    $("#FORM_IdSucursal1").val($('#FORM_IdSucursal option:selected').val());
    
    $("form").submit();
});