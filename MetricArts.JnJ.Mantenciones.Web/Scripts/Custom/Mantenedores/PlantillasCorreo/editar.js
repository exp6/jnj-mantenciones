﻿/**  
**  INIT
**/
var editorElemento;

function init() {
    console.log("[Mantenedores - Plantillas Correo - Editar] Inicializando variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    initTextEditor();

    console.log("[Mantenedores - Plantillas Correo - Editar] Variables inicializadas...");
}

function initTextEditor() {
    editorElemento = CKEDITOR.replace('editor', {
        uiColor: '#B41601'
    });
    editorElemento.config.height = '500px';
}

$("button[name='boton-tag']").click(function () {
    CKEDITOR.instances.editor.insertText($(this).val());
});