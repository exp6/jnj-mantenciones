﻿/**  
**  INIT
**/


function init() {
    console.log("[Mantenciones - Usuarios - Crear] Inicializando variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Mantenciones - Usuarios - Crear] Variables inicializadas...");
}