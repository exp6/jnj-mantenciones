﻿/**  
**  INIT
**/


function init() {
    console.log("[Mantenciones - Usuarios - Editar Datos Sistema] Inicializando variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Mantenciones - Usuarios - Editar Datos Sistema] Variables inicializadas...");
}
