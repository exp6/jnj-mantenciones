﻿/**  
**  INIT
**/


function init() {
    console.log("[Mantenciones - Usuarios - Editar Datos Personales] Inicializando variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Mantenciones - Usuarios - Editar Datos Personales] Variables inicializadas...");
}