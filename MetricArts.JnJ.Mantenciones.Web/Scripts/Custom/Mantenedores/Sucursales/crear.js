﻿/**  
**  INIT
**/
var urlGetClientes, urlGetComunas;

function init(url1, url2) {
    console.log("[Mantenedores - Sucursales - Crear] Inicializando variables...");

    urlGetClientes = url1;
    urlGetComunas = url2;

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Mantenedores - Sucursales - Crear] Variables inicializadas...");
}

/**
** DYNAMIC DROP DOWN LIST
**/
$('#FORM_IdFranquicia').on('change', function () {
    var clientes = $('#FORM_IdCliente');
    clientes.prop('disabled', true);
    clientes.empty();

    var franquicia = $(this).val();
    nombreFranquicia = $(this).find("option:selected").text();

    if (franquicia.length > 0) {
        $.getJSON(urlGetClientes, {
            franquiciaId: franquicia
        })
.done(function (data) {

    var msg = "No existen Clientes para la franquicia seleccionada";
    if (data.length > 0) {
        clientes.prop('disabled', false);
        msg = "Seleccione un cliente";
    }
    var option = $("<option/>").text(msg);
    clientes.append(option);

    $.each(data, function (i, cliente) {
        var option = $("<option/>").attr("value", cliente.Value).text(cliente.Text);
        clientes.append(option);
    });
})
.fail(function (jqxhr, textStatus, error) {
    var err = textStatus + ", " + error;
    console.log("Request Failed: " + err);
});
    }
});

$('#FORM_IdRegion').on('change', function () {
    var comunas = $('#FORM_IdComuna');
    comunas.prop('disabled', true);
    comunas.empty();

    var region = $(this).val();
    nombreRegion = $(this).find("option:selected").text();

    if (region.length > 0) {
        $.getJSON(urlGetComunas, {
            regionId: region
        })
.done(function (data) {

    var msg = "No existen Comunas para la región seleccionada";
    if (data.length > 0) {
        comunas.prop('disabled', false);
        msg = "Seleccione una comuna";
    }
    var option = $("<option/>").text(msg);
    comunas.append(option);

    $.each(data, function (i, comuna) {
        var option = $("<option/>").attr("value", comuna.Value).text(comuna.Text);
        comunas.append(option);
    });
})
.fail(function (jqxhr, textStatus, error) {
    var err = textStatus + ", " + error;
    console.log("Request Failed: " + err);
});
    }
});

$("#crear-sucursal").click(function () {
    $("#FORM_IdCliente1").val($('#FORM_IdCliente option:selected').val());
    $("#FORM_IdComuna1").val($('#FORM_IdComuna option:selected').val());

    $("form").submit();
});