﻿/**  
**  INIT
**/
var urlGetClientes, urlGetSucursales, urlGetSucursalEquipos;

function init(url1, url2, url3) {
    console.log("[Mantenedores - KIT Repuestos - Crear] Inicializando variables...");

    urlGetClientes = url1;
    urlGetSucursales = url2;
    urlGetSucursalEquipos = url3;

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Mantenedores - KIT Repuestos - Crear] Variables inicializadas...");
}

/**
** DYNAMIC DROP DOWN LIST
**/
$('#FORM_IdFranquicia').on('change', function () {
    var clientes = $('#FORM_IdCliente');
    clientes.prop('disabled', true);
    var sucursales = $('#FORM_IdSucursal');
    sucursales.prop('disabled', true);
    sucursales.empty();
    clientes.empty();
    var equipos = $('#equipos-planificacion');
    equipos.empty();
    
    var franquicia = $(this).val();
    nombreFranquicia = $(this).find("option:selected").text();

    if (franquicia.length > 0) {
        $.getJSON(urlGetClientes, {
            franquiciaId: franquicia
        })
.done(function (data) {

    var msg = "No existen Clientes para la franquicia seleccionada";
    if (data.length > 0) {
        clientes.prop('disabled', false);
        msg = "Seleccione un cliente";
    }
    var option = $("<option/>").text(msg);
    clientes.append(option);

    $.each(data, function (i, cliente) {
        var option = $("<option/>").attr("value", cliente.Value).text(cliente.Text);
        clientes.append(option);
    });
})
.fail(function (jqxhr, textStatus, error) {
    var err = textStatus + ", " + error;
    console.log("Request Failed: " + err);
});
    }
});

$('#FORM_IdCliente').on('change', function () {

    var sucursales = $('#FORM_IdSucursal');
    sucursales.prop('disabled', true);
    sucursales.empty();
    var equipos = $('#equipos-planificacion');
    equipos.empty();
    var cliente = $(this).val();
    nombreCliente = $(this).find("option:selected").text();

    if (cliente.length > 0) {

        $.getJSON(urlGetSucursales, {
            idCliente: cliente
        })
        .done(function (data) {
            var msg = "No existen Sucursales para el cliente seleccionado";
            if (data.length > 0) {
                sucursales.prop('disabled', false);
                msg = "Seleccione una sucursal";
            }
            var option = $("<option/>").attr("value", "0").text(msg);
            sucursales.append(option);

            $.each(data, function (i, sucursal) {

                var option = $("<option/>").attr("value", sucursal.Value).text(sucursal.Text);
                sucursales.append(option);
            });
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
    }
});

/**
** EQUIPOS PLANIFICACION
**/
$('#FORM_IdSucursal').on('change', function () {
    var equipos = $('#equipos-planificacion');
    var IdSucursalSelected = $(this).val();

    $.getJSON(urlGetSucursalEquipos, {
        idSucursal: IdSucursalSelected
    })
        .done(function (data) {
            var msg = "No existen equipos asociados a la sucursal.";
            equipos.empty();
            if (data.length > 0) {

                $.each(data, function (i, equipo) {
                    var option = $('<li id="' + equipo.idEquipo + '" class="list-group-item"><span id="' + equipo.idEquipo + '">   ' + equipo.nombreEquipo + ' (Cod.:' + equipo.codigoEquipo + ')</span></li>');
                    equipos.append(option);
                });
            }
            else {
                var option = $('<li class="list-group-item">' + msg + '</li>');
                equipos.append(option);
            }
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
});

$("#crear-kit").click(function () {
    var elemento = $("#equipos-planificacion li.selected-JJ");
    var id = $(elemento).attr("id");

    $("#FORM_IdEquipo").val(id);

    $("form").submit();
});


$("body").on('click', '.select li', function (e) {
    $(this).parent().parent().find('.selected-JJ').removeClass('selected-JJ');
    if ($(this).hasClass('selected-JJ')) {
        $(this).removeClass('selected-JJ');
    }
    else {
        $(this).addClass('selected-JJ');
    }
});

//$(document).on('dblclick', '#equipos-planificacion li', function (e) {
//    var elemento = $("#equipos-planificacion li.selected-JJ");
//    var id = $(elemento).attr("id");
//    var texto = $(elemento).text();

//    var equipos = $('#equipos-planificacion-asociados');
//    var option = $('<li class="list-group-item">' + id.trim() + ' - ' + texto.trim() + '</li>');
//    equipos.append(option);
    
//    console.log(id.trim() + ' - ' + texto.trim());

//    $(elemento).remove();
    
//});

