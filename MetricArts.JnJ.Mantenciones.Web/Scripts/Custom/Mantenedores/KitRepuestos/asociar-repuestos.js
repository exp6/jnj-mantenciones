﻿/**  
**  INIT
**/


function init() {
    console.log("[Mantenedores - KIT Repuestos - Asociar Repuestos] Inicializando variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Mantenedores - KIT Repuestos - Asociar Repuestos] Variables inicializadas...");
}

$("body").on('click', '.select li', function (e) {
    $(this).parent().parent().find('.selected-JJ').removeClass('selected-JJ');
    if ($(this).hasClass('selected-JJ')) {
        $(this).removeClass('selected-JJ');
    }
    else {
        $(this).addClass('selected-JJ');
    }
});

$("#asociar-repuesto").click(function () {

    if ($("#repuestos-disponibles li").length == 0) return false;
    if ($("#repuestos-disponibles li.selected-JJ").length == 0) return false;

    var clienteId = $("#repuestos-disponibles li.selected-JJ").attr("id");
    var clienteNombre = $("#repuestos-disponibles li.selected-JJ").text();

    var disponibles = $("#repuestos-disponibles");
    var asociados = $("#repuestos-asociados");

    var asociadoVacio = $("#vacio-asociados p");

    if (asociadoVacio.length > 0) {
        asociadoVacio.remove();
    }

    var elemento = $("<li/>").attr("id", clienteId).attr("class", "list-group-item").text(clienteNombre);
    disponibles.find("#" + clienteId + "").remove();
    asociados.append(elemento);

    if ($("#repuestos-disponibles li").length == 0) $("#vacio-disponibles").append($("<p/>").attr("class", "list-group-item").text("No existen clientes disponibles en el sistema"));
});

$(document).on('dblclick', '#repuestos-disponibles li', function (e) {
    if ($("#repuestos-disponibles li").length == 0) return false;
    if ($("#repuestos-disponibles li.selected-JJ").length == 0) return false;

    var clienteId = $("#repuestos-disponibles li.selected-JJ").attr("id");
    var clienteNombre = $("#repuestos-disponibles li.selected-JJ").text();

    var disponibles = $("#repuestos-disponibles");
    var asociados = $("#repuestos-asociados");

    var asociadoVacio = $("#vacio-asociados p");

    if (asociadoVacio.length > 0) {
        asociadoVacio.remove();
    }

    var elemento = $("<li/>").attr("id", clienteId).attr("class", "list-group-item").text(clienteNombre);
    disponibles.find("#" + clienteId + "").remove();
    asociados.append(elemento);

    if ($("#repuestos-disponibles li").length == 0) $("#vacio-disponibles").append($("<p/>").attr("class", "list-group-item").text("No existen repuestos disponibles en el sistema"));
});

$("#desasociar-repuesto").click(function () {
    if ($("#repuestos-asociados li").length == 0) return false;
    if ($("#repuestos-asociados li.selected-JJ").length == 0) return false;

    var clienteId = $("#repuestos-asociados li.selected-JJ").attr("id");
    var clienteNombre = $("#repuestos-asociados li.selected-JJ").text();

    var disponibles = $("#repuestos-disponibles");
    var asociados = $("#repuestos-asociados");

    var disponibleVacio = $("#vacio-disponibles p");

    if (disponibleVacio.length > 0) {
        disponibleVacio.remove();
    }

    var elemento = $("<li/>").attr("id", clienteId).attr("class", "list-group-item").text(clienteNombre);
    asociados.find("#" + clienteId + "").remove();
    disponibles.append(elemento);

    if ($("#repuestos-asociados li").length == 0) $("#vacio-asociados").append($("<p/>").attr("class", "list-group-item").text("No existen repuestos asociados para este KIT"));
});

$(document).on('dblclick', '#repuestos-asociados li', function (e) {
    if ($("#repuestos-asociados li").length == 0) return false;
    if ($("#repuestos-asociados li.selected-JJ").length == 0) return false;

    var clienteId = $("#repuestos-asociados li.selected-JJ").attr("id");
    var clienteNombre = $("#repuestos-asociados li.selected-JJ").text();

    var disponibles = $("#repuestos-disponibles");
    var asociados = $("#repuestos-asociados");

    var disponibleVacio = $("#vacio-disponibles p");

    if (disponibleVacio.length > 0) {
        disponibleVacio.remove();
    }

    var elemento = $("<li/>").attr("id", clienteId).attr("class", "list-group-item").text(clienteNombre);
    asociados.find("#" + clienteId + "").remove();
    disponibles.append(elemento);

    if ($("#repuestos-asociados li").length == 0) $("#vacio-asociados").append($("<p/>").attr("class", "list-group-item").text("No existen repuestos asociados para este KIT"));
});

$("#guardar-repuestos").click(function () {
    var REPUESTOS = [];

    if ($("#repuestos-asociados li").length > 0) {
        $("#repuestos-asociados li").each(function (index, element) {
            var repuestosStr = $(element).attr("id");
            REPUESTOS.push(repuestosStr);
        });
        var repuestosJoin = REPUESTOS.join(";");
        $("#FORM_RepuestosIds").val(repuestosJoin);
    }

    console.log(REPUESTOS);

    $("form").submit();
});