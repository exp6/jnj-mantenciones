﻿/**  
**  INIT
**/
var listadoMantenedor;

var urlKitRepuestoRepuesto;

function init(url1) {
    console.log("[Mantenedores - KIT Repuestos - Listado] Inicializando variables...");

    urlKitRepuestoRepuesto = url1;

    iniciarTabla();

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Mantenedores - KIT Repuestos - Listado] Variables inicializadas...");
}

function iniciarTabla() {
    $('#listado-mantenedor').DataTable({
        language: {
            lengthMenu: "Mostrar _MENU_ registros por página",
            zeroRecords: "No existen elementos en el mantenedor.",
            info: "Mostrando página _PAGE_ de _PAGES_",
            infoEmpty: "Puede crear nuevos elementos seleccionando la opción Crear Nuevo",
            paginate: {
                first: "Primero",
                last: "Último",
                next: "Siguiente",
                previous: "Anterior"
            },
            search: "Buscar:",
            infoFiltered: "(Filtrado de un total de _MAX_ registros)"
        }
    });
}

/**
** MODALS
**/
$('#listado-mantenedor .btn-modal-trigger-repuestos').click(function () {
    var idKitRepuesto = $(this).attr('name');
    var nombreKit = $(this).attr('value');

    $.getJSON(urlKitRepuestoRepuesto, {
        IdKitRepuesto: idKitRepuesto
    })
    .done(function (data) {
        $("#titulo-modal").text("Repuestos asociados al KIT: " + nombreKit);
        var repuestos = $("#listado-elementos-repuestos");
        repuestos.empty();

        var elemento;

        $.each(data, function (i, item) {
            elemento = $("<li/>").text(item.Codigo + " - " + item.Descripcion + " - Cant.: " + item.Cantidad + " - " + item.Categoria);
            repuestos.append(elemento);
        });

        if (data.length == 0) {
            elemento = $("<li/>").text("No existen repuestos asociados a este KIT");
            repuestos.append(elemento);
        }

        $('#ModalPreviewRepuestos').modal();
    })
    .fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ", " + error;
        console.log("Request Failed: " + err);
    });
});