﻿/**  
**  INIT
**/
var urlObtenerPlantilla;

var listadoMantenedor;

function init(url1) {
    console.log("[Mantenedores - Plantillas PDF - Listado] Inicializando variables...");

    urlObtenerPlantilla = url1;

    iniciarTabla();

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Mantenedores - Plantillas PDF - Listado] Variables inicializadas...");
}

function iniciarTabla() {
    $('#listado-mantenedor').DataTable({
        language: {
            lengthMenu: "Mostrar _MENU_ registros por página",
            zeroRecords: "No existen elementos en el mantenedor.",
            info: "Mostrando página _PAGE_ de _PAGES_",
            infoEmpty: "Puede crear nuevos elementos seleccionando la opción Crear Nuevo",
            paginate: {
                first: "Primero",
                last: "Último",
                next: "Siguiente",
                previous: "Anterior"
            },
            search: "Buscar:",
            infoFiltered: "(Filtrado de un total de _MAX_ registros)"
        }
    });
}

$("button[name='pdf-preview']").click(function () {
    $.ajax({
        url: urlObtenerPlantilla,
        data: { IdPlantilla: $(this).attr("id") },
        traditional: true,
        dataType: "html",
        success: function (data) {
            $("#contenido-modal").html(data);
            $('#ModalPreviewPlantilla').modal();
        }
    });
});