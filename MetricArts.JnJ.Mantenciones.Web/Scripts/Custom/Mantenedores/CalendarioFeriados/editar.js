﻿/**  
**  INIT
**/


function init() {
    console.log("[Mantenedores - Calendario de Feriados - Editar] Inicializando variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Mantenedores - Calendario de Feriados - Editar] Variables inicializadas...");
}

$('.InputAddContentPartialEdit .btn-success').click(function () {
    $('.InputAddContentPartialEdit').append('<div class="form-group"><label class="col-sm-4 control-label"></label><div class="col-sm-6"><div class="input-group tags-cliente">  <span class="input-group-btn"><button type="button" name="valor-nuevo" class="btn btn-danger"><span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span></button></span>  <input type="text" class="form-control datepickershow" readonly = "readonly" value=""><span class="input-group-btn"><button class="btn btn-default dateButton" type="button"><span class="glyphicon glyphicon-calendar"></span></button></span></div></div></div></div>');
});

$('body').on('click', '.InputAddContentPartialEdit .btn-danger', function () {
    $(this).closest('.form-group').remove();
});

$("#editar-calendario").click(function () {
    if ($("#FORM_IdPais option:selected").val() == '') { alert("Debe ingresar un pais."); return false; }
    if ($("#FORM_Anio option:selected").val() == '') { alert("Debe ingresar un año"); return false; }
    if ($("#tags-clientes").val() == '') { alert("Debe ingresar por lo menos una fecha para el calendario"); return false; }

    var TAGS = [];
    var TAGS_IDS = [];
    TAGS.push($("#tag-cliente").val().trim());

    var valorId = $("#tag-cliente").parent().find("button").attr('name');

    TAGS_IDS.push(valorId);

    if ($(".tags-cliente").length > 0) {
        $(".tags-cliente").each(function (index, element) {
            var valorStr = $(element).find("input").val();
            valorId = $(element).find("button").attr('name');
            TAGS.push(valorStr.trim());
            TAGS_IDS.push(valorId);
        });
        var valoresJoin = TAGS.join(";");
        var valoresIdsJoin = TAGS_IDS.join(";");
        $("#FORM_Fechas").val(valoresJoin);
        $("#FORM_FechasIds").val(valoresIdsJoin);
    }
    else {
        $("#FORM_Fechas").val(TAGS[0]);
        $("#FORM_FechasIds").val(TAGS_IDS[0]);
    }

    console.log(TAGS);
    console.log(TAGS_IDS);

    if ($(".tags-cliente").length == 0 && $("#tag-cliente").val().trim() == "") { alert("Debe ingresar por lo menos una fecha"); return false; }

    $("form").submit();
});

$('body').on('click', '.dateButton', function () {
    $(this).parent().parent().find('.datepickershow').datepicker("show");
});