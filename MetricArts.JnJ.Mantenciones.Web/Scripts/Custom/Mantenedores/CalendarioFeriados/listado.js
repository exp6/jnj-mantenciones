﻿/**  
**  INIT
**/
var listadoMantenedor;
var urlFechasFeriado;

function init(url1) {
    console.log("[Mantenedores - Calendario de Feriados - Listado] Inicializando variables...");

    iniciarTabla();

    urlFechasFeriado = url1;

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Mantenedores - Calendario de Feriados - Listado] Variables inicializadas...");
}

function iniciarTabla() {
    $('#listado-mantenedor').DataTable({
        language: {
            lengthMenu: "Mostrar _MENU_ registros por página",
            zeroRecords: "No existen elementos en el mantenedor.",
            info: "Mostrando página _PAGE_ de _PAGES_",
            infoEmpty: "Puede crear nuevos elementos seleccionando la opción Crear Nuevo",
            paginate: {
                first: "Primero",
                last: "Último",
                next: "Siguiente",
                previous: "Anterior"
            },
            search: "Buscar:",
            infoFiltered: "(Filtrado de un total de _MAX_ registros)"
        }
    });
}

/**
** MODALS
**/
$('#listado-mantenedor .btn-modal-trigger-tags').click(function () {
    var idCliente = $(this).attr('name');
    var nombreCliente = $(this).attr('value');

    $.getJSON(urlFechasFeriado, {
        IdCalendario: idCliente
    })
    .done(function (data) {
        $("#titulo-modal").text("Fechas Feriados para el año: " + nombreCliente);
        var tags = $("#listado-elementos-tag");
        tags.empty();

        var elemento;

        $.each(data, function (i, item) {
            elemento = $("<li/>").text(item.FechaCalendario);
            tags.append(elemento);
        });

        if (data.length == 0) {
            elemento = $("<li/>").text("No existen tags asociados a este cliente");
            tags.append(elemento);
        }

        $('#ModalPreviewFechas').modal();
    })
    .fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ", " + error;
        console.log("Request Failed: " + err);
    });
});