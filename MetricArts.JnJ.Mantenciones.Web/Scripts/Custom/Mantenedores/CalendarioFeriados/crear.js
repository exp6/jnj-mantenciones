﻿/**  
**  INIT
**/


function init() {
    console.log("[Mantenedores - Calendario de Feriados - Crear] Inicializando variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Mantenedores - Calendario de Feriados - Crear] Variables inicializadas...");
}

$('.InputAddContentPartial .btn-success').click(function () {
    $('.InputAddContentPartial').append('<div class="form-group"><label class="col-sm-4 control-label"></label><div class="col-sm-6"><div class="input-group tags-clientes">  <span class="input-group-btn"><button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span></button></span>  <input type="text" class="form-control datepickershow" readonly = "readonly" value=""><span class="input-group-btn"><button class="btn btn-default dateButton" type="button"><span class="glyphicon glyphicon-calendar"></span></button></span></div></div></div></div>');
});

$('body').on('click', '.InputAddContentPartial .btn-danger', function () {
    $(this).closest('.form-group').remove();
});

$("#crear-calendario").click(function () {
    if ($("#FORM_IdPais option:selected").val() == '') { alert("Debe ingresar un pais."); return false; }
    if ($("#FORM_Anio option:selected").val() == '') { alert("Debe ingresar un año"); return false; }
    if ($("#tags-clientes").val() == '') { alert("Debe ingresar por lo menos una fecha para el calendario"); return false; }

    var TAGS = [];
    TAGS.push($("#tags-clientes").val());

    if ($(".tags-clientes").length > 0) {
        $(".tags-clientes").each(function (index, element) {
            var tagsStr = $(element).find("input").val();
            TAGS.push(tagsStr);
        });
        var tagsJoin = TAGS.join(";");
        $("#FORM_Fechas").val(tagsJoin);
    }
    else {
        $("#FORM_Fechas").val(TAGS[0]);
    }

    $("form").submit();
});

$('body').on('click', '.dateButton', function () {
    $(this).parent().parent().find('.datepickershow').datepicker("show");
});