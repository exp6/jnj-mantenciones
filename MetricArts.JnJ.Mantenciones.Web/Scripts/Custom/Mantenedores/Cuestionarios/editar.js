﻿/**  
**  INIT
**/
var urlGetTiposPregunta;
var tiposPregunta;
var ContQ1;
var tipoRespuesta;

var editor1Elemento, editor2Elemento;

function init(url1) {
    console.log("[Mantenedores - Cuestionarios - Editar] Inicializando variables...");

    urlGetTiposPregunta = url1;
    ContQ1 = parseInt($("input[name='preguntas-count']").val());
    tipoRespuesta = "";

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    initTextEditor();
    initTiposPregunta();

    console.log("[Mantenedores - Cuestionarios - Editar] Variables inicializadas...");
}

function initTextEditor() {
    editor1Elemento = CKEDITOR.replace('editor1', {
        uiColor: '#B41601'
    });
    editor2Elemento = CKEDITOR.replace('editor2', {
        uiColor: '#B41601'
    });
}

function initTiposPregunta() {
    tiposPregunta = "";

    tiposPregunta += '<select class="form-control input-sm tipo-pregunta">';

    $.getJSON(urlGetTiposPregunta)
        .done(function (data) {
            if (data.length > 0) {
                tiposPregunta = tiposPregunta + '<option value="0">Elija un tipo de pregunta</option>';

                $.each(data, function (i, tiposPregunta1) {
                    tiposPregunta = tiposPregunta + '<option value="' + tiposPregunta1.codigo + '">' + tiposPregunta1.tipoPregunta + '</option>';
                });
            }
            else {
                tiposPregunta += '<option>No existen datos en el sistema</option>';
            }

            tiposPregunta += '</select>';
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
}

$('body').on('change', 'select.tipo-pregunta', function () {
    var codigoP = $(this).find("option:selected").attr("value");

    var targetElement = $(this).parent().parent().parent().find("div.tipo-respuesta-elemento");

    //console.log(targetElement.children().length);

    tipoRespuesta = "";
    
    switch (codigoP) {
        case '1':
            tipoRespuesta += '<div class="col-sm-4"><label class="col-sm-2 control-label">Agregar alternativas</label> <button type="button" class="addAlt1 btn btn-default pull-right"><span class="glyphicon glyphicon-plus-sign"></span></button></div></div>'
                        + '<div class="form-group"><div class="PanelCuestionarioAlt col-md-6 col-md-offset-2"></div></div>';
            break;
        case '2':
            tipoRespuesta += '<div class="col-sm-4"><label class="col-sm-2 control-label">Agregar alternativas</label> <button type="button" class="addAlt1 btn btn-default pull-right"><span class="glyphicon glyphicon-plus-sign"></span></button></div></div>'
                        + '<div class="form-group"><div class="PanelCuestionarioAlt col-md-6 col-md-offset-2"></div></div>';
            break;
        case '3':
            tipoRespuesta += '<div class="col-sm-4"><label class="col-sm-2 control-label">Agregar alternativas</label> <button type="button" class="addAlt1 btn btn-default pull-right"><span class="glyphicon glyphicon-plus-sign"></span></button></div></div>'
                        + '<div class="form-group"><div class="PanelCuestionarioAlt col-md-6 col-md-offset-2"></div></div>';
            break;
        case '4':
            tipoRespuesta += '<div class="col-sm-4"><label class="col-sm-2 control-label">Agregar alternativas</label> <button type="button" class="addAlt1 btn btn-default pull-right"><span class="glyphicon glyphicon-plus-sign"></span></button></div></div>'
                        + '<div class="form-group"><div class="PanelCuestionarioAlt col-md-6 col-md-offset-2"></div></div>';
            break;
        case '5':
            tipoRespuesta += '<div class="form-group"><label class="col-sm-2 control-label">Contenedor Respuesta</label>'
                        + '<div class="col-sm-6"><textarea class="form-control" rows="3" disabled="disabled"></textarea></div></div>';
            break;
    }

    if (targetElement.children().length == 0) {
        $(targetElement).empty();
        $(targetElement).html(tipoRespuesta);
    }
    else {
        var areaTexto = targetElement.children().find("textarea");

        if (codigoP == '5') {
            $(targetElement).empty();
            $(targetElement).html(tipoRespuesta);
        }
        else {
            if (areaTexto.length == 1) {
                $(targetElement).empty();
                $(targetElement).html(tipoRespuesta);
            }
        }
    }

    //$(targetElement).empty();
    //$(targetElement).html(tipoRespuesta);
});

$('body').on('click', '.addQ1', function () {
    
    tipoRespuesta = "";
    $('.PanelCuestionarioPreguntas').append(
        '<div class="panel panel-default panel-pregunta"><div class="panel-heading" role="tab" id="heading' + ContQ1 + '">'
        + '<h4 class="panel-title"><div class="row">'
        + '<div class="col-md-6">'
        + '<span id="valor-nuevo" class="tituloCuestionario" role="button" data-toggle="collapse" data-parent="#accordion" href="#pregunta' + ContQ1 + '" aria-expanded="true" aria-controls="pregunta' + ContQ1 + '">'
        + 'Pregunta ' + ContQ1 + '</span>'
        + '</div>'
        + '<div class="col-md-6">'
        + '<button type="button" class="btn btn-danger pull-right subQ1">'
        + '<span class="glyphicon glyphicon-minus-sign">'
        + '</span></button></div></div></h4></div>'
        + '<div id="pregunta' + ContQ1 + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' + ContQ1 + '">'
        + '<div class="panel-body"><div class="row-fluid">'
        + '<div class="form-group"><label class="col-sm-2 control-label">Tipo Pregunta</label><div class="col-sm-6">'
        + tiposPregunta
        + '</div></div>'
        + '<div class="form-group"><label class="col-sm-2 control-label">Enunciado Pregunta</label>'
        + '<div class="col-sm-6"><textarea class="form-control enunciado-texto" rows="3"></textarea></div></div>'
        + '<div class="tipo-respuesta-elemento">' + tipoRespuesta + '</div>'
        + '</div>'
        + '</div></div></div>'
        );

    ContQ1 = ContQ1 + 1;
});

$('body').on('click', '.subQ1', function () {
    ContQ1 = ContQ1 - 1;
    $(this).closest('.panel').remove();
});

$("#guardar-cuestionario").click(function () {
    if (!validacionesForm()) { return false; };

    var preguntas = $(".panel-pregunta");
    var tipoPElemento, tipoP, enunciado1, alternativas1, alternativasElement;
    var alternativasId1, idPregunta1;

    var data = [];
    var counter = 0;

    $(preguntas).each(function (index, element) {
        tipoPElemento = $(element).find(".tipo-pregunta");
        tipoP = $(tipoPElemento).find("option:selected").attr("value");

        enunciado1 = $(element).find(".enunciado-texto").val();

        var dataObjJson = {
            idPregunta: "",
            enunciado: "",
            tipo: "",
            alternativas: [],
            alternativasId: []
        };

        dataObjJson.enunciado = enunciado1;
        dataObjJson.tipo = tipoP;
        
        idPregunta1 = $(element).find(".tituloCuestionario").attr("id");

        dataObjJson.idPregunta = idPregunta1;

        alternativasElement = $(element).find(".PanelCuestionarioAlt");
        alternativas1 = $(alternativasElement).find("input");

        $(alternativas1).each(function (index1, element1) {
            alternativasId1 = $(element1).attr("name");

            dataObjJson.alternativas.push($(element1).val());
            dataObjJson.alternativasId.push(alternativasId1);
        });

        data.push(dataObjJson);
    });

    var jsonData = JSON.stringify(data);

    $("#FORM_preguntasJson").val(jsonData);

    $("form").submit();
});

/**  
**  VALIDACIONES
**/
function validacionesForm() {
    var result = true;

    if ($("#FORM_Descripcion").val() == "") { alert("Debe ingresar un nombre para el Cuestionario."); result = false; }
    if ($("#FORM_IdTipoCuestionario option:selected").attr("value") == "") { alert("Debe seleccionar un tipo de Cuestionario."); result = false; }
    if (editor1Elemento.getData() == "") { alert("Debe ingresar un encabezado para el Cuestionario."); result = false; }
    if (editor2Elemento.getData() == "") { alert("Debe ingresar un pie para el Cuestionario."); result = false; }

    var preguntas = $(".panel-pregunta");

    if (preguntas.length == 0) { alert("Debe ingresar por lo menos una pregunta al Cuestionario."); result = false; }

    $(preguntas).each(function (index, element) {
        var enunciado1 = $(element).find(".enunciado-texto").val();
        var tipoPElemento = $(element).find(".tipo-pregunta");
        var tipoP = $(tipoPElemento).find("option:selected").attr("value");

        if (tipoP == "0") { alert("Una de las preguntas no tiene Tipo. Favor ingresar correctamente los datos."); result = false; return false; }

        if (enunciado1 == "") { alert("Una de las preguntas no tiene Enunciado. Favor ingresar correctamente los datos."); result = false; return false; }

        var alternativasElement = $(element).find(".PanelCuestionarioAlt");
        var alternativas1 = $(alternativasElement).find("input");

        if ($(alternativas1).length == 0 && tipoP != "5") { alert("Una de las preguntas de tipo Seleccion no tiene Alternativas. Favor ingresar correctamente los datos."); result = false; return false; }

        $(alternativas1).each(function (index1, element1) {
            if ($(element1).val() == "") { alert("Una de las Alternativas no tiene Valor. Favor ingresar correctamente los datos."); result = false; return false; }
        });
    });

    return result;
}

$('body').on('click', '.addAlt1', function () {
    $(this).closest('.tipo-respuesta-elemento')
        .find('.PanelCuestionarioAlt')
        .append('<div class="input-group"><input name="valor-nuevo" type="text" class="form-control"><span class="input-group-btn"><button type="button" class="btn btn-default subAlt1"><span class="glyphicon glyphicon-minus-sign"></span></button></span></div>');
});

$('body').on('click', '.subAlt1', function () {
    $(this).closest('.input-group').remove();
});

/**
** MODAL SEGUIMIENTO - PREVISUALIZAR CUESTIONARIO
**/
$("#previsualizar-cuestionario-btn").click(function() {
    $("#nombre-cuestionario").text($("#FORM_Descripcion").val());

    $("#tipo-cuestionario-valor").text($("#FORM_IdTipoCuestionario option:selected").text());

    var preguntas = $(".panel-pregunta");

    var htmlDatosCliente = "";

    if ($("#FORM_Abierto").prop('checked') == true) {
        var htmlDatosCliente = "";
        htmlDatosCliente += '<div class="row">';
        htmlDatosCliente += '<label class="col-md-3">Nombre del centro:</label>';
        htmlDatosCliente += '<div class="col-sm-8">';
        htmlDatosCliente += '<input name="ff" type="text" class="form-control input-sm" value="Hospital XYZ - Central">';
        htmlDatosCliente += '</div>';
        htmlDatosCliente += '</div>';
        htmlDatosCliente += '<div class="row">';
        htmlDatosCliente += '<label class="col-md-3">Dirección:</label>';
        htmlDatosCliente += '<div class="col-sm-8">';
        htmlDatosCliente += '<input name="ff" type="text" class="form-control input-sm" value="Pudahuel">';
        htmlDatosCliente += '</div>';
        htmlDatosCliente += '</div>';
        htmlDatosCliente += '<div class="row">';
        htmlDatosCliente += '<label class="col-md-3">Departamento/Ubicación:</label>';
        htmlDatosCliente += '<div class="col-sm-8">';
        htmlDatosCliente += '<input name="ff" type="text" class="form-control input-sm" value="">';
        htmlDatosCliente += '</div>';
        htmlDatosCliente += '</div>';
        htmlDatosCliente += '<div class="row">';
        htmlDatosCliente += '<label class="col-md-3">Ciudad:</label>';
        htmlDatosCliente += '<div class="col-sm-8">';
        htmlDatosCliente += '<input name="ff" type="text" class="form-control input-sm" value="">';
        htmlDatosCliente += '</div>';
        htmlDatosCliente += '</div>';
        htmlDatosCliente += '<div class="row">';
        htmlDatosCliente += '<label class="col-md-3">Estado:</label>';
        htmlDatosCliente += '<div class="col-sm-8">';
        htmlDatosCliente += '<input name="ff" type="text" class="form-control input-sm" value="RM">';
        htmlDatosCliente += '</div>';
        htmlDatosCliente += '</div>';
        htmlDatosCliente += '<div class="row">';
        htmlDatosCliente += '<label class="col-md-3">Código Postal:</label>';
        htmlDatosCliente += '<div class="col-sm-8">';
        htmlDatosCliente += '<input name="ff" type="text" class="form-control input-sm" value="">';
        htmlDatosCliente += '</div>';
        htmlDatosCliente += '</div>';
        htmlDatosCliente += '<div class="row">';
        htmlDatosCliente += '<label class="col-md-3">Contacto principal:</label>';
        htmlDatosCliente += '<div class="col-sm-8">';
        htmlDatosCliente += '<input name="ff" type="text" class="form-control input-sm" value="">';
        htmlDatosCliente += '</div>';
        htmlDatosCliente += '</div>';
        htmlDatosCliente += '<div class="row">';
        htmlDatosCliente += '<label class="col-md-3">Nombre del técnico:</label>';
        htmlDatosCliente += '<div class="col-sm-8">';
        htmlDatosCliente += '<input name="ff" type="text" class="form-control input-sm" value="Juan Perez">';
        htmlDatosCliente += '</div>';
        htmlDatosCliente += '</div>';
        htmlDatosCliente += '<div class="row">';
        htmlDatosCliente += '<label class="col-md-3">Fecha:</label>';
        htmlDatosCliente += '<div class="col-sm-8">';
        htmlDatosCliente += '<input name="ff" type="text" class="form-control input-sm" value="15-06-2016">';
        htmlDatosCliente += '</div>';
        htmlDatosCliente += '</div>';
        htmlDatosCliente += '<hr />';
    }

    $("#datos-cliente").html(htmlDatosCliente);

    var htmlPregunta = "";

    $(preguntas).each(function (index, element) {
        var enunciado1 = $(element).find(".enunciado-texto").val();
        var tipoPElemento = $(element).find(".tipo-pregunta");
        var tipoP = $(tipoPElemento).find("option:selected").attr("value");
        var alternativasElement = $(element).find(".PanelCuestionarioAlt");
        var alternativas1 = $(alternativasElement).find("input");

        htmlPregunta += '<div class="CuestionarioPregunta">';
        htmlPregunta += '<div class="panel-body">';
        htmlPregunta += '<div class="row-fluid">';
        htmlPregunta += '<span>' + enunciado1 + '</span>';
        htmlPregunta += '</div>';
        htmlPregunta += '<br />';
        htmlPregunta += '<div class="col-md-12 pregunta-container">';

        switch (parseInt(tipoP)) {
            case 1:
                htmlPregunta += '<div class="row-fluid no-seleccionable PanelCuestionario respuesta">';
                htmlPregunta += '<div class="respuesta-alternativas">';

                $(alternativas1).each(function (index1, element1) {
                    htmlPregunta += '<div class="btn-group">';
                    htmlPregunta += '<button name="' + tipoP + '" type="button" class="btn btn-default"><span class="glyphicon glyphicon-minus-sign"></span></button>';
                    htmlPregunta += '<p class="input-group-addon form-control">' + $(element1).val() + '</p>';
                    htmlPregunta += '</div>';
                });

                htmlPregunta += '</div>';
                htmlPregunta += '</div>';
                break;
            case 2:
                htmlPregunta += '<div class="row-fluid no-seleccionable PanelCuestionario respuesta">';
                htmlPregunta += '<div class="respuesta-alternativas">';

                $(alternativas1).each(function (index1, element1) {
                    htmlPregunta += '<div class="btn-group">';
                    htmlPregunta += '<button name="' + tipoP + '" type="button" class="btn btn-default"><span class="glyphicon glyphicon-minus-sign"></span></button>';
                    htmlPregunta += '<p class="input-group-addon form-control">' + $(element1).val() + '</p>';
                    htmlPregunta += '</div>';
                });

                htmlPregunta += '</div>';
                htmlPregunta += '</div>';
                break;
            case 3:
                htmlPregunta += '<div class="row-fluid no-seleccionable PanelCuestionario respuesta">';
                htmlPregunta += '<div class="respuesta-alternativas">';

                $(alternativas1).each(function (index1, element1) {
                    htmlPregunta += '<div class="btn-group">';
                    htmlPregunta += '<button name="' + tipoP + '" type="button" class="btn btn-default"><span class="glyphicon glyphicon-minus-sign"></span></button>';
                    htmlPregunta += '<p class="input-group-addon form-control">' + $(element1).val() + '</p>';
                    htmlPregunta += '</div>';
                });

                htmlPregunta += '</div>';
                htmlPregunta += '</div>';
                break;
            case 4:
                htmlPregunta += '<div class="row-fluid no-seleccionable PanelCuestionario respuesta">';
                htmlPregunta += '<div class="respuesta-alternativas">';

                $(alternativas1).each(function (index1, element1) {
                    htmlPregunta += '<div class="btn-group">';
                    htmlPregunta += '<button name="' + tipoP + '" type="button" class="btn btn-default"><span class="glyphicon glyphicon-minus-sign"></span></button>';
                    htmlPregunta += '<p class="input-group-addon form-control">' + $(element1).val() + '</p>';
                    htmlPregunta += '</div>';
                });

                htmlPregunta += '</div>';
                htmlPregunta += '</div>';
                break;
            case 5:
                htmlPregunta += '<div class="respuesta">';
                htmlPregunta += '<div class="respuesta-desarrollo">';
                htmlPregunta += '<textarea class="form-control"></textarea>';
                htmlPregunta += '</div>';
                htmlPregunta += '</div>';
                break;
        }

        htmlPregunta += '</div>';
        htmlPregunta += '</div>';
        htmlPregunta += '</div>';
    });

    $("#preguntas-cuestionario").html(htmlPregunta);

    $("#encabezado-cuestionario").html(editor1Elemento.getData());

    $("#pie-cuestionario").html(editor2Elemento.getData());

    $('#ModalChecklist').modal();
});


$('body').on('click', '.PanelCuestionario button', function () {

    console.log($(this).attr('name'));

    if ($(this).attr('name') == "2") {
        $(this).parent().parent().find('.selected-JJ').removeClass('selected-JJ');
        $(this).parent().parent().find('.glyphicon-ok-sign').addClass('glyphicon-minus-sign');
        $(this).parent().parent().find('.glyphicon-ok-sign').removeClass('glyphicon-ok-sign');
    }

    if ($(this).hasClass('selected-JJ')) {
        $(this).removeClass('selected-JJ');
        $(this).children().removeClass('glyphicon-ok-sign');
        $(this).children().addClass('glyphicon-minus-sign');
    }
    else {
        $(this).addClass('selected-JJ');
        $(this).children().removeClass('glyphicon-minus-sign');
        $(this).children().addClass('glyphicon-ok-sign');
    }
});

