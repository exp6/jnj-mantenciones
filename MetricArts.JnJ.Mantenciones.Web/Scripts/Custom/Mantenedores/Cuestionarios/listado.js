﻿/**  
**  INIT
**/
var listadoMantenedor;

function init() {
    console.log("[Mantenedores - Cuestionarios - Listado] Inicializando variables...");

    iniciarTabla();

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Mantenedores - Cuestionarios - Listado] Variables inicializadas...");
}

function iniciarTabla() {
    $('#listado-mantenedor').DataTable({
        language: {
            lengthMenu: "Mostrar _MENU_ registros por página",
            zeroRecords: "No existen elementos en el mantenedor.",
            info: "Mostrando página _PAGE_ de _PAGES_",
            infoEmpty: "Puede crear nuevos elementos seleccionando la opción Crear Nuevo",
            paginate: {
                first: "Primero",
                last: "Último",
                next: "Siguiente",
                previous: "Anterior"
            },
            search: "Buscar:",
            infoFiltered: "(Filtrado de un total de _MAX_ registros)"
        }
    });
}

/**
** MODAL SEGUIMIENTO - PREVISUALIZAR CUESTIONARIO
**/
function previsualizarCuestionario() {
    mantencionIDGl = mantencionId;

    $.ajax({
        url: urlResponderChecklist,
        data: { IdMantencion: mantencionId },
        traditional: true,
        dataType: "html",
        success: function (data) {
            triggerCloseModal = 0;
            $("#checklist-contenido").html(data);
            $('#ModalChecklist').modal();
        }
    });
}