﻿/**  
**  INIT
**/


function init() {
    console.log("[Mantenedores - Distribuidores - Crear] Inicializando variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Mantenedores - Distribuidores - Crear] Variables inicializadas...");
}
