﻿/**  
**  INIT
**/


function init() {
    console.log("[Mantenedores - Distribuidores - Editar] Inicializando variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Mantenedores - Distribuidores - Editar] Variables inicializadas...");
}