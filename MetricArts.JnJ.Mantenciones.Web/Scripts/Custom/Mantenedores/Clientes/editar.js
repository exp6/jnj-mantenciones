﻿/**  
**  INIT
**/


function init() {
    console.log("[Mantenedores - Clientes - Editar] Inicializando variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Mantenedores - Clientes - Editar] Variables inicializadas...");
}