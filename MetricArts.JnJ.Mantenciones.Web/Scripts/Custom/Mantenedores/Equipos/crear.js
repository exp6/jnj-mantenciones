﻿/**  
**  INIT
**/


function init() {
    console.log("[Mantenedores - Equipos - Crear] Inicializando variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Mantenedores - Equipos - Crear] Variables inicializadas...");
}

$('.dateButtonEquipo').parent().parent().find('.datepickershow-equipo').datepicker({
    format: 'dd-mm-yyyy',
    clearBtn: true
});

$('.dateButtonEquipo').click(function () {
    $(this).parent().parent().find('.datepickershow-equipo').datepicker("show");
});