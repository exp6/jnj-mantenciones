﻿/**  
**  INIT
**/


function init() {
    console.log("[Mantenedores - Franquicias - Editar] Inicializando variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Mantenedores - Franquicias - Editar] Variables inicializadas...");
}