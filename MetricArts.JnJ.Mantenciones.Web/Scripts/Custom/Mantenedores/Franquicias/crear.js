﻿/**  
**  INIT
**/


function init() {
    console.log("[Mantenedores - Franquicias - Crear] Inicializando variables...");

    $('.fadeOutMantenedor').fadeTo(3000, 1).fadeOut('slow');

    console.log("[Mantenedores - Franquicias - Crear] Variables inicializadas...");
}