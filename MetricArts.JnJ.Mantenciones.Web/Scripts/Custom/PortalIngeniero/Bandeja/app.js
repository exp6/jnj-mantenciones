﻿/**  
**  INIT
**/
var urlGetSucursales, urlCalendarioJsonFeed, urlGetMantencion, urlCambiarNuevoEstado, urlGetKits, urlGetRepuestosKit, urlEnviarSolicitudRepuesto, urlResponderChecklist;
var mantencionIDGl;
var IDEquipo;
var eventosCalendario;
var triggerCloseModal = 0;
var htmlFormularioCambioEstado;

function init(url1, url2, url3, url4, url5, url6, url7, url8) {
    console.log("[Portal Ingeniero - Bandeja] Inicializando variables...");

    urlGetSucursales = url1;
    urlCalendarioJsonFeed = url2;
    urlGetMantencion = url3;
    urlCambiarNuevoEstado = url4;
    urlGetKits = url5;
    urlGetRepuestosKit = url6;
    urlEnviarSolicitudRepuesto = url7;
    urlResponderChecklist = url8;

    inicializaCalendar();

    $('a[role="tab"]').on('shown.bs.tab', function (e) {
        $('#calendario-bandeja-ingeniero').fullCalendar('render');
    });
    $('#Lista a:first').tab('show');

    htmlFormularioCambioEstado = $('#ModalDet').find("#cambiar-estado-seguimiento").find("#estados-formulario-cambio").children();

    console.log("[Portal Ingeniero - Bandeja] Variables inicializadas...");
}

/**
** CALENDARIO
**/
function inicializaCalendar() {
    eventosCalendario = {
        events: []
    };

    $.getJSON(urlCalendarioJsonFeed)
        .done(function (data) {
            mantencionesDetalle = [];

            $.each(data, function (i, evento) {
                var nuevoEvento = {
                    id: evento.id,
                    title: evento.title,
                    color: evento.color,
                    start: moment(evento.start, 'MMM Do YYYY h:mm:ssa')
                };

                var nuevaMantencion = {
                    idMantencion: evento.id,
                    equipo: evento.title,
                    tipoContrato: evento.tipoContrato,
                    estadoMantencion: evento.estadoMantencion,
                    sucursal: evento.sucursal,
                    cliente: evento.cliente,
                    ingeniero: evento.ingeniero,
                    codigoEquipo: evento.codigoEquipo
                };

                eventosCalendario.events.push(nuevoEvento);
                mantencionesDetalle.push(nuevaMantencion);
            });

            // Inicializa Calendario con opciones
            $('#calendario-bandeja-ingeniero').fullCalendar({
                lang: 'es',
                today: false,
                weekends: false,
                editable: false,
                defaultDate: moment().startOf('day'),
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                buttonText: {
                    prev: "<",
                    next: ">",
                    prevYear: "prev year",
                    nextYear: "next year",
                    year: 'Año', // TODO: locale files need to specify this
                    today: 'Hoy',
                    month: 'Mes',
                    week: 'Sem',
                    day: 'Día'
                },
                events: eventosCalendario.events,
                eventClick: function (calEvent, jsEvent, view) {
                    gestionarMantencion(calEvent.id);
                }
            });
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
}

/**
** DYNAMIC DROP DOWN LIST
**/
$('#FORM_IdCliente').on('change', function () {
    var sucursales = $('#FORM_IdSucursal');
    sucursales.prop('disabled', true);
    sucursales.empty();
    var cliente = $(this).val();
    nombreCliente = $(this).find("option:selected").text();

    if (cliente.length > 0) {

        $.getJSON(urlGetSucursales, {
            idCliente: cliente
        })
        .done(function (data) {
            var msg = "No existen Sucursales para el cliente seleccionado";
            if (data.length > 0) {
                sucursales.prop('disabled', false);
                msg = "Todas las Sucursales";
            }
            var option = $("<option/>").attr("value", "0").text(msg);
            sucursales.append(option);

            $.each(data, function (i, sucursal) {

                var option = $("<option/>").attr("value", sucursal.Value).text(sucursal.Text);
                sucursales.append(option);
            });
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
    }
});

/**
** MODAL SEGUIMIENTO
**/
function llenadoCamposModal(mantencion) {

    if (mantencion == null) { alert("Ocurrió un error de comunicación. Intentelo más tarde."); return false; }

    var modalGestion = $('#ModalDet');

    var seccionDetalle,
        seccionHistoricos,
        seccionChecklist,
        seccionEstado,
        seccionRepuestos,
        seccionLotus;

    seccionDetalle = modalGestion.find("#detalle-seguimiento");
    seccionHistoricos = modalGestion.find("#historicos-seguimiento");
    seccionChecklist = modalGestion.find("#checklist-seguimiento");
    seccionEstado = modalGestion.find("#cambiar-estado-seguimiento");
    seccionRepuestos = modalGestion.find("#repuestos-seguimiento");
    seccionLotus = modalGestion.find("#lotus-seguimiento");

    //Titulo
    modalGestion.find("#detalle-seguimiento-equipo-titulo").text("(" + mantencion.equipo + " Cod.: " + mantencion.codigoEquipo + ")");

    //Seccion Detalle
    seccionDetalle.find("#detalle-seguimiento-cliente").text(mantencion.cliente);
    seccionDetalle.find("#detalle-seguimiento-sucursal").text(mantencion.sucursal);
    seccionDetalle.find("#detalle-seguimiento-planificacion").text(mantencion.planificacion);
    seccionDetalle.find("#detalle-seguimiento-ingeniero").text(mantencion.ingeniero);

    seccionDetalle.find("#detalle-seguimiento-tabla-codigo-equipo").text(mantencion.codigoEquipo);
    seccionDetalle.find("#detalle-seguimiento-tabla-nombre-equipo").text(mantencion.equipo);
    seccionDetalle.find("#detalle-seguimiento-tabla-franquicia").text(mantencion.franquicia);
    seccionDetalle.find("#detalle-seguimiento-tabla-descripcion").text("-");
    seccionDetalle.find("#detalle-seguimiento-tabla-fecha-instalacion").text(mantencion.fechaInstalacionEquipo);
    seccionDetalle.find("#detalle-seguimiento-tabla-fecha-visita").text(mantencion.fechaConfirmacion.format('DD-MM-YYYY'));

    //Seccion Cambiar Estado Mantención
    if (mantencion.checkListRespondido) {
        
        seccionEstado.find("#estados-formulario-cambio").empty();
        seccionEstado.find("#estados-formulario-cambio").append('<label class="col-sm-4 control-label">La Mantención ha finalizado. No puede realizar cambios de estado.</label>');
    }
    else {
        seccionEstado.find("#estados-formulario-cambio").empty();
        seccionEstado.find("#estados-formulario-cambio").append(htmlFormularioCambioEstado);
        seccionEstado.find("#estados-seguimiento-valores").val(mantencion.estado);
    }

    //Seccion Estados Historicos
    var cuerpoTabla = $("#estados-historicos-seguimiento-registros");
    cuerpoTabla.empty();

    var filaHistorico = "";
    var fechaActualizacion;

    for (var i = 0; i < mantencion.estadosHistoricos.length; i++) {

        fechaActualizacion = moment(mantencion.estadosHistoricos[i].fechaModificacion, 'MMM Do YYYY h:mm:ssa');

        filaHistorico += "<tr>";
        filaHistorico += "<th>" + mantencion.estadosHistoricos[i].estadoDescripcion + "</th>";
        filaHistorico += "<td>" + (mantencion.estadosHistoricos[i].observaciones == '' ? "-" : mantencion.estadosHistoricos[i].observaciones) + "</td>";
        filaHistorico += "<td>" + mantencion.estadosHistoricos[i].atendidoPor + "</td>";
        filaHistorico += "<td>" + fechaActualizacion.format('DD-MM-YYYY HH:mm') + "</td>";
        filaHistorico += "</tr>";
    }
    cuerpoTabla.append(filaHistorico);

    //Seccion CheckList

    //Seccion Solicitud de Respuestos
    var selectKitRepuestos = $("#repuestos-seguimiento-kit-seleccion");
    selectKitRepuestos.empty();
    selectKitRepuestos.prop('disabled', true);

    $("#repuestos-seguimiento-seleccion-botones").hide();

    $.getJSON(urlGetKits, { IdEquipo: mantencion.idEquipo })
        .done(function (data) {
            var msg = "No existen KIT para el equipo.";
            if (data.length > 0) {
                selectKitRepuestos.prop('disabled', false);
                msg = "Seleccione un KIT";
            }
            var option = $("<option/>").attr("value", "0").text(msg);
            selectKitRepuestos.append(option);

            $.each(data, function (i, kit) {
                option = $("<option/>").attr("value", kit.Value).text(kit.Text);
                selectKitRepuestos.append(option);
            });

            modalGestion.modal();
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
            alert("Ocurrió un error de comunicación. Intentelo más tarde.");
        });

    //Seccion Lotus

    
}

function gestionarMantencion(mantencionId) {
    var mantencion;
    mantencionIDGl = mantencionId;

    $.getJSON(urlGetMantencion, { IdMantencion: mantencionId })
        .done(function (data) {
            mantencion = {
                idMantencion: data[0].id,
                equipo: data[0].equipo,
                tipoContrato: data[0].tipoContrato,
                estadoMantencion: data[0].estadoMantencion,
                sucursal: data[0].sucursal,
                cliente: data[0].cliente,
                ingeniero: data[0].ingeniero,
                codigoEquipo: data[0].codigoEquipo,
                fechaConfirmacion: moment(data[0].fechaConfirmacion, 'MMM Do YYYY h:mm:ssa'),
                fechaInstalacionEquipo: data[0].fechaInstalacionEquipo,
                planificacion: data[0].planificacion,
                franquicia: data[0].franquicia,
                estado: data[0].estadoMantencionCodigo,
                estadosHistoricos: data[0].estadosHistorico,
                idEquipo: data[0].idEquipo,
                checkListRespondido: data[0].checkListRespondido
            };

            llenadoCamposModal(mantencion);
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
            alert("Ocurrió un error de comunicación. Intentelo más tarde.");
        });
}

/**
** MODAL SEGUIMIENTO - GUARDAR NUEVO ESTADO
**/
$(document).on('click', '#guardar-nuevo-estado', function () {
    var modalGestion = $('#ModalDet');
    var nuevoEstadoTemp = $("#estados-seguimiento-valores").val();
    var numeroEstado = parseInt(nuevoEstadoTemp);
    var observaciones = $("#estados-seguimiento-observaciones").val();

    console.log("works!");

    $.ajax({
        url: urlCambiarNuevoEstado,
        method: "POST",
        data: { IdMantencion: mantencionIDGl, nuevoEstado: numeroEstado, obs: observaciones },
        traditional: true,
        success: function (data) {
            actualizarColoresLista(nuevoEstadoTemp);
            actualizarColoresCalendario(numeroEstado);
            $("#estados-seguimiento-observaciones").val("");
            modalGestion.modal('hide');
        }
    }); 
});

function actualizarColoresLista(nuevoEstadoTemp) {
    var elementoFila = $("#" + mantencionIDGl.toLowerCase()).parent().parent();

    console.log(mantencionIDGl);

    var claseEstado;
    var numEstado;
    var elementoCounterEstado;

    var claseEstadoBoton = elementoFila.attr("class");
    var numEstadoAntiguo;

    var elementoCol = elementoFila.find(".checklist-col");
    elementoCol.empty();
    elementoCol.append("-");

    switch (claseEstadoBoton) {
        case "warning":
            elementoCounterEstado = $("#espera-visita-boton").find(".badge");
            break;
        case "info":
            elementoCounterEstado = $("#en-curso-boton").find(".badge");
            break;
        case "danger":
            elementoCounterEstado = $("#en-espera-momento-boton").find(".badge");
            break;
        case "success":
            elementoCounterEstado = $("#finalizada-boton").find(".badge");
            break;
    }
    numEstadoAntiguo = parseInt(elementoCounterEstado.text());
    elementoCounterEstado.text(numEstadoAntiguo - 1);

    switch (nuevoEstadoTemp) {
        case "2":
            claseEstado = "warning";
            elementoCounterEstado = $("#espera-visita-boton").find(".badge");
            break;
        case "3":
            claseEstado = "info";
            elementoCounterEstado = $("#en-curso-boton").find(".badge");
            break;
        case "4":
            claseEstado = "danger";
            elementoCounterEstado = $("#en-espera-momento-boton").find(".badge");
            break;
        case "5":
            claseEstado = "success";
            elementoCounterEstado = $("#finalizada-boton").find(".badge");
            elementoCol.empty();
            elementoCol.append("<button id='" + mantencionIDGl + "' class='btn btn-default' type='button' onclick='responderChecklist(\"" + mantencionIDGl + "\");'><span class='glyphicon glyphicon-check'></span></button>");
            break;
        default:
            claseEstado = "warning";
            elementoCounterEstado = $("#espera-visita-boton").find(".badge");
            break;
    }
    numEstado = parseInt(elementoCounterEstado.text());
    elementoCounterEstado.text(numEstado + 1);

    elementoFila.attr("class", claseEstado);
}

function actualizarColoresCalendario(numEstadoInput) {
    var eventoAntiguo, eventoNuevo;
    var coloresEventos = ["#F89406", "#0088CC", "#BD362F", "#51A351"];

    for (var i = 0; i < eventosCalendario.events.length; i++) {
        if (eventosCalendario.events[i].id.toUpperCase() == mantencionIDGl.toUpperCase()) {
            eventoAntiguo = eventosCalendario.events[i];
            break;
        }
    }
    eventoNuevo = eventoAntiguo;
    eventoNuevo.color = coloresEventos[numEstadoInput-2];

    $("#calendario-bandeja-ingeniero").fullCalendar('removeEvents', eventoAntiguo.id);
    $("#calendario-bandeja-ingeniero").fullCalendar('renderEvent', eventoNuevo, true);
}

/**
** MODAL SEGUIMIENTO - SOLICITUD DE REPUESTOS
**/
$('.SelectAll').click(function () {
    $('.ListPlan').find('input').prop('checked', true);
});

$('.UnSelect').click(function () {
    $('.ListPlan').find('input').prop('checked', false);
});

$('#repuestos-seguimiento-kit-seleccion').on('change', function () {
    var kit = $(this).val();

    if (kit == "0") {
        $("#repuestos-seguimiento-seleccion-botones").hide();
    }
    else {
        var tablaCuerpo = $("#repuestos-seguimiento-cuerpo-tabla-equipos");
        tablaCuerpo.empty();

        if (kit.length > 0) {
            $.getJSON(urlGetRepuestosKit, {
                idKitRepuestos: kit
            })
            .done(function (data) {
                var contenidoTabla = "";

                $.each(data, function (i, repuesto) {
                    contenidoTabla += "<tr>";
                    contenidoTabla += "<td><input id='" + repuesto.IdRepuesto + "' type='checkbox' checked></td>";
                    contenidoTabla += "<td>" + repuesto.Codigo + "</td>";
                    contenidoTabla += "<td>" + repuesto.Descripcion + "</td>";
                    contenidoTabla += "<td>" + repuesto.Categoria + "</td>";
                    contenidoTabla += "<td>";
                    contenidoTabla += "<input type='number' class='form-control input-sm' value='1' min='" + repuesto.Cantidad + "' />";
                    contenidoTabla += "</td>";
                    contenidoTabla += "</tr>";
                });

                tablaCuerpo.append(contenidoTabla);

                $("#repuestos-seguimiento-seleccion-botones").show();

            })
            .fail(function (jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                console.log("Request Failed: " + err);
            });
    }

    
    }
});

$("#repuestos-seguimiento-enviar-solicitud").click(function () {
    var modalGestion = $('#ModalDet');

    var respuestosChecked = $("#repuestos-seguimiento-cuerpo-tabla-equipos input:checked").map(function () {
        return $(this).attr('id');
    }).get();

    var respuestosInput = [];
    var elemento;

    for (var i = 0 ; i < respuestosChecked.length; i++) {
        elemento = $("#" + respuestosChecked[i]).parent().parent().find("input[type='number']");
        respuestosInput.push(elemento.val());
    }

    var idKit = $("#repuestos-seguimiento-kit-seleccion").val();

    $.ajax({
        url: urlEnviarSolicitudRepuesto,
        type: "POST",
        data: { IdMantencion: mantencionIDGl, IdKit: idKit, RespuestosSeleccionados: respuestosChecked, RespuestosCantidad: respuestosInput },
        traditional: true,
        success: function (data) {

            $('#ModalExitoRepuesto').modal();

            $("#ModalExitoRepuestoTitulo").text("Se ha realizado la solicitud con éxito. Puede visualizar el estado de la orden en su bandeja de solicitudes.");
        },
        error: function (result) {
            alert("Error");
        }
    });

    modalGestion.modal('hide');
});


/**
** MODAL SEGUIMIENTO - RESPONDER CHECKLIST
**/
function responderChecklist(mantencionId)
{
    mantencionIDGl = mantencionId;

    $.ajax({
        url: urlResponderChecklist,
        data: { IdMantencion: mantencionId },
        traditional: true,
        dataType: "html",
        success: function (data) {
            triggerCloseModal = 0;
            $("#checklist-contenido").html(data);
            $('#ModalChecklist').modal();
        }
    });
}

$('#ModalChecklist').on('hidden.bs.modal', function () {
    if (triggerCloseModal == 0) {
        window.location.reload();
    }
})

$("#salir-boton-cerrar").click(function () {
    triggerCloseModal = 1;
    $('#ModalChecklist').modal('hide');
});

$("#salir-boton-x").click(function () {
    triggerCloseModal = 1;
    $('#ModalChecklist').modal('hide');
});