﻿/**  
**  INIT
**/
var urlDetalleSolicitudRepuestos;

function init(url1) {
    console.log("[Portal Ingeniero - Repuestos] Inicializando variables...");

    urlDetalleSolicitudRepuestos = url1;

    console.log("[Portal Ingeniero - Repuestos] Variables inicializadas...");
}

$(".btn-modal-detalle-solicitud-repuestos").click(function () {
    var modalGestion = $('#ModalDet');
    var idSolicitudKit = $(this).attr('id');

    var tablaCuerpo = $("#detalle-cuerpo-tabla");
    tablaCuerpo.empty();

    var elementoFila = $(this).parent().parent();

    $("#detalle-equipo").text(elementoFila.find(".repuesto-equipo-nombre").text());
    $("#detalle-fecha-solicitud").text(elementoFila.find(".repuesto-fecha-solicitud").text());
    $("#detalle-kit").text(elementoFila.find(".repuesto-kit").text());
    $("#detalle-estado").text(elementoFila.find(".repuesto-estado").text());


    $.getJSON(urlDetalleSolicitudRepuestos, {
        IdSolicitudKit: idSolicitudKit
    })
            .done(function (data) {
                var contenidoTabla = "";

                $.each(data, function (i, repuesto) {
                    contenidoTabla += "<tr>";
                    contenidoTabla += "<th>" + repuesto.Codigo + "</th>";
                    contenidoTabla += "<td>" + repuesto.Descripcion + "</td>";
                    contenidoTabla += "<td>" + repuesto.Categoria + "</td>";
                    contenidoTabla += "<td>" + repuesto.Cantidad + "</td>";
                    contenidoTabla += "</tr>";
                });

                tablaCuerpo.append(contenidoTabla);
                modalGestion.modal();
            })
            .fail(function (jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                console.log("Request Failed: " + err);
            });

});