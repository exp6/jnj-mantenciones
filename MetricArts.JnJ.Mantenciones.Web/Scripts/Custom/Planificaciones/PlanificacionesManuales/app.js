﻿/**  
**  INIT
**/
var urlGetClientes, urlGetSucursales, urlGetEquiposSucursales, urlGetPlanificaciones, urlGetFormatosCorreo, urlGetEquiposSucursal, urlGetFormatosPDF, urlGetPDFTemplatePreview, urlGetMailTemplatePreview;

function init(url1, url2, url3) {
    console.log("[Planificaciones - Crear Manual] Inicializando variables...");

    urlGetClientes = url1;
    urlGetSucursales = url2;
    urlGetEquiposSucursales = url3;

    var crearPlanificacionStep = 1;

    //Inicializa wizard
    $('#crear-planificacion-wz').bootstrapWizard({
        'tabClass': 'nav nav-tabs', 'debug': false, onShow: function (tab, navigation, index) {
        }, onNext: function (tab, navigation, index) {

            if (!validacionStep(crearPlanificacionStep)) { return false; }

            if (crearPlanificacionStep == 1) {
                $("#franquicia-bd1").text($('#formSeleccionNegocio_IdFranquicia option:selected').text());
                $("#cliente-bd1").text($('#FORM_IdCliente option:selected').text());
                $("#sucursal-bd1").text($('#FORM_IdSucursal option:selected').text());

                $("#franquicia-bd2").text($('#formSeleccionNegocio_IdFranquicia option:selected').text());
                $("#cliente-bd2").text($('#FORM_IdCliente option:selected').text());
                $("#sucursal-bd2").text($('#FORM_IdSucursal option:selected').text());
               
            }

            if (crearPlanificacionStep == 2) {
                nextStep();
            }
            
            ++crearPlanificacionStep;

        }, onPrevious: function (tab, navigation, index) {

            --crearPlanificacionStep;

        }, onLast: function (tab, navigation, index) {
        }, onTabClick: function (tab, navigation, index) {
        }, onTabShow: function (tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index + 1;
            var $percent = ($current / $total) * 100;
            $('#crear-planificacion-wz .progress-bar').css({ width: $percent + '%' });
            // If it's the last tab then hide the last button and show the finish instead
            if ($current >= $total) {
                $('#crear-planificacion-wz').find('.pager .next').hide();
                $('#crear-planificacion-wz').find('.pager .finish').show();
                $('#crear-planificacion-wz').find('.pager .finish').removeClass('disabled');
            } else {
                $('#crear-planificacion-wz').find('.pager .next').show();
                $('#crear-planificacion-wz').find('.pager .finish').hide();
            }

        }
    });

    $(".nav-tabs").click(function () {
        return false;
    });

    console.log("[Planificaciones - Crear Manual] Variables inicializadas...");
}

/**  
**  VALIDACIONES
**/
function validacionStep(step) {
    var result = true;

    switch (step) {
        case 1:
            if ($("#FORM_IdSucursal option:selected").val() == '0' || $("#FORM_IdSucursal option:selected").length == 0) {
                alert('Debe seleccionar una sucursal.');
                result = false;
            }
            return result;
            break;
        case 2:
            if ($('#FORM_Nombre').val() == '') { alert('Debe colocar un nombre de planificación.'); result = false; }
            if (($('#FORM_Anio option:selected').val() == '' || $("#FORM_Anio option:selected").length == 0)) { alert('Debe seleccionar un año de planificación.'); result = false; }

            return result;
            break;
        case 3:
            var idsEquiposSeleccionados = $("#equipos-sucursal input:checked").map(function () {
                return $(this).attr('id');
            }).get();
            console.log(idsEquiposSeleccionados);
            //if (idsEquiposSeleccionados.length == 0) { alert("Debe considerar por lo menos un equipo."); result = false; }
            return result;
            break;
        default:
            return result;
            break;
    }
}

/**
** DYNAMIC DROP DOWN LIST
**/
$('#formSeleccionNegocio_IdFranquicia').on('change', function () {
    var clientes = $('#FORM_IdCliente');
    clientes.prop('disabled', true);
    var sucursales = $('#FORM_IdSucursal');
    sucursales.prop('disabled', true);
    sucursales.empty();
    clientes.empty();

    var franquicia = $(this).val();
    if (franquicia.length > 0) {
        $.getJSON(urlGetClientes, {
            franquiciaId: franquicia
        })
        .done(function (data) {

            var msg = "No existen Clientes para la franquicia seleccionada";
            if (data.length > 0) {
                clientes.prop('disabled', false);
                msg = "Seleccione un cliente";
            }
            var option = $("<option/>").text(msg);
            clientes.append(option);

            $.each(data, function (i, cliente) {
                var option = $("<option/>").attr("value", cliente.Value).text(cliente.Text);
                clientes.append(option);
            });


        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
    }
});

$('#FORM_IdCliente').on('change', function () {
    var sucursales = $('#FORM_IdSucursal');
    sucursales.prop('disabled', true);
    sucursales.empty();
    var cliente = $(this).val();

    if (cliente.length > 0) {

        $.getJSON(urlGetSucursales, {
            idCliente: cliente
        })
        .done(function (data) {
            var msg = "No existen Sucursales para el cliente seleccionado";
            if (data.length > 0) {
                sucursales.prop('disabled', false);
                msg = "Seleccione una sucursal";
            }
            var option = $("<option/>").attr("value", "0").text(msg);
            sucursales.append(option);

            $.each(data, function (i, sucursal) {

                var option = $("<option/>").attr("value", sucursal.Value).text(sucursal.Text);
                sucursales.append(option);
            });
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
    }
});




function nextStep() {
    var selectedSucursal = $('#FORM_IdSucursal option:selected');
    //var selectedContrato = $('#tipo-contrato-select option:selected');
    var selectedAnioPlanificacion = $('#FORM_Anio option:selected');
    var modoPlanAutomatico = $('input[name=ModoPlanAutomatico]:checked').attr('id');

    if (selectedSucursal.length > 0 && $(selectedAnioPlanificacion).val() != '') {
        getEquipos($(selectedSucursal).val(), $(selectedAnioPlanificacion).val(), modoPlanAutomatico);
    }
}

function getEquipos(idSucursalSelected, anioPlanificacion, modoPlanAutomatico) {
    $("#tipo-contrato").text((modoPlanAutomatico == "0" ? ' (Selección Manual)' : ' (Calculadas automáticamente según contrato)'));
    $.ajax({
        url: urlGetEquiposSucursales,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        data: { idSucursal: idSucursalSelected, AnioPlanificacion: anioPlanificacion },
        dataType: "json",
        success: function (data) {
            var row = "";
            var element;
            var inputtextcalendario;
            $.each(data, function (index, item) {
                inputtextcalendario = "";
                inputtextcalendario += "";
                inputtextcalendario += getMesesAnio();

                element = (modoPlanAutomatico == "0" ? "<td>Escoja el mes propuesto para la mantención del equipo<ul>" + inputtextcalendario + " del <strong>" + anioPlanificacion + "</strong>" : "<td>Períodos (Contrato: " + item.contrato + ") <ul>" + fechasToString(item.fechasPropuesta));

                row += "<tr><td><input id='" + item.IdEquipo + "' type='checkbox' checked='checked' data-toggle='toggle'></td>"
                + "<th>" + item.nombreEquipo + " (Cod.: " + item.codigoEquipo + ")</th>"
                + element
                + "</ul>"
                + "Fecha instalación en Sucursal: <strong>" + item.fechaInstalacion + "</strong>"
                + "<br />"
                + "Fecha última mantención: <strong>" + item.fechaUltimaMantencion + "</strong>"
                + "</td></tr>";
            });
            $("#equipos-sucursal").html(row);
            
        },
        error: function (result) {
            alert("Error");
        }
    });
}

function fechasToString(fechas) {
    var fechasStr = "";
    for (var i = 0; i < fechas.length; i++) {
        fechasStr += "<li><strong>" + fechas[i] + "</strong></li>";
    }
    return fechasStr;
}

function getMesesAnio() {
    return "<select class='input-sm' name='mes-mantencion' onchange='' size='1'>" +
    "<option value='01'>Enero</option>"+
    "<option value='02'>Febrero</option>"+
    "<option value='03'>Marzo</option>"+
    "<option value='04'>Abril</option>"+
    "<option value='05'>Mayo</option>"+
    "<option value='06'>Junio</option>"+
    "<option value='07'>Julio</option>"+
    "<option value='08'>Agosto</option>"+
    "<option value='09'>Septiembre</option>"+
    "<option value='10'>Octubre</option>"+
    "<option value='11'>Noviembre</option>"+
    "<option value='12'>Diciembre</option>"+
    "</select>";
}

/**
** FIN WIZARD
**/
$("#fin-crear-planificacion").click(function () {
    var idsEquiposSeleccionados = $("#equipos-sucursal input:checked").map(function () {
        return $(this).attr('id');
    }).get();

    $("#FORM_equiposSeleccionadosStr").val(idsEquiposSeleccionados.join(";"));

    var modoPlanAutomatico = $('input[name=ModoPlanAutomatico]:checked').attr('id');
    $("#FORM_modoPlan").val(modoPlanAutomatico);

    if (modoPlanAutomatico == "0") {
        var periodosEquipos = [];
        var periodo;

        $(idsEquiposSeleccionados).each(function (index, element) {
            periodo = $('#' + element).parent().parent().find('select option:selected').val();
            periodosEquipos.push(periodo);
        });

        $("#FORM_equiposSeleccionadosPeriodosStr").val(periodosEquipos.join(";"));
    }

    $("#FORM_IdSucursal1").val($('#FORM_IdSucursal option:selected').val());

    if (!validacionStep(3)) { return false; }

    $("form").submit();
});