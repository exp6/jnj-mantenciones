﻿/**  
**  INIT
**/
var urlSeleccion,
    urlPreviewPdf,
    urlGetFranquicias,
    urlGetClientes,
    planAutomaticoStep,
    tablaClientesSeleccionados,
    cantidadClientes,
    datosTabla;

function init(url1, url2, url3, url4) {
    console.log("[Planificaciones Automáticas] Inicializando variables...");

    urlSeleccion = url1;
    urlPreviewPdf = url2;
    urlGetFranquicias = url3;
    urlGetClientes = url4;

    planAutomaticoStep = 1;

    //Inicializa wizard
    $('#plan-automaticas-wz').bootstrapWizard({
        'tabClass': 'nav nav-tabs', 'debug': false, onShow: function (tab, navigation, index) {
        }, onNext: function (tab, navigation, index) {

            if (!validacionStep(planAutomaticoStep)) { return false; }

            if (planAutomaticoStep == 1) {
                setTablaClienteSeleccionados(urlSeleccion, tablaClientesSeleccionados);
            }

            if (planAutomaticoStep == 2) {
                $("#fecha-envio-automatico-out").text($("#FORM_fechaEnvio").val());
            }

            ++planAutomaticoStep;

        }, onPrevious: function (tab, navigation, index) {

            --planAutomaticoStep;

        }, onLast: function (tab, navigation, index) {
        }, onTabClick: function (tab, navigation, index) {
        }, onTabShow: function (tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index + 1;
            var $percent = ($current / $total) * 100;
            $('#plan-automaticas-wz .progress-bar').css({ width: $percent + '%' });
            // If it's the last tab then hide the last button and show the finish instead
            if ($current >= $total) {
                $('#plan-automaticas-wz').find('.pager .next').hide();
                $('#plan-automaticas-wz').find('.pager .finish').show();
                $('#plan-automaticas-wz').find('.pager .finish').removeClass('disabled');
            } else {
                $('#plan-automaticas-wz').find('.pager .next').show();
                $('#plan-automaticas-wz').find('.pager .finish').hide();
            }
        }
    });

    tablaClientesSeleccionados = $('#tabla-clientes-seleccionados').DataTable({
        data: [],
        columns: [
            { title: "Unidad de Negocio" },
            { title: "Cliente" },
            { title: "Sucursal" },
            { title: "Planificación" },
            { title: "PDF" }
        ],
        language: {
            lengthMenu: "Mostrar _MENU_ registros por página",
            zeroRecords: "No existen elementos para mostrar.",
            info: "Mostrando página _PAGE_ de _PAGES_",
            infoEmpty: "No hay elementos disponibles",
            paginate: {
                first: "Primero",
                last: "Último",
                next: "Siguiente",
                previous: "Anterior"
            },
            search: "Buscar:",
            infoFiltered: "(Filtrado de un total de _MAX_ registros)"
        }
    });

    $(".nav-tabs").click(function () {
        return false;
    });

    cantidadClientes = 0;
    datosTabla = [];

    langs = ['es', 'en', 'fr', 'pt']; //Define Lenguajes soportados

    $('.dateButton111').parent().parent().find('.datepickershow-automaticas').datepicker({
        format: 'dd-mm-yyyy',
        startDate: '+0d',
        language: 'es'
    });

    console.log("[Planificaciones Automáticas] Variables inicializadas...");
}

$('.dateButton111').click(function () {
    $(this).parent().parent().find('.datepickershow-automaticas').datepicker("show");
});

/**  
**  VALIDACIONES
**/
function validacionStep(step) {
    var result = true;

    switch (step) {
        case 1:
            if ($(".list-plan input:checked").length == 0) {
                alert("Debe escoger al menos un cliente.");
                result = false;
            }
            if ($("#FORM_anio").val() == "") {
                alert("Debe escoger un año de planificación.");
                result = false;
            }
            return result;
            break;
        case 2:
            if ($("#FORM_fechaEnvio").val() == "") { alert("Debe ingresar una fecha."); result = false; }
            return result;
            break;
        default:
            return result;
            break;
    }
}

/**  
**  SELECCION DE PLANIFICACIONES
**/
$(".select-all-clientes").change(function () {
    var idCheck = $(this).attr("id");
    $("#collapse" + idCheck + " .clientes-checkbox input:checkbox").prop('checked', $(this).prop("checked"));
});

function setTablaClienteSeleccionados(urlSeleccion, tablaClientesSeleccionados) {
    var lista = $(".list-plan .clientes-checkbox input:checked").map(function () {
        return $(this).attr('id');
    }).get();

    //console.log("Cantidad Clientes: " + lista.length);
    cantidadClientes = lista.length;
    $("#cantidad-clientes-out").text(cantidadClientes);
    //console.log(lista);

    var contenido;

    var anioValor = $("#FORM_anio option:selected").val();
    var anioNum = parseInt(anioValor);

    //console.log("Request a direccion: " + urlSeleccion);

    $.ajax({
        url: urlSeleccion,
        data: { IdClientesSeleccionados: lista, AnioPlanificacion: anioNum },
        traditional: true,
        success: function (data) {
            datosTabla = [];
            tablaClientesSeleccionados.clear().draw();
            console.log("Cantidad Sucursales: " + data.length);
            $("#cantidad-sucursales-out").text(data.length);

            var cantidadEquipos = 0;

            $.each(data, function (i, item) {
                var htmlColumna = "<button id='" + item.ID + "' class='btn btn-default preview-pdf' type='button' data-toggle='modal' data-target='#ModalPDF' onclick=\"previewPDF('" + item.ID + "')\">" +
                "<span class='glyphicon glyphicon-modal-window'></span>" +
                "</button>";
                contenido = [item.NombreFranquicia, item.NombreEmpresa, item.NombreSucursal, item.Tiempo, htmlColumna];
                datosTabla.push(contenido);

                tablaClientesSeleccionados.row.add(contenido).draw();

                cantidadEquipos += item.CantidadEquipos;
            });

            $("#cantidad-equipos-out").text(cantidadEquipos);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log('request failed: ' + textStatus + ", error: " + errorThrown);
        }
    });
}

/**  
**  FECHA DE ENVIO
**/


/**  
**  RESUMEN
**/
$('#FORM_IdPais').on('change', function () {
    var franquicias = $('#FORM_IdFranquicia');
    franquicias.prop('disabled', true);
    franquicias.empty();

    var pais = $(this).val();
    if (pais.length > 0) {
        $.getJSON(urlGetFranquicias, {
            paisId: pais
        })
    .done(function (data) {
        console.log(data);
        var msg = "No existen Franquicias para el país seleccionado";
        if (data.length > 0) {
            franquicias.prop('disabled', false);
            msg = "Seleccione una franquicia";
        }
        var option = $("<option/>").text(msg);
        franquicias.append(option);

        $.each(data, function (i, franquicia) {
            var option = $("<option/>").attr("value", franquicia.Value).text(franquicia.Text);
            franquicias.append(option);
        });
    })
    .fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ", " + error;
        console.log("Request Failed: " + err);
    });
    }
});

$('#FORM_IdFranquicia').on('change', function () {
    var clientes = $('#FORM_IdCliente');
    clientes.prop('disabled', true);
    clientes.empty();

    var franquicia = $(this).val();
    if (franquicia.length > 0) {
        $.getJSON(urlGetClientes, {
            franquiciaId: franquicia
        })
.done(function (data) {

    var msg = "No existen Clientes para la franquicia seleccionada";
    if (data.length > 0) {
        clientes.prop('disabled', false);
        msg = "Seleccione un cliente";
    }
    var option = $("<option/>").text(msg);
    clientes.append(option);

    $.each(data, function (i, cliente) {
        var option = $("<option/>").attr("value", cliente.Value).text(cliente.Text);
        clientes.append(option);
    });
})
.fail(function (jqxhr, textStatus, error) {
    var err = textStatus + ", " + error;
    console.log("Request Failed: " + err);
});
    }
});

function previewPDF(idSucursal) {
    var anioValor = $("#FORM_anio option:selected").val();
    var anioNum = parseInt(anioValor);

    $.ajax({
        url: urlPreviewPdf,
        data: { IdSucursal: idSucursal, Anio: anioNum },
        dataType: "html",
        success: function (data) {
            $("#PDF-preview").html(data);
        }
    });
}

