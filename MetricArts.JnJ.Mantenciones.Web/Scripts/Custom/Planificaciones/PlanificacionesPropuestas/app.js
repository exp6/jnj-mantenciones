﻿/**  
**  INIT
**/
var urlObtenerPropuestasPeriodo, urlObtenerFechasMantencion, urlGetMantencion, urlReasignarFecha, urlInactivarMantencion, urlHabilitarMantencion;
var fechasMantenciones;
var paginador;
var options;

function init(url1, url2, url3, url4, url5, url6) {
    console.log("[Planificaciones - Propuestas] Inicializando variables...");

    urlObtenerPropuestasPeriodo = url1;
    urlObtenerFechasMantencion = url2;
    urlGetMantencion = url3;
    urlReasignarFecha = url4;
    urlInactivarMantencion = url5;
    urlHabilitarMantencion = url6;

    iniciarTabla();

    fechasMantenciones = [];

    langs = ['es', 'en', 'fr', 'pt']; //Define Lenguajes soportados
    //iniciarMesesPag();

    $('a[role="tab"]').on('shown.bs.tab', function (e) {
        iniciarMesesPag();
    });
    $('#Lista a:first').tab('show');

    console.log("[Planificaciones - Propuestas] Variables inicializadas...");
}

/**
** TABLA INICIAL
**/
function iniciarTabla() {
    $('#listado-propuestas').DataTable({
        language: {
            lengthMenu: "Mostrar _MENU_ registros por página",
            zeroRecords: "No existen Mantenciones.",
            info: "Mostrando página _PAGE_ de _PAGES_",
            infoEmpty: "No hay mantenciones Propuestas",
            paginate: {
                first: "Primero",
                last: "Último",
                next: "Siguiente",
                previous: "Anterior"
            },
            search: "Buscar:",
            infoFiltered: "(Filtrado de un total de _MAX_ registros)"
        }
    });

    $('#listado-vigentes-inactivos').DataTable({
        language: {
            lengthMenu: "Mostrar _MENU_ registros por página",
            zeroRecords: "No existen Mantenciones.",
            info: "Mostrando página _PAGE_ de _PAGES_",
            infoEmpty: "No hay mantenciones Vigentes",
            paginate: {
                first: "Primero",
                last: "Último",
                next: "Siguiente",
                previous: "Anterior"
            },
            search: "Buscar:",
            infoFiltered: "(Filtrado de un total de _MAX_ registros)"
        }
    });
}

/**
** PAGINADOR FECHAS
**/
function iniciarMesesPag() {
    //var anioSeleccionado = $("#anio-seleccionado").val();
    //var startDateStr = moment({ year: anioSeleccionado }).startOf('year');

    //var endDateStr = moment({ year: anioSeleccionado }).endOf('year');

    var fechaActualStr = $("#fecha-seleccionada").val();
    //$("#fecha-seleccionada-post").val(fechaActualStr);

    if ($.inArray(moment.locale(), langs) == -1) {
        moment.locale(langs[0]);
    };
    options = {
        //selectedDate: moment('11-2015', 'MM-YYYY'),
        selectedDate: moment(fechaActualStr, 'MM-YYYY'),
        //startDate: startDateStr,
        //endDate: endDateStr,
        onSelectedDateChanged: function (event, date) {
            var mesSelected = date.format('MM');
            var anioPlanificacion = date.format('YYYY');

            var mesNum = parseInt(mesSelected);
            var anioNum = parseInt(anioPlanificacion);

            //$("#fecha-seleccionada-post").val(fechaSelected);

            var tablaContainer = $("#mantenciones-tabla");
            tablaContainer.empty();

            var tablaVigentes = "<table class='table table-striped'>";
            tablaVigentes += "<thead>";

            $.getJSON(urlObtenerPropuestasPeriodo, {
                mesPeriodo: mesNum, anio: anioNum
            })
            .done(function (data) {
                if (data.length > 0) {
                    tablaVigentes += "<tr>";
                    tablaVigentes += "<th>Equipo</th>";
                    tablaVigentes += "<th>Contrato</th>";
                    tablaVigentes += "<th>Código</th>";
                    tablaVigentes += "<th>Año</th>";
                    tablaVigentes += "<th>Cliente</th>";
                    tablaVigentes += "<th>Sucursal</th>";
                    tablaVigentes += "<th>Ingeniero asignado</th>";
                    tablaVigentes += "<th>Reasignar Periodo Propuesto</th>";
                    tablaVigentes += "<th>Cancelar Mantención</th>";
                    tablaVigentes += "</tr>";
                    tablaVigentes += "</thead>";
                    tablaVigentes += "<tbody>";

                    $.each(data, function (i, mantencion) {
                        tablaVigentes += "<tr id='" + mantencion.ID + "'>";
                        tablaVigentes += "<td>" + mantencion.Equipo + "</td>";
                        tablaVigentes += "<td>" + mantencion.Contrato + "</td>";
                        tablaVigentes += "<td>" + mantencion.Codigo + "</td>";
                        tablaVigentes += "<td>" + mantencion.Anio + "</td>";
                        tablaVigentes += "<td>" + mantencion.Cliente + "</td>";
                        tablaVigentes += "<td>" + mantencion.Sucursal + "</td>";
                        tablaVigentes += "<td>" + mantencion.Ingeniero + "</td>";
                        tablaVigentes += '<td><button class="btn btn-default" type="button" onclick="reasignarFecha(\'' + mantencion.ID + '\');"><span class="glyphicon glyphicon-th"></span></button></td>';
                        tablaVigentes += '<td><button class="btn btn-default" type="button" onclick="darDeBaja(\'' + mantencion.ID + '\');"><span class="glyphicon glyphicon-remove"></span></button></td>';
                        tablaVigentes += "</tr>";
                    });

                    
                }
                else {
                    tablaVigentes += "<tr>";
                    tablaVigentes += "<th></th>";
                    tablaVigentes += "</tr>";
                    tablaVigentes += "</thead>";
                    tablaVigentes += "<tbody>";
                    tablaVigentes += "<tr>";
                    tablaVigentes += "<td>No existen mantenciones para el periodo seleccionado</td>";
                    tablaVigentes += "</tr>";
                }
                tablaVigentes += "</tbody>";
                tablaVigentes += "</table>";
                tablaContainer.append(tablaVigentes);

                actualizarColoresPaginador();
            })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
        }
    }

    paginador = $('#paginador-propuestas').datepaginatormensual(options);
    obtenerFechas();
};

function obtenerFechas() {
    $.getJSON(urlObtenerFechasMantencion)
            .done(function (data) {
                if (data.length > 0) {
                    $.each(data, function (i, mantencion) {
                        fechasMantenciones.push(mantencion.Fecha);
                    });

                    actualizarColoresPaginador();
                }
            })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
}

function actualizarColoresPaginador() {
    var itemPag = $(".dp-item");
    
    itemPag.each(function (index) {
        for (var i = 0; i < fechasMantenciones.length; i++) {
            if ($(this).attr("data-moment") == fechasMantenciones[i]) {
                $(this).addClass("dp-item-con-mantenciones");
                break;
            }
        }
    });
}

/**
** REASIGNACION DE FECHA
**/
$('.dateButtonPropuestas').click(function () {
    $(this).parent().parent().find('.datepickershow-propuestas').datepicker("show");
});

function reasignarFecha(idMantencion) {
    var mantencion;

    $.getJSON(urlGetMantencion, { IdMantencion: idMantencion })
         .done(function (data) {
             mantencion = {
                 fechaConfirmacion: moment(data[0].periodoPropuesto, 'MM-YYYY')
             };

             $('.dateButtonPropuestas').parent().parent().find('.datepickershow-propuestas').datepicker({
                 format: 'mm-yyyy',
                 viewMode: "months",
                 minViewMode: "months"
             });

             $("#ModalFecha-vigentes").find("#fecha-visita").val(mantencion.fechaConfirmacion.format('MM-YYYY'));
             $("#ModalFecha-vigentes").find("#mantencion-id").val(idMantencion);
         })
         .fail(function (jqxhr, textStatus, error) {
             var err = textStatus + ", " + error;
             console.log("Request Failed: " + err);
         });

    $('#ModalFecha-vigentes').modal();
}

$("#reasignar-fecha-submit").click(function () {
    var fechaInput = $("#fecha-visita").val();
    var idMantencion = $("#mantencion-id").val();

    $.ajax({
        url: urlReasignarFecha,
        method: "POST",
        data: { fechaSelected: fechaInput, IdMantencion: idMantencion },
        traditional: true,
        success: function (data) {
            $('#paginador-propuestas').datepaginatormensual('setSelectedDate', [fechaInput, 'MM-YYYY']);
            
            $("#cerrar-modal-fecha").trigger("click");
        }
    });
});

/**
** DAR DE BAJA / HABILITAR MANTENCION
**/
function darDeBaja(idMantencion) {
    var r = confirm("Se inactivará la mantención y el ingeniero no podrá atenderla. ¿Desea continuar?");
    if (r == true) {
        $.ajax({
            url: urlInactivarMantencion,
            type: "POST",
            data: { IdMantencion: idMantencion },
            traditional: true,
            success: function (data) {
                alert("Se ha inactivado la mantención con éxito");
                location.reload();
            },
            error: function (result) {
                alert("Error");
            }
        });

    }
}

function habilitarMantencion(idMantencion) {
    var r = confirm("Se habilitará nuevamente la mantención. ¿Desea continuar?");
    if (r == true) {
        $.ajax({
            url: urlHabilitarMantencion,
            type: "POST",
            data: { IdMantencion: idMantencion },
            traditional: true,
            success: function (data) {
                alert("Se ha habilitado la mantención con éxito");
                location.reload();
            },
            error: function (result) {
                alert("Error");
            }
        });
    }
}
