﻿/**  
**  INIT
**/
var langs, urlObtenerVigentesFecha, urlGetSucursales, urlCalendarioJsonFeed, urlGetIngenieros, urlGetMantencion, urlCambiarNuevoEstado, urlGetChecklist, urlAsignarChecklist;
var mantencionesDetalle;
var mantencionIDGl;
var eventosCalendario;
var htmlFormularioCambioEstado;

function init(url1, url2, url3, url4, url5, url6, url7, url8) {
    console.log("[Mantenciones - Seguimiento] Inicializando variables...");

    urlObtenerVigentesFecha = url1;
    urlGetSucursales = url2;
    urlCalendarioJsonFeed = url3;
    urlGetIngenieros = url4;
    urlGetMantencion = url5;
    urlCambiarNuevoEstado = url6;
    urlGetChecklist = url7;
    urlAsignarChecklist = url8;

    iniciarTabla();

    mantencionesDetalle = [];

    langs = ['es', 'en', 'fr', 'pt']; //Define Lenguajes soportados
    iniciarDiasPag();
    inicializaCalendar();

    $('a[role="tab"]').on('shown.bs.tab', function (e) {
        $('#calendario-seguimiento').fullCalendar('render');
    });
    $('#Lista a:first').tab('show');

    htmlFormularioCambioEstado = $('#ModalDet').find("#cambiar-estado-seguimiento").find("#estados-formulario-cambio").children();

    console.log("[Mantenciones - Seguimiento] Variables inicializadas...");
}

/**
** TABLA INICIAL
**/
function iniciarTabla() {
    $('#listado-seguimiento').DataTable({
        language: {
            lengthMenu: "Mostrar _MENU_ registros por página",
            zeroRecords: "No existen Mantenciones.",
            info: "Mostrando página _PAGE_ de _PAGES_",
            infoEmpty: "No hay mantenciones Activas",
            paginate: {
                first: "Primero",
                last: "Último",
                next: "Siguiente",
                previous: "Anterior"
            },
            search: "Buscar:",
            infoFiltered: "(Filtrado de un total de _MAX_ registros)"
        }
    });
}

$('#todas-boton').click(function () {
    $('#listado-completo').trigger('click');
});

/**
** PAGINADOR FECHAS
**/
function iniciarDiasPag() {
    var anioSeleccionado = $("#anio-seleccionado").val();
    var startDateStr = moment({ year: anioSeleccionado }).startOf('year');
    var endDateStr = moment({ year: anioSeleccionado }).endOf('year');

    var fechaActualStr = $("#fecha-seleccionada").val();
    $("#fecha-seleccionada-post").val(fechaActualStr);

    if ($.inArray(moment.locale(), langs) == -1) {
        moment.locale(langs[0]);
    };
    var options = {
        selectedDate: moment(fechaActualStr, 'DD-MM-YYYY'),
        startDate: startDateStr,
        endDate: endDateStr,
        onSelectedDateChanged: function (event, date) {
            var fechaSelected = date.format('DD-MM-YYYY');
            $("#fecha-seleccionada-post").val(fechaSelected);

            var tablaContainer = $("#mantenciones-tabla");
            tablaContainer.empty();

            var tablaSeguimiento = "<table class='table table-striped'>";
            tablaSeguimiento += "<thead>";

            $.getJSON(urlObtenerVigentesFecha, {
                FechaSeleccionada: fechaSelected, tipoMantencion: 1
            })
            .done(function (data) {
                if (data.length > 0) {
                    tablaSeguimiento += "<tr>";
                    tablaSeguimiento += "<th>Gestionar</th>";
                    tablaSeguimiento += "<th>Equipo</th>";
                    tablaSeguimiento += "<th>Código</th>";
                    tablaSeguimiento += "<th>Ingeniero asignado</th>";
                    tablaSeguimiento += "<th>Año</th>";
                    tablaSeguimiento += "<th>Cliente</th>";
                    tablaSeguimiento += "<th>Checklist</th>";
                    tablaSeguimiento += "<th>Asignar Checklist</th>";
                    tablaSeguimiento += "</tr>";
                    tablaSeguimiento += "</thead>";
                    tablaSeguimiento += "<tbody>";

                    $.each(data, function (i, mantencion) {
                        tablaSeguimiento += "<tr id='" + mantencion.ID + "' class='" + obtenerClaseFila(mantencion.EstadoCodigo) + "'>";
                        tablaSeguimiento += '<td><button class="btn btn-default" type="button" onclick="gestionarMantencion(\'' + mantencion.ID + '\');"><span class="glyphicon glyphicon-new-window"></span></button></td>';
                        tablaSeguimiento += "<td>" + mantencion.Equipo + "</td>";
                        tablaSeguimiento += "<td>" + mantencion.Codigo + "</td>";
                        tablaSeguimiento += "<td>" + mantencion.Ingeniero + "</td>";
                        tablaSeguimiento += "<td>" + mantencion.Anio + "</td>";
                        tablaSeguimiento += "<td>" + mantencion.Cliente + " (" + mantencion.Sucursal + ")</td>";
                        tablaSeguimiento += "<td id='checklist-asignado-" + mantencion.ID + "'>" + mantencion.Checklist + "</td>";
                        tablaSeguimiento += '<td>' + (mantencion.ChecklistRespondido ? 'Checklist respondido por Ingeniero' : '<button class="btn btn-default" type="button" onclick="asignarChecklist(\'' + mantencion.ID + '\');"><span class="glyphicon glyphicon-list-alt"></span></button>') + '</td>';
                        tablaSeguimiento += "</tr>";
                        
                    });
                }
                else {
                    tablaSeguimiento += "<tr>";
                    tablaSeguimiento += "<th></th>";
                    tablaSeguimiento += "</tr>";
                    tablaSeguimiento += "</thead>";
                    tablaSeguimiento += "<tbody>";
                    tablaSeguimiento += "<tr>";
                    tablaSeguimiento += "<td>No existen mantenciones para la fecha seleccionada</td>";
                    tablaSeguimiento += "</tr>";
                }
                tablaSeguimiento += "</tbody>";
                tablaSeguimiento += "</table>";
                tablaContainer.append(tablaSeguimiento);
            })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
        }
    }
    $('#paginador-seguimiento').datepaginator(options);
};

function obtenerClaseFila(numEstado) {
    var numero = parseInt(numEstado);
    switch (numero) {
        case 2:
            return "warning";
        case 3:
            return "info";
        case 4:
            return "danger";
        case 5:
            return "success";
        default:
            return "warning";
    }
}

/**
** CALENDARIO
**/
function inicializaCalendar() {
    eventosCalendario = {
        events: []
    };

    var mesesAbbr = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var meses = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];

    $.getJSON(urlCalendarioJsonFeed, {
        tipoMantencion: 1
    })
        .done(function (data) {
            mantencionesDetalle = [];

            $.each(data, function (i, evento) {

                var subs = evento.start.substring(0, 3);
                var mes;
                var subsRestante = evento.start.substring(3);

                for (var i = 0; i < mesesAbbr.length; i++) {
                    if (subs == mesesAbbr[i]) {
                        mes = meses[i];
                        break;
                    }
                }

                var fechaFormat = mes + subsRestante;

                var nuevoEvento = {
                    id: evento.id,
                    title: evento.title,
                    color: evento.color,
                    start: moment(fechaFormat, 'MMMM Do YYYY h:mm:ss')
                    //start: moment(evento.start, 'MMM Do YYYY h:mm:ssa')
                };

                var nuevaMantencion = {
                    idMantencion: evento.id,
                    equipo: evento.title,
                    tipoContrato: evento.tipoContrato,
                    estadoMantencion: evento.estadoMantencion,
                    sucursal: evento.sucursal,
                    cliente: evento.cliente,
                    ingeniero: evento.ingeniero,
                    codigoEquipo: evento.codigoEquipo
                };

                eventosCalendario.events.push(nuevoEvento);
                mantencionesDetalle.push(nuevaMantencion);
            });

            // Inicializa Calendario con opciones
            $('#calendario-seguimiento').fullCalendar({
                lang: 'es',
                today: false,
                weekends: false,
                editable: false,
                defaultDate: moment().startOf('day'),
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                buttonText: {
                    prev: "<",
                    next: ">",
                    prevYear: "prev year",
                    nextYear: "next year",
                    year: 'Año', // TODO: locale files need to specify this
                    today: 'Hoy',
                    month: 'Mes',
                    week: 'Sem',
                    day: 'Día'
                },
                events: eventosCalendario.events,
                eventClick: function (calEvent, jsEvent, view) {
                    $('#paginador-seguimiento').datepaginator('setSelectedDate', [calEvent.start.format('DD-MM-YYYY'), 'DD-MM-YYYY']);
                    gestionarMantencion(calEvent.id);
                },
            });
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
}

$("#calendario-tab").click(function () {
    var fechaGoto = $("#fecha-seleccionada-post").val();
    $("#calendario-seguimiento").fullCalendar('gotoDate', moment(fechaGoto, 'DD-MM-YYYY'));
});

/**
** MODAL SEGUIMIENTO
**/
function llenadoCamposModal(mantencion) {
    
    if (mantencion == null) { alert("Ocurrió un error de comunicación. Intentelo más tarde."); return false;}

    var modalGestion = $('#ModalDet');

    var seccionDetalle,
        seccionHistoricos,
        seccionChecklist,
        seccionEstado,
        seccionRepuestos,
        seccionLotus;

    seccionDetalle = modalGestion.find("#detalle-seguimiento");
    seccionHistoricos = modalGestion.find("#historicos-seguimiento");
    seccionChecklist = modalGestion.find("#checklist-seguimiento");
    seccionEstado = modalGestion.find("#cambiar-estado-seguimiento");
    seccionRepuestos = modalGestion.find("#repuestos-seguimiento");
    seccionLotus = modalGestion.find("#lotus-seguimiento");

    //Seccion Detalle
    seccionDetalle.find("#detalle-seguimiento-cliente").text(mantencion.cliente);
    seccionDetalle.find("#detalle-seguimiento-sucursal").text(mantencion.sucursal);
    seccionDetalle.find("#detalle-seguimiento-planificacion").text(mantencion.planificacion);
    seccionDetalle.find("#detalle-seguimiento-ingeniero").text(mantencion.ingeniero);

    seccionDetalle.find("#detalle-seguimiento-tabla-codigo-equipo").text(mantencion.codigoEquipo);
    seccionDetalle.find("#detalle-seguimiento-tabla-nombre-equipo").text(mantencion.equipo);
    seccionDetalle.find("#detalle-seguimiento-tabla-franquicia").text(mantencion.franquicia);
    seccionDetalle.find("#detalle-seguimiento-tabla-descripcion").text("-");
    seccionDetalle.find("#detalle-seguimiento-tabla-fecha-instalacion").text(mantencion.fechaInstalacionEquipo);
    seccionDetalle.find("#detalle-seguimiento-tabla-fecha-visita").text(mantencion.fechaConfirmacion.format('DD-MM-YYYY'));

    //Seccion Cambiar Estado Mantención
    
    if (mantencion.checkListRespondido) {
        seccionEstado.find("#estados-formulario-cambio").empty();
        seccionEstado.find("#estados-formulario-cambio").append('<label class="col-sm-4 control-label">La Mantención ha finalizado. No puede realizar cambios de estado.</label>');
    }
    else {
        seccionEstado.find("#estados-formulario-cambio").empty();
        seccionEstado.find("#estados-formulario-cambio").append(htmlFormularioCambioEstado);
        seccionEstado.find("#estados-seguimiento-valores").val(mantencion.estado);
    }

    //Seccion Estados Historicos
    var cuerpoTabla = $("#estados-historicos-seguimiento-registros");
    cuerpoTabla.empty();

    var filaHistorico = "";
    var fechaActualizacion;

    for (var i = 0; i < mantencion.estadosHistoricos.length; i++) {

        fechaActualizacion = moment(mantencion.estadosHistoricos[i].fechaModificacion, 'MMM Do YYYY h:mm:ssa');

        filaHistorico += "<tr>";
        filaHistorico += "<th>" + mantencion.estadosHistoricos[i].estadoDescripcion + "</th>";
        filaHistorico += "<td>" + (mantencion.estadosHistoricos[i].observaciones == '' ? "-" : mantencion.estadosHistoricos[i].observaciones) + "</td>";
        filaHistorico += "<td>" + mantencion.estadosHistoricos[i].atendidoPor + "</td>";
        filaHistorico += "<td>" + fechaActualizacion.format('DD-MM-YYYY HH:mm') + "</td>";
        filaHistorico += "</tr>";
    }
    cuerpoTabla.append(filaHistorico);

    //Seccion CheckList

    //Seccion Solicitud de Respuestos


    //Seccion Lotus


    modalGestion.modal();
}

function gestionarMantencion(mantencionId) {
    var mantencion;
    mantencionIDGl = mantencionId;

    $.getJSON(urlGetMantencion, { IdMantencion: mantencionId })
        .done(function (data) {
            mantencion = {
                idMantencion: data[0].id,
                equipo: data[0].equipo,
                tipoContrato: data[0].tipoContrato,
                estadoMantencion: data[0].estadoMantencion,
                sucursal: data[0].sucursal,
                cliente: data[0].cliente,
                ingeniero: data[0].ingeniero,
                codigoEquipo: data[0].codigoEquipo,
                fechaConfirmacion: moment(data[0].fechaConfirmacion, 'MMM Do YYYY h:mm:ssa'),
                fechaInstalacionEquipo: data[0].fechaInstalacionEquipo,
                planificacion: data[0].planificacion,
                franquicia: data[0].franquicia,
                estado: data[0].estadoMantencionCodigo,
                estadosHistoricos: data[0].estadosHistorico,
                checkListRespondido: data[0].checkListRespondido
            };

            llenadoCamposModal(mantencion);
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
            alert("Ocurrió un error de comunicación. Intentelo más tarde.");
        });
}

/**
** MODAL SEGUIMIENTO - GUARDAR NUEVO ESTADO
**/
$(document).on('click', '#guardar-nuevo-estado', function () {
    var modalGestion = $('#ModalDet');
    var nuevoEstadoTemp = $("#estados-seguimiento-valores").val();
    var numeroEstado = parseInt(nuevoEstadoTemp);

    var fechaSelected = $("#fecha-seleccionada-post").val();
    var anioSeleccionado = $("#anio-seleccionado").val();
    var observaciones = $("#estados-seguimiento-observaciones").val();

    $.ajax({
        url: urlCambiarNuevoEstado,
        method: "POST",
        data: { IdMantencion: mantencionIDGl, nuevoEstado: numeroEstado, anio: anioSeleccionado, fecha: fechaSelected, obs: observaciones},
        traditional: true,
        success: function (data) {
            actualizarColoresLista(nuevoEstadoTemp);
            actualizarColoresCalendario(numeroEstado);
            $("#fecha-seleccionada-post").val(fechaSelected);
            $("#estados-seguimiento-observaciones").val("");
            modalGestion.modal('hide');
        }
    });
});

function actualizarColoresLista(nuevoEstadoTemp) {
    var elementoFila = $("#" + mantencionIDGl.toLowerCase());

    var claseEstado;
    var numEstado;
    var elementoCounterEstado;

    var claseEstadoBoton = elementoFila.attr("class");
    var numEstadoAntiguo;

    console.log(claseEstadoBoton);

    switch (claseEstadoBoton) {
        case "warning":
            elementoCounterEstado = $("#espera-visita-boton").find(".badge");
            break;
        case "info":
            elementoCounterEstado = $("#en-curso-boton").find(".badge");
            break;
        case "danger":
            elementoCounterEstado = $("#en-espera-momento-boton").find(".badge");
            break;
        case "success":
            elementoCounterEstado = $("#finalizada-boton").find(".badge");
            break;
    }
    numEstadoAntiguo = parseInt(elementoCounterEstado.text());
    elementoCounterEstado.text(numEstadoAntiguo - 1);

    switch (nuevoEstadoTemp) {
        case "2":
            claseEstado = "warning";
            elementoCounterEstado = $("#espera-visita-boton").find(".badge");
            break;
        case "3":
            claseEstado = "info";
            elementoCounterEstado = $("#en-curso-boton").find(".badge");
            break;
        case "4":
            claseEstado = "danger";
            elementoCounterEstado = $("#en-espera-momento-boton").find(".badge");
            break;
        case "5":
            claseEstado = "success";
            elementoCounterEstado = $("#finalizada-boton").find(".badge");
            break;
        default:
            claseEstado = "warning";
            elementoCounterEstado = $("#espera-visita-boton").find(".badge");
            break;
    }
    numEstado = parseInt(elementoCounterEstado.text());
    elementoCounterEstado.text(numEstado + 1);

    elementoFila.attr("class", claseEstado);
}

function actualizarColoresCalendario(numEstadoInput) {
    var eventoAntiguo, eventoNuevo;
    var coloresEventos = ["#F89406", "#0088CC", "#BD362F", "#51A351"];

    for (var i = 0; i < eventosCalendario.events.length; i++) {
        if (eventosCalendario.events[i].id.toUpperCase() == mantencionIDGl.toUpperCase()) {
            eventoAntiguo = eventosCalendario.events[i];
            break;
        }
    }
    eventoNuevo = eventoAntiguo;
    eventoNuevo.color = coloresEventos[numEstadoInput - 2];

    $("#calendario-seguimiento").fullCalendar('removeEvents', eventoAntiguo.id);
    $("#calendario-seguimiento").fullCalendar('renderEvent', eventoNuevo, true);
}

/**
** ASIGNACION CHECKLIST
**/
function asignarChecklist(idMantencion) {
    $("#ModalChecklist-vigentes").find("#mantencion-checklist-id").val(idMantencion);

    var checklists = $('#checklist-asignacion');
    checklists.empty();

    $.getJSON(urlGetChecklist)
        .done(function (data) {
            var option;

            $.each(data, function (i, checklist) {
                option = $("<option/>").attr("value", checklist.Value).text(checklist.Text);
                checklists.append(option);
            });

            $('#ModalChecklist-vigentes').modal();
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
}

$("#asignar-checklist-submit").click(function () {
    var checklistSeleccionado = $("#checklist-asignacion option:selected");
    var idMantencion = $("#mantencion-checklist-id").val();

    var col = $("#checklist-asignado-" + idMantencion);

    $.ajax({
        url: urlAsignarChecklist,
        method: "POST",
        data: { IdCuestionario: $(checklistSeleccionado).val(), IdMantencion: idMantencion },
        traditional: true,
        success: function (data) {
            col.empty();
            $(col).text($(checklistSeleccionado).text());

            $("#cerrar-modal-checklist").trigger("click");
        }
    });
});