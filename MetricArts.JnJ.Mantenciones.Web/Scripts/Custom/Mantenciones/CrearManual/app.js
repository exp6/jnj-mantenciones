﻿/**  
**  INIT
**/
var urlGetClientes, urlGetSucursales, urlGetEquiposSucursales, urlGetPlanificaciones, urlGetFormatosCorreo, urlGetEquiposSucursal, urlGetFormatosPDF, urlGetPDFTemplatePreview, urlGetMailTemplatePreview;

function init(url1, url2, url3) {
    console.log("[Mantenciones - Crear Manual] Inicializando variables...");

    urlGetClientes = url1;
    urlGetSucursales = url2;
    urlGetEquiposSucursales = url3;

    var crearPlanificacionStep = 1;

    //Inicializa wizard
    $('#crear-planificacion-wz').bootstrapWizard({
        'tabClass': 'nav nav-tabs', 'debug': false, onShow: function (tab, navigation, index) {
        }, onNext: function (tab, navigation, index) {

            if (crearPlanificacionStep == 2 && $('#FORM_Nombre').val() == '') {
                alert('Debe colocar un nombre de planificación.');
                return false;
            }

            if (crearPlanificacionStep == 2 && ($('#tipo-contrato-select option:selected').val() == '' || $("#tipo-contrato-select option:selected").length == 0)) {
                alert('Debe seleccionar un tipo de contrato.');
                return false;
            }

            if (crearPlanificacionStep == 1 && $("#FORM_IdSucursal option:selected").val() == '0' || $("#FORM_IdSucursal option:selected").length == 0) {
                alert('Debe seleccionar una sucursal.');
                return false;
            }
            else { crearPlanificacionStep = 2; }


        }, onPrevious: function (tab, navigation, index) {

            if (crearPlanificacionStep == 2) {
                crearPlanificacionStep = 1;
            }

        }, onLast: function (tab, navigation, index) {
        }, onTabClick: function (tab, navigation, index) {
        }, onTabShow: function (tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index + 1;
            var $percent = ($current / $total) * 100;
            $('#crear-planificacion-wz .progress-bar').css({ width: $percent + '%' });
            // If it's the last tab then hide the last button and show the finish instead
            if ($current >= $total) {
                $('#crear-planificacion-wz').find('.pager .next').hide();
                $('#crear-planificacion-wz').find('.pager .finish').show();
                $('#crear-planificacion-wz').find('.pager .finish').removeClass('disabled');
            } else {
                $('#crear-planificacion-wz').find('.pager .next').show();
                $('#crear-planificacion-wz').find('.pager .finish').hide();
            }

        }
    });

    $(".nav-tabs").click(function () {
        return false;
    });

    console.log("[Mantenciones - Crear Manual] Variables inicializadas...");
}


/**
** DYNAMIC DROP DOWN LIST
**/
$('#formSeleccionNegocio_IdFranquicia').on('change', function () {
    var clientes = $('#FORM_IdCliente');
    clientes.prop('disabled', true);
    var sucursales = $('#FORM_IdSucursal');
    sucursales.prop('disabled', true);
    sucursales.empty();
    clientes.empty();

    var franquicia = $(this).val();
    if (franquicia.length > 0) {
        $.getJSON(urlGetClientes, {
            franquiciaId: franquicia
        })
        .done(function (data) {

            var msg = "No existen Clientes para la franquicia seleccionada";
            if (data.length > 0) {
                clientes.prop('disabled', false);
                msg = "Seleccione un cliente";
            }
            var option = $("<option/>").text(msg);
            clientes.append(option);

            $.each(data, function (i, cliente) {
                var option = $("<option/>").attr("value", cliente.Value).text(cliente.Text);
                clientes.append(option);
            });


        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
    }
});

$('#FORM_IdCliente').on('change', function () {
    var sucursales = $('#FORM_IdSucursal');
    sucursales.prop('disabled', true);
    sucursales.empty();
    var cliente = $(this).val();

    if (cliente.length > 0) {

        $.getJSON(urlGetSucursales, {
            idCliente: cliente
        })
        .done(function (data) {
            var msg = "No existen Sucursales para el cliente seleccionado";
            if (data.length > 0) {
                sucursales.prop('disabled', false);
                msg = "Seleccione una sucursal";
            }
            var option = $("<option/>").attr("value", "0").text(msg);
            sucursales.append(option);

            $.each(data, function (i, sucursal) {

                var option = $("<option/>").attr("value", sucursal.Value).text(sucursal.Text);
                sucursales.append(option);
            });
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
    }
});




function nextStep() {
    var selectedSucursal = $('#FORM_IdSucursal option:selected');
    var selectedContrato = $('#tipo-contrato-select option:selected');
    if (selectedSucursal.length > 0 && $(selectedContrato).val() != '0') {
        getEquipos($(selectedSucursal).val(), $(selectedContrato).val());
        $("#tipo-contrato").text('(Según Contrato: ' + $(selectedContrato).text() + ')');
    }  
}

function getEquipos(idSucursalSelected, idTipoContrato) {
    $.ajax({
        url: urlGetEquiposSucursales,
        type: "GET",
    contentType: "application/json; charset=utf-8",
    data: { idSucursal: idSucursalSelected, tipoContrato: idTipoContrato },
    dataType: "json",
    success: function (data) {
        var row = "";
        $.each(data, function (index, item) {
            row += "<tr><td><input type='checkbox' data-toggle='toggle'></td>"
            +"<th>"+ item.nombreEquipo +" (Cod.: "+ item.codigoEquipo +")</th>"
            +"<td>Períodos: <ul>"
            +fechasToString(item.fechasPropuesta)
            +"</ul>"
            +"Fecha instalación en Sucursal: <strong>"+ item.fechaInstalacion +"</strong></td></tr>"; 
        });
        $("#equipos-sucursal").html(row);
    },
    error: function (result) {
        alert("Error");
    }
});
}

function fechasToString(fechas) {
    var fechasStr = "";
    for(var i = 0; i < fechas.length; i++) {
        fechasStr += "<li><strong>"+fechas[i]+"</strong></li>";
    }
    return fechasStr;
}