﻿/**  
**  INIT
**/
var urlGetContactosDistribuidor, urlEnviarSolicitud, urlGetMailTemplatePreview;
var idSolicitudKitRepuesto;


function init(url1, url2, url3) {
    console.log("[Mantenciones - Repuestos] Inicializando variables...");
    
    urlGetContactosDistribuidor = url1;
    urlEnviarSolicitud = url2;
    urlGetMailTemplatePreview = url3;

    $(".Add").hide();
    $(".Add1").hide();
    $("#previsualizacion-correo").hide();
    $("#boton-enviar-solicitud").hide();

    console.log("[Mantenciones - Repuestos] Variables inicializadas...");
}

/**  
**  MODAL - ENVIO DE SOLICITUD A DISTRIBUIDOR
**/
$(".btn-modal-detalle-solicitud-repuestos").click(function () {
    var modalRepuesto = $("#ModalDet");
    idSolicitudKitRepuesto = $(this).attr('id');
    modalRepuesto.modal();
});

$('.Add .btn-success').click(function () {
    $('.Add').append('<div class="form-group"><label class="col-sm-2 control-label"></label><div class="col-sm-9"><div class="input-group contacto-destinatario"><select class="form-control input-sm"><option value="1">Para:</option><option value="2">CC:</option><option value="3">BCC:</option></select><span class="input-group-addon">@</span><input type="text" class="form-control" ><span class="input-group-btn"><button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span></button></span></div></div></div>');
});

$('body').on('click', '.Add .btn-danger', function () {
    $(this).closest('.form-group').remove();
});

$('#FORM_IdDistribuidor').on('change', function () {
    $(".Add").hide();
    $(".Add1").hide();
    var idDistribuidor = $(this).val();

    if ($(this).find("option:selected").val() != 0) {
        $.getJSON(urlGetContactosDistribuidor, {
            IdDistribuidor: idDistribuidor
        })
        .done(function (data) {
            var msg = "Contacto inválido. Favor dirigirse al mantenedor correspondiente y agregar un contacto válido.";
            if (data.length > 0) {
                $('.Add #contacto-distribuidor').val(data);
                $(".Add").show();
                $(".Add1").show();
                $("#PreviewCorreo").prop('disabled', true);
                $("#boton-enviar-solicitud").show();

                var btn = $("#PreviewCorreo");

                if ($("#FORM_IdFormatoCorreo option:selected").val() != "") {
                    btn.prop('disabled', false);
                }
                $("#previsualizacion-correo").hide();
            }
            else {
                alert(msg);
            }
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
    }
});

$("#enviar-solicitud").click(function () {
    var idDistribuidor = $("#FORM_IdDistribuidor option:selected").val();
    var idMail = $("#FORM_IdFormatoCorreo option:selected").val();

    if (idMail == "") { alert("Debe seleccionar un formato de correo válido."); return false; }

    var PARA = [];
    var CC = [];
    var BCC = [];

    var conParaJoin, conCCJoin, conBCCJoin;

    PARA.push($("#contacto-distribuidor").val());

    if ($(".contacto-destinatario").length > 0) {
        $(".contacto-destinatario").each(function (index, element) {
            var seleccion = $(element).find("select option:selected").val();
            var contactoStr = $(element).find("input").val();

            if (contactoStr == "") { return true; }

            switch (seleccion) {
                case "1":
                    PARA.push(contactoStr);
                    break;
                case "2":
                    CC.push(contactoStr);
                    break;
                case "3":
                    BCC.push(contactoStr);
                    break;
            }
        });
        conCCJoin = CC.join(",");
        conBCCJoin = BCC.join(",");
    }
    else {
        conCCJoin = '';
        conBCCJoin = '';
    }
    conParaJoin = PARA.join(",");

    $.ajax({
        url: urlEnviarSolicitud,
        method: "POST",
        data: { IdMailTemplate: idMail, IdDistribuidor: idDistribuidor, contactosPara: conParaJoin, contactosCC: conCCJoin, contactosBCC: conBCCJoin, IdSolicitudKitRepuesto: idSolicitudKitRepuesto },
        traditional: true,
        dataType: "html",
        success: function (data) {
            
            if (data === 'true') {
                $("#ModalExitoRepuestoTitulo").text("Se ha enviado la solicitud a la cola de correos. Puede visualizar el estado de ésta en su bandeja de notificaciones.");
            }
            else {
                $("#ModalExitoRepuestoTitulo").text("Ha ocurrido un error de comunicación al enviar la solicitud. Inténtelo más tarde.");
            }

            $('#ModalExitoRepuesto').modal();
            
        }
    });

    $("#cerrar-modal").trigger("click");
});

$('#PreviewCorreo').click(function () {
    var idMail = $("#FORM_IdFormatoCorreo option:selected").val();
    $("#titulo-formato-correo").text("(" + $("#FORM_IdFormatoCorreo option:selected").text() + ")");
    var idDistribuidor = $("#FORM_IdDistribuidor option:selected").val();
    
    $.ajax({
        url: urlGetMailTemplatePreview,
        data: { IdMailTemplate: idMail, IdDistribuidor: idDistribuidor, IdSolicitudKitRepuesto: idSolicitudKitRepuesto },
        traditional: true,
        dataType: "html",
        success: function (data) {
            $("#correo-body-html").html(data);
            $("#previsualizacion-correo").show();
        }
    });
});

$('#FORM_IdFormatoCorreo').on('change', function () {
    $("#previsualizacion-correo").hide();
    var btn = $("#PreviewCorreo");

    if ($(this).val() == "") {
        btn.prop('disabled', true);
    }
    else {
        btn.prop('disabled', false);
    }
});

$("#cerrar-previsualizacion-correo").click(function () {
    $("#previsualizacion-correo").hide();
});

$('#ModalExitoRepuesto').on('hidden.bs.modal', function () {
    window.location.reload();
});

