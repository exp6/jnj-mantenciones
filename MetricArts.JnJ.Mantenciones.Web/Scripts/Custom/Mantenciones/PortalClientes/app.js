﻿/**  
**  INIT
**/
var urlSaveFechas, urlGetFeriados, urlGetDisponibilidadIngenieros;
var idModalSelected; //ID del Equipo
var idPlanificacion, idEquipo, idMensajeStage;
var feriados = [];
var fechasNoDisponiblesIngenieros = [];
var fechasLista = [];  //  seleccionadas del datepicker

function init(url1, url2, url3) {
    console.log("[Mantenciones - Portal Clientes] Inicializando variables...");

    urlSaveFechas = url1;
    urlGetFeriados = url2;
    urlGetDisponibilidadIngenieros = url3;
    idPlanificacion = $("#planificacion-id").val();
    idMensajeStage = $("#msg-stage-id").val();

    initFeriados();
    

    console.log("[Mantenciones - Portal Clientes] Variables inicializadas...");
}

function initFeriados() {
    $.getJSON(urlGetFeriados, {
        IdPlanificacion: idPlanificacion
        })
        .done(function (data) {
            if (data.length > 0) {
                $.each(data, function (i, element) {
                    feriados.push(element.fecha);
                });
            }

            initDisponibilidadIngeniero();
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
}

function initDisponibilidadIngeniero() {
    $.getJSON(urlGetDisponibilidadIngenieros, {
        IdPlanificacion: idPlanificacion
        })
        .done(function (data) {
            if (data.length > 0) {
                $.each(data, function (i, element) {
                    fechasNoDisponiblesIngenieros.push(element);
                });
            }

            $('.dateButtonAgenda').parent().parent().find('.datepickershow-agenda').datepicker({
                format: 'dd-mm-yyyy',
                daysOfWeekDisabled: [0, 6],
                language: 'es',
                beforeShowDay: function (date) {
                    var formattedDate = $.fn.datepicker.DPGlobal.formatDate(date, 'dd-mm-yyyy', 'en');
                    if ($.inArray(formattedDate.toString(), feriados) != -1 || $.inArray(formattedDate.toString(), fechasNoDisponiblesIngenieros) != -1) {
                        return {
                            enabled: false
                        };
                    }
                    return;
                }
            });

        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
}

$('.dateButtonAgenda').click(function () {
    $(this).parent().parent().find('.datepickershow-agenda').datepicker("show");
});

$("button[data-toggle='modal']").click(function () {
    idModalSelected = $(this).attr("data-target");
    idEquipo = idModalSelected.substring(1);
});

$(".guardar-cambios").click(function () {
    var fechasInput = $(this).parent().parent().find("input");

    fechasLista.push($(fechasInput).val());

    ////  comentamos esto para sacar la lista fuera de la función
    //var fechasLista = $(fechasInput).map(function () {
    //    return $(this).val();
    //}).get();

    for (var i = 0; i < fechasLista.length; i++) {
        if (fechasLista[i] == "") {
            alert("Debe completar todos los campos con fechas.");
            return false;
        }
    }

    var r = confirm("Se guardaran los cambios y no podrá revertirlos. ¿Desea continuar?");
    if (r == true) {
        $.ajax({
            url: urlSaveFechas,
            type: "POST",
            data: { IdPlanificacion: idPlanificacion, IdMensajeStage: idMensajeStage, IdEquipo: idEquipo, fechasSelected: fechasLista },
            traditional: true,
            success: function (data) {

                $(".modal").find(".close").trigger("click");
                var elementoTD = $("#mantenciones-agenda").find("button[data-target='" + idModalSelected + "']").parent();
                $(elementoTD).empty();
                $(elementoTD).append("<i class='glyphicon glyphicon-ok'></i>");

                $("#mantenciones-agenda").find("#__" + idEquipo).text("Registro Completo");
                $("#mantenciones-agenda").find("#___" + idEquipo).append("<a href='#' data-toggle='modal' data-target='" + idModalSelected + "'>Revisar</a>");

                //$("#" + idEquipo).empty();
                var modalRegistered = $("#" + idEquipo).find("#____" + idEquipo);
                $(modalRegistered).find(".row").each(function (index, element) {
                    $(element).find("span.input-group-btn").empty();

                    var inputs = $(element).find("div.input-group");
                    var inputsValue = $(inputs).find("input").val();
                    inputs.empty();
                    inputs.append("</span>" + inputsValue + "</span>");


                    console.log($(element).find("div.input-group").find("input").length);
                });
                $(modalRegistered).find("div.modal-footer").empty();

                idModalSelected = "";
                idEquipo = "";
            },
            error: function (result) {
            alert("Error");
        }
        });

        $('.dateButtonAgenda').parent().parent().find('.datepickershow-agenda').datepicker('destroy');

        var disabledDaysOriginal = $.merge(feriados, fechasNoDisponiblesIngenieros)
        var disabledDays = $.merge(disabledDaysOriginal, fechasLista)

        $('.dateButtonAgenda').parent().parent().find('.datepickershow-agenda').datepicker({
            format: 'dd-mm-yyyy',
            daysOfWeekDisabled: [0, 6],
            language: 'es',
            beforeShowDay: function (date) {
                var formattedDate = $.fn.datepicker.DPGlobal.formatDate(date, 'dd-mm-yyyy', 'en');
                if ($.inArray(formattedDate.toString(), disabledDays) != -1) {
                    return {
                        enabled: false
                    };
                }
                return;
            }
        });
    }    
});