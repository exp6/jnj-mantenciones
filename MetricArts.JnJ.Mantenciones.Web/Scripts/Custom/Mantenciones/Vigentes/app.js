﻿/**  
**  INIT
**/
var langs, urlObtenerVigentesFecha, urlGetSucursales, urlCalendarioJsonFeed, urlGetIngenieros, urlReasignarIngeniero, urlGetMantencion, urlReasignarFecha, urlGetChecklist, urlAsignarChecklist, urlInactivarMantencion, urlHabilitarMantencion;
var mantencionesDetalle;
var eventosCalendario;

function init(url1, url2, url3, url4, url5, url6, url7, url8, url9, url10, url11) {
    console.log("[Mantenciones - Vigentes] Inicializando variables...");

    urlObtenerVigentesFecha = url1;
    urlGetSucursales = url2;
    urlCalendarioJsonFeed = url3;
    urlGetIngenieros = url4;
    urlReasignarIngeniero = url5;
    urlGetMantencion = url6;
    urlReasignarFecha = url7;
    urlGetChecklist = url8;
    urlAsignarChecklist = url9;
    urlInactivarMantencion = url10;
    urlHabilitarMantencion = url11;

    iniciarTabla();

    mantencionesDetalle = [];

    langs = ['es', 'en', 'fr', 'pt']; //Define Lenguajes soportados
    iniciarDiasPag();
    inicializaCalendar();

    $('a[role="tab"]').on('shown.bs.tab', function (e) {
        $('#calendario-vigentes').fullCalendar('render');
    });
    $('#Lista a:first').tab('show');

    console.log("[Mantenciones - Vigentes] Variables inicializadas...");
}

/**
** TABLA INICIAL
**/
function iniciarTabla() {
    $('#listado-vigentes').DataTable({
        language: {
            lengthMenu: "Mostrar _MENU_ registros por página",
            zeroRecords: "No existen Mantenciones.",
            info: "Mostrando página _PAGE_ de _PAGES_",
            infoEmpty: "No hay mantenciones Vigentes",
            paginate: {
                first: "Primero",
                last: "Último",
                next: "Siguiente",
                previous: "Anterior"
            },
            search: "Buscar:",
            infoFiltered: "(Filtrado de un total de _MAX_ registros)"
        }
    });

    $('#listado-vigentes-inactivos').DataTable({
        language: {
            lengthMenu: "Mostrar _MENU_ registros por página",
            zeroRecords: "No existen Mantenciones.",
            info: "Mostrando página _PAGE_ de _PAGES_",
            infoEmpty: "No hay mantenciones Vigentes",
            paginate: {
                first: "Primero",
                last: "Último",
                next: "Siguiente",
                previous: "Anterior"
            },
            search: "Buscar:",
            infoFiltered: "(Filtrado de un total de _MAX_ registros)"
        }
    });
}

$('#todas-boton').click(function () {
    $('#listado-completo').trigger('click');
});

/**
** PAGINADOR FECHAS
**/
function iniciarDiasPag() {
    var anioSeleccionado = $("#anio-seleccionado").val();
    var startDateStr = moment({ year: anioSeleccionado }).startOf('year');
    var endDateStr = moment({ year: anioSeleccionado }).endOf('year');

    var fechaActualStr = $("#fecha-seleccionada").val();
    $("#fecha-seleccionada-post").val(fechaActualStr);

    if ($.inArray(moment.locale(), langs) == -1) {
        moment.locale(langs[0]);
    };
    var options = {
        selectedDate: moment(fechaActualStr, 'DD-MM-YYYY'),
        startDate: startDateStr,
        endDate: endDateStr,
        onSelectedDateChanged: function (event, date) {
            var fechaSelected = date.format('DD-MM-YYYY');
            $("#fecha-seleccionada-post").val(fechaSelected);

            var tablaContainer = $("#mantenciones-tabla");
            tablaContainer.empty();

            var tablaVigentes = "<table class='table table-striped'>";
            tablaVigentes += "<thead>";
            
            $.getJSON(urlObtenerVigentesFecha, {
                FechaSeleccionada: fechaSelected, tipoMantencion: 0
            })
            .done(function (data) {
                if (data.length > 0) {
                    tablaVigentes += "<tr>";
                    tablaVigentes += "<th>Equipo</th>";
                    tablaVigentes += "<th>Código</th>";
                    tablaVigentes += "<th>Año</th>";
                    tablaVigentes += "<th>Cliente</th>";
                    tablaVigentes += "<th>Sucursal</th>";
                    tablaVigentes += "<th>Ingeniero asignado</th>";
                    tablaVigentes += "<th>Checklist</th>";
                    tablaVigentes += "<th>Reasignar ingeniero</th>";
                    tablaVigentes += "<th>Reasignar fecha de visita</th>";
                    tablaVigentes += "<th>Asignar Checklist</th>";
                    tablaVigentes += "<th>Cancelar Mantención</th>";
                    tablaVigentes += "</tr>";
                    tablaVigentes += "</thead>";
                    tablaVigentes += "<tbody>";

                    $.each(data, function (i, mantencion) {
                        tablaVigentes += "<tr id='" + mantencion.ID + "'>";
                        tablaVigentes += "<td>" + mantencion.Equipo + "</td>";
                        tablaVigentes += "<td>" + mantencion.Codigo + "</td>";
                        tablaVigentes += "<td>" + mantencion.Anio + "</td>";
                        tablaVigentes += "<td>" + mantencion.Cliente + "</td>";
                        tablaVigentes += "<td>" + mantencion.Sucursal + "</td>";
                        tablaVigentes += "<td id='ingeniero-asignado-" + mantencion.IdIngeniero + "'>" + mantencion.Ingeniero + "</td>";
                        tablaVigentes += "<td id='checklist-asignado-" + mantencion.ID + "'>" + mantencion.Checklist + "</td>";
                        tablaVigentes += '<td><button id="'+ mantencion.ID +'" class="btn btn-default btn-modal-trigger" type="button" onclick="reasignarIng(\'' + mantencion.IdIngeniero + '\');"><span class="glyphicon glyphicon-link"></span></button></td>';
                        tablaVigentes += '<td><button class="btn btn-default" type="button" onclick="reasignarFecha(\'' + mantencion.ID + '\');"><span class="glyphicon glyphicon-th"></span></button></td>';
                        tablaVigentes += '<td><button class="btn btn-default" type="button" onclick="asignarChecklist(\'' + mantencion.ID + '\');"><span class="glyphicon glyphicon-list-alt"></span></button></td>';
                        tablaVigentes += '<td><button class="btn btn-default" type="button" onclick="darDeBaja(\'' + mantencion.ID + '\');"><span class="glyphicon glyphicon-remove"></span></button></td>';
                        tablaVigentes += "</tr>";
                    });
                }
                else {
                    tablaVigentes += "<tr>";
                    tablaVigentes += "<th></th>";
                    tablaVigentes += "</tr>";
                    tablaVigentes += "</thead>";
                    tablaVigentes += "<tbody>";
                    tablaVigentes += "<tr>";
                    tablaVigentes += "<td>No existen mantenciones para la fecha seleccionada</td>";
                    tablaVigentes += "</tr>";
                }
                tablaVigentes += "</tbody>";
                tablaVigentes += "</table>";
                tablaContainer.append(tablaVigentes);
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
        }
    }

    $('#paginador-vigentes').datepaginator(options);
};

/**
** DYNAMIC DROP DOWN LIST
**/
$('#FORM_IdCliente').on('change', function () {
    var sucursales = $('#FORM_IdSucursal');
    sucursales.prop('disabled', true);
    sucursales.empty();
    var cliente = $(this).val();
    nombreCliente = $(this).find("option:selected").text();

    if (cliente.length > 0) {

        $.getJSON(urlGetSucursales, {
            idCliente: cliente
        })
        .done(function (data) {
            var msg = "No existen Sucursales para el cliente seleccionado";
            if (data.length > 0) {
                sucursales.prop('disabled', false);
                msg = "Todas las Sucursales";
            }
            var option = $("<option/>").attr("value", "0").text(msg);
            sucursales.append(option);

            $.each(data, function (i, sucursal) {

                var option = $("<option/>").attr("value", sucursal.Value).text(sucursal.Text);
                sucursales.append(option);
            });
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
    }
});

/**
** CALENDARIO
**/
function inicializaCalendar() {
    eventosCalendario = {
        events: []
    };

    var mesesAbbr = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var meses = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];

    $.getJSON(urlCalendarioJsonFeed, {
        tipoMantencion: 0
    })
        .done(function (data) {
            mantencionesDetalle = [];

            $.each(data, function (i, evento) {

                
                var subs = evento.start.substring(0, 3);
                var mes;
                var subsRestante = evento.start.substring(3);

                for(var i = 0; i < mesesAbbr.length; i++)
                {
                    if (subs == mesesAbbr[i]) {
                        mes = meses[i];
                        break;
                    }
                }
                
                var fechaFormat = mes + subsRestante;
                

                var nuevoEvento = {
                    id: evento.id,
                    title: evento.title,
                    color: evento.color,
                    start: moment(fechaFormat, 'MMMM Do YYYY h:mm:ss')
                    //start: moment(evento.start, 'MMM Do YYYY h:mm:ss')
                };

                var nuevaMantencion = {
                    idMantencion: evento.id,
                    equipo: evento.title,
                    tipoContrato: evento.tipoContrato,
                    estadoMantencion: evento.estadoMantencion,
                    sucursal: evento.sucursal,
                    cliente: evento.cliente,
                    ingeniero: evento.ingeniero,
                    codigoEquipo: evento.codigoEquipo
                };

                eventosCalendario.events.push(nuevoEvento);
                mantencionesDetalle.push(nuevaMantencion);
            });

            console.log(eventosCalendario.events);

            // Inicializa Calendario con opciones
            $('#calendario-vigentes').fullCalendar({
                lang: 'es',
                today: false,
                weekends: false,
                editable: true,
                defaultDate: moment().startOf('day'),
                eventDrop: function(event, delta, revertFunc) {

                    if (!confirm(event.title + " se cambiará a la fecha " + event.start.format('DD-MM-YYYY') + ". ¿Desea efectuar el cambio?")) {
                        revertFunc();
                    }
                    else {
                        var fechaInput = event.start.format('DD-MM-YYYY');
                        var idMantencion = event.id;

                        $.ajax({
                            url: urlReasignarFecha,
                            method: "POST",
                            data: { fechaSelected: fechaInput, IdMantencion: idMantencion },
                            traditional: true,
                            success: function (data) {
                                $('#paginador-vigentes').datepaginator('setSelectedDate', [fechaInput, 'DD-MM-YYYY']);
                            }
                        });
                    }

                },
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                buttonText: {
                    prev: "<",
                    next: ">",
                    prevYear: "prev year",
                    nextYear: "next year",
                    year: 'Año', // TODO: locale files need to specify this
                    today: 'Hoy',
                    month: 'Mes',
                    week: 'Sem',
                    day: 'Día'
                },
                events: eventosCalendario.events,
                eventClick: function (calEvent, jsEvent, view) {
                    $('#paginador-vigentes').datepaginator('setSelectedDate', [calEvent.start.format('DD-MM-YYYY'), 'DD-MM-YYYY']);
                    detalleMantencion(calEvent, jsEvent, view);
                }
            });
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
}


function detalleMantencion(calEvent, jsEvent, view) {
    var mantencionId = calEvent.id;
    var mantencion;

    $.getJSON(urlGetMantencion, {IdMantencion: mantencionId})
        .done(function (data) {
            mantencion = {
                idMantencion: data[0].id,
                equipo: data[0].equipo,
                tipoContrato: data[0].tipoContrato,
                estadoMantencion: data[0].estadoMantencion,
                sucursal: data[0].sucursal,
                cliente: data[0].cliente,
                ingeniero: data[0].ingeniero,
                codigoEquipo: data[0].codigoEquipo
            };

            var modalDetalle = $("#ModalDet");
            $(modalDetalle).find("#fecha-mantencion")
                .text("(Fecha visita: " + calEvent.start.format("DD-MM-YYYY") + ")");
            $(modalDetalle).find("#equipo-detalle").text(mantencion.equipo);
            $(modalDetalle).find("#tipo-contrato-detalle").text(mantencion.tipoContrato);
            $(modalDetalle).find("#ingeniero-detalle").text(mantencion.ingeniero);
            $(modalDetalle).find("#cliente-detalle").text(mantencion.cliente);
            $(modalDetalle).find("#sucursal-detalle").text(mantencion.sucursal);
            $(modalDetalle).find("#estado-mantencion-detalle").text(mantencion.estadoMantencion);
            $(modalDetalle).find("#codigo-equipo-detalle").text(mantencion.codigoEquipo);

            $(modalDetalle).modal();
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
}

$("#calendario-tab").click(function () {
    var fechaGoto = $("#fecha-seleccionada-post").val();
    $("#calendario-vigentes").fullCalendar('gotoDate', moment(fechaGoto, 'DD-MM-YYYY'));
});

/**
** REASIGNACION DE INGENIERO
**/
var idIngenieroReasignado;

function reasignarIng(idIngeniero) {
    idIngenieroReasignado = idIngeniero;

    var ingenieros = $('#ingenieros-reasignacion');
    ingenieros.empty();

    $.getJSON(urlGetIngenieros)
        .done(function (data) {
            var option;

            $.each(data, function (i, ingeniero) {
                option = $("<option/>").attr("value", ingeniero.Value);

                if (ingeniero.Value == idIngeniero) {
                    option = $(option).attr("selected", "selected");
                }

                option = $(option).text(ingeniero.Text);
                ingenieros.append(option);
            });
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });

    $('#ModalIng-vigentes').modal();
}

$("#reasignar-ingeniero-submit").click(function () {
    var ingenieroSeleccionado = $("#ingenieros-reasignacion option:selected");
    var col = $("#ingeniero-asignado-" + idIngenieroReasignado);
    var botonTrigger = $(col).parent().find("td button.btn-modal-trigger");
    var idMantencion = $(botonTrigger).attr("id");

    $.ajax({
        url: urlReasignarIngeniero,
        method: "POST",
        data: { IdIngeniero: $(ingenieroSeleccionado).val(), IdMantencion: idMantencion },
        traditional: true,
        success: function (data) {
            $(botonTrigger).prop('onclick', null).off('click');
            $(botonTrigger).attr('onclick', 'reasignarIng(\'' + $(ingenieroSeleccionado).val() + '\');');
            col.empty();
            $(col).text($(ingenieroSeleccionado).text());
            $(col).attr("id", "ingeniero-asignado-" + $(ingenieroSeleccionado).val());
            idIngenieroReasignado = $(ingenieroSeleccionado).val();
            $("#cerrar-modal").trigger("click");
        }
    });
});

/**
** REASIGNACION DE FECHA
**/
function reasignarFecha(idMantencion) {
    var mantencion;

    $.getJSON(urlGetMantencion, { IdMantencion: idMantencion })
         .done(function (data) {
             mantencion = {
                 fechaConfirmacion: moment(data[0].fechaConfirmacion, 'MMM Do YYYY h:mm:ssa')
             };
             $("#ModalFecha-vigentes").find("#fecha-visita").val(mantencion.fechaConfirmacion.format('DD-MM-YYYY'));
             $("#ModalFecha-vigentes").find("#mantencion-id").val(idMantencion);
         })
         .fail(function (jqxhr, textStatus, error) {
             var err = textStatus + ", " + error;
             console.log("Request Failed: " + err);
         });

    $('#ModalFecha-vigentes').modal();
}

$("#reasignar-fecha-submit").click(function () {
    var fechaInput = $("#fecha-visita").val();
    var idMantencion = $("#mantencion-id").val();

    $.ajax({
        url: urlReasignarFecha,
        method: "POST",
        data: { fechaSelected: fechaInput, IdMantencion: idMantencion },
        traditional: true,
        success: function (data) {
            $('#paginador-vigentes').datepaginator('setSelectedDate', [fechaInput, 'DD-MM-YYYY']);
            var eventoAntiguo, eventoNuevo;
            
            for (var i = 0; i < eventosCalendario.events.length; i++) {
                if (eventosCalendario.events[i].id.toUpperCase() == idMantencion.toUpperCase()) {
                    eventoAntiguo = eventosCalendario.events[i];
                    break;
                }
            }
            eventoNuevo = eventoAntiguo;
            eventoNuevo.start = moment(fechaInput, 'DD-MM-YYYY');

            $("#calendario-vigentes").fullCalendar('removeEvents', eventoAntiguo.id);
            $("#calendario-vigentes").fullCalendar('renderEvent', eventoNuevo, true);
            $("#cerrar-modal-fecha").trigger("click");
        }
    });
});

/**
** ASIGNACION CHECKLIST
**/
function asignarChecklist(idMantencion) {
    $("#ModalChecklist-vigentes").find("#mantencion-checklist-id").val(idMantencion);

    var checklists = $('#checklist-asignacion');
    checklists.empty();

    $.getJSON(urlGetChecklist)
        .done(function (data) {
            var option;

            $.each(data, function (i, checklist) {
                option = $("<option/>").attr("value", checklist.Value).text(checklist.Text);
                checklists.append(option);
            });

            $('#ModalChecklist-vigentes').modal();
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
}

$("#asignar-checklist-submit").click(function () {
    var checklistSeleccionado = $("#checklist-asignacion option:selected");
    var idMantencion = $("#mantencion-checklist-id").val();

    var col = $("#checklist-asignado-" + idMantencion);

    $.ajax({
        url: urlAsignarChecklist,
        method: "POST",
        data: { IdCuestionario: $(checklistSeleccionado).val(), IdMantencion: idMantencion },
        traditional: true,
        success: function (data) {
            col.empty();
            $(col).text($(checklistSeleccionado).text());
            
            $("#cerrar-modal-checklist").trigger("click");
        }
    });
});

/**
** DAR DE BAJA / HABILITAR MANTENCION
**/
function darDeBaja(idMantencion) {
    var r = confirm("Se inactivará la mantención y el ingeniero no podrá atenderla. ¿Desea continuar?");
    if (r == true) {
        $.ajax({
            url: urlInactivarMantencion,
            type: "POST",
            data: { IdMantencion: idMantencion },
            traditional: true,
            success: function (data) {
                alert("Se ha inactivado la mantención con éxito");
                location.reload();
            },
            error: function (result) {
                alert("Error");
            }
        });

    }
}

function habilitarMantencion(idMantencion) {
    var r = confirm("Se habilitará nuevamente la mantención. ¿Desea continuar?");
    if (r == true) {
        $.ajax({
            url: urlHabilitarMantencion,
            type: "POST",
            data: { IdMantencion: idMantencion },
            traditional: true,
            success: function (data) {
                alert("Se ha habilitado la mantención con éxito");
                location.reload();
            },
            error: function (result) {
                alert("Error");
            }
        });
    }  
}