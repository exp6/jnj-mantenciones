﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using MetricArts.JnJ.Mantenciones.Configuration;

namespace MetricArts.JnJ.Mantenciones
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


           // routes.MapRoute(
           //    "Default",
           //    "{controller}.aspx/{action}/{id}",
           //    new { controller = "Home", action = "Index", id = UrlParameter.Optional }
           //);

           // routes.MapRoute(
           // "Account",
           // "",
           // new { controller = "Account", action = "LogOn" }
           //  );

        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterRoutes(RouteTable.Routes);

            IList<IConfigurable> configurations = new List<IConfigurable>();
            configurations.Add(new MapperConfiguration());
            ModelBinders.Binders.Add(typeof(DateTime?), new CurrentCultureDateTimeBinder());
            ModelBinders.Binders.Add(typeof(DateTime), new CurrentCultureDateTimeBinder());

            foreach (var item in configurations)
            {
                item.Configure();

            }

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
        }
    }
}