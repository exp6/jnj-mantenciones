﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Configuration;
using System.Threading.Tasks;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.MailSend
{
    class Engine
    {
        private readonly SmtpClient _smtpClient = new SmtpClient();
        private const string _from = "notificaciones@JnJ.cl";

        public Engine(SmtpClient smtpClient)
        {
            if (smtpClient == null) throw new ArgumentNullException("smtpClient");
            _smtpClient = smtpClient;
        }

        public virtual void EnviarCorreo(Correo correo)
        {
            string Address = ConfigurationSettings.AppSettings["Address"].ToString();
            string Host = ConfigurationSettings.AppSettings["Host"].ToString();
            int Port = int.Parse(ConfigurationSettings.AppSettings["Port"].ToString());
            string UserCredential = ConfigurationSettings.AppSettings["UserCredential"].ToString();
            string PasswordCredential = ConfigurationSettings.AppSettings["PasswordCredential"].ToString();

            MailMessage message = new MailMessage();
            message.From = new MailAddress(Address);
            message.To.Add(correo.Destinatario);
            message.Subject = correo.Asunto;
            if (!String.IsNullOrEmpty(correo.CC))
                message.CC.Add(correo.CC);
            message.IsBodyHtml = true;
            message.Body = correo.CuerpoHTML;

            SmtpClient smtpClient = new SmtpClient();
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Host = Host;
            smtpClient.Port = Port;
            smtpClient.EnableSsl = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(UserCredential, PasswordCredential);
            smtpClient.Send(message);
        }
    }
}
