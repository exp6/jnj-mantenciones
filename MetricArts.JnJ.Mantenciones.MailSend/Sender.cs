﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config;
using MetricArts.JnJ.Mantenciones.Core;

namespace MetricArts.JnJ.Mantenciones.MailSend
{
    class Sender
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(Sender));
        private static MantencionesDataContext _db = new MantencionesDataContext().WithConnectionStringFromConfiguration();

        private static readonly Random random = new Random();
        private static readonly object syncLock = new object();

        private static int Min { get; set; }
        private static int Max { get; set; }
        public static int RandomNumber()
        {
            lock (syncLock)
            { // synchronize
                return random.Next(Min, Max);
            }
        }

        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();

            MensajeStage msg = null;
            EstadoMensaje estadoMsg;

            try 
            {
                _log.Info("Configurando servidor de correos...");
                SmtpClient _smtpClient = new SmtpClient();
                _smtpClient.EnableSsl = true;
                Engine emisorCorreos = new Engine(_smtpClient);
                _log.Info("Rescatando correo para enviar...");

                foreach (var correo in _db.Correos.Where(x => x.FechaPublicacion == null).AsEnumerable().Where(x => !String.IsNullOrEmpty(x.Destinatario)))
                {
                    
                    int tiempoDormido = RandomNumber();
                    System.Threading.Thread.Sleep(tiempoDormido);

                    _log.InfoFormat("Enviando correo = {0}",
                        correo.Destinatario);

                    Guid idMsgStage = correo.IdMensajeStage;
                    msg = _db.MensajeStages.Where(x => x.IdMensajeStage == idMsgStage).SingleOrDefault();
                    estadoMsg = _db.EstadoMensajes.Where(x => x.Codigo == 3).SingleOrDefault();
                    msg.EstadoMensaje = estadoMsg;

                    // Envío del correo
                    emisorCorreos.EnviarCorreo(correo);
                    correo.FechaPublicacion = DateTime.Now;

                    _db.SubmitChanges();
                }
            }
            catch (Exception ex) 
            {
                estadoMsg = _db.EstadoMensajes.Where(x => x.Codigo == 5).SingleOrDefault();
                msg.EstadoMensaje = estadoMsg;

                _db.SubmitChanges();
                _log.Error(ex);
            }
        }
    }
}
